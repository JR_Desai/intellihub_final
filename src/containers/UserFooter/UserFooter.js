import React from "react";

const getCurrentYear = () => {
    return new Date().getFullYear();
};

const UserFooter = () => {
    return (
        <>
            <section className="footer-section">
                <div className="container">
                <div class="site-info">
                    <span class="site-title">Copyright © {getCurrentYear()} <strong>Intellihub</strong>. All Rights Reserved.</span></div>
                </div>
            </section>
        </>
    );
};

export default UserFooter