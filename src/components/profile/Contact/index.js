import React from "react";
import Widget from "components/Widget";
import { useSelector } from "react-redux";

const Contact = () => {


  const { company } = useSelector(({companies}) => companies);

  return (
    <Widget title="Contact" styleName="gx-card-profile-sm">
        {repeatingFunction("email", "Email", company.email)}
        {repeatingFunction("link", "Website", company.website)}
        {repeatingFunction("phone", "Phone", company.phone)}
    </Widget>
  )
}

export default Contact;


function repeatingFunction(icon, title, value) {
  return <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
    <div className="gx-mr-3">
      <i className={`icon icon-${icon} gx-fs-xxl gx-text-grey`} />
    </div>
    <div className="gx-media-body">
      <span className="gx-mb-0 gx-text-grey gx-fs-sm">{title}</span>
        <p className="gx-mb-0">{value}</p>
    </div>
  </div>;
}

