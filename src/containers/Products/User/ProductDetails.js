import { Spin } from "antd";
import moment from "moment";
import React, { useEffect } from "react";
import { FiBookmark, FiShare2 } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { getProducts } from "../../../appRedux/actions";

const ProductDetails = (props) => {
  const productId = props.match.params.productId;

  const dispatch = useDispatch();
  const history = useHistory();
  const { product } = useSelector(({ products }) => products);
  const { loading } = useSelector(({ common }) => common);

  useEffect(() => {
    getProducts(productId, dispatch);
  }, [props.match.params.id]);

  const images = [product?.primary_image];
  return (
    <Spin spinning={loading}>
      <div className="productDetailsWrapper">
        <div className="container">
          <div className="productDetailsContent">
            <div className="productDetailsZoomSlider">
              <div
                id="carouselExampleIndicators"
                className="carousel slide productDetailsSlider"
                data-ride="carousel"
              >
                <ol className="carousel-indicators">
                  {images.map((image, index) => {
                    return (
                      <li
                        data-target="#carouselExampleIndicators"
                        data-slide-to={String(index)}
                        className={index === 0 ? "active" : ""}
                      >
                        <img
                          style={{ height: 75 }}
                          className="d-block w-100"
                          src={image}
                          alt=""
                        />
                      </li>
                    );
                  })}
                </ol>
                <div className="carousel-inner">
                  {images.map((image, index) => {
                    return (
                      <div
                        className={`carousel-item ${
                          index === 0 ? "active" : ""
                        }`}
                      >
                        <img
                          className="d-block w-100"
                          src={image}
                          alt="First slide"
                        />
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
            <div className="productDetailsInformation">
              <div className="productDetailsHead">
                <div className="productDetailsTitleBlock">
                  <div className="productRecordIdDate">
                    <ul>
                      <li>
                        Record ID -{" "}
                        <span className="recordID">{product?.id}</span>
                      </li>
                      <li>
                        Date -{" "}
                        <span>
                          {moment(product?.date_published).format("DD/MM/YYYY")}
                        </span>
                      </li>
                    </ul>
                  </div>
                  <h2 className="productTitle">{product?.name}</h2>
                  <h4 className="productSubtitle">{product?.variant}</h4>
                </div>
                <div className="productHeadActionBlock">
                  <div className="productDownloadDetails">
                    <a
                      className="download-dropdown-toggle dropdown-toggle"
                      href="#"
                      id="donwloadActions"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      Download All Details
                    </a>
                    <div
                      className="dropdown-menu downloadDropdown"
                      aria-labelledby="donwloadActions"
                    >
                      <ul>
                        <li>
                          <a href="#">All Format</a>
                        </li>
                        <li>
                          <a href="#">Excel</a>
                        </li>
                        <li>
                          <a href="#">PDF</a>
                        </li>
                        <li>
                          <a href="#">Powerpoint</a>
                        </li>
                        <li>
                          <a href="#">Images</a>
                        </li>
                        <li>
                          <a href="#">HTML</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="productShortDescription">
                <p>
                  {Array.isArray(product?.ingredients)
                    ? product?.ingredients
                        .map((ingredient) => ingredient?.name)
                        .join(", ")
                    : ""}
                </p>
              </div>
              <div className="productDetailsPriceInfo">
                <div className="productPrice">
                  Price{" "}
                  <span>
                    {product?.currency?.code} {product?.price_local_currency}
                  </span>{" "}
                  <span>/</span> <span>${product?.price_us_dollars}</span>{" "}
                  <span>/</span> <span>€{product?.price_euros}</span>
                </div>
                <div className="productAction-list">
                  <div className="productShareItem">
                    <ul>
                      <li>
                        <a href="#" className="rawMaterialIcon iconBookmark">
                          <FiBookmark />
                        </a>
                      </li>
                      <li>
                        <a href="#" className="rawMaterialIcon iconShare">
                          <FiShare2 />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="productDetailsTab">
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                  <li className="nav-item" role="presentation">
                    <button
                      className="nav-link active"
                      id="productDetail-tab"
                      data-toggle="tab"
                      data-target="#productDetail"
                      type="button"
                      role="tab"
                      aria-controls="productDetail"
                      aria-selected="true"
                    >
                      Product Details
                    </button>
                  </li>
                  <li className="nav-item" role="presentation">
                    <button
                      className="nav-link"
                      id="company-tab"
                      data-toggle="tab"
                      data-target="#company"
                      type="button"
                      role="tab"
                      aria-controls="company"
                      aria-selected="false"
                    >
                      Company
                    </button>
                  </li>
                </ul>
                <div className="tab-content" id="myTabContent">
                  <div
                    className="tab-pane fade show active"
                    id="productDetail"
                    role="tabpanel"
                    aria-labelledby="productDetail-tab"
                  >
                    <div className="tableListView">
                      <ul>
                        <li>
                          <span className="tableListTitle">
                            Company Name <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.brand?.company?.name}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Brand <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {" "}
                            {product?.brand?.name}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Parent Company / Institution{" "}
                            <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {" "}
                            {product?.brand?.company?.name}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Market <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {" "}
                            {product?.brand?.company?.market}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Category <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.category?.name}{" "}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Unit Pack Size (ml/g){" "}
                            <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.unit_pack_size}
                            {product?.packaging_units?.name}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Packaging Units <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.packaging_units?.name}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Package Type <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.package_type?.name}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Package Material <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.package_material?.name}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Nutrition <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            <div
                              dangerouslySetInnerHTML={{
                                __html: product?.nutrition || "NA",
                              }}
                            ></div>
                          </span>
                        </li>
                      </ul>

                      <ul>
                        <li>
                          <span className="tableListTitle">
                            Storage <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.storage || "NA"}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Company county/State{" "}
                            <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.brand?.company?.country?.name || "NA"}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Private Label <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.private_label?.name}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Bar Code <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.bar_code}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Production Code <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.production_code || "NA"}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Flavours <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.flavours?.name || "NA"}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Fragrances <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.fragrances?.name || "Unavailable"}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Record hyperlink <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.flavours?.hyperlink || "NA"}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Launch Type <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.launch_type?.name || "NA"}
                          </span>
                        </li>
                        <li>
                          <span className="tableListTitle">
                            Number of Variants{" "}
                            <span className="colonTxt">:</span>
                          </span>
                          <span className="tableListInfo">
                            {product?.number_of_variants}
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="company"
                    role="tabpanel"
                    aria-labelledby="company-tab"
                  >
                    <p>
                      <div
                        dangerouslySetInnerHTML={{
                          __html: product?.brand?.company?.description ?? "",
                        }}
                      ></div>
                    </p>
                  </div>
                </div>
              </div>
              <div className="productDetailsDescription">
                <h3>Product Description</h3>
                <p>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: product?.description ?? "",
                    }}
                  ></div>
                </p>

                <h3>Positioning Claims</h3>
                <p>
                  {Array.isArray(product?.positioning_claims)
                    ? product?.positioning_claims
                        .map((positioning_claim) => positioning_claim?.name)
                        .join(" - ")
                    : ""}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Spin>
  );
};

export default ProductDetails;
