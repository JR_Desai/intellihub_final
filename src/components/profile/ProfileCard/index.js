import React from "react";

import Widget from "components/Widget/index";
import EventItem from "./EventItem";
import {eventList} from "../../../containers/CompanyProfile/data"
import { Button } from "antd";
import { Link } from "react-router-dom";

const ProfileCard = ({title, companyId, data}) => {
  
  return (
    <Widget styleName="gx-card-profile">
      <div className="ant-card-head gx-card-tabs gx-w-100 gx-flex-row gx-justify-content-between">
        <span className="ant-card-head-title">{title}</span>
        <Link to={`/companies/${companyId}/createNews`}>
          <Button type="text" className="gx-text-primary">
            Add News
          </Button>
        </Link>
      </div>
      <div className="gx-pt-md-3">
        {data?.map((data, index) =>
          <EventItem key={index} date={data.date} desc={data.short_description} image={data.image_link} name={data.headline} />
        )}
      </div>
    </Widget>
  );
}

export default ProfileCard;
