import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import clsx from "clsx";

import { ChevronLeft as ChevronLeftIcon,
         ChevronRight as ChevronRightIcon, 
         Menu as MenuIcon,
         Search,
        } from '@material-ui/icons'; 

import { makeStyles,
         Typography,
         Drawer,
         IconButton,
         Box,
         ButtonGroup,
         Button,
         FormControlLabel,
         Checkbox,
        } from '@material-ui/core';

import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import { Icon, Image, Grid, Divider} from 'semantic-ui-react'

import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";

const drawerWidth = '240';

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex"
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    hide: {
        display: "none"
    },
    drawer: {
        width: drawerWidth,
    },
    drawerPaper: {
        width: drawerWidth.concat,
        position: 'relative',
        marginTop: '109px',
        zIndex:'1029'
    },
    drawerHeader: {
        alignItems: "center",
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: "flex-end"
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
        }),
        marginLeft: -drawerWidth
    },
    contentShift: {
        transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
        }),
        marginLeft: 0
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const DealsAndInvestment = () => {

  const url = "http://3.108.46.195:9000";
  const classes = useStyles();

  const theme = createMuiTheme({
    typography: {
        h4 : {
            fontFamily: `Kite Display`,
            color: '#415A6C',
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: '22px',
            lineHeight: '31px',
        },
        subtitle1 : {
            fontFamily: `Kite Display`,
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: '18px',
            color: '#EA3592',
        },
        subtitle2 : {
            fontFamily: `'Nanum Gothic', sans-serif`,
            fontStyle: `normal`,
            fontSize: '10px',
            color: '#415A6C',
        },
        body1 : {
            fontFamily: `'Lato', sans-serif`,
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: '10px',
        },
        body2 : {
            fontFamily: `'Lato', sans-serif`,
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: '10px',
            color: '#9DA5B3',
        },
        caption : {
            fontFamily: `'Lato', sans-serif`,
            fontSize: '10px',
            color: '#9DA5B3',
        }
    },
  })

/*****Drawer*****/
  const [open, setOpen] = useState(false);

  const handleDrawerOpen = () => setOpen(true)
  const handleDrawerClose = () => setOpen(false)

/******Deals & Investment Render******/
  const [pageData, setPageData] = useState([]);

  const fetchPageData = () => {
      fetch(`${url}/acquisition/view/?filters={"type":"operator","data":{"attribute":"row_status","operator":"=","value":"1"}}`)
      .then(res => res.json())
      .then((result) => setPageData(result.results))
  }

  useEffect(() => {fetchPageData()}, [pageData]);

const [clicked, setClicked] = useState(false);
const toggle = (index) => {
  if (clicked === index) return setClicked(null);
  setClicked(index);
};
   //Render tags
   const [parentTag, setParentTag] = useState([]);
   const [childTag, setChildTag] = useState([]);
   useEffect(() => { fetchParentTag() }, [])
   useEffect(() => { fetchChildTag() }, [])
   
   const fetchParentTag = () => {
     fetch(`${url}/tag_parent/view/`)
     .then((res) => res.json())
     .then((result) => setParentTag(result))
   }
   const fetchChildTag = () => {
     fetch(`${url}/tag_child/view/`)
     .then((res) => res.json())
     .then((result) => setChildTag(result))
   }
 
   var [tagState, setTagState] = useState([undefined]);
   var [tagCheck, setTagCheck] = useState([]);
 
   const handleTagChange = (event) => {
    console.log(event.target.checked, event.target.name);
setTagCheck({...tagCheck, [event.target.name]: event.target.checked})
      
    if(event.target.checked) {
      if(tagState.indexOf(parseInt(event.target.name)) === -1) {
        tagState.push(parseInt(event.target.name));
      }
    }
    if(!event.target.checked) {
      var index = tagState.indexOf(parseInt(event.target.name))
        if (index !== -1) {
          tagState.splice(index, 1);
          // setTagState([...tagState]);
        }
      // tagState = tagState.filter(tag => parseInt(tag) !== parseInt(event.target.name))
    }
    const fetchTagData = () => {  
      if(tagState.length === 0) {
        fetch(`${url}/deals_investments/view/`)
        .then((res) => res.json())
        .then((result) => setPageData(result.results))
      }
      else {
        fetch(`${url}/deals_investments/view/?filters={"type":"operator","data":{"attribute":"tag","operator":"in","value":[${tagState}]}}`)
        .then((res) => res.json())
        .then((result) => setPageData(result.results))
      }
    }
    fetchTagData();
   }
   
   
   var filterTags = parentTag.filter(tag => tag.module === 11)
     console.log(parentTag);
     const renderTags = filterTags.map(tag => {
       var currentParentTag = tag.id;
       var currentModule = tag.module;
       var filterChildTag = childTag.filter(tag => tag.module === currentModule && tag.parent_id === currentParentTag)
 
       return <>      
       <div className="wrap" onClick={() => toggle(tag.id)}>
       <h6>{tag.name}</h6>
       <span>
           {clicked === tag.id ? (<AiOutlineMinus className="acordian_icon" />) : 
           (<AiOutlinePlus className="acordian_icon" />)}
       </span>
       </div>
       {clicked === tag.id ? (
       filterChildTag.map((tag) => (
         <div className="checking">
           <div className="acordian_drop">
           <FormControlLabel
             control={<Checkbox checked={tagCheck.childId}
             onChange={handleTagChange} name={tag.name} />}
             label={tag.name}
           />
           </div>
         </div>
         ))
       ) : null}
       </>
     }) 
  var sortedPageData = [...pageData]

  var renderPageData = sortedPageData.map(pageData => {
      return <>
        <Grid.Column>
            <Grid.Row>
            <div className="sub-cards ml-3">
            <div className="deals-image">
                <Link to={`/user/dealsandinvestments/${pageData.id}`}>
                <Image src={pageData.company.image_link} size='small' className="card-img mt-3"/>
                </Link>
                </div>
                <div className={open ? "card-info" : "card-info ml-5"}  style={open ? {marginLeft: "-50px"} : {} }>
                <Link to={`/user/dealsandinvestments/${pageData.id}`}>

                    <Typography variant="subtitle1">{pageData.company.name}</Typography>
                </Link>
                
                    <Typography variant="subtitle2" gutterBottom>{pageData.company.name} . {pageData.company.hq}</Typography>
                    <Typography variant="body2" gutterBottom>Founded Data: {pageData.company.year_of_incorporation}</Typography>
                    <div >
                    <div className="box">
                      <p className="char-cut-line-3 mb-2">
                        {pageData.company.description}
                      </p>
                      <h6 className="deals-pest">
                        Pest Control · Insect Killers/Repellents 
                      </h6>
                    </div>
                    {/* <ClampLines text = {pageData.company.description} 
                    lines={open ? 2 : 3}
                    id="default"
                    /> */}
                    </div>
                    <Typography variant="caption" gutterBottom>{pageData.acquisition_status.announced_date}</Typography>
                </div>
                
            </div>
            </Grid.Row>
            <Divider section />
        </Grid.Column>
      </>
  })
  
  //Data Sorting


    return (
        <div className="page newMainBlock">
        <Drawer
            variant="persistent"
            anchor="left"
            open={open}
            classes={{
                paper: classes.drawerPaper
            }}
        >
            <div className="acordian">
                <div>
                    <IconButton onClick={handleDrawerClose}>
                    <ChevronLeftIcon />
                    <MenuIcon />
                    </IconButton>
                </div>
                <div className="acordian_header">
                    <div className="acordian_search">
                    <Search className="acordian_search_icon" />
                    <input
                        type="text"
                        aria-label="Text input with dropdown button"
                        placeholder="search here"
                        className="acordian_ip"
                    />
                    </div>
                </div>
                          {/***************acordian***************/}
                <div className="acordian_content">
                    {renderTags}
                </div>
            </div>
        </Drawer>
        <main className={clsx(classes.content, {
          [classes.contentShift]: open
        })}>
        <div className={classes.drawerHeader} />
        <div className="container">
        <div className="news">
            <ThemeProvider theme={theme}>
                <Box borderBottom={1}>
                <div className="news_header">
                    <div className="news_left_header">
                        {open || (<IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        >
                        <MenuIcon />
                       
                        </IconButton>)}
                            <Typography variant="h4" style={{lineHeight: '2.235'}}>Deals &amp; Investment</Typography>
                    </div>
                    <div className="news_right_header">
                        <ButtonGroup size="large" variant="outlined" color="secondary" aria-label="large outlined button group" className="">
                            <Button startIcon={<Icon title="Descending" name='sort content descending' size='large' />} ></Button>
                            <Button startIcon={<Icon name='sort content ascending' size='large' />} ></Button>
                        </ButtonGroup>
                    </div>
                </div>
                </Box>

                <div className="news_mid mt-4">
                <div className="deal-and-invest">
                <Grid columns={open ? 1 : 2}>
                {renderPageData}
                </Grid>
                </div>
                </div>
            </ThemeProvider>
        </div>
        </div>
        </main>
        </div>
    )
}

export default DealsAndInvestment
