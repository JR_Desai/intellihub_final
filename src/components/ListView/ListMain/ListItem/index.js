import React, { useEffect, useState } from 'react'
import { Badge, Checkbox, Switch } from "antd";
import labels from "../../../../containers/Technology/data/labels";
import { Link } from 'react-router-dom';
import Avatar from 'antd/lib/avatar/avatar';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import ConfirmPopup from '../../../ConfirmPopup';
import { showConfirmRemove } from '../../../../appRedux/actions/Companies';
import { deleteDeal } from '../../../../appRedux/actions/DealsAndInvestment';
import { deleteInvestors } from '../../../../appRedux/actions/Investors';
import ListView from '../../../ListView';


const ListItem = ({ item, link, updateModuleItem , onRemoveItem}) => {
    
    const dispatch = useDispatch();
    const history = useHistory();

    // const onApproveClick = () => {
    //     item.row_status = 1
    //     updateModuleItem(item.id, item, dispatch)
    // }

    // const onDisapproveClick = () => {
    //     item.row_status = 2
    //     updateModuleItem(item.id, item, dispatch)
    // }

    const handleSwitchChange = (checked) => {
        console.log(checked)
        if(checked) {
            item.row_status = 1
            updateModuleItem(item.id, item, dispatch)
        }
        if(!checked) {
            item.row_status = 2
            updateModuleItem(item.id, item, dispatch)
        }
    }
    console.log(item);
    return (
        <>
        <Link to={link == 'investors' || link == 'dai' ? `/${link}` : `/${link}/${item.id}`} className="gx-module-list-item">
            <Avatar shape="square" size="large" className="gx-mr-4" src={item.cover_image_upload|| item.cover_image ||item.upload_image || item.image_link} alt={item.name} />
            <div className="gx-module-list-info" onClick={() => {

            }}>
                <div className="gx-module-list-content">
                    <div className={`gx-subject gx-text-black gx-mb-2 gx-mt-2`} style={{ 'maxWidth': '400px' }}  >
                        {item.name}
                    </div>
                </div>
            </div>

            {(link == 'investors' || link == 'dai') ?
                <div className="gx-flex-row gx-justify-content-end">
                    <Link to={`/${link}/update/${item.id}`}>
                        <i className="gx-icon-btn icon icon-edit" />
                    </Link>
                    <i className="gx-icon-btn icon icon-trash" onClick={() =>
                    onRemoveItem(item.id)
                    }
                    />
                </div>
                : ''}

            {/* {(item.row_status == null || item.row_status.status_code == "READY FOR REVIEW")
                ?
                <Link to={`/${link}`}>
                    {clickableButton(onApproveClick, "Approve", "green")}
                    {clickableButton(onDisapproveClick, "Disapprove", "red")}
                </Link>
                :
                <Link to={`/${link}`}>
                    {unclickableButton(item.row_status.status_code, item.row_status.status_code == "APPROVED" ? "green" : "red")}
                </Link>
            } */}
            
            {/* <ConfirmPopup showStatus={showAlert} onConfirm={onRemove} onCancel={onCancel} /> */}
            <Link to={`/${link}`} onClick={ (event) => event.preventDefault() }>
                <Switch 
                checkedChildren={<><i className={`icon icon-check-circle-o`} /><span>Approved</span></>} 

                unCheckedChildren={<><i className={`icon icon-close-circle`} /><span>Disapproved</span></>} 
                
                checked={(item.row_status.status_code === "APPROVED") ? true : false }               
                className="mr-5 ml-5"
                onChange={handleSwitchChange}
                />
            </Link>
        </Link>

        </>
    )
}

export default ListItem;

const clickableButton = (onClick, title, color) => {
    return (
        <button onClick={onClick} className={`gx-neuo-button gx-text-${color} gx-border-${color}`}>
            {title}
        </button>
    );
}

const unclickableButton = (title, color) => {
    return (
        <button disabled={true} className={`gx-btn gx-w-100 gx-bg-white gx-border-${color} gx-text-${color}`}>
            {title}
        </button>
    );
}

