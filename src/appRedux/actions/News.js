import axios from 'axios';
import { CREATE_NEWS_SUCCESS, GET_ALL_NEWS_SUCCESS, GET_NEWS_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_NEWS_SUCCESS } from '../../constants/ActionTypes';
import { updateNewsCompany } from './Companies';

export const createNews = async (companyUpdate, company, data, imageUpload, dispatch) => {
        console.log('image',imageUpload)
        try {
                const news = await axios.post(`/news/create/`, data);
                const newsId = news.data.id;

                const fileUpdate = await axios.patch(`news/update/${newsId}`, imageUpload, {
                        headers: {
                                'Content-Type': 'multipart/form-data'
                        }
                })

                dispatch({
                type: CREATE_NEWS_SUCCESS,
                payload: fileUpdate.data
                });
        
                companyUpdate ? updateNewsCompany(news.data.id, company, dispatch) : getAllNews(dispatch);
                getAllNews(dispatch);
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllNews = async (dispatch) => {
        try {
                const news = await axios.get(`/news/view/`);
                news.data.results.forEach((result) => result["name"] = result["headline"]);
                dispatch({
                        type: GET_ALL_NEWS_SUCCESS,
                        payload: news.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getNews = async (newsId, dispatch) => {
        try {
                const news = await axios.get(`/news/view/?id=${newsId}`);
                dispatch({
                        type: GET_NEWS_SUCCESS,
                        payload: news.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateNews = async (newsId, data, image, dispatch) => {
        try {
                const news = await axios.all(
                        [axios.put(`/news/update/${newsId}`,{
                                "headline": data.headline,
                                "date": data.date,
                                "short_description": data.short_description,
                                "long_description": data.long_description,
                                "image_link": data.image_link,
                                "link": data.link,
                                "video_link": data.video_link,
                                "tag": data.tag,
                                "status": true
                        }),
                        axios.patch(`news/update/${newsId}`, image, {
                                headers: {
                                        'Content-Type': 'multipart/form-data'
                                }
                        })]
                );
                dispatch({
                        type: UPDATE_NEWS_SUCCESS,
                        payload: news[1].data
                })
                getAllNews(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusNews = async (newsId, data, dispatch) => {
        try {
                const news = await axios.put(`/news/update/${newsId}`, {
                        headline: data.headline,
                        long_description: data.long_description,
                        row_status: data.row_status
                }
                );
                dispatch({
                        type: UPDATE_NEWS_SUCCESS,
                        payload: news.data
                })
                getAllNews(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const deleteNews = async (technologyID, dispatch) => {
        try {
                await axios.delete(`/news/delete/${technologyID}`);
                dispatch ({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
                getAllNews(dispatch);
                } catch (err) {
                console.log(err);
        }
};
