import React from "react";
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import {Route, Switch} from "react-router-dom";
// import "semantic-ui-css/semantic.min.css";
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.js';
import $ from 'jquery';
import Popper from 'popper.js';
import configureStore, { history } from './appRedux/store';
import Main from "./containers/Main";
import UserMain from "./containers/Main/User/Main"
import "./assets/vendors/style";

const store = configureStore();


const App = () => {

  return (
    <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route path="/user">
          <UserMain />
        </Route>
        <Route path="/">
          <Main />
        </Route>
      </Switch>
    </ConnectedRouter>
  </Provider>
  );
}

export default App;
