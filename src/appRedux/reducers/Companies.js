import { GET_ALL_COMPANIES_SUCCESS, GET_COMPANY_SUCCESS, CREATE_COMPANY_SUCCESS, UPDATE_COMPANY_SUCCESS, SHOW_CONFIRM_REMOVE, EDIT_COMPANY_DETAILS, ADD_TO_COMPARE_DATA, ADD_TO_COMPARE_COLUMN, GET_ALL_COMPANY_NAMES_SUCCESS } from "../../constants/ActionTypes";


const INIT_STATE = {
    loader: false,
    companies: [],
    company: {},
    showRemove: false,
    showEdit: false,
    companyNames: [],
};


export default function companyReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_COMPANY_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case ADD_TO_COMPARE_DATA: {
            return {
                ...state,
                loader: false,
                compareData: action.payload
            }
        }
        case ADD_TO_COMPARE_COLUMN: {
            return {
                ...state,
                loader: false,
                compareColumns: action.payload
            }
        }
        case GET_ALL_COMPANIES_SUCCESS: {
            return {
                ...state,
                loader: false,
                companies: action.payload
            }
        }
        case GET_ALL_COMPANY_NAMES_SUCCESS: {
            return {
                ...state,
                loader: false,
                companyNames: action.payload
            }
        }
        case GET_COMPANY_SUCCESS: {
            return {
                ...state,
                loader: false,
                company: action.payload[0]
            }
        }
        case CREATE_COMPANY_SUCCESS: {
            return {
                ...state,
                loader: false,
                company: action.payload
            }
        }
        case UPDATE_COMPANY_SUCCESS: {
            return {
                ...state,
                loader: false,
                company: action.payload
            }
        }    
        default:
            return state;
    }
}
