import moment from "moment";
import React from "react";
import { FiBookmark, FiShare2 } from "react-icons/fi";
import { Link } from "react-router-dom";
import dummyImage from "images/dummy-image.png";

const ProductItem = ({
  product,
  selectedProductsForComparision,
  setselectedProductsForComparision,
}) => {
  return (
    <div className="productList-col">
      <div className="productList-content">
        <div className="productListImage">
          <Link to={`/user/products/${product?.id}`}>
            <img
              src={
                product?.primary_image ||
                product?.primary_image_upload ||
                dummyImage
              }
              alt=""
            />
          </Link>
        </div>
        <div className="productInfo">
          <div className="productRecordIdDate">
            <ul>
              <li>
                <Link to={`/user/products/${product?.id}`}>
                  Record ID - <span className="recordID">{product?.id}</span>
                </Link>
              </li>
              <li>
                Date -{" "}
                <span>
                  {moment(product?.date_published).format("DD/MM/YYYY")}
                </span>
              </li>
            </ul>
          </div>
          <h3 className="productTitle">
            <Link to={`/user/products/${product?.id}`}>{product?.name} </Link>
          </h3>
          <h4 className="productSubtitle">{product?.product_variant}</h4>
          <div className="productDescription">
            <h5>
              {product?.brand?.name} - {product?.brand?.company?.market}
            </h5>
            <p className="char-cut-line-1">
              {Array.isArray(product?.positioning_claims)
                ? product?.positioning_claims
                    .map((positioning_claim) => positioning_claim?.name)
                    .join(" - ")
                : ""}
            </p>
          </div>
          <div className="productInfoList">
            <ul>
              <li>Pest Control</li>
              <li>{product?.category?.name}</li>
              <li>
                {product?.unit_pack_size}
                {product?.packaging_units?.name}
              </li>
            </ul>
          </div>
          <div className="productPrice">
            Price{" "}
            <span>
              {product?.currency?.code} {product?.price_local_currency}
            </span>{" "}
            <span>/</span> <span>${product?.price_us_dollars}</span>{" "}
            <span>/</span> <span>€{product?.price_euros}</span>
          </div>
          <div className="productActions">
            <div className="productAction-list">
              <div className="product_check">
                <input
                  checked={selectedProductsForComparision.find(
                    (p) => p?.id === product?.id
                  )}
                  type="checkbox"
                  className="checkbox"
                  onChange={(e) => {
                    if (e.target.checked) {
                      setselectedProductsForComparision([
                        ...selectedProductsForComparision,
                        product,
                      ]);
                    } else {
                      setselectedProductsForComparision(
                        [...selectedProductsForComparision].filter(
                          (p) => p.id !== product.id
                        )
                      );
                    }
                  }}
                />
                <span className="checkbox_title"> Compare </span>
              </div>
            </div>
            <div className="productAction-list">
              <a
                className="download-dropdown-toggle dropdown-toggle"
                href="#"
                id="donwloadActions"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Download
              </a>
              <div
                className="dropdown-menu downloadDropdown"
                aria-labelledby="donwloadActions"
              >
                <ul>
                  <li>
                    <a href="#">All Format</a>
                  </li>
                  <li>
                    <a href="#">Excel</a>
                  </li>
                  <li>
                    <a href="#">PDF</a>
                  </li>
                  <li>
                    <a href="#">Powerpoint</a>
                  </li>
                  <li>
                    <a href="#">Images</a>
                  </li>
                  <li>
                    <a href="#">HTML</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="productAction-list">
              <div className="productShareItem">
                <ul>
                  <li>
                    <a href="#" className="rawMaterialIcon iconBookmark">
                      <FiBookmark />
                    </a>
                  </li>
                  <li>
                    <a href="#" className="rawMaterialIcon iconShare">
                      <FiShare2 />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductItem;
