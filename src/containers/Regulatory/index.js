import React from 'react'
import ListView from '../../components/ListView';

const Regulatory = () => {
    const list = [
        {
            id: "1",
            name: "Name",
            description: "Description",
            image_link: "",
        },
        {
            id: "2",
            name: "Name",
            description: "Description",
            image_link: "",
        },
        {
            id: "3",
            name: "Name",
            description: "Description",
            image_link: "",
        },
    ]

    return (
        <ListView listToBeShown={list} searchTitleText="regulatory and safety" link="ras" title="Regulatory & Safety" />
    )
}

export default Regulatory;
