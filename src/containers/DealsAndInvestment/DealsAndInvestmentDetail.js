import React, { useEffect } from 'react';
import { useSelector, UseDispatch, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';

import { Avatar, Badge, Col, Row } from "antd";
import ConfirmPopup from '../../components/ConfirmPopup';
import { showConfirmRemove } from '../../appRedux/actions/Companies';
import Widget from '../../components/Widget';

import { getDealsAndInvestment, deleteDeal } from '../../appRedux/actions'

const DealsAndInvestmentDetail = props => {

    const dispatch = useDispatch();
    const history = useHistory();

    const { deal, showRemove } = useSelector(({ deals }) => deals);

    const dealId = props.match.params.id;

    useEffect(() => {
        getDealsAndInvestment(dealId, dispatch);
    }, []);

    const editDeal = () => {
        history.push(`update/${dealId}`);
    }

    const onRemove = () => {
        deleteDeal(dealId, dispatch);
        history.replace('/dai');
    }

    const removeDeal = () => {
        showConfirmRemove(true, dispatch);
    }

    return (
        <div className="gx-main-content-wrapper">
            <div className="gx-module-detail gx-module-list">
                <div className="gx-module-detail-item gx-module-detail-header">
                    <Row>
                        <Col xs={24} sm={12} md={17} lg={12} xl={17}>
                            <div className="gx-flex-row">
                                <div className="gx-user-name gx-mr-md-4 gx-mr-2 gx-my-1">
                                    <div className="gx-flex-row gx-align-items-center gx-pointer">
                                        <Avatar className="gx-mr-3" src="" alt="" />
                                        <h4 className="gx-mb-0">Demo Name</h4>
                                    </div>
                                </div>
                            </div>
                        </Col>

                        <Col xs={24} sm={12} md={7} lg={12} xl={7}>
                            <div className="gx-flex-row gx-justify-content-end">
                                <i className="gx-icon-btn icon icon-edit" onClick={() =>
                                    editDeal()}
                                />
                                <i className="gx-icon-btn icon icon-trash" onClick={() =>
                                    removeDeal()}
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
                <Widget className="gx-module-detail-item">
                    <div className="gx-form-group gx-flex-row gx-align-items-center gx-flex-nowrap">
                        <Col className="gx-flex-row gx-flex-1 gx-flex-nowrap">
                            <div className="gx-w-25">
                                {/* <img
                                    className='' src={deal.image_link}
                                    alt="..." /> */}
                            </div>
                            <div className="gx-w-75 gx-ml-5">
                                <div className="h1">
                                    {deal.acquired_by}
                                </div>
                                <span className="gx-ml-auto gx-mb-3">{deal.announced_date}</span>
                                <div className="gx-mt-3 gx-mb-5">
                                    <span className="h3">Acquisition Terms: </span>
                                    <p className="gx-text-grey">
                                        {deal.acquisition_terms}
                                    </p>
                                </div>
                                {/* <div className="gx-mt-3 gx-mb-5">
                                    <span className="h3">Company: </span>
                                    <p className="gx-text-grey">
                                        {deal.company.name}
                                    </p>
                                </div> */}
                            </div>
                        </Col>
                    </div>
                </Widget>
                <ConfirmPopup showStatus={showRemove} onConfirm={onRemove} />
            </div>
        </div>
    )


}

export default DealsAndInvestmentDetail;

