import axios from 'axios';

import {ADD_TO_COMPARE_COLUMN, ADD_TO_COMPARE_DATA, CREATE_COMPANY_SUCCESS, EDIT_COMPANY_DETAILS, GET_ALL_COMPANIES_SUCCESS, GET_ALL_COMPANY_NAMES_SUCCESS, GET_COMPANY_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_COMPANY_SUCCESS } from '../../constants/ActionTypes';



export const showConfirmRemove = (showConfirmRemove, dispatch) => {
        dispatch ({
                type: SHOW_CONFIRM_REMOVE,
                payload: showConfirmRemove
        })
}

export const editCompanyDetails = (editCompanyDetails, dispatch) => {

        dispatch ({
                type: EDIT_COMPANY_DETAILS,
                payload: editCompanyDetails
        })
}


export const addToCompareColumns = (companyColumn, dispatch) => {
        dispatch ({
                type: ADD_TO_COMPARE_COLUMN,
                payload: companyColumn
        })
}

export const addToCompareData = (company, dispatch) => {
        dispatch ({
                type: ADD_TO_COMPARE_DATA,
                payload: company
        })
}

export const createCompany = async (formData, dispatch) => {
        console.log(formData.get("location"));
        try {
                const company = await axios.post(`/company/create/`, formData,
                        {
                                headers: {
                                        "Content-Type": "multipart/form-data;boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL"
                                }
                        }
                );
                dispatch({
                        type: CREATE_COMPANY_SUCCESS,
                        payload: company.data
                })
                getAllCompanies(dispatch);
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllCompanies = async (dispatch) => {
        try {
                const companies = await axios.get(`/company/view/`);
                dispatch({
                        type: GET_ALL_COMPANIES_SUCCESS,
                        payload: companies.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getAllCompanyNames = async (dispatch) => {
        try {
                const companyNames = await axios.get(`/company/names/`);
                dispatch({
                        type: GET_ALL_COMPANY_NAMES_SUCCESS,
                        payload: companyNames.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const getCompany = async (companyID, dispatch) => {
        try {
                const company = await axios.get(`/company/view/?id=${companyID}`);
                console.log(company);
                dispatch({
                        type: GET_COMPANY_SUCCESS,
                        payload: company.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateCompany = async (companyID, data, dispatch) => {
        try {
                const company = await axios.put(`/company/update/${companyID}`, data);
                dispatch({
                        type: UPDATE_COMPANY_SUCCESS,
                        payload: company.data
                })
                getAllCompanies(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const updateNewsCompany = async (newsId, data, dispatch) => {

        const newsIds = [];
        data.news.map((news) => newsIds.push(news.id));

        try {
                const company = await axios.put(`/company/update/${data.id}`, {
                        name: data.name,
                        description: data.description,
                        year_of_incorporation: data.year_of_incorporation,
                        news: [...newsIds, newsId],
                        country_phone_code: data.country_phone_code, 
                        phone: data.phone
                });
                
                dispatch({
                        type: UPDATE_COMPANY_SUCCESS,
                        payload: company.data
                })
                getAllCompanies(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const deleteCompany = async (companyID, dispatch) => {
        try {
                await axios.delete(`/company/delete/${companyID}`);
                dispatch ({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
                getAllCompanies(dispatch);
                } catch (err) {
                console.log(err);
        }
};
