import React from "react";
import {Col, Row, Tabs} from "antd";
import Widget from "components/Widget";
import AboutItem from "./AboutItem";
import { useSelector } from "react-redux";

const TabPane = Tabs.TabPane;

const About = () => {

  const {company} = useSelector(({companies}) => companies);

  return (
    <Widget title="Overview" styleName="gx-card-tabs gx-card-profile">
      <Tabs className='gx-tabs-right' defaultActiveKey="1">
        <TabPane tab="Keymatrics" key="1">
          <div className="gx-mb-2">
            <Row>
                <Col xl={8} lg={12} md={12} sm={12} xs={24}>
                      <AboutItem icon="table" desc={company.year_of_incorporation} title={"Year of Incorporation"} />
                </Col>
                <Col xl={8} lg={12} md={12} sm={12} xs={24}>
                      <AboutItem icon="shopping-cart" desc={company.market} title={"Market"} />
                </Col>
                <Col xl={8} lg={12} md={12} sm={12} xs={24}>
                      <AboutItem icon="pricing-table" isValue={true} desc={company.funding} title={"Funding"} />
                </Col>
                <Col xl={8} lg={12} md={12} sm={12} xs={24}>
                      <AboutItem icon="growth" isValue={true} desc={company.valuation} title={"Valuation"} />
                </Col>
                <Col xl={8} lg={12} md={12} sm={12} xs={24}>
                      <AboutItem icon="family" desc={company.no_of_employees} title={"No. of employees"} />
                </Col>
                <Col xl={8} lg={12} md={12} sm={12} xs={24}>
                      {/* <AboutItem icon="revenue-new" isValue={true} desc={company.business_revenue_mix[2020]} title={"Annual Revenue"} /> */}
                </Col>
            </Row>
          </div>
        </TabPane>
      </Tabs>
    </Widget>
  );
}


export default About;
