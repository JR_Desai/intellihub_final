import axios from "axios";
import React from "react";
import { Icon } from "semantic-ui-react";
import { Button, ButtonGroup, IconButton } from "@material-ui/core";
import { Menu } from "@material-ui/icons";

const ProductContentHeader = ({
  hqDrawer,
  handleDrawerOpen,
  listIconActiveClass,
  handleChangeList,
  gridIconActiveClass,
  handleChangeGrid,
}) => {
  return (
    <>
      <div className="news_header">
        <div className="news_left_header">
          {hqDrawer || (
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
            >
              <Menu />
              {/* <ChevronRight /> */}
            </IconButton>
          )}
          <h4 className="main-header">Products</h4>
        </div>
        <div className="news_right_header product_header">
          <ButtonGroup
            size="large"
            variant="outlined"
            color="secondary"
            aria-label="large outlined button group"
            className="mr-3"
          >
            <Button
              className={listIconActiveClass}
              startIcon={<Icon name="list" size="large" />}
              onClick={handleChangeList}
            ></Button>
            <Button
              className={gridIconActiveClass}
              startIcon={<Icon name="grid layout" size="large" />}
              onClick={handleChangeGrid}
            ></Button>
          </ButtonGroup>
          <div className="product_analytics_actions">
            <div className="product_analytics_btn">
              <a
                href="/user/product-analytics/teste"
                className="btn btn-primary btn-outline"
              >
                Analyse
              </a>
            </div>
            <div className="productDownloadDetails product_analytics_btn">
              <a
                className="download-dropdown-toggle dropdown-toggle"
                href="#"
                id="donwloadActions"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Download All
              </a>
              <div
                className="dropdown-menu downloadDropdown"
                aria-labelledby="donwloadActions"
              >
                <ul>
                  <li>
                    <a href="#">All Format</a>
                  </li>
                  <li>
                    <a
                      href="javascript::void(0)"
                      onClick={() =>
                        window.open(
                          `${axios.defaults.baseURL}/finished_product/download`
                        )
                      }
                    >
                      Excel
                    </a>
                  </li>
                  <li>
                    <a href="#">PDF</a>
                  </li>
                  <li>
                    <a href="#">Powerpoint</a>
                  </li>
                  <li>
                    <a href="#">Images</a>
                  </li>
                  <li>
                    <a href="#">HTML</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="news_line"></div>
    </>
  );
};

export default ProductContentHeader;
