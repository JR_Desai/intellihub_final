import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import clsx from "clsx";

import { ChevronLeft as ChevronLeftIcon,
         ChevronRight as ChevronRightIcon, 
         Menu as MenuIcon,
         Search,
        } from '@material-ui/icons'; 

import { makeStyles,
         Drawer,
         IconButton,
         Box,
         ButtonGroup,
         Button,
         FormControlLabel,
         Checkbox
        } from '@material-ui/core';

import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import { Icon, Image, Grid, Card, Divider, Button as SemanticButton} from 'semantic-ui-react'

import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import './DealsAndInvestor.css'

const drawerWidth = '240';

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex"
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    hide: {
        display: "none"
    },
    drawer: {
        width: drawerWidth,
    },
    drawerPaper: {
        width: drawerWidth.concat,
        position: 'relative',
        marginTop: '109px',
        zIndex:'1029'
    },
    drawerHeader: {
        alignItems: "center",
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: "flex-end"
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
        }),
        marginLeft: -drawerWidth
    },
    contentShift: {
        transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
        }),
        marginLeft: 0
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const DealsAndInvestment = () => {

  const url = "http://3.108.46.195:9000";
  const classes = useStyles();

  const theme = createMuiTheme({
    typography: {
        h4 : {
            fontFamily: `Kite Display`,
            color: '#415A6C',
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: '22px',
            lineHeight: '31px',
        },
        h5 : {
            fontFamily: `Kite Display`,
            color: '#415A6C',
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: '20px',
            lineHeight: '31px',
        },
    },
  })

/*****Drawer*****/
  const [open, setOpen] = useState(false);

  const handleDrawerOpen = () => setOpen(true)
  const handleDrawerClose = () => setOpen(false)

/******Investors Render******/
  const [pageData, setPageData] = useState([]);


  const fetchPageData = () => {
    fetch(`${url}/investor/view/?filters={"type":"operator","data":{"attribute":"row_status","operator":"=","value":"1"}}`)
    .then((res) => res.json())
    .then((result) => setPageData(result.results))
  }

  useEffect(() => {fetchPageData()}, [pageData]);
  const [clicked, setClicked] = useState(false);
const toggle = (index) => {
  if (clicked === index) return setClicked(null);
  setClicked(index);
};
   //Render tags
   const [parentTag, setParentTag] = useState([]);
   const [childTag, setChildTag] = useState([]);
   useEffect(() => { fetchParentTag() }, [])
   useEffect(() => { fetchChildTag() }, [])
   
   const fetchParentTag = () => {
     fetch(`${url}/tag_parent/view/`)
     .then((res) => res.json())
     .then((result) => setParentTag(result))
   }
   const fetchChildTag = () => {
     fetch(`${url}/tag_child/view/`)
     .then((res) => res.json())
     .then((result) => setChildTag(result))
   }
 
   var [tagState, setTagState] = useState([undefined]);
var [tagCheck, setTagCheck] = useState([]);
 
   const handleTagChange = (event) => {
    console.log(event.target.checked, event.target.name);
setTagCheck({...tagCheck, [event.target.name]: event.target.checked})
      
    if(event.target.checked) {
      if(tagState.indexOf(parseInt(event.target.name)) === -1) {
        tagState.push(parseInt(event.target.name));
      }
    }
    if(!event.target.checked) {
      var index = tagState.indexOf(parseInt(event.target.name))
        if (index !== -1) {
          tagState.splice(index, 1);
          // setTagState([...tagState]);
        }
      // tagState = tagState.filter(tag => parseInt(tag) !== parseInt(event.target.name))
    }
    const fetchTagData = () => {  
      if(tagState.length === 0) {
        fetch(`${url}/deals_investments/view/`)
        .then((res) => res.json())
        .then((result) => setPageData(result.results))
      }
      else {
        fetch(`${url}/deals_investments/view/?filters={"type":"operator","data":{"attribute":"tag","operator":"in","value":[${tagState}]}}`)
        .then((res) => res.json())
        .then((result) => setPageData(result.results))
      }
    }
    fetchTagData();
   }
   
   
   var filterTags = parentTag.filter(tag => tag.module === 11)
     console.log(parentTag);
     const renderTags = filterTags.map(tag => {
       var currentParentTag = tag.id;
       var currentModule = tag.module;
       var filterChildTag = childTag.filter(tag => tag.module === currentModule && tag.parent_id === currentParentTag)
 
       return <>      
       <div className="wrap" onClick={() => toggle(tag.id)}>
       <h6>{tag.name}</h6>
       <span>
           {clicked === tag.id ? (<AiOutlineMinus className="acordian_icon" />) : 
           (<AiOutlinePlus className="acordian_icon" />)}
       </span>
       </div>
       {clicked === tag.id ? (
       filterChildTag.map((tag) => (
         <div className="checking">
           <div className="acordian_drop">
           <FormControlLabel
             control={<Checkbox checked={tagCheck.childId}
             onChange={handleTagChange} name={tag.name} />}
             label={tag.name}
           />
           </div>
         </div>
         ))
       ) : null}
       </>
     }) 
  var sortedPageData = [...pageData]
  const placeholder_image = "https://react.semantic-ui.com/images/wireframe/image.png";
  var renderPageData = sortedPageData.map(pageData => {
      return <>
      <Grid.Column>
        <Card>
            <Image
                src={pageData.logo_link === 'url' ? placeholder_image : pageData.logo_link} wrapped
                ui={false}
                className="p-4"
                size="small"
            />
            <Card.Content textAlign="center">
            <div className="p-3">
            <Card.Header>
            <div className="company-header">
              {pageData.investor_name}
            </div>
            </Card.Header>
            <SemanticButton basic className="mt-2 ui investor-btn" fluid>
                <Link to={`/user/investor/${pageData.id}`}>
                <h3>Activities</h3>
                </Link>
            </SemanticButton>
            </div>
            </Card.Content>
        </Card>
        </Grid.Column>
      </>
  })
  
  //Data Sorting


    return (
        <div className="page newMainBlock">
        <Drawer
            variant="persistent"
            anchor="left"
            open={open}
            classes={{
                paper: classes.drawerPaper
            }}
        >
            <div className="acordian">
                <div>
                    <IconButton onClick={handleDrawerClose}>
                    <ChevronLeftIcon />
                    <MenuIcon />
                    </IconButton>
                </div>
                <div className="acordian_header">
                    <div className="acordian_search">
                    <Search className="acordian_search_icon" />
                    <input
                        type="text"
                        aria-label="Text input with dropdown button"
                        placeholder="search here"
                        className="acordian_ip"
                    />
                    </div>
                </div>
                <div className="acordian_content">
                    {renderTags}
                </div>
            </div>
        </Drawer>
        <main className={clsx(classes.content, {
          [classes.contentShift]: open
        })}>
        <div className={classes.drawerHeader} />
        <div className="container">
        <div className="news">
            <ThemeProvider theme={theme}>
                <Box borderBottom={1}>
                <div className="news_header">
                    <div className="news_left_header">
                        {open || (<IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        >
                        <MenuIcon />

                        </IconButton>)}
                        <h4 className="main-header" >All Investors</h4>
                    </div>
                    <div className="news_right_header">
                    <ButtonGroup size="large" variant="outlined" color="secondary" aria-label="large outlined button group" className="">
                        <Button startIcon={<Icon name='sort content descending' size='large' />} ></Button>
                        <Button startIcon={<Icon name='sort content ascending' size='large' />} ></Button>
                    </ButtonGroup>
                    </div>
                </div>
                </Box>

                <div className="news_mid mt-4">
                <Grid columns={open ? 4 : 6}>
                  {renderPageData}
                </Grid>
                </div>
            </ThemeProvider>
        </div>
        </div>
        </main>
        </div>
    )
}

export default DealsAndInvestment
