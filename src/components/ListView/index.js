import React, { useState, useEffect } from 'react';
import ListSidebar from '../../components/ListSidebar';
import AppModuleHeader from "components/AppModuleHeader/index";

import { Drawer, Pagination } from 'antd';

import filters from "../../containers/Technology/data/filters";
import labels from "../../containers/Technology/data/labels";
import ListMain from '../../components/ListView/ListMain';
import { getAllTechnologies } from '../../appRedux/actions/Technologies';
// import Paginations from "../../components/Pagination";


const ListView = ({ title, listToBeShown, searchTitleText, link, updateModuleItem, refreshList }) => {

    const [searchList, setSearchList] = useState();
    const [list, setList] = useState([]);
    const [filterId, setFilterId] = useState(1);
    // const [selectedTechList, setSelectedTechList] = useState();
    const [drawer, toggleDrawer] = useState(false);

    useEffect(() => {
        setList(listToBeShown);
    }, [listToBeShown])

    const searchlist = (searchText) => {
        if (searchText === '') {
            setList(listToBeShown.filter((company) => !company.deleted));
        } else {
            const searchList = listToBeShown.filter((item) =>
                !item.deleted && item.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1);
            setList(searchList)
        }
    };

    const updateSearch = (e) => {
        setSearchList(e.target.value);
        searchlist(e.target.value);
    }

    const Filters = () => {
        return filters.map((filter, index) =>
            <li key={index} onClick={() => {
                const filterList = listToBeShown.filter(item => {
                    if (filter.id === 1) {
                        return item
                    } else if (filter.id === 2 && item.row_status.status_code == "APPROVED") {
                        return item
                    } else if (filter.id === 3 && item.row_status.status_code == "DISAPPROVED") {
                        return item
                    } else {
                        return null
                    }
                });
                setFilterId(filter.id);
                setList(filterList);
            }
            }>
                <span className={filter.id === filterId ? 'gx-link active' : 'gx-link'}>
                    <i className={`icon icon-${filter.icon}`} />
                    <span>{filter.title}</span>
                </span>
            </li>
        )
    };

    const Categories = () => {
        return labels.map((label, index) =>
            <li key={index} onClick={() => {
                const categoryList = listToBeShown.filter(item => item.catgories.includes(label.id));
                setList(categoryList);
            }
            }>
                <span className="gx-link">
                    <i className={`icon icon-circle`} />
                    <span>{label.title}</span>
                </span>
            </li>
        )
    };

    // const onAllTodoSelect = () => {
    //     const selectAll = selectedlist.length < list.length;
    //     if (selectAll) {
    //         getAllTodo();
    //     } else {
    //         getUnselectedAllTodo();
    //     }
    // }
    const [numEachPage, setnumEachPage] = useState(10);
    const [curPage, setCurPage] = useState(1);
    const [listTrim, setListTrim] = useState([{minValue : 0 , maxValue : 10}]);
    const handlePagination = page => {
        setCurPage(page)
        setListTrim({
            minValue: (page - 1) * numEachPage,
            maxValue: page * numEachPage
        })
    }
    return (
        <div className="gx-main-content">
            <div className="gx-app-module">
                <div className="gx-d-block gx-d-lg-none">
                    <Drawer
                        placement="left"
                        closable={false}
                        visible={drawer}
                        onClose={() => { toggleDrawer(!drawer) }}>
                        <ListSidebar
                            NavFilters={Filters}
                            NavLabels={Categories}
                            title={title}
                            link={`/${link}/create`}
                        />
                    </Drawer>
                </div>
                <div className="gx-module-sidenav gx-d-none gx-d-lg-flex" style={{ zIndex: '0' }} >
                    <ListSidebar
                        NavFilters={Filters}
                        NavLabels={Categories}
                        title={title}
                        link={`/${link}/create`}
                    />
                </div>

                <div className="gx-module-box">
                    <div className="gx-module-box-header">

                        <span className="gx-drawer-btn gx-d-flex gx-d-lg-none">
                            <i className="icon icon-menu gx-icon-btn" aria-label="Menu"
                                onClick={() => toggleDrawer(!drawer)}
                            />
                        </span>
                        <AppModuleHeader placeholder={`Search ${searchTitleText}`}
                            onChange={updateSearch}
                            value={searchList}
                        />
                    </div>
                    <div className="gx-module-box-content">
                        {/* <div className="gx-module-box-topbar gx-module-box-topbar-todo">
                    <Auxiliary>
                    <Checkbox 
                        className="gx-icon-btn" 
                        color="primary"
                        indeterminate={selectedTechList > 0 && selectedTechList < filteredTechList.length}
                        checked={selectedTechList > 0}
                        value="SelectMail"
                    />
                    <Dropdown placement="bottomRight" trigger={['click']}>
                        <div>
                        <span className="gx-px-2">a</span>
                        <i className="icon icon-charvlet-down"/>
                        </div>
                    </Dropdown>
                    </Auxiliary>

                <Dropdown placement="bottomRight" trigger={['click']}>
                    <i className="gx-icon-btn icon icon-tag"/>
                </Dropdown>
                </div> */}
                        <ListMain list={list.slice(listTrim.minValue || 0, listTrim.maxValue || 10)} link={link} updateModuleItem={updateModuleItem} refreshList={refreshList} />
                        <div style={{textAlign:"center"}} className="mt-4">
                            <Pagination 
                            current={curPage}
                            defaultPageSize={numEachPage}
                            total={list.length}
                            onChange={handlePagination} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}



export default ListView;
