import axios from 'axios';
import { CREATE_EVENT_SUCCESS, GET_ALL_EVENTS_SUCCESS, GET_EVENT_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_EVENT_SUCCESS } from '../../constants/ActionTypes';


export const createEvents = async (data , dispatch) => {
        try {
                const event = await axios.post(`/event_conference/create/`, data);
                
                dispatch({
                        type: CREATE_EVENT_SUCCESS,
                        payload: event.data.results
                });
                getAllEvents(dispatch);
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllEvents = async (dispatch) => {
        try {
                const events = await axios.get(`/event_conference/view/`);
                events.data.results.forEach((result) => result["name"] = result["event_name"]);
                dispatch({
                        type: GET_ALL_EVENTS_SUCCESS,
                        payload: events.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getEvents = async (eventId, dispatch) => {
        try {
                const event = await axios.get(`/event_conference/view/?id=${eventId}`);
                dispatch({
                        type: GET_EVENT_SUCCESS,
                        payload: event.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateEvents = async (eventId, data, dispatch) => {
        try {
                const event = await axios.put(`/event_conference/update/${eventId}`, data);
                dispatch({
                        type: UPDATE_EVENT_SUCCESS,
                        payload: event.data
                })
                getAllEvents(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusEvent = async (eventId, data, dispatch) => {
        try {
                const event = await axios.put(`/event_conference/update/${eventId}`, {
                                event_name: data.event_name,
                                row_status: data.row_status
                        }
                );
                dispatch({
                        type: UPDATE_EVENT_SUCCESS,
                        payload: event.data
                })
                getAllEvents(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const deleteEvents = async (eventId, dispatch) => {
        try {
                await axios.delete(`/event/delete/${eventId}`);
                dispatch ({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
                getAllEvents(dispatch);
                } catch (err) {
                console.log(err);
        }
};
