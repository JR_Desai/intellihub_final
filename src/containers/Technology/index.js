import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getAllTechnologies, getTechnologyTags, updateStatusTechnology } from '../../appRedux/actions/Technologies';
import ListView from '../../components/ListView';

const Technology = () => {

    const {technologies, tags} = useSelector(({technologies}) => technologies);

    const dispatch = useDispatch();
    
    useEffect(() => {
        getAllTechnologies(dispatch);
        getTechnologyTags(dispatch);
    }, [])

    return (
        <ListView listToBeShown={technologies} link="technology" searchTitleText="Technology" title="Technology" getAllFunction={getAllTechnologies} updateModuleItem={updateStatusTechnology} tags={tags} />
    );
}

export default Technology
