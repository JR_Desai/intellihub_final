import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import { Typography,
         Drawer,
         IconButton,
         MenuItem,
         FormControl,
         Select,
         FormControlLabel,
         Checkbox,
         ButtonGroup,
         Button
        } from '@material-ui/core'
import { ChevronLeft as ChevronLeftIcon,
         ChevronRight as ChevronRightIcon,
         Menu as MenuIcon,
         Share as ShareIcon,
         BookmarkBorder as BookmarkBorderIcon,
         Bookmark,
        } from '@material-ui/icons'
import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import * as ReactBootStrap from "react-bootstrap";
import { FiSearch, FiFacebook, FiLinkedin, FiTwitter } from "react-icons/fi";
import { Grid, Image, Divider, Label, Item, Icon, Popup, Button as SUIButton, Form, TextArea } from 'semantic-ui-react'
import moment from 'moment';
import { AutoComplete, Spin } from 'antd';

import Fuse from 'fuse.js'
import './Technology.css'

const drawerWidth = '240';

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth.concat,
    position: 'relative',
    marginTop: '109px',
    zIndex:'1029'
  },
  drawerHeader: {
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginLeft: 0
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const Technology = () => {
  const url = "http://3.108.46.195:9000";

  const classes = useStyles();
  const [eventDrawer, setEventDrawer] = useState(false);

  const [bookmarks, setBookmark] = useState();
  const handleBookMark = (event) => { 
    console.log(event.target.checked)
    if(event.target.checked){
      console.log(event.target.value)
    }
  }
  const handleDrawerOpen = () => setEventDrawer(true)
  const handleDrawerClose = () => setEventDrawer(false)

  const [clicked, setClicked] = useState(false);

  const toggle = (index) => {
    if (clicked === index) return setClicked(null)
    setClicked(index);
  };

  const [events, setData] = useState([])
  const [sort, setSort] = useState('dateDesc');

  useEffect(() => fetchData(), [])
  
  const fetchData = () => {
    fetch(`${url}/technology/view/?filters={"type":"operator","data":{"attribute":"row_status","operator":"=","value":"1"}}`)
    .then((res) => res.json())
    .then((result) => setData(result.results))
  }

  var sortedData = [...events]
  const [loading, setLoading] = useState(false)

  const handleDescChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateDesc'), 2000)
    
    console.log(event.target)
  }
  
  const handleAscChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateAsc'), 2000)    

    console.log(event.target)
  }

  
  if(sort === 'dateDesc') sortedData = events.sort((a, b) => a.name.localeCompare(b.name))
  else if(sort === 'dateAsc') sortedData = events.sort((a, b) => b.name.localeCompare(a.name))

console.log(sortedData);
  const renderEvents = sortedData.map((data) => {
    return  <>
      <Grid className="mb-2 mt-2">
        <Grid.Column width={2}>
          <div class="technology-image">
        <Link to={`/user/technology/${data.id}`}>
          <Image size="small" src={data.image_link} />
        </Link>
        </div>
        </Grid.Column>
        

        <Grid.Column width={11}>
        <div className="technology-content">
        <Item> 
        <Item.Content>
          <Link to={`/user/technology/${data.id}`}>
            <Item.Header>
              <p className="card_title">{data.name}</p>
            </Item.Header>
          </Link>
            <Item.Description>
              <div className="box">
                <p>
                  {data.description}
                </p>
              </div>
                <p>
                <Link to={`/user/technology/${data.id}`} style={{color:'#FD0090'}}>
                  Read More
                </Link>
                </p>
            </Item.Description>
            <Item.Extra>
            {data.company.map((data) =>
                <Label circular basic as='company_label'  key={data.id}>
                    <Image avatar spaced='right' src={data.image_link || "http://gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=25"} />
                    {data.name}
                </Label>
            )}
            </Item.Extra>
        </Item.Content>
        </Item>
        </div>
        </Grid.Column>
        <Grid.Column width={2}>
          <div className="technology-bookmark-share-icon">
            <span>
            <Checkbox icon= {<BookmarkBorderIcon />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={data.id}/>
            &#xa0;&#x0358;&#xa0;
            <Popup
        trigger={
          <IconButton><ShareIcon /></IconButton>
        }
        position="left center"
        on="click"
        flowing
        style={{height:'auto'}}
      >
        <div style={{width:"450px",wordBreak:"break-all"}}>
        <label>Share On</label><br/>
        <div className="row pl-3">
          <Link className="socialIcon socialLinkedin mr-1"><FiLinkedin /></Link>
          <Link className="socialIcon socialTwitter mr-1"><FiTwitter /></Link>
          <Link className="socialIcon socialFacebook mr-1"><FiFacebook /></Link>
        </div>
        <Divider />
        <label>Share With</label><br/>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
          Elliot@gmail.com
          <Icon name='delete' />
        </Label>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
          Stevie@gmail.com
          <Icon name='delete' />
        </Label>
        
        <Form className="mt-2">
          <Form.Field>
            <input placeholder='Enter email id' />
          </Form.Field>
          <Form.Field>
          <TextArea placeholder='Type your message here' />
          </Form.Field>
          <Form.Field>
            <SUIButton type='submit' color="pink">Submit</SUIButton>
            <SUIButton type='submit' basic>Cancel</SUIButton>
          </Form.Field>
        </Form>
        </div>
      </Popup>
            </span>
          </div>
        </Grid.Column>
      </Grid>
    <Divider />
    </>
  });

  //Render tags
  const [parentTag, setParentTag] = useState([]);
  const [childTag, setChildTag] = useState([]);
  useEffect(() => { fetchParentTag() }, [])
  useEffect(() => { fetchChildTag() }, [])
  
  const fetchParentTag = () => {
    fetch(`${url}/tag_parent/view/`)
    .then((res) => res.json())
    .then((result) => setParentTag(result))
  }
  const fetchChildTag = () => {
    fetch(`${url}/tag_child/view/`)
    .then((res) => res.json())
    .then((result) => setChildTag(result))
  }

  const [tagState, setTagState] = useState([undefined]);
  var [tagCheck, setTagCheck] = useState([]);

  const handleTagChange = (event) => {
      console.log(event.target.checked, event.target.name);
      setTagCheck({...tagCheck, [event.target.name]: event.target.checked})

      const fetchNews = () => {
      if(event.target.checked && event.target.name === 'mobile') {
        fetch(`${url}/news/view/?tag=5`)
        .then((res) => res.json())
        // .then((result) => setNews(result.results))
        .then((result) => console.log(result.results))
      }
      else if(event.target.checked && event.target.name === 'Tablet') {
        fetch(`${url}/news/view/?tag=1`)
        .then((res) => res.json())
        // .then((result) => setNews(result.results))
        .then((result) => console.log(result.results))

      }
      else {
        fetch(`${url}/news/view/`)
        .then((res) => res.json())
        // .then((result) => setNews(result.results))
        .then((result) => console.log(result.results))
      }
    }
    fetchNews();
  }
  
  
  var filterTags = parentTag.filter(tag => tag.module === 10)
    
    const renderTags = filterTags.map(tag => {
      var currentParentTag = tag.id;
      var currentModule = tag.module;
      var filterChildTag = childTag.filter(tag => tag.module === 1 && tag.parent_id === currentParentTag)

      return <>      
      <div className="wrap" onClick={() => toggle(tag.id)}>
      <h6>{tag.name}</h6>
      <span>
          {clicked === tag.id ? (<AiOutlineMinus className="acordian_icon" />) : 
          (<AiOutlinePlus className="acordian_icon" />)}
      </span>
      </div>
      {clicked === tag.id ? (
      filterChildTag.map((tag) => (
        <div className="checking">
          <div className="acordian_drop">
          <FormControlLabel
            control={<Checkbox checked={tagCheck.childId}
            onChange={handleTagChange} name={tag.name} />}
            label={tag.name}
          />
          </div>
        </div>
        ))
      ) : null}
      </>
    })

  //Main Page
  
  return (
    <div className="page newMainBlock">
      <Drawer
        variant="persistent"
        anchor="left"
        open={eventDrawer}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className="acordian">
        <div>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
            <MenuIcon />
          </IconButton>
        </div>
        <div className="acordian_header">
          <div className="acordian_search">
            <FiSearch className="acordian_search_icon" />
            <input
              type="text"
              aria-label="Text input with dropdown button"
              placeholder="search here"
              className="acordian_ip"
            />
          </div>
        </div>

          {/***************acordian***************/}
            <div className="acordian_content">
                {renderTags}
            </div>
        </div>
      </Drawer>
      
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: eventDrawer
        })}
      >
       <div className="container"> 
      <div className={classes.drawerHeader} />
        
      <div className="news">
        <div className="news_header">
          <div className="news_left_header">
            {eventDrawer || (<IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
            >
              <MenuIcon />
             
            </IconButton>)}
            <h4 className="main-header">Technology Type</h4>
          </div>
          <div className="news_right_header"> 
          <ButtonGroup size="large" variant="outlined" color="secondary" aria-label="large outlined button group" className="" disabled={loading}>
              <Button onClick={handleDescChange}  startIcon={<Icon name='sort content descending' size='large' />} ></Button>
              <Button onClick={handleAscChange} startIcon={<Icon name='sort content ascending' size='large' />} ></Button>
          </ButtonGroup>
          </div>
        </div>
        <div className="news_line"></div>
        {/* Events list */}
        <Spin spinning={loading}>
        <Item.Group>
          <div className="technologyListBlock technologyListCol1">
          {renderEvents}
          </div>
        </Item.Group>
        </Spin>
      </div>
        </div>      
      </main>
    </div>
  );
};

export default Technology;
