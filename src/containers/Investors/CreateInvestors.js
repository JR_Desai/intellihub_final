import React, { useEffect, useState, setState } from 'react'
import { Form, Button, Card, Select, Input } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import InputGroup from '../../components/InputGroup';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { createInvestors, updateInvestors, getInvestors, getCategories, getInvestorType, getInvestorStages } from '../../appRedux/actions/Investors';
import { getAllCompanyNames } from '../../appRedux/actions/Companies';
import { getAllDealsAndInvestment } from '../../appRedux/actions/DealsAndInvestment';
import { convertToRaw, EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";

const CreateInvestors = props => {

    const Option = Select.Option;
    const history = useHistory();

    const { investor, category, investorTypes, investorStages } = useSelector(({ investors }) => investors);
    const { companyNames } = useSelector(({ companies }) => companies);
    const { deals } = useSelector(({ deals }) => deals);

    const investorId = props.match.params.id;

    const companiesOption = companyNames ?.map(
        (company, _) => <Option key={company.id}>{company.name}</Option>
    );

    const dealsOption = deals ?.map(
        (deal, _) => <Option key={deal.id}>{deal.acquired_by}</Option>
    );

    const categoryOption = category ?.map(
        (categ, _) => <Option key={categ.id} value={categ.id}>{categ.name}</Option>
    );

    const investorTypeOption = investorTypes ?.map(
        (type, _) => <Option key={type.id}>{type.name}</Option>
    );

    const investorStageOption = investorStages ?.map(
        (stage, _) => <Option key={stage.id}>{stage.name}</Option>
    );

    const initState = {
        "investor_name": "",
        "logo_link": "",
        "logo": null,
        "about": "",
        "company": null,
        "investments_count": null,
        "exists_count": null,
        "acquisition_details": null,
        "portfolio_companies": null,
        "category": null,
        "investor_type": null,
        "investor_stage": null,
        "row_status": 3,
        "status": false
    }

    useEffect(() => {
        getAllCompanyNames(dispatch);
        getAllDealsAndInvestment(dispatch);
        getCategories(dispatch);
        getInvestorType(dispatch);
        getInvestorStages(dispatch);
        investorId && getInvestors(investorId, dispatch);
    }, [])


    if (investorId) {
        if (investor.company && investor.company.id) {
            investor.company = investor.company.id;
        }
        if (investor.category && investor.category.id) {
            investor.category = investor.category.id;
        }
        if (investor.investor_stage && investor.investor_stage.id) {
            investor.investor_stage = investor.investor_stage.id;
        }
        if (investor.investor_type && investor.investor_type.id) {
            investor.investor_type = investor.investor_type.id;
        }
        if (investor.portfolio_companies && investor.portfolio_companies.id) {
            investor.portfolio_companies = investor.portfolio_companies.id;
        }
        if (investor.acquisition_details && investor.acquisition_details.id) {
            investor.acquisition_details = investor.acquisition_details.id;
        }
        if (investor.row_status && investor.row_status.id) {
            investor.row_status = investor.row_status.id;
        }
    }


    const values = investorId ? investor : initState

    const [inves, setInves] = useState(values);

    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        investorId ? updateInvestors(inves.id, inves, dispatch) : createInvestors(inves, dispatch);
        history.replace("/investors");
    }

    const handleEditorAbout = (editorState) => {
        var htmlData = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        setInves({ ...inves, about: htmlData })
    }

    const handleClose = (e) => {
        history.replace("/investors");
    }

    const header = investorId ? "Update" : "Create"

    return (
        <div className="gx-main-content-wrapper">
            <Card className="gx-card" title={`${header} Investors`}>
                <Form
                    name="register"
                    scrollToFirstError
                >
                    <InputGroup name="investor_name" label="Investor name: " type="text" value={inves.investor_name}
                        onChange={(e) => setInves({ ...inves, investor_name: e.target.value })}
                    />

                    <InputGroup name="logo_link" label="Logo link: " type="text" value={inves.logo_link}
                        onChange={(e) => setInves({ ...inves, logo_link: e.target.value })}
                    />

                    <div className="gx-form-group">
                        <label htmlFor="logo" className="gx-form-label">Logo:</label>
                        <Input name="logo" onChange={(e) => setInves({ ...inves, logo: e.target.files[0] })}
                            encType='multipart/form-data' type="file" accept="image/*" placeholder="Add Image"
                        />
                    </div>

                    <div className="gx-form-group">
                        <label name="about" className="gx-form-label">About: </label>
                        {/* <TextArea
                            name="about"
                            value={inves.about}
                            rows={3}
                            onChange={(e) => setInves({ ...inves, about: e.target.value })}
                        /> */}
                        <Editor editorStyle={{
                            width: '100%',
                            minHeight: 100,
                            borderWidth: 1,
                            borderStyle: 'solid',
                            borderColor: 'lightgray'
                        }}
                            wrapperClassName="demo-wrapper"
                            onEditorStateChange={handleEditorAbout}
                        />
                    </div>

                    <div className="gx-form-group">
                        <label className="gx-form-label">Company: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={investorId && inves.company}
                            placeholder="Select"
                            onChange={(value) => setInves({ ...inves, company: value })}
                        >
                            {companiesOption}
                        </Select>
                    </div>

                    <InputGroup
                        name="investments_count"
                        label="Investments count: "
                        type="number"
                        value={inves.investments_count}
                        onChange={(e) => setInves({ ...inves, investments_count: e.target.value })}
                    />

                    <InputGroup
                        name="exists_count"
                        label="Exists count: "
                        type="number"
                        value={inves.exists_count}
                        onChange={(e) => setInves({ ...inves, exists_count: e.target.value })}
                    />

                    <div className="gx-form-group">
                        <label className="gx-form-label">Acquisition details: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={investorId && inves.acquisition_details}
                            placeholder="Select"
                            onChange={(value) => setInves({ ...inves, acquisition_details: value })}
                        >
                            {dealsOption}
                        </Select>
                    </div>

                    <div className="gx-form-group">
                        <label className="gx-form-label">Portfolio companies: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={investorId && inves.portfolio_companies}
                            placeholder="Select"
                            onChange={(value) => setInves({ ...inves, portfolio_companies: value })}
                        >
                            {companiesOption}
                        </Select>
                    </div>

                    <div className="gx-form-group">
                        <label className="gx-form-label">Category: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={investorId && inves.category}
                            placeholder="Select"
                            onChange={(value) => setInves({ ...inves, category: value })}
                        >
                            {categoryOption}
                        </Select>
                    </div>

                    <div className="gx-form-group">
                        <label className="gx-form-label">Investor type: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={investorId && inves.investor_type}
                            placeholder="Select"
                            onChange={(value) => setInves({ ...inves, investor_type: value })}
                        >
                            {investorTypeOption}
                        </Select>
                    </div>

                    <div className="gx-form-group">
                        <label className="gx-form-label">Investor stage: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={investorId && inves.investor_stage}
                            placeholder="Select"
                            onChange={(value) => setInves({ ...inves, investor_stage: value })}
                        >
                            {investorStageOption}
                        </Select>
                    </div>
                <Button onClick={handleSubmit} style={{float:'right'}} className="gx-w-20" type="primary" htmlType="submit">
                    Submit Details
                </Button>
                <Button onClick={handleClose} style={{float:'right', marginRight:'15px'}} className="gx-w-20" type="primary" htmlType="submit">
                    Close
                </Button>
                </Form>
            </Card>
        </div>
    )
}

export default CreateInvestors
