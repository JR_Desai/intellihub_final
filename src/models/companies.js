export const  productsData=[
    {
        key: "3",
        parameters: "Year of Incorporation",
        company1  : "2019-01-10",
        company2  : "2019-01-12",
    },
    {
        key: "4",
        parameters: "Valuation",
        company1  : "200110",
        company2  : "201122",
    },
    {
        key: "5",
        parameters: "Funding",
        company1  : "200110",
        company2  : "201122",
    },
    {
        key: "6",
        parameters: "Location",
        company1  : "200110",
        company2  : "201122",
    },
    {
        key: "7",
        parameters: "Number of employees",
        company1  : "200110",
        company2  : "201122",
    },
    {
        key: "8",
        parameters: "Phone No.",
        company1  : "200110",
        company2  : "201122",
    },
    {
        key: "10",
        parameters: "Email ID",
        company1  : "abcd@gmail.com",
        company2  : "abcd@gmail.com",
    },
    
]

export const companiesData = [
    {
        "id": "1",
        "name": "Namo",
        "description": "Namo NamoNamo",
        "yoi": "2019-12-01"
    },
    {
        "id": "2",
        "name": "Namo",
        "description": "Namo",
        "yoi": "2019-12-01"
    },
    {
        "id": "3",
        "name": "Namo",
        "description": "NamoNamo NamoNamo",
        "yoi": "2019-12-01"
    },
    {
        "id": "4",
        "name": "Namo",
        "description": "Namo",
        "yoi": "2019-12-01"
    },
    {
        "id": "5",
        "name": "Namo",
        "description": "Namo",
        "yoi": "2019-12-01"
    },
    {
        "id": "6",
        "name": "Namo",
        "description": "Namo",
        "yoi": "2019-12-01"
    }
]