import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom"
import { useParams } from 'react-router-dom'
import * as ReactBootStrap from "react-bootstrap"
import { Grid, Image, Breadcrumb, Divider, Icon } from 'semantic-ui-react'
import { Button, Typography, Box } from '@material-ui/core'
import { LinkedIn } from '@material-ui/icons';
import {FiLinkedin, FiTwitter, FiMail, FiBookmark} from "react-icons/fi";


const Details = () => {
  const url = "http://3.108.46.195:9000";
  
  const { techId } = useParams()
  const [magDetail, setMagDetail] = useState([])

  useEffect(() => {  
    const fetchMag = () => {
      fetch(`${url}/technology/view/?id=${techId}`)
      .then((res) => res.json())
      .then((result) => {
        console.log(result)
        setMagDetail(result.results);
        }
      )
    }
    fetchMag()
  }, [])

  const renderParticularMags = magDetail.map(data => { 
      return <>
        <div className="container">
          <div className="miniContainer">
          <div className="newsDetails-social-block">
            <ul className="newsDetails-socialList">
              <li><Link className="socialIcon socialLinkedin"><FiLinkedin /></Link></li>
              <li><Link className="socialIcon socialTwitter"><FiTwitter /></Link></li>
              <li><Link className="socialIcon socialMail"><FiMail /></Link></li>
              <li><Link className="socialIcon socialSave"><FiBookmark /></Link></li>
            </ul>
          </div>
          <div className="magazine_left">
            <Link to="/user/technology">
            Technology
            </Link>
            <Icon name='right chevron' />
            {data.name} 
            {/* <Divider hidden /> */}

            <div className="newsDetails-block">
              <h2>{data.name}</h2>
              <div className="maga newsTwoColImageBlock">
                <div className="newsTwoColImage">
                  <ReactBootStrap.Image
                    className="magazine-page-img"
                    src={data.image_link}
                  />
                </div>
                <div className="newsTwoColImage">
                  <ReactBootStrap.Image
                    className="magazine-page-img"
                    src={data.image_link}
                  />
                </div>
              </div>
              <div className="newsDesctiptions">
                <p>{data.description}</p>
              </div>
            </div>
          </div>

          {/* <Grid centered>
              <Typography variant="h4" >
                <Box fontWeight="fontWeightBold">
                  {data.name}
                </Box>
              </Typography>

              <Grid.Row>
                <Grid.Column width={3} textAlign="center" className="mt-4">
                  <p><Icon circular inverted color='blue' name='linkedin alternate' size="large"/></p>
                  <p><Icon circular inverted color='teal' name='twitter' size="large"/></p>
                  <p><Icon circular inverted color='brown' name='mail' size="large"/></p>
                  <p><Icon circular inverted color='black' name='bookmark' size="large"/></p>
                </Grid.Column>
                <Grid.Column width={13}>
                  <Grid.Row className="m-4">
                    <Image size="medium" src={data.image_link} />
                    <Image size="medium" src={data.image_link} className="ml-5"/>
                  </Grid.Row>
                    <p>{data.description}</p>
                </Grid.Column>
              </Grid.Row>
          </Grid> */}
          </div>
        </div>
      </>
    })

  return (
    <div className="magazine-1 inner-wrapper">
      {renderParticularMags}
    </div>
  )
};

export default Details;
