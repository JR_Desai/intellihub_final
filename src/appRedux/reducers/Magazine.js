import { CREATE_MAGAZINES_SUCCESS, EDIT_MAGAZINES_DETAILS, GET_ALL_MAGAZINES_SUCCESS, GET_MAGAZINES_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_MAGAZINES_SUCCESS } from "../../constants/ActionTypes"


const INIT_STATE = {
    loader: false,
    magazines: [],
    magazine: {},
    showRemove: false,
    showEdit: false
}

export default function magazineReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_MAGAZINES_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_MAGAZINES_SUCCESS: {
            return {
                ...state,
                loader: false,
                magazines: action.payload
            }
        }
        case GET_MAGAZINES_SUCCESS: {
            return {
                ...state,
                loader: false,
                magazine: action.payload[0]
            }
        }
        case CREATE_MAGAZINES_SUCCESS: {
            return {
                ...state,
                loader: false,
                magazine: action.payload
            }
        }
        case UPDATE_MAGAZINES_SUCCESS: {
            return {
                ...state,
                loader: false,
                magazine: action.payload
            }
        }    
        default:
            return state;
    }
}
