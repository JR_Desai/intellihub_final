import { CREATE_INVESTOR_SUCCESS, EDIT_INVESTOR_DETAILS, GET_ALL_INVESTORS_SUCCESS, GET_INVESTOR_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_INVESTOR_SUCCESS, GET_ALL_CATEGORY_SUCCESS, GET_ALL_INVESTOR_TYPE_SUCCESS, GET_ALL_INVESTOR_STAGE_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    investors: [],
    investor: {},
    showRemove: false,
    showEdit: false,
    category: [],
    investorTypes:[],
    investorStages:[]
}

export default function investorReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_INVESTOR_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_INVESTORS_SUCCESS: {
            return {
                ...state,
                loader: false,
                investors: action.payload
            }
        }
        case GET_INVESTOR_SUCCESS: {
            return {
                ...state,
                loader: false,
                investor: action.payload[0]
            }
        }
        case GET_ALL_CATEGORY_SUCCESS: {
            return {
                ...state,
                loader: false,
                category: action.payload
            }
        }

        case GET_ALL_INVESTOR_TYPE_SUCCESS: {
            return {
                ...state,
                loader: false,
                investorTypes: action.payload
            }
        }

        case GET_ALL_INVESTOR_STAGE_SUCCESS: {
            return {
                ...state,
                loader: false,
                investorStages: action.payload
            }
        }

        case CREATE_INVESTOR_SUCCESS: {
            return {
                ...state,
                loader: false,
                investor: action.payload
            }
        }
        case UPDATE_INVESTOR_SUCCESS: {
            return {
                ...state,
                loader: false,
                investor: action.payload
            }
        }
        default:
            return state;
    }
}
