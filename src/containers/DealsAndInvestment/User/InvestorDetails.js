import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom"
import { useParams } from 'react-router-dom'
import * as ReactBootStrap from "react-bootstrap"
import { Divider, Icon, Popup, Button as SUIButton, Label, Form, TextArea } from 'semantic-ui-react'
import { BookmarkBorder, Share, Bookmark } from '@material-ui/icons';
import { Checkbox, IconButton } from '@material-ui/core';
import { Descriptions } from 'antd';
import moment from 'moment'

import { FiFacebook, FiLinkedin, FiTwitter } from "react-icons/fi";

const InvestorDetails = () => {
  const url = "http://3.108.46.195:9000";
  
  const { investorId } = useParams()
  const [investorDetail, setinvestorDetail] = useState([])
  const [bookmarks, setBookmark] = useState();
  const handleBookMark = (event) => { 
    console.log(event.target.checked)
    if(event.target.checked){
      console.log(event.target.value)
    }
  }
  useEffect(() => {  
    const fetchMag = () => {
      fetch(`${url}/investor/view/?id=${investorId}`)
      .then((res) => res.json())
      .then((result) => {
        console.log(result)
        setinvestorDetail(result.results);
        }
      )
    }
    fetchMag()
  }, [])
  const placeholder_image = "https://react.semantic-ui.com/images/wireframe/image.png";

  const renderInvestor = investorDetail.map(data => { 
    console.log(data)

      return <>
        <div className="container">
          <Link to="/user/investor">
          Investors
          </Link>
          <Icon name='right chevron' />
          {data.investor_name}
        <Divider hidden />
        <div className="row">
        <div className="col-md-3">
          <div className="maga">
            <ReactBootStrap.Image
              style={{width:"165px",
              height: "165px",marginLeft:"50px"}}
              src={data.logo_link}
            />
          </div>
        </div>
        <div className="col-md-6 mt-5">
          <div className="maga-info">
          <div className="company-header"><h4>{data.investor_name}</h4></div>
            <p className="company-meta">
              {data.about}
            </p>
            <p>
            </p>
          </div>
        </div>
        <div className="col-md-3 mt-5">
          <div className="info2 row">
            <p className="more ml-3 mr-5">
            <Checkbox icon= {<BookmarkBorder />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={data.id}/>
            </p>
            <p className="more ml-2">
            <Popup
        trigger={
          <IconButton><Share /></IconButton>
        }
        position="left center"
        on="click"
        flowing
        style={{height:'auto'}}
      >
        <div style={{width:"450px",wordBreak:"break-all"}}>
        <label>Share On</label><br/>
        <div className="row pl-3">
          <Link className="socialIcon socialLinkedin mr-1"><FiLinkedin /></Link>
          <Link className="socialIcon socialTwitter mr-1"><FiTwitter /></Link>
          <Link className="socialIcon socialFacebook mr-1"><FiFacebook /></Link>
        </div>
        <Divider />
        <label>Share With</label><br/>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
          Elliot@gmail.com
          <Icon name='delete' />
        </Label>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
          Stevie@gmail.com
          <Icon name='delete' />
        </Label>
        
        <Form className="mt-2">
          <Form.Field>
            <input placeholder='Enter email id' />
          </Form.Field>
          <Form.Field>
          <TextArea placeholder='Type your message here' />
          </Form.Field>
          <Form.Field>
            <SUIButton type='submit' color="pink">Submit</SUIButton>
            <SUIButton type='submit' basic>Cancel</SUIButton>
          </Form.Field>
        </Form>
        </div>
      </Popup>
            </p>
          </div>
        </div>
        </div>
        <Divider />
        <div>
        <Descriptions bordered>
          <Descriptions.Item label="Organization Type" span={2}>Cloud Database</Descriptions.Item>
          <Descriptions.Item label="Investor Type" span={2}>{data.investor_type}</Descriptions.Item>
          <Descriptions.Item label="No of Investments" span={2}>{data.investments_count}</Descriptions.Item>
          <Descriptions.Item label="Investment Stage " span={2}>{data.investor_stage}</Descriptions.Item>
          <Descriptions.Item label="No Of Exits" span={2}>{data.exists_count}
          </Descriptions.Item>
          <Descriptions.Item label="Website" span={2}>{data.company.website}</Descriptions.Item>
          <Descriptions.Item label="Team members" span={3}>$80.00</Descriptions.Item>
          <Descriptions.Item label="Portfolio Companies" span={3}>{data.portfolio_companies.name}</Descriptions.Item>
          <Descriptions.Item label="Category" span={3}>{data.category.name}</Descriptions.Item>
          <Descriptions.Item label="Sub Category" span={3}>{data.category.name}</Descriptions.Item>
        </Descriptions>
        </div>
        </div>
      </>
    })

  return (
    <div className="magazine-1 mb-5">
      {renderInvestor}
    </div>
  )
};

export default InvestorDetails;
