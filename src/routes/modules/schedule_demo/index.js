import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import ScheduleDemo from '../../../containers/ScheduleDemo/User/ScheduleDemo';

const ScheduleDemoRoutes = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={ScheduleDemo} />
        </Switch>
    )
}

export default ServiceRoutes;
