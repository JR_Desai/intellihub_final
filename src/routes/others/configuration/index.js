import React from 'react'
import Configuration from '../../../containers/Configuration'
import { Route, Switch, useRouteMatch } from 'react-router';

const ConfigurationRoute = () => {

    const match = useRouteMatch();

    return (
        <div className="gx-main-content-wrapper">
            <Switch>
                <Route exact={false} path={`${match.url}`} component={Configuration} />
            </Switch>
        </div>
    )
}

export default ConfigurationRoute
