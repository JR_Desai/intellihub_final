import React, { useEffect, useState } from 'react'
import {
    Form,
    Button,
    Card,
    Select,
    DatePicker,
    Input,
} from 'antd';

import { useDispatch, useSelector } from 'react-redux';
import InputGroup from '../../components/InputGroup';
import { useHistory } from 'react-router';
import { createEvents, updateEvents } from '../../appRedux/actions/Event';
import {convertToRaw, EditorState} from "draft-js";
import {Editor} from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import { createTechnology, updateTechnology, getTechnologyTags } from '../../appRedux/actions/Technologies';

const CreateEvent = props => {

    useEffect(() => {
        getTechnologyTags(dispatch);
    }, [])
    const Option = Select.Option;


    const history = useHistory();
    const { technology, tags } = useSelector(({ technologies }) => technologies);

    const eventId = props.match.params.id;

    const initState = {
        event_name: "Name",
        organizer: "organizer",
        description: "description",
        image_link: null,
        image_upload: null,
        start_date: "2019-02-11",
        end_date: "2019-02-11",
        location: "hq",
        industry: "hq",
        url: "hq",
        row_status: 3
    }

    const values = eventId ? initState : initState

    const header = eventId ? "Update" : "Create"

    const [event, setEvent] = useState(values);
    const dispatch = useDispatch();

    const formData = new FormData();
    const parentTagOption = tags ?.map(
        (tag_parent, _) => <Option key={tag_parent.id} value={tag_parent.id}>{tag_parent.name}</Option>
    );
    if (eventId) {
        if (technology.company && technology.company.length > 0) {
            let Company = [];
            technology.company.map((res) => {
                res.id ? Company.push(res.id) : Company.push(res);
            });
            technology.company = Company;
        }
        if (technology.tags && technology.tags.length > 0) {
            let Tags = [];
            technology.tags.map((res) => {
                res.id ? Tags.push(res.id) : Tags.push(res);
            });
            technology.tags = Tags;
        }
        if (technology.row_status && technology.row_status.id) {
            technology.row_status = technology.row_status.id;
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        // formData.append("name", event.name);
        // formData.append("description", event.description);
        // formData.append("organizer", event.organizer);
        // formData.append("image_link", event.image_link, event.image_link.name);
        // formData.append("start_date", event.start_date);
        // formData.append("end_date", event.end_date);
        // formData.append("location", event.location);
        // formData.append("industry", event.industry);
        // formData.append("url", event.url);
        eventId ? updateEvents(eventId, event, dispatch) : createEvents(event, dispatch);
        history.replace("/event");
    }
    const handleClose = (e) => {
        history.replace("/event")
    }
    const handleEditor = (editorState) => {
        console.log( draftToHtml(convertToRaw(editorState.getCurrentContent())) )
        var htmlData = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        setEvent({...event, description: htmlData})
    }
    return (
        <div className="gx-main-content-wrapper">
        <Card className="gx-card" title={`${header} Event & Conference`}>
        <Form
            name="register"
            scrollToFirstError
            >
                <InputGroup name="name" label="Event Name: " type="text" value={event.event_name} 
                    onChange={(e) => setEvent({...event, event_name: e.target.value})} 
                />
                <div className="gx-form-group">
                    <label htmlFor="image_link" className="gx-form-label">Image:</label>
                    <Input name="image_link" onChange={(e) => setEvent({...event, image_upload: e.target.files[0]})} 
                        encType='multipart/form-data' type="file" accept="image/*" placeholder="Add Image"
                    />
                </div>
                <InputGroup name="image" label="Image Link: " type="text" value={event.image_link} 
                    onChange={(e) => setEvent({...event, image_link: e.target.value})} 
                />
                <InputGroup name="organizer" label="Organizer: " type="text" value={event.organizer} 
                    onChange={(e) => setEvent({...event, organizer: e.target.value})} 
                />
                {/* <InputGroup name="description" label="Description: " type="text" value={event.description} 
                    onChange={(e) => setEvent({...event, description: e.target.value})} 
                /> */}
                <div className="gx-form-group">
                    <label htmlFor="image_link" className="gx-form-label ">Description:</label>
                    <Editor editorStyle={{
                        width: '100%',
                        minHeight: 100,
                        borderWidth: 1,
                        borderStyle: 'solid',
                        borderColor: 'lightgray'
                    }}
                        wrapperClassName="demo-wrapper"
                        value={event.description}
                        onEditorStateChange={handleEditor}
                    />
                </div>
                <div className="gx-form-group">
                    <label htmlFor="start_date" className="gx-form-label">Start Date:</label>
                    <DatePicker name="start_date" placeholder={event.start_date}
                        onChange={(e) => setEvent({...event, start_date: e.format("YYYY-MM-DD")})} className="gx-mb-3 gx-w-100"
                    />
                </div>
                <div className="gx-form-group">
                    <label htmlFor="end_date" className="gx-form-label">End Date:</label>
                    <DatePicker name="end_date" placeholder={event.end_date}
                        onChange={(e) => setEvent({...event, end_date: e.format("YYYY-MM-DD")})} className="gx-mb-3 gx-w-100"
                    />
                </div>
                <InputGroup name="location" label="Location: " type="text" value={event.location} 
                    onChange={(e) => setEvent({...event, location: e.target.value})} 
                /><InputGroup name="industry" label="Industry: " type="text" value={event.industry} 
                    onChange={(e) => setEvent({...event, industry: e.target.value})} 
                />
                <div className="gx-form-group">
                    <label className="gx-form-label">Tags: </label>
                    <Select
                        className="gx-w-100"
                        mode="multiple"
                        defaultValue={eventId && event.tags}
                        placeholder="Select Tags"
                        onChange={(value) => setEvent({ ...event, tags: value })}
                    >
                        {parentTagOption}
                    </Select>
                </div>
                <InputGroup name="url" label="Registration URL: " type="text" value={event.url} 
                    onChange={(e) => setEvent({...event, url: e.target.value})} 
                />
                <Button onClick={handleSubmit} style={{float:'right'}} className="gx-w-20" type="primary" htmlType="submit">
                    Submit Details
                </Button>
                <Button onClick={handleClose} style={{float:'right', marginRight:'15px'}} className="gx-w-20" type="primary" htmlType="submit">
                    Close
                </Button>
        </Form>
    </Card>
    </div>
    )
}

export default CreateEvent;
