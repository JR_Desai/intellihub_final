import React from "react";
import { Route, Switch, useRouteMatch } from "react-router";
import RawMaterials from "../../../containers/RawMaterials";
import RawMaterialCreate from "../../../containers/RawMaterials/RawMaterialCreate";
import RawMaterialDetails from "../../../containers/RawMaterials/RawMaterialDetails";

const RawMaterialRoutes = () => {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route exact={true} path={`${match.url}`} component={RawMaterials} />
      <Route
        exact={true}
        path={`${match.url}/create`}
        component={RawMaterialCreate}
      />
      <Route path={`${match.url}/update/:id`} component={RawMaterialCreate} />
      <Route path={`${match.url}/:id`} component={RawMaterialDetails} />
    </Switch>
  );
};

export default RawMaterialRoutes;
