import React, { useEffect } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import CompanyRoutes from './companies';
import MarketReportsRoutes from './market_reports';
import NewsLetterRoutes from './news_letter';
import ProductRoutes from './products';
import RegulatoryRoutes from './regulatory';
import ResearchRoutes from './research';
import TechnologyRoutes from './technology';
import DealsAndInvestmentRoutes from './deals_and_investment';
import { getAllModules } from '../../appRedux/actions/Modules';
import { useDispatch } from 'react-redux';
import NewsRoutes from './news';
import MagazineRoutes from './magazine';
import EventsRoutes from './events';
import RawMaterialRoutes from './raw_materials';
import ServiceRoutes from './services';
import InvestorRoutes from './investors';
import UserInterface from './UserInterface';

const Routes = () => {

    const match = useRouteMatch();
    const dispatch = useDispatch();

    useEffect(() => {
        getAllModules(dispatch);
    }, []);

    return (
            <Switch>
                <Route exact={false} path={`${match.url}news`} component={NewsRoutes} />
                <Route exact={false} path={`${match.url}magazine`} component={MagazineRoutes} />
                <Route exact={false} path={`${match.url}event`} component={EventsRoutes} />
                <Route exact={false} path={`${match.url}raw_materials`} component={RawMaterialRoutes} />
                <Route exact={false} path={`${match.url}products`} component={ProductRoutes} />
                <Route exact={false} path={`${match.url}services`} component={ServiceRoutes} />
                <Route exact={false} path={`${match.url}companies`} component={CompanyRoutes} />
                <Route exact={false} path={`${match.url}technology`} component={TechnologyRoutes} />
                <Route exact={false} path={`${match.url}dai`} component={DealsAndInvestmentRoutes} />
                <Route exact={false} path={`${match.url}investors`} component={InvestorRoutes} />
                <Route exact={false} path={`${match.url}mr`} component={MarketReportsRoutes} />
                <Route exact={false} path={`${match.url}ras`} component={RegulatoryRoutes} />
                <Route exact={false} path={`${match.url}ral`} component={ResearchRoutes} />
                <Route exact={false} path={`${match.url}newsletter`} component={NewsLetterRoutes} />
                <Route exact={false} path={`${match.url}user`} component={UserInterface} />
            </Switch>
    );
}

export default Routes;
