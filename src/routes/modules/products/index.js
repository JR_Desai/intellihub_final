import React from "react";
import { Route, Switch, useRouteMatch } from "react-router";
import Products from "../../../containers/Products";
import CreateProduct from "../../../containers/Products/CreateProduct";
import ProductDetail from "../../../containers/Products/ProductDetail";

const ProductRoutes = () => {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route exact={true} path={`${match.url}`} component={Products} />
      <Route exact path={`${match.url}/create`} component={CreateProduct} />
      {/* <Route exact path={`${match.url}/:id/update`} component={CreateCompany} /> */}
      <Route exact path={`${match.url}/:id`} component={ProductDetail} />
      <Route exact path={`${match.url}/update/:id`} component={CreateProduct} />
      {/* <Route exact path={`${match.url}/:id/createnews`} component={CreateNews} /> */}
    </Switch>
  );
};

export default ProductRoutes;
