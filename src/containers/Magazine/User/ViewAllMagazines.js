import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";

import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import * as ReactBootStrap from "react-bootstrap";
import moment from "moment";
import { FiSearch, FiLinkedin, FiTwitter, FiMail, FiBookmark, FiFacebook } from "react-icons/fi";
import DOMPurify from 'dompurify';
import {
    Menu as MenuIcon,
    Share as ShareIcon,
    ChevronLeft as ChevronLeftIcon,
    ChevronRight as ChevronRightIcon,
    BookmarkBorder as BookmarkBorderIcon,
    Bookmark,
} from '@material-ui/icons'; 

import { 
    Drawer,
    IconButton,
    FormControlLabel,
    Checkbox,
    ButtonGroup,
    Button,
} from '@material-ui/core';

import { Icon, Card, Divider, Popup, Button as SUIButton, Form, Label, TextArea, Item } from "semantic-ui-react";
import './Magazines.css'
import { AutoComplete, Spin, DatePicker } from 'antd';

import Fuse from 'fuse.js'
import axios from "axios";

const drawerWidth = '240';

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth.concat,
    position: 'relative',
    marginTop: '109px',
    zIndex:'1029'
  },
  drawerHeader: {
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginLeft: 0
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const Magazines = () => {
  const url = "http://3.108.46.195:9000";
  const createMarkup = (html) => {
    return  {
      __html: DOMPurify.sanitize(html)
    }
  }
  const classes = useStyles();
  const [magDrawer, setMagDrawer] = useState(false);
  const [sort, setSort] = useState('dateDesc');

  const handleDrawerOpen = () => {
    setMagDrawer(true);
  };

  const handleDrawerClose = () => {
    setMagDrawer(false);
  };

  const [mags, setMags] = useState([])
  
  useEffect(() => {
    fetchMags()
  }, [])
  
  const fetchMags = () => {
    fetch(`${url}/magazines/view/?filters={"type":"operator","data":{"attribute":"row_status","operator":"=","value":"1"}}`)
    .then((res) => res.json())
    .then((result) => setMags(result.results))
  }

  const [clicked, setClicked] = useState(false)

  const toggle = (index) => {
    if (clicked === index) return setClicked(null)
    setClicked(index)
  };
  const [datePick ,setDatePickValue] = useState();

  var sortedMagData = [...mags];

  const [loading, setLoading] = useState(false)

  const handleDescChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateDesc'), 2000)
  }
  
  const handleAscChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateAsc'), 2000)
  }
  
  if(sort === 'dateDesc') sortedMagData = mags.sort((a, b) => new Date(b.published_date) - new Date(a.published_date))
  else if(sort === 'dateAsc') sortedMagData = mags.sort((a, b) => new Date(a.published_date) - new Date(b.published_date))


  var [renderSelectedMag, setRenderMag] = useState([])
  const [selectedMag, setSelectedMag] = useState([])

  var listMain = []

  const handleTagChange = (event, isChecked) => {
    console.log(isChecked, event.target.name)
    setMagCheck({...magCheck, [event.target.name]: event.target.checked})
    if(isChecked === true) {
      setRenderMag(renderSelectedMag => [...renderSelectedMag, event.target.name])
      setTimeout(() => {
        console.log('true')
        listMain = renderSelectedMag
        console.log(listMain)
      }, 1000)
    }
    if(isChecked === false) {
      var index = renderSelectedMag.filter(mag => mag !== event.target.name)
      setRenderMag(index)
      setTimeout(() => {
        console.log('inside',false)
        listMain = renderSelectedMag
        console.log(listMain)
      }, 1000)
    }
    searchMags(renderSelectedMag)
  }

  const searchMags = (tags) => {
    fetch(`${url}/magazines/view/?filters={"type":"operator","data":{"attribute":"id","operator":"in","value":${JSON.stringify(tags)}}}`)
    .then((res) => res.json())
    .then((result) => setSelectedMag(result.results))
  }

  const [bookmarks, setBookmark] = useState();
  const handleBookMark = (event) => {
    if(event.target.checked){
      console.log(event.target.value)
    }
  }

  
  if(datePick){
    sortedMagData = mags.filter(mag => mag.published_date === datePick)
  } else {
    sortedMagData = [...mags]
  }

  const renderMagazines = (renderSelectedMag.length < 1 ? sortedMagData : selectedMag).map(mag => {
    return  <>
    <div className="sub_magazines2">
      <Link className="newThumb-right" to={`/user/magazines/${mag.id}`}>
      <ReactBootStrap.Image
        className="magazines_img"
        src={mag.cover_image_upload || mag.cover_image}
      />
      </Link>
      <div className="magazines_info ">
        <Card className="magazines_card">
        <Card.Content>
          <Card.Header>
          <p className="date">{moment(mag.published_date).format("MMMM Do YYYY")}</p>
          <Link to={`/user/magazines/${mag.id}`}>
          <h6 className="card_title char-cut-line-1">{mag.name}</h6>
          </Link>
          </Card.Header>

          <Card.Description>
          <div class="box">
            <p className="mb-10" dangerouslySetInnerHTML={createMarkup(mag.short_description)}>
            </p>
            {/* <Popup
              trigger={<span style={{color:'#EA3592',marginBottom:'10px',cursor: 'pointer'}}>Read More</span>}
              position="right center"
              on="click"
              flowing
              style={{height:'auto'}}
            >
            <p style={{width:"500px",wordBreak:"break-all"}}>
              {mag.short_description || 'Description'}
            </p>
            </Popup> */}
          </div>
          </Card.Description>
        </Card.Content>

        <Card.Content extra >
          <span style={{float:"left", paddingTop: '8px'}}>
            <a href="#" className="download mr-20" download="magazine.pdf">
            Download
          </a>
          {/* <Link to={`/user/magazines/${mag.id}`} className="download">
            View
          </Link> */}
          <Link to={ mag.attachment ||`magazine.pdf`} target="_blank" className="download">
            View
          </Link>
          </span>
          <span className="socialActionBlock" style={{float: "right"}}>
            <Checkbox icon= {<BookmarkBorderIcon />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={mag.id}/>
            {/* &#xa0;&#x0358;&#xa0; */}
            <Popup
              trigger={
                <IconButton><ShareIcon /></IconButton>
              }
              position="left center"
              on="click"
              flowing
              style={{height:'auto'}}
            >
              <div style={{width:"450px",wordBreak:"break-all"}}>
              <label>Share On</label><br/>
              <div className="row pl-3">
                <Link className="socialIcon socialLinkedin mr-1"><FiLinkedin /></Link>
                <Link className="socialIcon socialTwitter mr-1"><FiTwitter /></Link>
                <Link className="socialIcon socialFacebook mr-1"><FiFacebook /></Link>
              </div>
              <Divider />
              <label>Share With</label><br/>
              <Label as='a'>
                <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
                Elliot@gmail.com
                <Icon name='delete' />
              </Label>
              <Label as='a'>
                <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
                Stevie@gmail.com
                <Icon name='delete' />
              </Label>
              
              <Form className="mt-2">
                <Form.Field>
                  <input placeholder='Enter email id' />
                </Form.Field>
                <Form.Field>
                <TextArea placeholder='Type your message here' />
                </Form.Field>
                <Form.Field>
                  <SUIButton type='submit' color="pink">Submit</SUIButton>
                  <SUIButton type='submit' basic>Cancel</SUIButton>
                </Form.Field>
              </Form>
              </div>
            </Popup> 
          </span>
        </Card.Content>
      </Card>
      </div>
    </div>
    </>
  });

  //Render tags
  
var options = [];
var [selectOption, setSelectOption] = useState([]);
const [magCheck, setMagCheck] = useState([])
const [selectedSearchBar, setSearchBar] = useState(false);
const onSearch = (searchText) => {
  const fuse = new Fuse(mags, {keys : ['published_date', 'name']});

  var results = fuse.search(searchText);
  results.length > 0 ? setSearchBar(true) : setSearchBar(false)

  const characterResults = results.map(searchedOption => searchedOption.item)
  setSelectOption(characterResults);
};


const renderSearchedMag = selectOption.map(mag => {
  return <>
  <div className="acordian_drop">
    <FormControlLabel
      control={<Checkbox checked={magCheck[mag.id]} onChange={handleTagChange} name={mag.id} />}
      label={<span className="char-cut-line-1 ">{mag.name}</span>}
    />
  </div>
  </>
})

const renderDefaultMag = (selectedSearchBar?selectOption:mags).map(mag => {
  let magId = mag.id
  return <>
  <div className="acordian_drop">
    <FormControlLabel
      control={<Checkbox checked={magCheck.magId} onChange={handleTagChange} name={mag.id} />}
      label={<span className="char-cut-line-1 ">{mag.name}</span>}
    />
  </div>                      
  </>
})

  
  //Main Page
  return (
    <div className="page newMainBlock">
      <Drawer
        variant="persistent"
        anchor="left"
        open={magDrawer}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className="acordian">
        <div>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
            <MenuIcon />
          </IconButton>
        </div>
        <div className="acordian_header">
          <div className="acordian_search">
            <FiSearch className="acordian_search_icon" />
            <br />

            <AutoComplete
              options={selectOption}
              style={{
                width: 200,
              }}
              onSearch={onSearch}
              placeholder="Search here"
              open={false}
              >
              <input
                type="text"
                aria-label="Text input with dropdown button"
                // placeholder="search here"
                className="acordian_ip"
              />
            </AutoComplete>
            </div>
            </div>

            {/***************acordian***************/}
            {/* <div className="acordian_content">
              <Item.Group relaxed='very' divided>
              <div className="wrap" onClick={() => toggle('magazines')}>
                <h6>Magazines</h6>
                <span>
                    {clicked === 'magazines' ? (<AiOutlineMinus className="acordian_icon" />) : 
                    (<AiOutlinePlus className="acordian_icon" />)}
                </span>
              </div>
                {renderSearchedMagazines}
              </Item.Group>
            </div> */}
            <div className="acordian_content">
              <div className="wrap" onClick={() => toggle(1)} >
                <h6>Magazines</h6>
                <span>
                  {clicked === 1 ? (
                    <AiOutlineMinus className="acordian_icon" />
                  ) : (
                    <AiOutlinePlus className="acordian_icon" />
                  )}
                </span>
              </div>
              {clicked === 1 ? (
                <div className="checking">
                  {renderDefaultMag}
                </div>
              ) : null}
              <div className="wrap" onClick={() => toggle(2)}>
                <h6>Published Date</h6>
                <span>
                  {clicked === 2 ? (
                    <AiOutlineMinus className="acordian_icon" />
                  ) : (
                    <AiOutlinePlus className="acordian_icon" />
                  )}
                </span>
              </div>
              {clicked === 2 ? (
                <div className="checking">
                  <div className="acordian_drop">
                    <DatePicker name="date_of_publication"
                        onChange={(e) => {
                          if(e)setDatePickValue(e.format("YYYY-MM-DD"));
                          else setDatePickValue("");
                        }} className="gx-mb-3 gx-w-100"
                    />
                  </div>
                </div>
              ) : null}
            </div>
        </div>
      </Drawer>
      {/* Mid Magazines */}
      <div className="container">
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: magDrawer
        })}
      >
      <div className={classes.drawerHeader} />

      <div className="news">
        <div className="news_header">
          <div className="news_left_header">
            {magDrawer || (<IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
            >
              <MenuIcon />
              {/* <ChevronRightIcon /> */}
            </IconButton>)}
            <h4 className="main-header">Magazines</h4>
          </div>
          <div className="news_right_header">
          <ButtonGroup size="large" variant="outlined" color="secondary" aria-label="large outlined button group" className="" disabled={loading}>
              <Button startIcon={<Icon name='sort content descending' size='large'/>}  onClick={handleDescChange} ></Button>
              <Button startIcon={<Icon name='sort content ascending' size='large'/>} onClick={handleAscChange} ></Button>
          </ButtonGroup>
          </div>
        </div>
        <Divider />
        {/* Magazines list */}
        <Spin spinning={loading}>
        <div className="magazines">
          {renderMagazines}
        </div>
        </Spin>
      </div>
      </main>
      </div>
    </div>
  )
};

export default Magazines;
