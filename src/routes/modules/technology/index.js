import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch, useRouteMatch } from 'react-router';
import { getAllTechnologies } from '../../../appRedux/actions/Technologies';
import ListDetail from '../../../components/ListView/ListDetail';
import Technology from '../../../containers/Technology';
import CreateTechnology from '../../../containers/Technology/CreateTechnology';

const TechnologyRoutes = () => {

    const match = useRouteMatch();
    const dispatch = useDispatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={Technology} />
            <Route exact={true} path={`${match.url}/create`} component={CreateTechnology} />
            <Route exact={true} path={`${match.url}/:id`} component={ListDetail} />
            <Route exact={true} path={`${match.url}/update/:id`} component={CreateTechnology} />
        </Switch>
    )
}

export default TechnologyRoutes
