import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import Auth from "./Auth";
import Common from "./Common";
import Settings from './Settings';
import Companies from "./Companies";
import Technology from './Technology';
import Config from './Config';
import News from './News';
import Magazine from './Magazine';
import Event from './Event';
import RawMaterials from './RawMaterial';
import Products from './Products';
import Services from './Services';
import DealsAndInvestments from './DealsAndInvestments';
import Investors from './Investors';
import Research from './Research';
import Regulatory from './Regulatory';
import MarketReports from './MarketReports';
import ScheduleDemo from './ScheduleDemo';

const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  auth: Auth,
  common: Common,
  settings: Settings,
  companies: Companies,
  technologies: Technology,
  config: Config,
  news: News,
  magazines: Magazine,
  events: Event,
  rawMaterials: RawMaterials,
  products: Products,
  services: Services,
  deals: DealsAndInvestments,
  investors: Investors,
  researches: Research,
  regulatories: Regulatory,
  marketReports: MarketReports,
  scheduleDemo: ScheduleDemo
});

export default createRootReducer
