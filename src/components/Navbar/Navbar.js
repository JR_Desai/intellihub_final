import React from "react";
import { FiSearch } from "react-icons/fi";
import { IoIosNotificationsOutline } from "react-icons/io";
import * as ReactBootStrap from "react-bootstrap";
import { Link } from "react-router-dom";
import { Dropdown, Menu } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import logo from "images/intellihub-logo.png";

const Navbar = () => {
  const url = "https://1c78ba753881.ngrok.io";

  const dontClose = (e) => {
    e.stopPropagation();
  };

  return (
    <>
      <ReactBootStrap.Navbar
        collapseOnSelect
        expand="xl"
        variant="light"
        fixed="top"
        className="navbar"
      >
        {" "}
        <div className="sub_navbar1">
          <ReactBootStrap.Navbar.Brand href="/" className="brand">
            <ReactBootStrap.Image
                src={logo}
                className="main-logo"
            />
          </ReactBootStrap.Navbar.Brand>
          <div className="center_nav">
            <Dropdown text="Category" className="cato-btn">
              <Dropdown.Menu>
                <div className="category">
                  <div className="category-1">
                    <Dropdown.Item>
                      {" "}
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">
                          Latest
                        </span>
                      </div>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      {" "}
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">Products</span>
                      </div>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      {" "}
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">Companies</span>
                      </div>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      {" "}
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">Technology</span>
                      </div>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">
                          Deals
                        </span>
                      </div>{" "}
                    </Dropdown.Item>
                    <Dropdown.Item>
                      {" "}
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">Magazines</span>
                      </div>{" "}
                    </Dropdown.Item>
                  </div>
                  <div className="category-1">
                    <Dropdown.Item>
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">
                          Research & Literature
                        </span>
                      </div>{" "}
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">
                          Regulatory & Safety
                        </span>
                      </div>{" "}
                    </Dropdown.Item>
                    <Dropdown.Item>
                      {" "}
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">Reports</span>
                      </div>{" "}
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">Archive</span>
                      </div>{" "}
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">Newsletters</span>
                      </div>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      {" "}
                      <div className="category_check">
                        <input
                          type="checkbox"
                          className="checkbox"
                          onClick={dontClose}
                        />
                        <span className="checkbox_title">
                          Events &#38; Conferences
                        </span>
                      </div>
                    </Dropdown.Item>
                  </div>
                </div>
              </Dropdown.Menu>
            </Dropdown>

            <div className="search_box">
              <input
                type="text"
                aria-label="Text input with dropdown button"
                placeholder="search here"
                className="ip"
              />

              <button className="ip-btn">
                <FiSearch className="search" />
              </button>
            </div>
          </div>
          <div className="right_side">
            <div className="right">
              <ReactBootStrap.NavDropdown
                title={<IoIosNotificationsOutline className="bell" />}
                // id="basic-nav-dropdown"
                id="bell-dropdown"
                style={{ marginRight: "0.4rem" }}
              >
                <div className="notification">
                  <h6 className="notification_title">Notifications</h6>
                  <div className="notification_content">
                    <Link to="#" className="noti-link">
                      <div className="sub_notification">
                        <ReactBootStrap.Image
                          src="https://cdn.pixabay.com/photo/2018/03/22/02/37/email-3249062__340.png"
                          className="notification_img"
                        />
                        <div className="sub_notification_content">
                          <h6 className="sub_notification_title">
                            New Documentry Details WSDA's Hunt for the Asian
                            Giant Hornet
                          </h6>
                          <div className="notification_date">
                            <p>TECHNOLOGY</p>
                            <p>28 JAN,2021</p>
                          </div>
                        </div>
                      </div>
                    </Link>
                    <div className="sub_notification">
                      <ReactBootStrap.Image
                        src="https://cdn.pixabay.com/photo/2018/03/22/02/37/email-3249062__340.png"
                        className="notification_img"
                      />
                      <div className="sub_notification_content">
                        <h6 className="sub_notification_title">
                          New Documentry Details WSDA's Hunt for the Asian Giant
                          Hornet
                        </h6>
                        <div className="notification_date">
                          <p>TECHNOLOGY</p>
                          <p>28 JAN,2021</p>
                        </div>
                      </div>
                    </div>
                    <div className="sub_notification">
                      <ReactBootStrap.Image
                        src="https://cdn.pixabay.com/photo/2018/03/22/02/37/email-3249062__340.png"
                        className="notification_img"
                      />
                      <div className="sub_notification_content">
                        <h6 className="sub_notification_title">
                          New Documentry Details WSDA's Hunt for the Asian Giant
                          Hornet
                        </h6>
                        <div className="notification_date">
                          <p>TECHNOLOGY</p>
                          <p>28 JAN,2021</p>
                        </div>
                      </div>
                    </div>
                    <div className="sub_notification">
                      <ReactBootStrap.Image
                        src="https://cdn.pixabay.com/photo/2018/03/22/02/37/email-3249062__340.png"
                        className="notification_img"
                      />
                      <div className="sub_notification_content">
                        <h6 className="sub_notification_title">
                          New Documentry Details WSDA's Hunt for the Asian Giant
                          Hornet
                        </h6>
                        <div className="notification_date">
                          <p>TECHNOLOGY</p>
                          <p>28 JAN,2021</p>
                        </div>
                      </div>
                    </div>
                    <div className="sub_notification">
                      <ReactBootStrap.Image
                        src="https://cdn.pixabay.com/photo/2018/03/22/02/37/email-3249062__340.png"
                        className="notification_img"
                      />
                      <div className="sub_notification_content">
                        <h6 className="sub_notification_title">
                          New Documentry Details WSDA's Hunt for the Asian Giant
                          Hornet
                        </h6>
                        <div className="notification_date">
                          <p>TECHNOLOGY</p>
                          <p>28 JAN,2021</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* <ReactBootStrap.NavDropdown.Item href="#">
                  Action
                </ReactBootStrap.NavDropdown.Item> */}
              </ReactBootStrap.NavDropdown>
            </div>

            <div className="user">
              <img
                src="https://cdn.pixabay.com/photo/2016/11/21/12/42/beard-1845166__340.jpg"
                className="avatar"
                alt="avatar"
              />
              <Dropdown
                simple
                text="karan"
                style={{ marginLeft: "1rem", color: "#415A6C" }}
              >
                <Dropdown.Menu>
                  <Dropdown.Item text="Your Profile" />
                  <Dropdown.Item text="Analyst Support" />
                  <Dropdown.Item text="Bookmarks" />
                  <Dropdown.Item text="Archive" />
                  <Dropdown.Item text="Notification Settings" />
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
        </div>
        <div className="nav-line"></div>
        <div className="sub_nav2">
          <ReactBootStrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <ReactBootStrap.Navbar.Collapse id="responsive-navbar-nav">
            <ReactBootStrap.Nav className="sub_nav">
              <Dropdown
                text="Latest"
                id="basic-nav-dropdown"
                className="nav-drop"
                style={{ width: "90%", display: "flex", alignItems: "center" }}
              >
                <Dropdown.Menu>
                  <Link to="/news" className="top_menu">
                    <Dropdown.Item href="/news" className="sub_menu">
                      <span className="submenu">
                        <ReactBootStrap.Image
                          src="https://cdn.pixabay.com/photo/2017/01/18/08/25/social-media-1989152__340.jpg"
                          className="avatar2"
                        />{" "}
                        <p>News</p>
                      </span>
                    </Dropdown.Item>
                  </Link>
                  <Link to="/magazines" style={{ textDecoration: "none" }}>
                    <Dropdown.Item href="/magazines" className="sub_menu">
                      <span className="submenu">
                        <ReactBootStrap.Image
                          src="https://cdn.pixabay.com/photo/2015/06/11/17/52/magazine-806073__340.jpg"
                          className="avatar2"
                        />{" "}
                        <p>Magazines</p>
                      </span>
                    </Dropdown.Item>
                  </Link>
                  <Link
                    to="/events-conferences"
                    style={{ textDecoration: "none" }}
                  >
                    <Dropdown.Item
                      href="/events-conferences"
                      className="sub_menu"
                    >
                      <span className="submenu">
                        <ReactBootStrap.Image
                          src="https://cdn.pixabay.com/photo/2015/05/15/14/22/conference-room-768441__340.jpg"
                          className="avatar2"
                        />{" "}
                        <p>Events & Conferences</p>
                      </span>
                    </Dropdown.Item>
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
              <Dropdown
                text="Products"
                id="basic-nav-dropdown"
                className="nav-drop"
                style={{ width: "55%", display: "flex", alignItems: "center" }}
              >
                <Dropdown.Menu>
                  <Link to="/products" style={{ textDecoration: "none" }}>
                    <Dropdown.Item href="/products" className="sub_menu">
                      <span className="submenu">
                        <ReactBootStrap.Image
                          src="https://cdn.pixabay.com/photo/2014/05/02/21/50/laptop-336378__340.jpg"
                          className="avatar2"
                        />{" "}
                        <p>Products</p>
                      </span>
                    </Dropdown.Item>
                  </Link>
                  <Link to="/services" style={{ textDecoration: "none" }}>
                    <Dropdown.Item href="/services" className="sub_menu">
                      <span className="submenu">
                        <ReactBootStrap.Image
                          src="https://cdn.pixabay.com/photo/2015/05/15/14/22/conference-room-768441__340.jpg"
                          className="avatar2"
                        />{" "}
                        <p>Services</p>
                      </span>
                    </Dropdown.Item>
                  </Link>
                  <Link to="/rawmaterials" style={{ textDecoration: "none" }}>
                    <Dropdown.Item href="/rawmaterials" className="sub_menu">
                      <span className="submenu">
                        <ReactBootStrap.Image
                          src="https://cdn.pixabay.com/photo/2015/05/15/14/22/conference-room-768441__340.jpg"
                          className="avatar2"
                        />{" "}
                        <p>Raw Materials</p>
                      </span>
                    </Dropdown.Item>
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
              <Link to="/headquarters" className="diff_submenu">
                Companies
              </Link>
              <Link to="/technology" className="diff_submenu">
                Technology
              </Link>
              <Dropdown
                text="Deals"
                textDecoration="none"
                id="basic-nav-dropdown"
                className="nav-drop"
                style={{
                  width: "90%",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <Dropdown.Menu>
                  <Link to="/all-investors" style={{ textDecoration: "none" }}>
                    <Dropdown.Item href="/all-investors" className="sub_menu">
                      <span className="submenu">
                        <ReactBootStrap.Image
                          src="https://cdn.pixabay.com/photo/2014/05/02/21/50/laptop-336378__340.jpg"
                          className="avatar2"
                        />{" "}
                        <p>Investors</p>
                      </span>
                    </Dropdown.Item>
                  </Link>
                  <Link
                    to="/deals-and-investment"
                    style={{ textDecoration: "none" }}
                  >
                    <Dropdown.Item
                      href="/deals-and-investment"
                      className="sub_menu"
                    >
                      <span className="submenu">
                        <ReactBootStrap.Image
                          src="https://cdn.pixabay.com/photo/2014/05/02/21/50/laptop-336378__340.jpg"
                          className="avatar2"
                        />{" "}
                        <p>Deals &#38; Investment</p>
                      </span>
                    </Dropdown.Item>
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
              <Link to="/research-literature" className="diff_submenu">
                Research
              </Link>
              <Link to="/regulatory-safety" className="diff_submenu">
                Regulatory
              </Link>
              <Link to="/market-reports" className="diff_submenu">
                Reports
              </Link>
            </ReactBootStrap.Nav>
          </ReactBootStrap.Navbar.Collapse>
        </div>
      </ReactBootStrap.Navbar>
    </>
  );
};

export default Navbar;
