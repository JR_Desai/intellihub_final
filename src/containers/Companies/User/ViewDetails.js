import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom"
import { useParams } from 'react-router-dom'
import * as ReactBootStrap from "react-bootstrap"
import { makeStyles } from '@material-ui/core/styles';
import { Paper ,Tabs ,Tab, Typography, Box, Checkbox, IconButton } from '@material-ui/core';
import moment from "moment";
import {
  Share as ShareIcon,
  BookmarkBorder,
  Bookmark,
  TableChartOutlined as TableChart,
  ShoppingCartOutlined,
  LocalAtmOutlined,
  TrendingUpOutlined,
  PeopleAltOutlined,
}
   from '@material-ui/icons'; 
import { Card , Row, Col, Avatar, Image, Tabs as ANTTabs, Descriptions } from 'antd';
import { LinkedinFilled } from '@ant-design/icons'
import 'antd/dist/antd.css';
import './Company.css'
import { Popup, Button as SUIButton, Divider, Label, Icon, Form, TextArea } from 'semantic-ui-react'
import { FiFacebook, FiLinkedin, FiTwitter } from "react-icons/fi";

const { TabPane } = ANTTabs;

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});
const gridStyle = {
  width: '33%',
  textAlign: 'left',
  boxShadow: 'none'
};
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const CompanyDetails = () => {
  const url = "http://3.108.46.195:9000";
 
  const classes = useStyles();
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const { companyId } = useParams()
  const [ companyDetail, setCompanyDetail ] = useState([])
  const [ companyNews, setCompanyNews ] = useState([]);
  const [ companyPeople, setCompanyPeople ] = useState([]);
  const [bookmarks, setBookmark] = useState();
  
  const handleBookMark = (event) => { 
    console.log(event.target.checked)
    if(event.target.checked){
      console.log(event.target.value)
    }
  }
  
  useEffect(() => {  
    const fetchMag = () => {
      fetch(`${url}/company/view/?id=${companyId}`)
      .then((res) => res.json())
      .then((result) => {
        
        var [ news ] = result.results.map(data => data.news)

        fetch(`${url}/news/view/?filters={"type":"operator","data":{"attribute":"id","operator":"in","value":${JSON.stringify(news)}}}`)
        .then((res) => res.json())
        .then((result) => setCompanyNews(result.results))
        
        fetch(`${url}/person/view/?filters={"type":"operator","data":{"attribute":"company","operator":"=","value":"${companyId}"}}`)
        .then((res) => res.json())
        .then((result) => setCompanyPeople(result))
        
        return result.results
      })
      .then((result) => setCompanyDetail(result))
    }

    fetchMag()
  }, [])

  console.log('company-detail',companyDetail, 'company-news', companyNews, 'company-people', companyPeople);
  
  const renderCompanyDetails = companyDetail.map(company => {
      return <div className="row">
        <div className="col-md-12">
          <div className="magazine_left">
            <Link to='/user/companies'>
              Companies
            </Link>
            <Icon name='angle right' />
            {company.name}
          </div>
          <Divider hidden />
        </div>

        <div className="col-md-12">
          <div className="companyBasicInfoContainer">
            <div className="companyBasicInfoImage">
              {/* <h6>
                <Link to="/user/companies">Companies </Link>{">"} {company.name}
              </h6> */}
              <div className="maga company-detail-header-image">
                <ReactBootStrap.Image
                  src={company.image_link}
                />
              </div>
            </div>
            <div className="companyBasicInfoDetails">
              <div className="maga-info">
                <div className="company-header">
                  <h4>{company.name}</h4>
                  <div className="companySocialAction">
                    <div className="info2">
                      <p className="more">
                        <Checkbox icon= {<BookmarkBorder />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={company.id}/>
                      </p>
                      <p className="more">
                        <Popup
                          trigger={
                            <IconButton><ShareIcon /></IconButton>
                          }
                          position="left center"
                          on="click"
                          flowing
                          style={{height:'auto'}}
                        >
                          <div style={{width:"450px",wordBreak:"break-all"}}>
                          <label>Share On</label><br/>
                          <div className="row pl-3">
                            <Link className="socialIcon socialLinkedin mr-1"><FiLinkedin /></Link>
                            <Link className="socialIcon socialTwitter mr-1"><FiTwitter /></Link>
                            <Link className="socialIcon socialFacebook mr-1"><FiFacebook /></Link>
                          </div>
                          <Divider />
                          <label>Share With</label><br/>
                          <Label as='a'>
                            <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
                            Elliot@gmail.com
                            <Icon name='delete' />
                          </Label>
                          <Label as='a'>
                            <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
                            Stevie@gmail.com
                            <Icon name='delete' />
                          </Label>
                          
                          <Form className="mt-2">
                            <Form.Field>
                              <input placeholder='Enter email id' />
                            </Form.Field>
                            <Form.Field>
                            <TextArea placeholder='Type your message here' />
                            </Form.Field>
                            <Form.Field>
                              <SUIButton type='submit' color="pink">Submit</SUIButton>
                              <SUIButton type='submit' basic>Cancel</SUIButton>
                            </Form.Field>
                          </Form>
                          </div>
                        </Popup> 
                      </p>
                    </div>
                  </div>
                </div>

                <p className="company-meta">
                  {company.description}
                </p>
              </div>
              <div className="companyEntryInfo">
                  <ul>
                    <li>
                      <p className="companyEntryInfoTitle">Established</p>
                      <h6 className="companyEntryInfoDetails">{moment(company.year_of_incorporation).format('YYYY')}</h6>
                    </li>
                    <li>
                      <p className="companyEntryInfoTitle">Location</p>
                      <h6 className="companyEntryInfoDetails">{company.hq}</h6>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    })
  
  const renderKeymatrix = companyDetail.map(data => {
    return <>
  <Box
  borderBottom={1}
  style={{borderBottomWidth:"3px", borderColor:"#fd0090", width:"17%"}}
  >
    <Typography variant="h4" color="secondary" className="p-4">
    Keymatrix
    </Typography>
  </Box>
  <Card.Grid style={gridStyle}>
    <Row>
      <Col span={5}>
        <Typography children={<TableChart style={{fontSize:"50px"}} color="secondary" />}/>
      </Col>
      <Col span={19}>
        <Typography variant="subtitle1">Year Of Incorporation</Typography>
        <Typography variant="h5">{data.year_of_incorporation}</Typography>
      </Col>
    </Row>
  </Card.Grid>
  <Card.Grid style={gridStyle}>
    <Row>
      <Col span={5}>
        <Typography children={<ShoppingCartOutlined style={{fontSize:"50px"}} color="secondary" />}/>
      </Col>
      <Col span={19}>
        <Typography variant="subtitle1">Market</Typography>
        <Typography variant="h5">{data.market}</Typography>
      </Col>
    </Row>
  </Card.Grid>
  <Card.Grid style={gridStyle}>
    <Row>
      <Col span={5}>
        <Typography children={<LocalAtmOutlined style={{fontSize:"50px"}} color="secondary" />}/>
      </Col>
      <Col span={19}>
        <Typography variant="subtitle1">Funding</Typography>
        <Typography variant="h5">{data.funding}</Typography>
      </Col>
    </Row>
  </Card.Grid>
  <Card.Grid style={gridStyle}>
    <Row>
      <Col span={5}>
        <Typography children={<TrendingUpOutlined style={{fontSize:"50px"}} color="secondary" />}/>
      </Col>
      <Col span={19}>
        <Typography variant="subtitle1">Valuation</Typography>
        <Typography variant="h5">{data.valuation}</Typography>
      </Col>
    </Row>
  </Card.Grid>
  <Card.Grid style={gridStyle}>
    <Row>
      <Col span={5}>
        <Typography children={<PeopleAltOutlined style={{fontSize:"50px"}} color="secondary" />}/>
      </Col>
      <Col span={19}>
        <Typography variant="subtitle1">No. of employees</Typography>
        <Typography variant="h5">{data.no_of_employees_max}</Typography>
      </Col>
    </Row>
  </Card.Grid>
    </>
  })

  const renderRecentNews = companyNews.map(news => {
    return <>
    <div className="sub-cards recentNewsCards">
      <div className="card-image-block">
    <Link to={`/user/news/${news.id}`}>
    <ReactBootStrap.Image
      src={news.image_link}
      className="card-img"
    />
    </Link>
    </div>
    <div className="below-news-card">
      <div className="recentNewsDate"><span className="date">06/08/2021</span></div>
        <Link to={`/user/news/${news.id}`}>
        <h6 className="news-header-right">
          {news.headline}
        </h6>
        </Link>
        <div className="box">
          <p className="char-cut-line-1">
          {news.short_description}
          </p>
          <span>
          <h6>
          <Link to={`/user/news/${news.id}`}>
          Read Full Article
          </Link>
          </h6>
          </span>
        </div>
    </div>
  </div>
  </>
  })

  const renderCompanyPeople = companyPeople.map(people => {
    return <>
        <Col span={12}>
          <div className="peopleCardWrap">
            <Card bordered={true}>
              <div className="peopleCard">
                <Row>
                  <Avatar size={80} src={<Image src={people.image_link} />} />
                  <div className="maga-info ml-3">
                    <div className="peopleCardUserInfo">
                      <div className="company-header"><h4>{people.name}</h4></div>
                      <p className="company-meta">
                        {people.designation.name}
                      </p>
                    </div>
                    <div className="maga-info">
                      <span>
                        <LinkedinFilled style={{color:"#0e76a8", fontSize:'24px',borderRadius: '50%', marginTop:"15px"}} shape={'circle'}/>
                      </span>
                    </div>
                  </div>
                </Row>
              </div>
            </Card>
          </div>
        </Col>
    </>
  })

  const renderCompanyFinance =  <>
      <ANTTabs tabPosition={'left'}>
      {companyDetail.map(company =>{
        return <>
        <TabPane tab="Funding" key="1">
        <div className="financeTableInfo">
          <Descriptions bordered title="Funding">
          <div className="companyOverviewTable">
            <div className="tableListView">
              <ul>
                <li>
                  <span className="tableListTitle">Estimated Revenue Range <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">INR 1Mn</span>
                </li>
                <li>
                  <span className="tableListTitle">Number of Founders <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">2</span>
                </li>
                <li>
                  <span className="tableListTitle">Founders <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">Friedrich Bayer, Johan Doe</span>
                </li>
                <li>
                  <span className="tableListTitle">Total Funding Amount <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">3000000000</span>
                </li>
                <li>
                  <span className="tableListTitle">Total Funding Amount Currency <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">EUR</span>
                </li>
                <li>
                  <span className="tableListTitle">Total Funding Amount (in USD) <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">3711777966</span>
                </li>
                <li>
                  <span className="tableListTitle">Top 5 Investors <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">Temasek Holdings</span>
                </li>
              </ul>
            </div>
          </div>

            {/* <Descriptions.Item label="Estimated Revenue Range" span={2}>{company.name || 'None'}</Descriptions.Item>
            <Descriptions.Item label="Number of Acquisitions" span={2}>{company.acquisition_count}</Descriptions.Item>
            <Descriptions.Item label="Number of Founders" span={2}>{company.alumni_founders}</Descriptions.Item>
            <Descriptions.Item label="Transaction Name" span={2}>{company.transaction || 'None'}</Descriptions.Item>
            <Descriptions.Item label="Founders" span={2}>None</Descriptions.Item>
            <Descriptions.Item label="Price" span={2}>None</Descriptions.Item>
            <Descriptions.Item label="Total Funding Amount" span={2}>None</Descriptions.Item>
            <Descriptions.Item label="Price Currency" span={2}>None</Descriptions.Item>
            <Descriptions.Item label="Total Funding Amount Currency" span={2}>None</Descriptions.Item>
            <Descriptions.Item label="Price Currency (in USD)" span={2}>None</Descriptions.Item>
            <Descriptions.Item label="Total Funding Amount (in USD)" span={2}>None</Descriptions.Item>
            <Descriptions.Item label="Top 5 Investors" span={2}>None</Descriptions.Item>
            <Descriptions.Item label="Number of Acquisitions" span={2}>None</Descriptions.Item> */}
          </Descriptions>
        </div>
        </TabPane>
        <TabPane tab="M&A" key="2">
        <div className="financeTableInfo">
          <Descriptions title="M&A" bordered>
            <Descriptions.Item label="Estimated Revenue: INR 1Mn" span={2}></Descriptions.Item>
            <Descriptions.Item label="Number Of Founders: 2" span={2}></Descriptions.Item>
            <Descriptions.Item label="Founders : " span={2}></Descriptions.Item>
            <Descriptions.Item label="Total Funding Amount: 300000000" span={2}></Descriptions.Item>
            <Descriptions.Item label="Total Funding Amount(USD): 319999" span={3}></Descriptions.Item>
            <Descriptions.Item label="No. Of acquisition : 4" span={2}></Descriptions.Item>
            <Descriptions.Item label="Price: None" span={2}></Descriptions.Item>
          </Descriptions>
        </div>
        </TabPane>
        <TabPane tab="Investors" key="3">
        <div className="financeTableInfo">
            <Descriptions title="Investment" bordered>
            <Descriptions.Item label="Estimated Revenue: INR 1Mn" span={3}></Descriptions.Item>
            <Descriptions.Item label="Number Of Founders: 2" span={3}></Descriptions.Item>
            <Descriptions.Item label="Founders : " span={3}></Descriptions.Item>
            <Descriptions.Item label="Total Funding Amount: 300000000" span={3}></Descriptions.Item>
            <Descriptions.Item label="Total Funding Amount(USD): 319999" span={3}></Descriptions.Item>
            <Descriptions.Item label="No. Of acquisition : 4" span={3}></Descriptions.Item>
            <Descriptions.Item label="Price: None" span={3}></Descriptions.Item>
            </Descriptions>
        </div>
        </TabPane>
        </>
      })}
      </ANTTabs>
    </>

    return (
    <div className="magazine-1 inner-wrapper companyDetailWrapper">
      <div className="container">
        {renderCompanyDetails}
      </div>
      <div className="companyDetailsTabHead">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <Paper className={classes.root}>
                <Tabs
                  value={value}
                  onChange={handleChange}
                  indicatorColor="secondary"
                  textColor="secondary"
                  variant="fullWidth"
                >
                  <Tab label="Overview" />
                  <Tab label="Recent News" />
                  <Tab label="People" />
                  <Tab label="Financials" />
                  <Tab label="Product Portfolio" />
                </Tabs>
              </Paper>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="companyTabContainer">
        <TabPanel value={value} index={0}>
        {companyDetail.map(company => { 
          return <>
          <h4 className="company-overview-tab">
            {company.description}
          </h4>
          </>
        })}
          {/* <Paper>
          <Card className="mt-4">
            {renderKeymatrix}
          </Card>
          </Paper> */}
          <div className="companyOverviewTable">
            <div className="tableListView">
              <ul>
                <li>
                  <span className="tableListTitle">Category <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">Pest</span>
                </li>
                <li>
                  <span className="tableListTitle">Sub-Category <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">Home Insecticides</span>
                </li>
                <li>
                  <span className="tableListTitle">Company Size <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">Startup</span>
                </li>
                <li>
                  <span className="tableListTitle">Number of Founders <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">2</span>
                </li>
                <li>
                  <span className="tableListTitle">Founders <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">Friedrich Bayer, Johan Doe</span>
                </li>
                <li>
                  <span className="tableListTitle">No of employees <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">50-100</span>
                </li>
              </ul>

              <ul>
                <li>
                  <span className="tableListTitle">Ownership Pattern <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">Ownership Pattern</span>
                </li>
                <li>
                  <span className="tableListTitle">Website <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">www.ecc.com</span>
                </li>
                <li>
                  <span className="tableListTitle">Email Id <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">hi@excelcorp.in</span>
                </li>
                <li>
                  <span className="tableListTitle">Linkedn  <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">https://www.linkedin.com/feed/</span>
                </li>
                <li>
                  <span className="tableListTitle">Phone No. <span className="colonTxt">:</span></span>
                  <span className="tableListInfo">011 264405211</span>
                </li>
              </ul>
            </div>
          </div>
        </TabPanel>

        <TabPanel value={value} index={1}>
          <div className="recentNewsBlock">
          <Card>
            {renderRecentNews}
          </Card>
          </div>
        </TabPanel>
        <TabPanel value={value} index={2}>
        <div className="site-card-wrapper">
          <div className="peopleNewsWrapper">
            <div className="companyTabTitle">
              <h2>People</h2>
            </div>
            <Row>
              {renderCompanyPeople}
            </Row>
          </div>
        </div>
        </TabPanel>
        <TabPanel value={value} index={3}>
          <div className="finacialVTabBlock">
            <div className="companyTabTitle">
              <h2>Financials</h2>
            </div>
            <div className="fundingVTabContainer">
              {renderCompanyFinance}
            </div>
          </div>
        </TabPanel>
        <TabPanel value={value} index={4}>
          <div className="product-portfolio-wrapper">
            <div className="companyTabTitle">
              <h2>Product Portfolio</h2>
              <a href="#" className="viewBroochuerLink">View Brochuer</a>
            </div>
            <div className="product-portfolio-list">
                <div className="product-portfolio-item">
                    <div className="product-portfolio-content">
                        <div className="product-portfolio-image">
                          <img src="https://cdn.coverstand.com/59756/690942/iphonejpg/320/2fd53ec016eeef7f5e7cf448fb3789c54d45f2d5.jpg" alt="" />
                        </div>
                        <div className="product-portfolio-information">
                            <h4>Sudocel</h4>
                            <h6>Excel care corp ltd. (India)</h6>
                            <div className="tableListView productPortfolioTable">
                                <ul>
                                  <li>
                                    <span className="tableListTitle">Category <span className="colonTxt">:</span></span>
                                    <span className="tableListInfo">Biopesticides</span>
                                  </li>
                                  <li>
                                    <span className="tableListTitle">Application Type <span className="colonTxt">:</span></span>
                                    <span className="tableListInfo">Powder</span>
                                  </li>
                                </ul>
                            </div>
                            <div class="product-portfolio-information-description">
                              <p>Ammonium Salt Of Glyphosate 71% SG , Hydrolate, Glycerin Technlogy Form</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="product-portfolio-item">
                    <div className="product-portfolio-content">
                        <div className="product-portfolio-image">
                          <img src="https://cdn.coverstand.com/59756/690942/iphonejpg/320/2fd53ec016eeef7f5e7cf448fb3789c54d45f2d5.jpg" alt="" />
                        </div>
                        <div className="product-portfolio-information">
                            <h4>Sudocel</h4>
                            <h6>Excel care corp ltd. (India)</h6>
                            <div className="tableListView productPortfolioTable">
                                <ul>
                                  <li>
                                    <span className="tableListTitle">Category <span className="colonTxt">:</span></span>
                                    <span className="tableListInfo">Biopesticides</span>
                                  </li>
                                  <li>
                                    <span className="tableListTitle">Application Type <span className="colonTxt">:</span></span>
                                    <span className="tableListInfo">Powder</span>
                                  </li>
                                </ul>
                            </div>
                            <div class="product-portfolio-information-description">
                              <p>Ammonium Salt Of Glyphosate 71% SG , Hydrolate, Glycerin Technlogy Form</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="product-portfolio-item">
                    <div className="product-portfolio-content">
                        <div className="product-portfolio-image">
                          <img src="https://cdn.coverstand.com/59756/690942/iphonejpg/320/2fd53ec016eeef7f5e7cf448fb3789c54d45f2d5.jpg" alt="" />
                        </div>
                        <div className="product-portfolio-information">
                            <h4>Sudocel</h4>
                            <h6>Excel care corp ltd. (India)</h6>
                            <div className="tableListView productPortfolioTable">
                                <ul>
                                  <li>
                                    <span className="tableListTitle">Category <span className="colonTxt">:</span></span>
                                    <span className="tableListInfo">Biopesticides</span>
                                  </li>
                                  <li>
                                    <span className="tableListTitle">Application Type <span className="colonTxt">:</span></span>
                                    <span className="tableListInfo">Powder</span>
                                  </li>
                                </ul>
                            </div>
                            <div class="product-portfolio-information-description">
                              <p>Ammonium Salt Of Glyphosate 71% SG , Hydrolate, Glycerin Technlogy Form</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="product-portfolio-item">
                    <div className="product-portfolio-content">
                        <div className="product-portfolio-image">
                          <img src="https://cdn.coverstand.com/59756/690942/iphonejpg/320/2fd53ec016eeef7f5e7cf448fb3789c54d45f2d5.jpg" alt="" />
                        </div>
                        <div className="product-portfolio-information">
                            <h4>Sudocel</h4>
                            <h6>Excel care corp ltd. (India)</h6>
                            <div className="tableListView productPortfolioTable">
                                <ul>
                                  <li>
                                    <span className="tableListTitle">Category <span className="colonTxt">:</span></span>
                                    <span className="tableListInfo">Biopesticides</span>
                                  </li>
                                  <li>
                                    <span className="tableListTitle">Application Type <span className="colonTxt">:</span></span>
                                    <span className="tableListInfo">Powder</span>
                                  </li>
                                </ul>
                            </div>
                            <div class="product-portfolio-information-description">
                              <p>Ammonium Salt Of Glyphosate 71% SG , Hydrolate, Glycerin Technlogy Form</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </TabPanel>
        </div>
      </div>
    </div>
    )
}

export default CompanyDetails
