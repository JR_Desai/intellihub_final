import React from "react";

const EventItem = ({image, name, desc, date }) => {

  return (
    <div className="gx-media gx-featured-item">
      <div className="gx-featured-thumb">
        <img className="gx-rounded-lg" src={image} alt="..."/>
      </div>
      <div className="gx-media-body gx-featured-content">
        <div className="gx-featured-content-left"> 
          <h3 className="gx-mb-2">{name}</h3>
          <div className="ant-row-flex">
            <div className={`gx-media gx-text-grey gx-mb-1`}>
                <span className="gx-media-body">{desc}</span>
            </div>
          </div>
        </div>
        <div className="gx-featured-content-right gx-profile-content-right">
          <h2 className=" gx-mb-1">
            <i className={`icon icon-calendar gx-fs-lg gx-mr-2 gx-d-inline-flex gx-vertical-align-middle`}/> <span
            className="gx-d-inline-flex gx-vertical-align-middle">{date}</span>
          </h2>
        </div>
      </div>
    </div>
  );
}

export default EventItem;
