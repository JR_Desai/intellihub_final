import { Form, Button, Card, Input } from 'antd';
import React from 'react';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';

const DynamicInputGroup = ({name, label, type, placeholder, dynamicObject, mainObject, setChange}) => {

const onFinish = values => {
    console.log('Received values of form:', values);
};

    return (
        <Form name="dynamic_form_item">
        <Form.List name="names">
            {(fields, { add, remove }) => {
            return (
                <div>
                {dynamicObject.map((field, index) => (
                    <div
                    style={{margin: "0 5px"}}
                    key={field.key}
                    >
                    <div className="gx-form-group">
                        <label htmlFor={name} className="gx-form-label">{ index === 0 ? label: ''}</label>
                        <Input placeholder={placeholder} value={dynamicObject[index]} name={name} type={type} onChange={(e) => setChange({...mainObject, dynamicObject: [...dynamicObject, dynamicObject[index]=e.target.value] })} />
                        <MinusCircleOutlined
                        className="dynamic-delete-button"
                        style={{ margin: '0 8px' }}
                        onClick={() => {
                            setChange({...mainObject, dynamicObject: dynamicObject.splice(index, 1) })}
                        }
                        />
                    </div>
                    </div>
                ))}
                <div className="gx-form-group">
                    <label className="gx-form-label"></label>
                        <Button
                        type="dashed"
                        className="gx-w-100"
                        onClick={() => {
                            setChange({...mainObject, dynamicObject: [...dynamicObject, dynamicObject.push("")] })}
                        }
                        >
                        <PlusOutlined /> Add {name}
                        </Button>
                    </div>
                </div>
            );
            }}
        </Form.List>
        </Form>
    )
}

export default DynamicInputGroup;
