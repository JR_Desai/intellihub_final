import { CREATE_DEALS_AND_INVESTMENT_SUCCESS, EDIT_DEALS_AND_INVESTMENT_DETAILS, GET_ALL_DEALS_AND_INVESTMENTS_SUCCESS, GET_DEALS_AND_INVESTMENT_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_DEALS_AND_INVESTMENT_SUCCESS, GET_ACQUISITION_STATUS_SUCCESS, GET_ACQUISITION_TYPE_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    deals: [],
    deal: {},
    acquisitionStatus: [],
    acquisitionType: [],
    showRemove: false,
    showEdit: false
}

export default function dealsAndInvestmentReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_DEALS_AND_INVESTMENT_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_DEALS_AND_INVESTMENTS_SUCCESS: {
            return {
                ...state,
                loader: false,
                deals: action.payload
            }
        }
        case GET_ACQUISITION_STATUS_SUCCESS: {
            return {
                ...state,
                loader: false,
                acquisitionStatus: action.payload
            }
        }
        case GET_ACQUISITION_TYPE_SUCCESS: {
            return {
                ...state,
                loader: false,
                acquisitionType: action.payload
            }
        }
        case GET_DEALS_AND_INVESTMENT_SUCCESS: {
            return {
                ...state,
                loader: false,
                deal: action.payload[0]
            }
        }
        case CREATE_DEALS_AND_INVESTMENT_SUCCESS: {
            return {
                ...state,
                loader: false,
                deal: action.payload
            }
        }
        case UPDATE_DEALS_AND_INVESTMENT_SUCCESS: {
            return {
                ...state,
                loader: false,
                deal: action.payload
            }
        }
        default:
            return state;
    }
}
