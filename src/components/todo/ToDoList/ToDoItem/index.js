import React from "react";
import {Avatar, Badge, Button, Checkbox} from "antd";

import labels from "../../../../containers/Todo/data/labels";
import users from "../../../../containers/Todo/data/users";
import Search from "antd/lib/input/Search";


const ToDoItem = (({todo, onTodoSelect, onTodoChecked, onMarkAsStart}) => {
  let user = null;
  if (todo.user > 0)
    user = users[todo.user - 1];
  return (
    <div className="gx-module-list-item">
      <div className="gx-module-list-icon">
        <Checkbox color="primary"
                  checked={todo.selected}
                  onClick={(event) => {
                    event.stopPropagation();
                    onTodoChecked(todo);
                  }}
                  value="SelectTodo"
                  className="gx-icon-btn"
        />

        <div onClick={() => {
          todo.starred = !todo.starred;
          onMarkAsStart(todo);
        }}>
          {todo.starred ?
            <i className="gx-icon-btn icon icon-star"/> :
            <i className="gx-icon-btn icon icon-star-o"/>
          }
        </div>
      </div>
      <div className="gx-module-list-info" onClick={() => {
        onTodoSelect(todo);
      }}>
        
        <div className="gx-module-todo-content">
          <div className={`gx-subject`}>
            {todo.title}
          </div>
          <div>
            {labels.map((label, index) => {
              return (todo.labels).includes(label.id) &&
                <Badge key={index} count={label.title} style={{backgroundColor: "#FD0090", marginRight: "5px"}}/>
            })}
          </div>
        </div>
          <Button type="default" className="gx-border-green gx-text-green" onClick="" >Approved</Button>
          <Button type="default" className="gx-border-red gx-text-red" onClick="" >Remove</Button>
      </div>
    </div>

  )
});

export default ToDoItem;
