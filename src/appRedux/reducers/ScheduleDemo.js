import { SCHEDULE_DEMO } from '../../constants/ActionTypes';

const INIT_STATE = {
	loader: false
};

export default function demoReducer(state = INIT_STATE, action) {
    switch (action.type) {
    	case SCHEDULE_DEMO: {
            return {
                ...state,
                loader: false,
                research: action.payload
            }
        }
        default:
            return state;
 	}
}