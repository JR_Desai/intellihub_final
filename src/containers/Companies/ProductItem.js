import React from "react";
import {Button} from "antd";
import { Link } from "react-router-dom";

const ProductItem = ({ item }) => {

  return (
    <Link to={`/companies/${item.id}`} className="gx-product-item gx-product-horizontal">
      <div className="gx-product-image">
        <img
          src={item.image_link} alt={item.name}
        />
      </div>

      <div className="gx-product-body">
        <div className="gx-product-name gx-mb-2 gx-text-black">
          {item.name}
        </div>
        <div className="gx-mb-3 gx-text-grey">
          {item.year_of_incorporation}
        </div>
        <div className="gx-product-price gx-pb-2 gx-text-black">$ {item.business_revenue_mix}</div>
      </div>
    </Link>
  );
};

export default ProductItem;
