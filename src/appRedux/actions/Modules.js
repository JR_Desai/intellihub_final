import axios from 'axios';
import { GET_ALL_MODULES } from '../../constants/ActionTypes';

export const getAllModules = async (dispatch) => {
    try {
        const modules = await axios.get("/module/view/");
        dispatch ({
            type: GET_ALL_MODULES,
            payload: modules.data.results
        });
    } catch (err) {
        console.log(err);
    }
}