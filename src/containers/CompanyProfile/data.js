
// const userImageList = [
//   {
//     id: 1,
//     image: "https://via.placeholder.com/150",
//   },
//   {
//     id: 2,
//     image: "https://via.placeholder.com/150",
//   },
//   {
//     id: 3,
//     image: "https://via.placeholder.com/150",

//   },
//   {
//     id: 4,
//     image: "https://via.placeholder.com/150",
//     name: 'Mila Alba',
//     rating: '5.0',
//     deals: '27 Deals',
//   },
// ]

export const aboutList = [
  {
    id: 1,
    title: 'Year Of Incorporation',
    icon: 'table',
    
    desc: '2019-01-01',
  },
  {
    id: 2,
    title: 'Market',
    icon: 'shopping-cart',
    
    desc: 'fgsfsrsdfs',
  },
  {
    id: 3,
    title: 'Funding',
    icon: 'pricing-table',
    desc: 22222,
  },
  {
    id: 4,
    title: 'Valuation',
    icon: 'growth',
    desc: '232345',
  },
  {
    id: 5,
    title: 'No of employees',
    icon: 'family',
    desc: '50'
  },
  {
    id: 6,
    title: 'Annual Revenue',
    icon: 'revenue-new',
    desc: '50'
  },
];

export const eventList = [
  {
    id: 1,
    image: "https://via.placeholder.com/125X125",
    title: 'Sundance Film Festival.',
    address: 'Downsview Park, Toronto, Ontario',
    date: 'Feb 23, 2019',
  },
  {
    id: 2,
    image: "https://via.placeholder.com/125X125",
    title: 'Underwater Musical Festival.',
    address: 'Street Sacramento, Toronto, Ontario',
    date: 'Feb 24, 2019',
  },
  {
    id: 3,
    image: "https://via.placeholder.com/125X125",
    title: 'Village Feast Fac',
    address: 'Union Street Eureka',
    date: 'Oct 25, 2019',
  }
];

export const contactList = [
  {
    id: 1,
    title: 'Email',
    icon: 'email',
    desc: "kiley.brown@example.com",
  },
  {
    id: 2,
    title: 'Web page',
    icon: 'link',
    desc: "example.com"
  }, {
    id: 3,
    title: 'Phone',
    icon: 'phone',
    desc: '+1-987 (454) 987'
  },
];

export const friendList = [
  {
    id: 1,
    image: "https://via.placeholder.com/150",
    name: 'Chelsea Johns',
    status: 'online'

  },
  {
    id: 2,
    image:"https://via.placeholder.com/150",
    name: 'Ken Ramirez',
    status: 'offline'
  },
  {
    id: 3,
    image: "https://via.placeholder.com/150",
    name: 'Chelsea Johns',
    status: 'away'

  },
  {
    id: 4,
    image:"https://via.placeholder.com/150",
    name: 'Ken Ramirez',
    status: 'away'
  },
];

export const demoList = [
  {
    "id": "b367aaeb-ff1c-4007-98db-d8df70251c8c",
    "name": "GlobalVox",
    "row_status": 1,
    "description": "GV Company",
    "year_of_incorporation": "2019-01-01",
    "location": {
        "HQ": "sdljdsfls",
        "Business Locations": [
            {
                "state": "Bengaluru, India",
                "address": "address1"
            }
        ]
    },
    "market": "knfkjdksanbd",
    "funding": 100.0,
    "valuation": 1001.0,
    "no_of_employees": 41,
    "business_model": "wkndkjnd",
    "country_phone_code": "+91",
    "phone": "9999999999",
    "email": "saaas@gmail.com",
    "similar_companies": {
        "companies": [
            {
                "link": "gv.com",
                "name": "GV"
            },
            {
                "link": "gv1.com",
                "name": "GV1"
            }
        ]
    },
    "business_revenue_mix": 12221.0
  }
]