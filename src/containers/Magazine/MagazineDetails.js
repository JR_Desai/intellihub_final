import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import DOMPurify from 'dompurify';
import {Avatar, Badge, Col, Row} from "antd";
import ConfirmPopup from '../../components/ConfirmPopup';
import { showConfirmRemove } from '../../appRedux/actions/Companies';
import Widget from '../../components/Widget';
import { deleteMagazine, getMagazine } from '../../appRedux/actions';

const MagazineDetails = props => {

    const dispatch = useDispatch();

    const history = useHistory();

    const {magazine, showRemove} = useSelector(({magazines}) => magazines);

    const magazineId = props.match.params.id;

    useEffect(() => {
        getMagazine(magazineId, dispatch);
    }, [])

    const editTech = () => {
        history.push(`update/${magazineId}`)
    }

    const onRemove = () => {
        deleteMagazine(magazineId, dispatch);
        history.replace("/magazine");
    }

    const removeTech = () => {
        showConfirmRemove(true, dispatch);
    }
    const createMarkup = (html) => {
        return  {
            __html: DOMPurify.sanitize(html)
        }
    }

    return (
        <div className="gx-main-content-wrapper">
            <div className="gx-module-detail gx-module-list">
                <div className="gx-module-detail-item gx-module-detail-header">
                    <Row>
                        <Col xs={24} sm={12} md={17} lg={12} xl={17}>
                            <div className="gx-flex-row">
                            <div className="gx-user-name gx-mr-md-4 gx-mr-2 gx-my-1">
                                <div className="gx-flex-row gx-align-items-center gx-pointer">
                                    <Avatar className="gx-mr-3" src="" alt="" />
                                    <h4 className="gx-mb-0">Demo Name</h4>
                                </div>
                            </div>
                            </div>
                        </Col>

                        <Col xs={24} sm={12} md={7} lg={12} xl={7}>
                            <div className="gx-flex-row gx-justify-content-end">
                                <i className="gx-icon-btn icon icon-edit" onClick={() =>
                                    editTech()}
                                />
                                <i className="gx-icon-btn icon icon-trash" onClick={() =>
                                    removeTech()}
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
                <Widget className="gx-module-detail-item">
                    <div className="gx-form-group gx-flex-row gx-align-items-center gx-flex-nowrap">
                        <Col className="gx-flex-row gx-flex-1 gx-flex-nowrap">
                            <div className="gx-w-25">
                                <img
                                className='' src={magazine.cover_image_upload||magazine.cover_image}
                                alt="..." />
                            </div>
                            <div className="gx-w-75 gx-ml-5">
                                <div className="h1">
                                    {magazine.name}
                                </div>
                                    <span className="gx-ml-auto gx-mt-1 gx-mb-3">{magazine.published_date}</span>
                                <div className="gx-mt-5 gx-mb-5">
                                    <span className="h3">Link: </span>
                                        <a href={magazine.link} target="_blank" className="gx-text-primary">{magazine.link}</a>
                                </div>
                                <div className="gx-mt-4 gx-mb-auto">
                                    Short Description:
                                    <span className="gx-ml-auto gx-mt-1 gx-mb-3" 
                                        dangerouslySetInnerHTML={createMarkup(magazine.short_description)}
                                    ></span>
                                </div>
                            </div>
                        </Col>
                    </div>
                </Widget>
                <ConfirmPopup showStatus={showRemove} onConfirm={onRemove} />
            </div>
        </div>
    )
}

export default MagazineDetails;
