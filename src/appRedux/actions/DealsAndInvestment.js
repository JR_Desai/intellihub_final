import axios from 'axios';
import { CREATE_DEALS_AND_INVESTMENT_SUCCESS, GET_ALL_DEALS_AND_INVESTMENTS_SUCCESS, GET_DEALS_AND_INVESTMENT_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_DEALS_AND_INVESTMENT_SUCCESS, GET_ACQUISITION_STATUS_SUCCESS, GET_ACQUISITION_TYPE_SUCCESS } from '../../constants/ActionTypes';

const module_link = "acquisition";

export const createDealsAndInvestment = async (data , dispatch) => {
        try {
                const deal = await axios.post(`/${module_link}/create/`, data);
                dispatch({
                        type: CREATE_DEALS_AND_INVESTMENT_SUCCESS,
                        payload: deal.data
                })
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllDealsAndInvestment = async (dispatch) => {
        try {
                const deals = await axios.get(`/${module_link}/view/`);
                deals.data.results.forEach((result) => result["name"] = result["acquired_by"]);
                dispatch({
                        type: GET_ALL_DEALS_AND_INVESTMENTS_SUCCESS,
                        payload: deals.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getDealsAndInvestment = async (dealID, dispatch) => {
        try {
                const deal = await axios.get(`/${module_link}/view/?id=${dealID}`);
                dispatch({
                        type: GET_DEALS_AND_INVESTMENT_SUCCESS,
                        payload: deal.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getDealsAndInvestmentTags = async (dispatch) => {
        try {
                const dealTags = await axios.get(`/tag/view/`);
                // dispatch({
                //         type: UPDATE_RAW_MATERIAL_SUCCESS,
                //         payload: service.data.results
                // })
        } catch (err) {
                console.log(err);
        }
};

export const updateDealsAndInvestments = async (dealID, data, dispatch) => {
        try {
                const deal = await axios.put(`/${module_link}/update/${dealID}`, data);
                dispatch({
                        type: UPDATE_DEALS_AND_INVESTMENT_SUCCESS,
                        payload: deal.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusDealsAndInvestments = async (dealID, data, dispatch) => {
        try {
                const deal = await axios.patch(`/${module_link}/update/${dealID}`, {
                        row_status: data.row_status
                }
                );
                dispatch({
                        type: UPDATE_DEALS_AND_INVESTMENT_SUCCESS,
                        payload: deal.data
                })
        } catch (err) {
                console.log(err);
        }
};

// Get Acquisition Status
export const getAcquisitionStatus = async (dispatch) => {
        try {
                const acquisitionStatus = await axios.get(`/acquisition_status/view/`);
                dispatch({
                        type: GET_ACQUISITION_STATUS_SUCCESS,
                        payload: acquisitionStatus.data
                })
        } catch (err) {
                console.log(err);
        }
};

// Get Acquisition Type
export const getAcquisitionType = async (dispatch) => {
        try {
                const acquisitionType = await axios.get(`/acquisition_type/view/`);
                dispatch({
                        type: GET_ACQUISITION_TYPE_SUCCESS,
                        payload: acquisitionType.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const deleteDeal = async (dealID, dispatch) => {
        try {
                await axios.delete(`/${module_link}/delete/${dealID}`);
                dispatch ({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
        } catch (err) {
                console.log(err);
        }
};
