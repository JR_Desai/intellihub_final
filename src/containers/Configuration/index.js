import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import ConfigList from '../../components/ConfigList';

const Configuration = () => {

    const history = useHistory();

    return (
        <div>
        <div className="gx-header-card-container">
            {configLinkButton("/others/configuration/products", "Products", history.location.pathname)}
            {configLinkButton("/others/configuration/companies", "Companies", history.location.pathname)}
            {configLinkButton("/others/configuration/technology", "Technology", history.location.pathname)}
            {configLinkButton("/others/configuration/dai", "Deals & Investments", history.location.pathname)}
            {configLinkButton("/others/configuration/ral", "Research & Literature", history.location.pathname)}
            {configLinkButton("/others/configuration/ras", "Regulatory & Safety", history.location.pathname)}
            {configLinkButton("/others/configuration/mr", "Market Reports", history.location.pathname)}
            {configLinkButton("/others/configuration/news", "News Letter", history.location.pathname)}
        </div>
            <ConfigList />
        </div>
    )
}

export default Configuration;


const configLinkButton = (link, title, url) => {
    return (
        <Link to={`${link}`} className={`${(url == link) ? "gx-header-card-clicked" : ""}  gx-header-card gx-align-items-center gx-flex-nowrap`}>
            <span className="gx-fs-lg gx-align-self-center gx-mb-1">{title}</span>
        </Link>
    );
}

