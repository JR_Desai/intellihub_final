import React from "react";
import { FiBookmark, FiShare2 } from "react-icons/fi";
import { Link } from "react-router-dom";
export const RawMaterialItem = ({ rawMaterial }) => {
  return (
    <div className="rawMaterial-col">
      <div className="rawMaterial-content">
        <div className="recordID">
          <Link to={`/user/rawmaterials/${rawMaterial?.id}`}>
            Record ID - <span>{rawMaterial?.id}</span>
          </Link>
        </div>
        <h3 className="rawMaterial-title">
          <Link to={`/user/rawmaterials/${rawMaterial?.id}`}>
            {rawMaterial?.name}
          </Link>
        </h3>
        <h4 className="rawMaterial-subtitle">{rawMaterial?.category?.name}</h4>
        <p className="rawMaterialDescription">
          <div
            dangerouslySetInnerHTML={{ __html: rawMaterial?.description ?? "" }}
          ></div>
        </p>
        <div className="rawMaterial-actions-wrap">
          <div className="rawMaterial-actions">
            <div className="rawMaterialAction-list">
              <div className="rawMaterialDownload">
                <a
                  className="download-dropdown-toggle dropdown-toggle"
                  href="#"
                  id="donwloadActions"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Download
                </a>
                <div
                  className="dropdown-menu downloadDropdown"
                  aria-labelledby="donwloadActions"
                >
                  <ul>
                    <li>
                      <a href="#">All Format</a>
                    </li>
                    <li>
                      <a href="#">Excel</a>
                    </li>
                    <li>
                      <a href="#">PDF</a>
                    </li>
                    <li>
                      <a href="#">Powerpoint</a>
                    </li>
                    <li>
                      <a href="#">Images</a>
                    </li>
                    <li>
                      <a href="#">HTML</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="rawMaterialAction-list">
              <div className="rawMaterialItem">
                <ul>
                  <li>
                    <a href="#" className="rawMaterialIcon iconBookmark">
                      <FiBookmark />
                    </a>
                  </li>
                  <li>
                    <a href="#" className="rawMaterialIcon iconShare">
                      <FiShare2 />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
