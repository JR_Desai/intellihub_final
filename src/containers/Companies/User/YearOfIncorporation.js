import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Drawer from '@material-ui/core/Drawer';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from "@material-ui/icons/Menu";
import { AiOutlinePlus } from "react-icons/ai";
import { AiOutlineMinus } from "react-icons/ai";
import * as ReactBootStrap from "react-bootstrap";
import { FiSearch } from "react-icons/fi";
import moment from "moment";

const drawerWidth = '240';

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth.concat,
    position: 'relative',
    marginTop: '109px'
  },
  drawerHeader: {
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginLeft: 0
  }
}));

const Headquarters = () => {
  const url = "https://91c538837d31.ngrok.io";

  const classes = useStyles();
  const [hqDrawer, setHqDrawer] = useState(false);

  const handleDrawerOpen = () => {
    setHqDrawer(true);
  };

  const handleDrawerClose = () => {
    setHqDrawer(false);
  };

  const [clicked, setClicked] = useState(false);

  const toggle = (index) => {
    if (clicked === index) {
      return setClicked(null);
    }
    setClicked(index);
  };

  const [show, setShow] = useState(false);
  const [bookmark, setBookmark] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const isBookmark = () => {
    setBookmark(!bookmark);
  };

  const [companies, setCompanies] = useState([])
  
  useEffect(() => {
    fetchCompanies()
  }, [])
  
  const fetchCompanies = () => {
    fetch(`${url}/company/view/`)
    .then((res) => res.json())
    .then((result) => {
        setCompanies(result.results);
      }
    )
  }
  const renderBelowCards = companies.map(company => {
    return <div className="sub-cards">
    <Link to="/company-details">
    <ReactBootStrap.Image
      src={company.image_link}
      className="card-img"
    />
    </Link>
    <div className="card-info">
    <Link to="/company-details">
      <h6 className="card_title">
      {company.name}
      </h6>
    </Link>
      <p className="card_details">
      {company.hq}
      </p>
      <div className="card-details-1">
        <p className="card-con">{company.business_model}</p>
      </div>
    </div>
  </div>
  })
    return (
      // Side navbar
  
      <div className="page">
      <Drawer
        variant="persistent"
        anchor="left"
        open={hqDrawer}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className="acordian">
          <div>
            <IconButton onClick={handleDrawerClose}>
              <ChevronLeftIcon />
              <MenuIcon />
            </IconButton>
          </div>
          <div className="acordian_header">
            <div className="acordian_search">
              <FiSearch className="acordian_search_icon" />
              <input
                type="text"
                aria-label="Text input with dropdown button"
                placeholder="search here"
                className="acordian_ip"
              />
            </div>
          </div>

          {/***************acordian***************/}
          <div className="acordian_content">
            <div className="wrap" onClick={() => toggle(1)}>
              <h6>Country</h6>
              <span>
                {clicked === 1 ? (
                  <AiOutlineMinus className="acordian_icon" />
                ) : (
                  <AiOutlinePlus className="acordian_icon" />
                )}
              </span>
            </div>
            {clicked === 1 ? (
              <div className="checking">
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">India</span>
                </div>
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">America</span>
                </div>
              </div>
            ) : null}

            <div className="wrap" onClick={() => toggle(2)}>
              <h6>Year of Incorporation</h6>
              <span>
                {clicked === 2 ? (
                  <AiOutlineMinus className="acordian_icon" />
                ) : (
                  <AiOutlinePlus className="acordian_icon" />
                )}
              </span>
            </div>
            {clicked === 2 ? (
              <div className="checking">
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">Link</span>
                </div>
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">Link</span>
                </div>
              </div>
            ) : null}

            <div className="wrap" onClick={() => toggle(3)}>
              <h6>Company Size</h6>
              <span>
                {clicked === 3 ? (
                  <AiOutlineMinus className="acordian_icon" />
                ) : (
                  <AiOutlinePlus className="acordian_icon" />
                )}
              </span>
            </div>
            {clicked === 3 ? (
              <div className="checking">
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">Link</span>
                </div>
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">Link</span>
                </div>
              </div>
            ) : null}

            <div className="wrap" onClick={() => toggle(4)}>
              <h6>Category</h6>
              <span>
                {clicked === 4 ? (
                  <AiOutlineMinus className="acordian_icon" />
                ) : (
                  <AiOutlinePlus className="acordian_icon" />
                )}
              </span>
            </div>
            {clicked === 4 ? (
              <div className="checking">
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">Link</span>
                </div>
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">Link</span>
                </div>
              </div>
            ) : null}

            <div className="wrap" onClick={() => toggle(5)}>
              <h6>Sub-Category</h6>
              <span>
                {clicked === 5 ? (
                  <AiOutlineMinus className="acordian_icon" />
                ) : (
                  <AiOutlinePlus className="acordian_icon" />
                )}
              </span>
            </div>
            {clicked === 5 ? (
              <div className="checking">
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">Link</span>
                </div>
                <div className="acordian_drop">
                  <input type="checkbox" className="side_check" />
                  <span className="side_name">Link</span>
                </div>
              </div>
            ) : null}
          </div>
        </div>
      </Drawer>
      {/* Mid Magazines */}
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: hqDrawer
        })}
      >
      <div className={classes.drawerHeader} />

      <div className="news">
        <div className="news_header">
          <div className="news_left_header">
            {hqDrawer || (<IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
            >
              <MenuIcon />
              <ChevronRightIcon />
            </IconButton>)}
            <Typography variant="h4" component="h4">Year Of Incorporation</Typography>
          </div>
          <div className="news_right_header">
            <select className="select">
              <option>Sort By Name</option>
              <option>Sort By Date</option>
            </select>
          </div>
        </div>
        <div className="news_line"></div>
        {/* Magazines list */}
        <div className="magazines">
          <div className="sub_magazines">
              {renderBelowCards}
          </div>
        </div>
      </div>
      </main>
    </div>
    )
}

export default Headquarters;