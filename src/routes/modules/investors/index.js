import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import Investors from '../../../containers/Investors';
import CreateInvestors from '../../../containers/Investors/CreateInvestors';

const InvestorRoutes = () => {

    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={Investors} />
            <Route exact={true} path={`${match.url}/create`} component={CreateInvestors} />
            <Route exact={true} path={`${match.url}/update/:id`} component={CreateInvestors} />
        </Switch>
    );
}

export default InvestorRoutes;
