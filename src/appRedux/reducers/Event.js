import { CREATE_EVENT_SUCCESS, EDIT_EVENT_DETAILS, GET_ALL_EVENTS_SUCCESS, GET_EVENT_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_EVENT_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    events: [],
    event: {},
    showRemove: false,
    showEdit: false
}

export default function eventReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_EVENT_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_EVENTS_SUCCESS: {
            return {
                ...state,
                loader: false,
                events: action.payload
            }
        }
        case GET_EVENT_SUCCESS: {
            return {
                ...state,
                loader: false,
                event: action.payload[0]
            }
        }
        case CREATE_EVENT_SUCCESS: {
            return {
                ...state,
                loader: false,
                event: action.payload
            }
        }
        case UPDATE_EVENT_SUCCESS: {
            return {
                ...state,
                loader: false,
                event: action.payload
            }
        }    
        default:
            return state;
    }
}
