import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import NewsLetter from '../../../containers/NewsLetter';

const NewsLetterRoutes = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={NewsLetter} />
        </Switch>
    )
}

export default NewsLetterRoutes;
