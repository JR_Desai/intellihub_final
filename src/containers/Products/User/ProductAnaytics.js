import React, { useEffect, useState } from "react";
import { Card, Grid, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";
import * as ReactBootStrap from "react-bootstrap";
import Fuse from 'fuse.js'
import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import { FiSearch } from "react-icons/fi";
import { Button, ButtonGroup, Checkbox, Drawer, FormControlLabel, IconButton, makeStyles } from "@material-ui/core";
import { ChevronLeft, ChevronRight, Menu } from "@material-ui/icons";
import { AutoComplete, Spin } from "antd";
import {FiMoreHorizontal} from "react-icons/fi";
import clsx from "clsx";
const drawerWidth = '240';

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex"
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    hide: {
        display: "none"
    },
    drawer: {
        width: drawerWidth,
    },
    drawerPaper: {
        width: drawerWidth.concat,
        position: 'relative',
        marginTop: '109px',
        zIndex: '1029'
    },
    drawerHeader: {
        alignItems: "center",
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: "flex-end"
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create("margin", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        marginLeft: -drawerWidth
    },
    contentShift: {
        transition: theme.transitions.create("margin", {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen
        }),
        marginLeft: 0
    }
}));
const ProductAnaytics = () => {

    const url = "http://3.108.46.195:9000";

    const classes = useStyles();
    const [hqDrawer, setHqDrawer] = useState(false);
    const [clicked, setClicked] = useState(false);
    const [show, setShow] = useState(false);
    const handleDrawerOpen = () => setHqDrawer(true)
    const handleDrawerClose = () => setHqDrawer(false)

    const toggle = (index) => {
        if (clicked === index) return setClicked(null);
        setClicked(index);
    };
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    const [companies, setCompanies] = useState([])
    const [sort, setSort] = useState('dateDesc');

    useEffect(() => fetchCompanies(), [])

    const fetchCompanies = () => {
        fetch(`${url}/product/view/?filters={"type":"operator","data":{"attribute":"row_status","operator":"=","value":"1"}}`)
            .then((res) => res.json())
            .then((result) => setCompanies(result.results))
    }

    var sortedCompaniesData = [...companies]

    const [loading, setLoading] = useState(false)

    const handleDescChange = (event) => {
        setLoading(true);
        event.preventDefault()
        setTimeout(() => setLoading(false), 2000)
        setTimeout(() => setSort('dateDesc'), 2000)

        console.log(event.target)
    }

    const handleAscChange = (event) => {
        setLoading(true);
        event.preventDefault()
        setTimeout(() => setLoading(false), 2000)
        setTimeout(() => setSort('dateAsc'), 2000)
        console.log(event.target)
    }


    if (sort === 'dateDesc') sortedCompaniesData = companies.sort((a, b) => new Date(b.year_of_incorporation) - new Date(a.year_of_incorporation))
    else if (sort === 'dateAsc') sortedCompaniesData = companies.sort((a, b) => new Date(a.year_of_incorporation) - new Date(b.year_of_incorporation))

    const renderBelowCards = sortedCompaniesData.map(product => {
        return <>
            <Grid.Column>
                <div className="product-card">
                    <Card>
                        <div className="row">
                            <Link to={`/user/companies/${product.id}`}>
                                <ReactBootStrap.Image
                                    src={product.image_link}
                                    className="product-image mr-4"
                                />
                            </Link>
                            <Card.Content>
                                <Card.Header>
                                    <Link to={`/user/companies/${product.id}`}>

                                        <div className="product-header">
                                            {product.name}
                                        </div>
                                    </Link>
                                </Card.Header>
                                <Card.Meta>
                                    <div className="product-meta">
                                        {product.hq}
                                    </div>
                                </Card.Meta>
                                <Card.Description>
                                    <div className="product-data">
                                        {product.business_model}
                                    </div>
                                </Card.Description>
                            </Card.Content>
                        </div>
                    </Card>
                </div>
            </Grid.Column>
        </>
    });

    //Render tags
    const [parentTag, setParentTag] = useState([]);
    const [childTag, setChildTag] = useState([]);
    const [allTags, setAllTag] = useState([]);

    useEffect(() => { fetchParentTag() }, [])
    useEffect(() => { fetchChildTag() }, [])
    useEffect(() => { fetchAllTag() }, []);


    const fetchParentTag = () => {
        fetch(`${url}/tag_parent/view/`)
            .then((res) => res.json())
            .then((result) => setParentTag(result))
    }
    const fetchChildTag = () => {
        fetch(`${url}/tag_child/view/`)
            .then((res) => res.json())
            .then((result) => setChildTag(result))
    }

    const fetchAllTag = () => {
        fetch(`${url}/tag/view/`)
            .then((res) => res.json())
            .then((result) => setAllTag(result))
    }

    var [tagState, setTagState] = useState([undefined]);

    const handleTagChange = (event) => {
        console.log(event.target.checked, event.target.name);

        if (event.target.checked) {
            if (tagState.indexOf(parseInt(event.target.name)) === -1) {
                tagState.push(parseInt(event.target.name));
            }
        }
        if (!event.target.checked) {
            var index = tagState.indexOf(parseInt(event.target.name))
            if (index !== -1) {
                tagState.splice(index, 1);
                // setTagState([...tagState]);
            }
            // tagState = tagState.filter(tag => parseInt(tag) !== parseInt(event.target.name))
        }
        const fetchTagData = () => {
            if (tagState.length === 0) {
                fetch(`${url}/product/view/`)
                    .then((res) => res.json())
                    .then((result) => setCompanies(result.results))
            }
            else {
                fetch(`${url}/product/view/?filters={"type":"operator","data":{"attribute":"tag","operator":"in","value":[${tagState}]}}`)
                    .then((res) => res.json())
                    .then((result) => setCompanies(result.results))
            }
        }
        fetchTagData();
    }


    var options = [];
    const [selectOption, setSelectOption] = useState([]);
    const [selectedSearchBar, setSearchBar] = useState(false);
    const onSearch = (searchText) => {
        console.log(searchText);
        const fuse = new Fuse(options, { keys: ['value', 'id'] });

        var results = fuse.search(searchText);
        results.length > 0 ? setSearchBar(true) : setSearchBar(false)
        console.log('searchresult', results);

        const characterResults = results.map(searchedOption => searchedOption.item)

        console.log('selectoptionvalue', characterResults)
        setSelectOption(characterResults);
    };

    const [renderSelectedTag, setRenderSelectedTag] = useState([]);

    const onSelect = (data) => {
        console.log('onSelect', data);
        var idOfTag = options.filter(option => option.value === data).map(option => option.id)

        var [tagId] = idOfTag;
        var filterSelectedChildTag = allTags.filter(tag => tag.id === tagId)
        var [parentID] = filterSelectedChildTag.map(tag => tag.parent_id);

        var filterSelectedParentTag = allTags.filter(tag => tag.id === parentID)

        console.log(filterSelectedParentTag, 'parent', parentID)

        var renderSelectedTag1 = filterSelectedParentTag.map(parentTag => {
            return <>
                <div className="wrap" onClick={() => toggle(parentTag.id)}>
                    <h6>{parentTag.name}</h6>
                    <span>
                    </span>
                </div>
                {
                    filterSelectedChildTag.map((childTag) => {
                        return <>
                            <div className="checking">
                                <div className="acordian_drop">
                                    <FormControlLabel
                                        control={<Checkbox checked={tagState[childTag.name]} onChange={handleTagChange} name={childTag.id} />}
                                        label={childTag.name}
                                    />
                                </div>
                            </div>
                        </>
                    })}
            </>
        })

        setRenderSelectedTag(renderSelectedTag1);
    };
    var filterTags = parentTag.filter(tag => tag.module === 3)

    const renderTags = filterTags.map(tag => {
        var currentParentTag = tag.id;
        var currentModule = tag.module;
        var filterChildTag = childTag.filter(tag => tag.module === currentModule && tag.parent_id === currentParentTag)

        return <>
            <div className="wrap" onClick={() => toggle(tag.id)}>
                <h6>{tag.name}</h6>
                <span>
                    {clicked === tag.id ? (<AiOutlineMinus className="acordian_icon" />) :
                        (<AiOutlinePlus className="acordian_icon" />)}
                </span>
            </div>
            {clicked === tag.id ? (
                filterChildTag.map((tag) => (
                    <div className="checking">
                        <div className="acordian_drop">
                            <FormControlLabel
                                control={<Checkbox checked={tagState[tag.name]} onChange={handleTagChange} name={tag.name} />}
                                label={tag.name}
                            />
                        </div>
                    </div>
                ))
            ) : null}
        </>
    });

    const [listIconActiveClass, setListIconActiveClass] = useState('active-filter');
    const [gridIconActiveClass, setGridIconActiveClass] = useState('');
    const [productListClass, setProductListClass] = useState('productList-row productList-1col');
    const handleChangeList = (event) => {
        event.preventDefault();
        setProductListClass('productList-row productList-1col');
        setGridIconActiveClass('');
        setListIconActiveClass('active-filter');
    };
    const handleChangeGrid = (event) => {
        event.preventDefault();
        setProductListClass('productList-row productListGridView');
        setGridIconActiveClass('active-filter');
        setListIconActiveClass('');
    };

    return (
        <>
            <div className="page newMainBlock">
            <Drawer
                variant="persistent"
                anchor="left"
                open={hqDrawer}
                classes={{
                    paper: classes.drawerPaper
                }}
            >
                <div className="acordian">
                    <div>
                        <IconButton onClick={handleDrawerClose}>
                            <ChevronLeft />
                            <Menu />
                        </IconButton>
                    </div>
                    <div className="acordian_header">
                        <div className="acordian_search">
                            <FiSearch className="acordian_search_icon" />
                            <br />
                            <AutoComplete
                                options={selectOption}
                                style={{
                                    width: 200,
                                }}
                                onSelect={onSelect}
                                onSearch={onSearch}
                                placeholder="Search here"
                            >
                                <input
                                    type="text"
                                    aria-label="Text input with dropdown button"
                                    // placeholder="search here"
                                    className="acordian_ip"
                                />
                            </AutoComplete>
                        </div>
                    </div>

                    {/***************acordian***************/}
                    <div className="acordian_content">
                        {selectedSearchBar ? renderSelectedTag : renderTags}
                    </div>
                </div>
            </Drawer>
            {/* Mid Magazines */}
            <div className="container">
                <main
                    className={clsx(classes.content, {
                        [classes.contentShift]: hqDrawer
                    })}
                >
                    <div className={classes.drawerHeader} />

                    <div className="news">
                        <div className="news_header product_header">
                            <div className="news_left_header">
                                {hqDrawer || (<IconButton
                                    color="inherit"
                                    aria-label="open drawer"
                                    onClick={handleDrawerOpen}
                                    edge="start"
                                >
                                    <Menu />
                                    {/* <ChevronRight /> */}
                                </IconButton>)}
                                <h4 className="main-header">Visualization</h4>
                            </div>
                            <div className="news_right_header">
                               <div className="product_analytics_actions">
                                    <div className="product_analytics_btn">
                                        <a href="/user/products" className="btn btn-primary btn-outline">Product</a>
                                    </div>
                                    <div className="productDownloadDetails product_analytics_btn">
                                        <a className="download-dropdown-toggle dropdown-toggle" href="#" id="donwloadActions" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Download All</a>
                                        <div className="dropdown-menu downloadDropdown" aria-labelledby="donwloadActions">
                                            <ul>
                                                <li><a href="#">All Format</a></li>
                                                <li><a href="#">Excel</a></li>
                                                <li><a href="#">PDF</a></li>
                                                <li><a href="#">Powerpoint</a></li>
                                                <li><a href="#">Images</a></li>
                                                <li><a href="#">HTML</a></li>
                                            </ul>
                                        </div>
                                    </div>
                               </div>
                            </div>
                            <div className="product_analytics_description">
                                <p>See this side-by-side comparison of Sudicel vs. Mortein based on preference data from user reviews.Sudicel rates 4.3/5 stars with 12 reviews. By contrast, Mortien rates 3.5/5 stars with 12 reviews. Each product's score is calculated with real-time data from verified user reviews, to help you make the best choice between these two options, and decide which one is best for your business needs.</p>
                            </div>
                        </div>
                        <div className="news_line"></div>
                        {/* Magazines list */}
                        <div className="productAnayticsWrap">
                             <div className="productAnayticsGraph">
                                <div className="productAnaticsGraphHead">
                                    <h3 className="title-font">Package Type</h3>
                                    <div className="productDownloadDetails graphDropdownBlock">
                                        <a className="graphDropdownBlockBtn" href="#" id="donwloadActions" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><FiMoreHorizontal /></a>
                                        <div className="dropdown-menu downloadDropdown dropdown-menu-right" aria-labelledby="donwloadActions">
                                            <ul>
                                                <li><a href="#">All Format</a></li>
                                                <li><a href="#">Excel</a></li>
                                                <li><a href="#">PDF</a></li>
                                                <li><a href="#">Powerpoint</a></li>
                                                <li><a href="#">Images</a></li>
                                                <li><a href="#">HTML</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="productAnaticsGraphBlock">
                                    Graph Comming Soon...
                                </div>
                             </div>

                             <div className="productAnayticsGraph">
                                <div className="productAnaticsGraphHead">
                                    <h3 className="title-font">Companies</h3>
                                    <div className="productDownloadDetails graphDropdownBlock">
                                        <a className="graphDropdownBlockBtn" href="#" id="donwloadActions" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><FiMoreHorizontal /></a>
                                        <div className="dropdown-menu downloadDropdown dropdown-menu-right" aria-labelledby="donwloadActions">
                                            <ul>
                                                <li><a href="#">All Format</a></li>
                                                <li><a href="#">Excel</a></li>
                                                <li><a href="#">PDF</a></li>
                                                <li><a href="#">Powerpoint</a></li>
                                                <li><a href="#">Images</a></li>
                                                <li><a href="#">HTML</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="productAnaticsGraphBlock">
                                    Graph Comming Soon...
                                </div>
                             </div>

                             <div className="productAnayticsGraph">
                                <div className="productAnaticsGraphHead">
                                    <h3 className="title-font">Sub - Categories</h3>
                                    <div className="productDownloadDetails graphDropdownBlock">
                                        <a className="graphDropdownBlockBtn" href="#" id="donwloadActions" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><FiMoreHorizontal /></a>
                                        <div className="dropdown-menu downloadDropdown dropdown-menu-right" aria-labelledby="donwloadActions">
                                            <ul>
                                                <li><a href="#">All Format</a></li>
                                                <li><a href="#">Excel</a></li>
                                                <li><a href="#">PDF</a></li>
                                                <li><a href="#">Powerpoint</a></li>
                                                <li><a href="#">Images</a></li>
                                                <li><a href="#">HTML</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="productAnaticsGraphBlock">
                                    Graph Comming Soon...
                                </div>
                             </div>
                        </div>
                        {/*<Spin spinning={loading}>*/}
                        {/*    {listView ?*/}
                        {/*        <Grid columns={1}>*/}
                        {/*            {renderBelowCards}*/}
                        {/*        </Grid> :*/}
                        {/*        <Grid columns={hqDrawer ? 2 : 3}>*/}
                        {/*            {renderBelowCards}*/}
                        {/*        </Grid>*/}
                        {/*    }*/}
                        {/*</Spin>*/}
                    </div>
                </main>
            </div>
        </div>
        </>
    )
};

export default ProductAnaytics;