import React, { useEffect, useState } from 'react'
import {
    Form,
    Button,
    Card,
    Select,
    DatePicker,
    Input,
    Spin,
    Tooltip
} from 'antd';

import { useDispatch, useSelector } from 'react-redux';
import InputGroup from '../../components/InputGroup';
import { useHistory } from 'react-router';
import { createMagazine, updateMagazine } from '../../appRedux/actions/Magazine';
import {convertToRaw, EditorState} from "draft-js";
// import {Editor} from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import custom_editor_config from '../EditorConfig'
import { CKEditor as Editor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { createTechnology, updateTechnology, getTechnologyTags } from '../../appRedux/actions/Technologies';


const CreateMagazine = props => {
    useEffect(() => {
        getTechnologyTags(dispatch);
    }, [])
    const Option = Select.Option;

    const history = useHistory();

    const magazineValues = useSelector(({magazines}) => magazines);
    const { technology, tags } = useSelector(({ technologies }) => technologies);


    const magValues = magazineValues.magazine;

    const magazineId = props.match.params.id;

    const initState = {
        name: "Name",
        cover_image: null,
        published_date: "2019-02-11",
        link: "",
        row_status: 3,
        short_description: "",
    }

    const values = magazineId ? magValues : initState

    const header = magazineId ? "Update" : "Create"

    const [magazine, setMagazine] = useState(values);
    const [loading, setLoading] = useState(false)
    const [uploadCoverImage, setCoverImage] = useState([{cover_image_upload : null}])
    const [uploadAttachment, setAttachment] = useState([{attachment : null}])

    const dispatch = useDispatch();

    const formData = new FormData();

    const parentTagOption = tags ?.map(
        (tag_parent, _) => <Option key={tag_parent.id} value={tag_parent.id}>{tag_parent.name}</Option>
    );
    if (magazineId) {
        if (technology.company && technology.company.length > 0) {
            let Company = [];
            technology.company.map((res) => {
                res.id ? Company.push(res.id) : Company.push(res);
            });
            technology.company = Company;
        }
        if (technology.tags && technology.tags.length > 0) {
            let Tags = [];
            technology.tags.map((res) => {
                res.id ? Tags.push(res.id) : Tags.push(res);
            });
            technology.tags = Tags;
        }
        if (technology.row_status && technology.row_status.id) {
            technology.row_status = technology.row_status.id;
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        let formData = new FormData();
        if(uploadCoverImage.cover_image_upload !== undefined) {
            formData.append("cover_image_upload", uploadCoverImage.cover_image_upload, uploadCoverImage.cover_image_upload.name);
        }

        if(uploadAttachment.attachment !== undefined) {
            formData.append("attachment", uploadAttachment.attachment, uploadAttachment.attachment.name);
        }
        
        setLoading(true)

        magazineId ? updateMagazine(magazineId, magazine, formData, dispatch) : createMagazine(magazine, formData, dispatch);

        setTimeout(() => {
            magazineId ? updateMagazine(magazineId, magazine, formData, dispatch) : createMagazine(magazine, formData, dispatch);
        }, 2000)

        setTimeout(() => {
            setLoading(false)
            // history.replace("/magazine");
        }, 2000)
    }
    const handleClose = (e) => {
        history.replace("/magazine")
    }
    
    const handleEditor = (editorState) => {
        console.log( draftToHtml(convertToRaw(editorState.getCurrentContent())) )
        var htmlData = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        setMagazine({...magazine, description: htmlData})
    }


    return (
        <Spin spinning={loading}>
        <div className="gx-main-content-wrapper">
        <Card className="gx-card" title={`${header} Magazine`}>
        <Form
            name="register"
            scrollToFirstError
            >
                <div className="gx-form-group">
                    <label htmlFor="cover_image" className="gx-form-label">Cover Image:</label>
                <Tooltip visible={magazine.cover_image?false:true} placement="bottomLeft" title="Cover Image is Required" color={'red'}>
                    
                    <Input name="image_link" 
                    value={magazine.cover_image}
                    onChange={(e) => setMagazine({...magazine, cover_image: e.target.value})} 
                    type="text" accept="image/*" placeholder="Add Image"
                    />
                </Tooltip>
                    <br/>
                </div>
                {
                    magazine.cover_image? 
                        <>
                        <div className="gx-form-group">
                        <label htmlFor="cover_image" className="gx-form-label">Current Cover Image:</label>
                        <div className="gx-w-25">
                            <img
                            className='' src={magazine.cover_image}
                            alt="Can't Load File" />
                        </div>
                        </div>
                        </> :
                        null
                }
                <div className="gx-form-group">
                    <label htmlFor="cover_image_upload" className="gx-form-label">Cover Image Upload:</label>
                    <Input name="image_link"
                    onChange={(e) => setCoverImage({cover_image_upload: e.target.files[0]})} 
                    encType='multipart/form-data' type="file" accept="image/*" placeholder="Add Image"
                    />
                </div>
                {
                    magazine.cover_image_upload? 
                        <>
                        <div className="gx-form-group">
                        <label htmlFor="cover_image" className="gx-form-label">Current Uploaded Cover Image:</label>
                        <div className="gx-w-25">
                            <img
                            className='' src={magazine.cover_image_upload}
                            alt="Error" />
                        </div>
                        </div>
                        </> :
                        null
                }
                <div className="gx-form-group">
                    <label htmlFor="Magazine_Name" className="gx-form-label">Magazine Name:</label>
                <Tooltip visible={magazine.name?false:true} placement="bottomLeft" title="Magazine Name is Required" color={'red'}>
                    <Input name="name" type="text"
                    value={magazine.name} 
                    onChange={(e) => setMagazine({...magazine, name: e.target.value})} 
                    />
                </Tooltip>
                </div>
                <div className="gx-form-group">
                    <label htmlFor="image_link" className="gx-form-label">Short Description:</label>
                    <div style={{width:"100%"}}>
                    <Editor
                    editor={ ClassicEditor }
                    data={magazine.short_description}
                    onChange={( event, editor ) => {
                        const data = editor.getData();
                        setMagazine({...magazine, short_description:data})
                    }}
                    config={custom_editor_config}
                    />
                    <Tooltip visible={magazine.short_description?false:true} placement="bottomLeft" title="Short Description is Required" color={'red'}>
                    </Tooltip>
                    </div>
                </div>
                <div className="gx-form-group">
                    <label htmlFor="date_of_publication" className="gx-form-label">Date of Publication:</label>
                    <Tooltip visible={magazine.published_date?false:true} placement="bottomLeft" title="Date of Publication is Required" color={'red'}>
                    <DatePicker name="date_of_publication"
                        placeholder={magazine.published_date}
                        onChange={(e) => setMagazine({...magazine, published_date: e?e.format("YYYY-MM-DD"):null})} className="gx-mb-3 gx-w-100"
                    />
                    </Tooltip>
                </div>
                <div className="gx-form-group">
                    <label htmlFor="Magazine_Name" className="gx-form-label">Link:</label>
                    <Tooltip visible={magazine.link?false:true} placement="bottomLeft" title="Link is Required" color={'red'}>
                    <Input name="link" type="text" value={magazine.link} 
                    onChange={
                        (e) => setMagazine({...magazine, link: e.target.value})
                    } 
                    />
                    </Tooltip>
                </div>
                <div className="gx-form-group">
                    <label htmlFor="attachment" className="gx-form-label">Attachment:</label>
                    <Input name="image_link"
                    onChange={(e) => setAttachment({attachment: e.target.files[0]})} 
                    encType='multipart/form-data' type="file" placeholder="Add Image"
                    />
                </div>
                <Tooltip placement="top" title={magazine.cover_image && magazine.name && magazine.short_description &&  magazine.link ? '' : "Please fill required fields" } >
                
                <Button onClick={handleSubmit} style={{float:'right'}} className="gx-w-20" type="primary" htmlType="submit"
                disabled={ magazine.cover_image && magazine.name && magazine.short_description &&  magazine.link ? false : true }
                >
                    Submit Details
                </Button>
                </Tooltip>
                <Button onClick={handleClose} style={{float:'right', marginRight:'15px'}} className="gx-w-20" type="primary" htmlType="submit">
                    Close
                </Button>
        </Form>
    </Card>
    </div>
    </Spin>
    )
}

export default CreateMagazine;
