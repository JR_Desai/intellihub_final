import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import {
  ContentState,
  convertFromHTML,
  convertToRaw,
  EditorState,
} from "draft-js";
import { Form, Button, Card, Select, Input, Typography, Cascader } from "antd";
import { Formik } from "formik";

import {
  getRawMaterial,
  createRawMaterials,
  updateRawMaterial,
} from "../../appRedux/actions/RawMaterials";
import InputGroup from "../../components/InputGroup";
import {
  createNewCategory,
  getAllCategoryModule,
  getAllContains,
  getAllCountries,
  getAllManufacturers,
  getAllModuleStatus,
  getAllUnits,
} from "../../appRedux/actions";

import {
  formateRequestPayload,
  getCategoryObj,
  getContainsObj,
  getFormatedOptions,
  getManufacturerObj,
  getPkUnitObj,
  initialValues,
  rawMaterialValidationSchema,
} from "./RawMaterialUtils";

const moduleCode = 7;

const RawMaterialCreate = (props) => {
  const formikRef = useRef();

  const rawMaterailId = props.match.params.id;
  const header = !rawMaterailId ? "Create" : "Update";
  const dispatch = useDispatch();
  const history = useHistory();
  const { rawMaterial } = useSelector(({ rawMaterials }) => rawMaterials);
  const { countries, manufacturer, units, contains, categoryModule } =
    useSelector(({ config }) => config);

  const [defaultEditorState, setDefaultEditorState] = useState();

  useEffect(() => {
    if (rawMaterailId) {
      getRawMaterial(rawMaterailId, dispatch);
    } else {
      if (formikRef.current) {
        Object.keys(initialValues).map((k) => {
          formikRef.current.setFieldValue(k, initialValues[k]);
        });
      }
    }
    getAllCountries(dispatch);
    getAllManufacturers(dispatch);
    getAllUnits(dispatch);
    getAllContains(dispatch);
    getAllModuleStatus(dispatch);
    getAllCategoryModule(dispatch);
  }, [props.match.params.id]);

  useEffect(() => {
    if (rawMaterial?.description) {
      setDefaultEditorState(
        EditorState.createWithContent(
          ContentState.createFromBlockArray(
            convertFromHTML(rawMaterial?.description ?? "")
          )
        )
      );
    }
    return () => setDefaultEditorState(null);
  }, [rawMaterial]);
  const handleEditor = (editorState, key, setFieldValue) => {
    const htmlData = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    setFieldValue(key, htmlData);
  };

  const handleSubmit = (values, resetForm) => {
    const reqPayload = formateRequestPayload(values);
    if (props.match.params.id) {
      updateRawMaterial(props.match.params.id, reqPayload, dispatch, () => {
        resetForm(initialValues);
        history.replace("/raw_materials");
      });
    } else {
      createRawMaterials(reqPayload, dispatch, () => {
        history.replace("/raw_materials");
      });
    }
  };

  const handleClose = (e) => {
    history.replace("/raw_materials");
  };

  if (props.match.params.id && !rawMaterial?.id) return "Loading...";
  return (
    <div className="gx-main-content-wrapper">
      <Card className="gx-card" title={`${header} Raw Material`}>
        <Formik
          innerRef={formikRef}
          initialValues={{
            ...initialValues,
            name: rawMaterial?.name,
            variant: rawMaterial?.variant,
            description: rawMaterial?.description,
            manufacturer: rawMaterial?.manufacturer?.id,
            manufacturerObj: rawMaterial?.manufacturer,
            country: [rawMaterial?.country?.id],
            parent_category: rawMaterial?.category?.parent,
            category: String(rawMaterial?.category?.id),
            categoryObj: rawMaterial?.category,
            web_link: rawMaterial?.web_link,
            image_link: rawMaterial?.image_link,
            row_status: rawMaterial?.row_status?.id,
            containsObj: rawMaterial?.contains,
            contains: Array.isArray(rawMaterial?.contains)
              ? rawMaterial?.contains.map((c) => c.id)
              : [],
            properties: rawMaterial?.properties,
            regions_covered: rawMaterial?.regions_covered,
            distributor_channel: rawMaterial?.distributor_channel,
            price: rawMaterial?.price,
            package_size: rawMaterial?.package_size,
            package_unit: rawMaterial?.package_unit?.id,
            package_unitObj: rawMaterial?.package_unit,
          }}
          validationSchema={rawMaterialValidationSchema}
          onSubmit={(values, { resetForm }) => {
            console.log({ values });
            handleSubmit(values, resetForm);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleSubmit,
            setFieldValue,
            resetForm,
          }) => (
            <Form name="register" scrollToFirstError onSubmit={handleSubmit}>
              {console.log({ values })}
              <InputGroup
                name="name"
                label="Name: "
                type="text"
                value={values.name}
                onChange={handleChange}
                error={touched.name ? errors.name : ""}
              />
              <InputGroup
                name="variant"
                label="Variant: "
                type="text"
                value={values.variant}
                onChange={handleChange}
                error={touched.variant ? errors.variant : ""}
              />

              {(props.match.params.id && defaultEditorState) ||
              !props.match.params.id ? (
                <div className="gx-form-group">
                  <div style={{ flex: 1 }}>
                    <label htmlFor="description" className="gx-form-label">
                      Description :
                    </label>
                    <Editor
                      editorStyle={{
                        width: "100%",
                        minHeight: 100,
                        borderWidth: 1,
                        borderStyle: "solid",
                        borderColor: "lightgray",
                      }}
                      wrapperClassName="demo-wrapper"
                      onEditorStateChange={(ed) =>
                        handleEditor(ed, "description", setFieldValue)
                      }
                      defaultEditorState={defaultEditorState}
                    />
                  </div>
                </div>
              ) : null}

              <div className="gx-form-group">
                <label className="gx-form-label">Manufacturer: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    name="manufacturer"
                    mode="tags"
                    className="gx-w-100"
                    value={values?.manufacturer}
                    placeholder="Select Manufacturer"
                    onSelect={(val, option) => {
                      setFieldValue("manufacturer", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "manufacturerObj",
                          getManufacturerObj({ name: val })
                        );
                      } else {
                        setFieldValue("manufacturerObj", option?.details);
                      }
                    }}
                    onDeselect={() => {
                      setFieldValue("manufacturer", undefined);
                      setFieldValue("manufacturerObj", undefined);
                    }}
                    options={getFormatedOptions(manufacturer)}
                    optionFilterProp="label"
                  />

                  {!values.manufacturer && (
                    <Typography.Text type="danger">
                      {"Please select manufacturer"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              <div className="gx-form-group">
                <label className="gx-form-label">Country: </label>
                <div style={{ flex: 1 }}>
                  <Cascader
                    autoComplete="true"
                    showSearch={true}
                    className="gx-w-100"
                    value={values?.country}
                    placeholder="Select Country"
                    options={getFormatedOptions(countries)}
                    onChange={(val) => setFieldValue("country", val)}
                    name="country"
                  />
                </div>
              </div>

              <div className="gx-form-group">
                <label className="gx-form-label">Parent Category: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    maxTagCount={1}
                    className="gx-w-100"
                    value={values?.parent_category}
                    placeholder="Select Parent Category"
                    options={getFormatedOptions(
                      categoryModule.filter(
                        (c) => c?.module === moduleCode && c?.parent === null
                      )
                    )}
                    onSelect={(val, option) => {
                      if (!option?.details?.id) {
                        createNewCategory(
                          {
                            name: val,
                            code: val.toUpperCase(),
                            parent: null,
                            module: moduleCode,
                            status: true,
                          },
                          dispatch,
                          (saved) => {
                            setFieldValue("parent_category", saved?.id);
                          }
                        );
                      } else {
                        setFieldValue("parent_category", val);
                      }
                      setFieldValue("category", undefined);
                      setFieldValue("categoryObj", undefined);
                    }}
                    name="category"
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("parent_category", undefined);
                      setFieldValue("parent_categoryObj", undefined);
                      setFieldValue("category", undefined);
                      setFieldValue("categoryObj", undefined);
                    }}
                  />
                  {!values.parent_category && (
                    <Typography.Text type="danger">
                      {"Please select parent category"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              {values?.parent_category && (
                <div className="gx-form-group">
                  <label className="gx-form-label">Category: </label>
                  <div style={{ flex: 1 }}>
                    <Select
                      mode="tags"
                      maxTagCount={1}
                      className="gx-w-100"
                      value={values?.category}
                      placeholder="Select  Category"
                      options={getFormatedOptions(
                        categoryModule
                          .filter(
                            (c) =>
                              c?.module === moduleCode &&
                              c?.parent === values?.parent_category
                          )
                          .map((d) => ({ ...d, id: String(d?.id) }))
                      )}
                      onSelect={(val, option) => {
                        setFieldValue("category", val);
                        if (option?.details?.id) {
                          setFieldValue("categoryObj", option?.details);
                        } else {
                          setFieldValue(
                            "categoryObj",
                            getCategoryObj({
                              name: val,
                              parent: values?.parent_category,
                              module: moduleCode,
                            })
                          );
                        }
                      }}
                      name="category"
                      optionFilterProp="label"
                      onDeselect={() => {
                        setFieldValue("category", undefined);
                        setFieldValue("categoryObj", undefined);
                      }}
                    />
                    {!values.category && (
                      <Typography.Text type="danger">
                        {"Please select category"}
                      </Typography.Text>
                    )}
                  </div>
                </div>
              )}
              <InputGroup
                name="web_link"
                label="Web link: "
                type="text"
                value={values.web_link}
                onChange={handleChange}
              />

              <InputGroup
                name="image_link"
                label="Image link: "
                type="text"
                value={values.image_link}
                onChange={handleChange}
              />

              <div className="gx-form-group">
                <label htmlFor="image_upload" className="gx-form-label">
                  Image Upload:
                </label>
                <div style={{ flex: 1 }}>
                  <Input
                    name="image_upload"
                    onChange={(e) =>
                      setFieldValue("image_upload", e.target.files[0])
                    }
                    encType="multipart/form-data"
                    type="file"
                    accept="image/*"
                    placeholder="Add Image"
                  />
                  {rawMaterial?.image_upload &&
                    rawMaterial?.image_upload !== "" && (
                      <div className="gx-w-25">
                        <img
                          className=""
                          src={rawMaterial?.image_upload}
                          alt="..."
                        />
                      </div>
                    )}
                </div>
              </div>

              {/* <div className="gx-form-group">
                <label className="gx-form-label">Row status: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    className="gx-w-100"
                    value={values?.row_status}
                    placeholder="Select Row status"
                    onSelect={(val) => setFieldValue("row_status", val)}
                    name="row_status"
                    options={getFormatedOptions(module_status, "status_code")}
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("row_status", undefined);
                      setFieldValue("row_statusObj", undefined);
                    }}
                  />
                </div>
              </div> */}

              <div className="gx-form-group">
                <label className="gx-form-label">Contains: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    value={values?.contains}
                    placeholder="Select Contains"
                    options={getFormatedOptions(contains)}
                    onChange={(val, option) => {
                      console.log({ option });
                      setFieldValue("contains", val);
                      const actualOptions = option.map((o, i) => {
                        if (o?.details?.id) {
                          return getContainsObj(o?.details);
                        }
                        return getContainsObj({ name: val[i] });
                      });
                      setFieldValue("containsObj", actualOptions);
                    }}
                    name="contains"
                  />
                </div>
              </div>

              <InputGroup
                name="properties"
                label="Properties: "
                type="text"
                value={values.properties}
                onChange={handleChange}
              />

              <InputGroup
                name="regions_covered"
                label="Regions covered: "
                type="text"
                value={values.regions_covered}
                onChange={handleChange}
              />

              <InputGroup
                name="distributor_channel"
                label="Distributor channel: "
                type="text"
                value={values.distributor_channel}
                onChange={handleChange}
              />

              <InputGroup
                name="price"
                label="Price: "
                type="number"
                value={values.price}
                onChange={handleChange}
              />

              <InputGroup
                name="package_size"
                label="Package size: "
                type="number"
                value={values.package_size}
                onChange={handleChange}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">Package Unit: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    value={values?.package_unit}
                    placeholder="Select Package unit"
                    options={getFormatedOptions(units)}
                    onSelect={(val, option) => {
                      setFieldValue("package_unit", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "package_unitObj",
                          getManufacturerObj(getPkUnitObj({ name: val }))
                        );
                      } else {
                        setFieldValue("package_unitObj", option?.details);
                      }
                    }}
                    name="package_unit"
                  />
                  {errors.package_unit && (
                    <Typography.Text type="danger">
                      {errors.package_unit}
                    </Typography.Text>
                  )}
                </div>
              </div>

              {/* <div className="gx-form-group">
                <label htmlFor="date" className="gx-form-label">
                  Approval date:
                </label>
                <DatePicker
                  name="approval_date"
                  placeholder="Select Date"
                  value={values.approval_date}
                  onChange={e => handleChange}
                  className="gx-mb-3 gx-w-100"
                />
              </div> */}

              <Button
                onClick={handleSubmit}
                style={{ float: "right" }}
                className="gx-w-20"
                type="primary"
                htmlType="submit"
              >
                Submit Details
              </Button>
              <Button
                onClick={() => {
                  resetForm(initialValues);
                  handleClose();
                }}
                style={{ float: "right", marginRight: "15px" }}
                className="gx-w-20"
                type="default"
                htmlType="submit"
              >
                Close
              </Button>
            </Form>
          )}
        </Formik>
      </Card>
    </div>
  );
};

export default RawMaterialCreate;
