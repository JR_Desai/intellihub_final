import axios from 'axios';
import { CREATE_INVESTOR_SUCCESS, GET_ALL_INVESTORS_SUCCESS, GET_INVESTOR_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_INVESTOR_SUCCESS, GET_ALL_CATEGORY_SUCCESS, GET_ALL_INVESTOR_TYPE_SUCCESS, GET_ALL_INVESTOR_STAGE_SUCCESS } from '../../constants/ActionTypes';

const module_link = "investor"

export const createInvestors = async (data, dispatch) => {
        try {
                const investor = await axios.post(`/${module_link}/create/`, data);
                dispatch({
                        type: CREATE_INVESTOR_SUCCESS,
                        payload: investor.data
                })
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllInvestors = async (dispatch) => {
        try {
                const investor = await axios.get(`/${module_link}/view/`);
                investor.data.results.forEach((result) => {
                        result["name"] = result["investor_name"];
                        result["image_link"] = result["logo_link"];
                });

                dispatch({
                        type: GET_ALL_INVESTORS_SUCCESS,
                        payload: investor.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getInvestors = async (investorID, dispatch) => {
        try {
                const investor = await axios.get(`/${module_link}/view/?id=${investorID}`);
                dispatch({
                        type: GET_INVESTOR_SUCCESS,
                        payload: investor.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getInvestorTags = async (dispatch) => {
        try {
                const investor = await axios.get(`/tag/view/`);
                // dispatch({
                //         type: UPDATE_RAW_MATERIAL_SUCCESS,
                //         payload: service.data.results
                // })
        } catch (err) {
                console.log(err);
        }
};


export const getCategories = async (dispatch) => {
        try {
                const investor = await axios.get(`/category/view/`);
                dispatch({
                        type: GET_ALL_CATEGORY_SUCCESS,
                        payload: investor.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const getInvestorType = async (dispatch) => {
        try {
                const types = await axios.get(`/investor_type/view/`);
                dispatch({
                        type: GET_ALL_INVESTOR_TYPE_SUCCESS,
                        payload: types.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const getInvestorStages = async (dispatch) => {
        try {
                const stages = await axios.get(`/investor_stage/view/`);
                dispatch({
                        type: GET_ALL_INVESTOR_STAGE_SUCCESS,
                        payload: stages.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateInvestors = async (investorID, data, dispatch) => {
        try {
                const investor = await axios.put(`/${module_link}/update/${investorID}`, data);
                dispatch({
                        type: UPDATE_INVESTOR_SUCCESS,
                        payload: investor.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusInvestors = async (investorID, data, dispatch) => {
        try {
                const investor = await axios.patch(`/${module_link}/update/${investorID}`, {
                        row_status: data.row_status
                }
                );
                dispatch({
                        type: UPDATE_INVESTOR_SUCCESS,
                        payload: investor.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const deleteInvestors = async (investorID, dispatch) => {
        try {
                await axios.delete(`/${module_link}/delete/${investorID}`);
                dispatch({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
        } catch (err) {
                console.log(err);
        }
};
