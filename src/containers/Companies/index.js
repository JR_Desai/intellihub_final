import React from 'react';
import './style.css'
import { useSelector } from 'react-redux';

import ListView from '../../components/ListView';
import { updateCompany } from '../../appRedux/actions/Companies';


const Companies = props => {

    const {companies} = useSelector(({companies}) => companies);

    return (
        <ListView listToBeShown={companies} searchTitleText="Companies" link="companies" title="Companies" updateModuleItem={updateCompany} />
    );
}



export default Companies;
