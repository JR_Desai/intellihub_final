import React, { useEffect, useState } from 'react'
import {
    Form,
    Button,
    Card,
    Upload,
    Input,
    DatePicker,
    Cascader
} from 'antd';
import FormData from "form-data";
import { useDispatch, useSelector } from 'react-redux';
import { createCompany, updateCompany } from '../../appRedux/actions/Companies';
import InputGroup from '../../components/InputGroup';
import { useHistory } from 'react-router';
import {convertToRaw, EditorState} from "draft-js";
import {Editor} from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import DynamicInputGroup from '../../components/DynamicInputGroup';
import { getAllCountries, getAllDesignations, getAllStates } from '../../appRedux/actions';
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';

const CreateCompany = (props) => {

    useEffect(() => {
        getAllCountries(dispatch);
        getAllDesignations(dispatch);
    }, [])

    const history = useHistory();

    const companies = useSelector(({companies}) => companies);
    const {countries, states} = useSelector(({config}) => config);

    const companyValues = companies.company;
    const companyId = props.match.params.id;

    let companiesOption = [];

    companiesOption = companyId ? companies.companies.filter((company) => companyId !== company.id) : null;


    const initState = {
        name: "Name",
        parent: "",
        description: "description",
        image_link: null,
        year_of_incorporation: "2019-02-11",
        headquarter: "hq",
        market: "market",
        funding: 223,
        valuation: 23,
        no_of_employees: 23,
        business_model: "model",
        country_phone_code: "+91",
        phone: "9999999999",
        email: "e@e.com",
        website: "www.w.com",
        similar_companies: [],
        business_revenue_mix: 999,
        location: {
            location: [
                "ds",
                "sds",
                "dsd"
            ],
        },
        country: null,
        state: null
    }

    const countryOptions = [];
    

    countries.map((country, index) => {
        countryOptions.push({value: country.id, label: country.name});
    });

    const values = companyId ? companyValues : initState

    const header = companyId ? "Update" : "Create"

    const [company, setCompany] = useState(values);
    const [stateOptions, setStateOptions] = useState([]);
    const dispatch = useDispatch();

    const formData = new FormData();

    const handleSubmit = (e) => {
        e.preventDefault()
        formData.append("image_link", company.image_link, company.image_link.name);
        formData.append("name", company.name);
        formData.append("description", company.description);
        formData.append("year_of_incorporation", company.year_of_incorporation);
        formData.append("country_phone_code", company.country_phone_code);
        formData.append("phone", company.phone);
        // formData.append("location", company.location);
        formData.append("hq", company.headquarter);
        formData.append("country", company.country);
        formData.append("website", company.website);
        formData.append("email", company.email);
        formData.append("funding", company.funding);
        formData.append("valuation", company.valuation);
        formData.append("business_model", company.business_model);
        formData.append("business_revenue_mix", company.business_revenue_mix);
        formData.append("market", company.market);
        console.log('formdata',formData);
        companyId ? updateCompany(companyId, company, dispatch) : createCompany(formData, dispatch);
        history.replace("/companies");
    }

    const onCountryChange = (value) => {
        setCompany({...company, country: value[0]});
        getAllStates(company.country,dispatch);
        // states.map((state, index) => {
        //     console.log(stateOptions.typeof())
        //     // setStateOptions(...stateOptions, {value: state.id, label: state.name})
        // });
        console.log("dispatch",dispatch)
    }
    
    // useEffect(() => {
        // getAllStates(company.country,dispatch);
        console.log(states)
        // states.map((state, index) => {
        //     setStateOptions(...stateOptions, {value: state.id, label: state.name})
        // });
    // },[company.country])

    const handleEditor = (editorState) => {
        console.log( draftToHtml(convertToRaw(editorState.getCurrentContent())) )
        var htmlData = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        setCompany({...company, description: htmlData})
    }

    return (
        <div className="gx-main-content-wrapper">
        <Card className="gx-card" title={`${header} Company`}>
        <Form
            name="register"
            scrollToFirstError
            >
                <InputGroup name="name" label="Name: " type="text" value={company.name} 
                    onChange={(e) => setCompany({...company, name: e.target.value})} 
                />
                <div className="gx-form-group">
                    <label htmlFor="image_link" className="gx-form-label">Description:</label>
                    <Editor editorStyle={{
                        width: '100%',
                        minHeight: 100,
                        borderWidth: 1,
                        borderStyle: 'solid',
                        borderColor: 'lightgray'
                    }}
                        wrapperClassName="demo-wrapper"
                        onEditorStateChange={handleEditor}
                    />
                </div>
                <div className="gx-form-group">
                    <label htmlFor="year_of_incorporation" className="gx-form-label">Year of incorporation:</label>
                    <DatePicker name="year_of_incorporation" placeholder={company.year_of_incorporation}
                        onChange={(e) => setCompany({...company, year_of_incorporation: e.format("YYYY-MM-DD")})} className="gx-mb-3 gx-w-100"
                    />
                </div>
                <div className="gx-form-group">
                    <label htmlFor="image_link" className="gx-form-label">Image</label>
                    <Input name="image_link" onChange={(e) => setCompany({...company, image_link: e.target.files[0]})} 
                        encType='multipart/form-data' type="file" accept="image/*" placeholder="Add Image"
                    />
                </div>
                <DynamicInputGroup label="Location: " name="location" placeholder="Add location or remove this field"
                    type="text" dynamicObject={company.location.location} mainObject={company} setChange={setCompany}
                />
                <InputGroup name="headquarter" label="Headquarter: " type="text" value={company.headquarter} 
                    onChange={(e) => setCompany({...company, headquarter: e.target.value})} 
                />
                <InputGroup name="market" label="Market: " type="text" value={company.market} 
                    onChange={(e) => setCompany({...company, market: e.target.value})} 
                />
                <InputGroup name="funding" label="Funding: " type="number" value={company.funding} 
                    onChange={(e) => setCompany({...company, funding: e.target.value})} 
                />
                <InputGroup name="valuation" label="Valuation: " type="number" value={company.valuation} 
                    onChange={(e) => setCompany({...company, valuation: e.target.value})} 
                />
                <InputGroup name="no_of_employees" label="Employees: " type="number" value={company.no_of_employees} 
                    onChange={(e) => setCompany({...company, no_of_employees: e.target.value})} 
                />
                <InputGroup name="business_model" label="Business Model: " type="text" value={company.business_model} 
                    onChange={(e) => setCompany({...company, business_model: e.target.value})} 
                />
                <div className="gx-form-group">
                    <label className="gx-form-label">Country: </label>
                    <Cascader className="gx-w-100" options={countryOptions} autoComplete="true" showSearch={true} placeholder="Please select a country" onChange={onCountryChange}  />
                </div>
                <div className="gx-form-group">
                    <label className="gx-form-label">State: </label>
                    <Cascader className="gx-w-100" options={states} autoComplete="true" showSearch={true} placeholder="Please select a state" onChange={(value) => {setCompany({...company, state: value[0]})}} />
                </div>
                <div className="gx-form-group">
                    <label className="gx-form-label">Phone: </label>
                    <PhoneInput
                        country={'in'}
                        enableSearch={true}
                        inputClass="gx-w-100"
                        value={company.country_phone_code + company.phone} 
                        onChange={(value, data) => setCompany({...company, phone: value.slice(data.dialCode.length), country_phone_code: "+" + data.dialCode})} 
                    />
                </div>
                <InputGroup name="email" label="Email: " type="text" value={company.email} 
                    onChange={(e) => setCompany({...company, email: e.target.value})} 
                />
                <InputGroup name="website" label="Website: " type="text" value={company.website} 
                    onChange={(e) => setCompany({...company, website: e.target.value})} 
                />
                <InputGroup name="business_revenue_mix" label="Revenue: " type="number" value={company.business_revenue_mix} 
                    onChange={(e) => setCompany({...company, business_revenue_mix: e.target.value})} 
                />
                <Button onClick={handleSubmit} className="gx-w-100" type="primary" htmlType="submit">
                    Submit Details
                </Button>
        </Form>
    </Card>
    </div>
    )
}

export default CreateCompany;


