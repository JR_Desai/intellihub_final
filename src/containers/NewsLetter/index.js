import React from 'react'
import ListView from '../../components/ListView'

const NewsLetter = () => {
    const newsletter = [
        {
            id: "1",
            name: "Name",
            description: "Description",
            image_link: "",
        },
        {
            id: "2",
            name: "Name",
            description: "Description",
            image_link: "",
        },
        {
            id: "3",
            name: "Name",
            description: "Description",
            image_link: "",
        },
    ]

    return (
        <ListView listToBeShown={newsletter} searchTitleText="news letter" link="newsletter" title="News Letter" />
    )
}

export default NewsLetter;
