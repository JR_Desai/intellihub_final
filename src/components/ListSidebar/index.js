import React from 'react'
import {Button} from "antd";
import CustomScrollbars from "utils/CustomScrollbars";
import { Link } from 'react-router-dom';

const ListSidebar = ({NavFilters, NavLabels, title, link}) => {
    return (
    <div className="gx-module-side">
        <div className="gx-module-side-header">
            <div className="gx-module-logo">
                {title}
            </div>
        </div>
        <div className="gx-module-side-content">
            <Link to={link} className="gx-module-add-task">
                <Button variant="raised" type="primary" className="gx-sidebar-btn"
                >
                    Add {title}
                </Button>
            </Link>
            <CustomScrollbars className="gx-module-side-scroll">
            <ul className="gx-module-nav">

                <li className="gx-module-nav-label gx-mt-3">
                Filters
                </li>

                {NavFilters()}
                {/* 
                <li className="gx-module-nav-label gx-mt-3">
                Tags
                </li>
                {NavLabels()} */}
            </ul>
            </CustomScrollbars>
        </div>
    </div>
    )
}

export default ListSidebar
