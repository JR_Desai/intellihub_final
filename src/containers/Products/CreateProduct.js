import React, { useEffect, useRef } from "react";
import {
  Form,
  Button,
  Card,
  Select,
  DatePicker,
  Input,
  Typography,
  Cascader,
} from "antd";

import { useDispatch, useSelector } from "react-redux";
import InputGroup from "../../components/InputGroup";
import { useHistory } from "react-router";
import {
  ContentState,
  convertFromHTML,
  convertToRaw,
  EditorState,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import {
  getAllTechnologies,
  getTechnologyTags,
} from "../../appRedux/actions/Technologies";
import {
  createNewCategory,
  createProducts,
  getAllAwards,
  getAllBrands,
  getAllCategoryModule,
  getAllCategoryParent,
  getAllCountries,
  getAllCurrency,
  getAllFlavour,
  getAllFragrance,
  getAllIngredient,
  getAllLaunchType,
  getAllModuleStatus,
  getAllNews,
  getAllPackageMaterial,
  getAllPackageType,
  getAllPositioningClaims,
  getAllPrivateLabel,
  getAllTagView,
  getAllUnits,
  getProducts,
  updateProducts,
} from "../../appRedux/actions";
import {
  formateRequestPayload,
  productUtils,
  initialValues,
  getFormatedOptions,
  getCommonObj,
  getBrandObj,
  getCategoryObj,
  packagingUnitsObj,
  getTechnologyObj,
} from "./ProductUtils";
import { Formik } from "formik";
import moment from "moment";

const moduleCode = 6;

const ProductNews = (props) => {
  const formikRef = useRef();
  const productId = props.match.params.id;
  const header = !productId ? "Create" : "Update";

  useEffect(() => {
    if (productId) {
      getProducts(productId, dispatch);
    } else {
      if (formikRef.current) {
        Object.keys(initialValues).map((k) => {
          formikRef.current.setFieldValue(k, initialValues[k]);
        });
      }
    }
    getTechnologyTags(dispatch);
    getAllBrands(dispatch);
    getAllCountries(dispatch);
    getAllCategoryModule(dispatch);
    getAllPositioningClaims(dispatch);
    getAllPackageType(dispatch);
    getAllPackageMaterial(dispatch);
    getAllUnits(dispatch);
    getAllLaunchType(dispatch);
    getAllPrivateLabel(dispatch);
    getAllCurrency(dispatch);
    getAllNews(dispatch);
    getAllAwards(dispatch);
    getAllTechnologies(dispatch);
    getAllIngredient(dispatch);
    getAllFragrance(dispatch);
    getAllFlavour(dispatch);
    getAllCategoryParent(dispatch);
    getAllModuleStatus(dispatch);
    getAllTagView(dispatch);
  }, []);
  const history = useHistory();
  const {
    brands,
    countries,
    positioning_claims,
    package_type,
    package_material,
    launch_type,
    private_label,
    currency,
    awards,
    ingredients,
    fragrances,
    flavours,
    categoryModule,
    units,
    tag_view,
  } = useSelector(({ config }) => config);
  const { news } = useSelector(({ news }) => news);
  const { product } = useSelector(({ products }) => products);
  let products = [];
  if (productId) {
    products = product;
  } else {
    products = [];
  }

  console.log({ products });

  const handleEditor = (editorState, key, setFieldValue) => {
    const htmlData = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    setFieldValue(key, htmlData);
  };

  const dispatch = useDispatch();

  const handleClose = (e) => {
    history.replace("/products");
  };

  const handleSubmit = (values) => {
    const reqPayload = formateRequestPayload(values);
    if (props.match.params.id) {
      updateProducts(props.match.params.id, reqPayload, dispatch, () => {
        history.replace("/products");
      });
    } else {
      createProducts(reqPayload, dispatch, () => {
        history.replace("/products");
      });
    }
  };

  if (props.match.params.id && !products?.id) return "Loading...";

  return (
    <div className="gx-main-content-wrapper">
      <Card className="gx-card" title={`${header} Product`}>
        <Formik
          innerRef={formikRef}
          initialValues={{
            ...initialValues,
            name: products.name,
            description: products.description,
            product_variant: products?.product_variant,
            brand: products.brand?.id,
            brandObj: products.brand,
            // row_status: products.row_status?.id,
            website: products.website,
            market: products.market,
            date_published: moment(products.date_published).format(
              "YYYY-MM-DD"
            ),
            parent_category: products.category?.parent,
            category: String(products.category?.id),
            categoryObj: products?.category,
            price_us_dollars: products.price_us_dollars,
            price_euros: products.price_euros,
            positioning_claims: Array.isArray(products?.positioning_claims)
              ? products?.positioning_claims.map((c) => c.id)
              : [],
            positioningClaimsObj: products.positioning_claims,
            storage: products.storage,
            unit_pack_size: products.unit_pack_size,
            packaging_units: products.packaging_units?.id,
            packagingUnitsObj: products.packaging_units,
            package_type: products.package_type?.id,
            package_material: products.package_material?.id,
            packageMaterialObj: products.package_material,
            nutrition: products.nutrition,
            number_of_variants: products.number_of_variants,
            launch_type: products.launch_type?.id,
            launchTypeObj: products.launch_type,
            private_label: products.private_label?.id,
            privateLabelObj: products.private_label,
            currency: products.currency?.id,
            price_local_currency: products.price_local_currency,
            bar_code: products.bar_code,
            production_code: products.production_code,
            flavours: products.flavours?.id,
            flavoursObj: products.flavours,
            fragrances: products.fragrances?.id,
            fragrancesObj: products.fragrances,
            primary_image: products.primary_image,
            // primary_image_upload: products.primary_image,
            hyperlink: products.hyperlink,
            ingredientsObj: products.ingredients,
            ingredients: Array.isArray(products?.ingredients)
              ? products?.ingredients.map((c) => c.id)
              : [],
            control_method: products.control_method,
            // // news: products.news,

            news: Array.isArray(products?.news)
              ? products?.news.map((c) => c)
              : [],
            // // awards: products.awards,

            awards: Array.isArray(products?.awards)
              ? products?.awards.map((c) => c)
              : [],
            ownership_pattern: products.ownership_pattern,
            // revenue: products.revenue,
            product_funding: products.product_funding,
            sustainablity: products.sustainablity,
            technology_type: products.technology_type?.id,
            // approval_date: moment(product.approval_date).format("YYYY-MM-DD"),
            status: true,
          }}
          validationSchema={productUtils}
          onSubmit={(values, { setSubmitting }) => {
            handleSubmit(values);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleSubmit,
            setFieldValue,
          }) => (
            <Form name="register" scrollToFirstError onSubmit={handleSubmit}>
              {console.log({ errors })}
              <InputGroup
                name="name"
                label="Name: "
                type="text"
                value={values.name}
                onChange={handleChange}
                error={touched.name ? errors.name : ""}
              />
              <div className="gx-form-group">
                <label htmlFor="description" className="gx-form-label">
                  Description:
                </label>
                <div style={{ flex: 1 }}>
                  <Editor
                    editorStyle={{
                      width: "100%",
                      minHeight: 100,
                      borderWidth: 1,
                      borderStyle: "solid",
                      borderColor: "lightgray",
                    }}
                    wrapperClassName="demo-wrapper"
                    onEditorStateChange={(ed) =>
                      handleEditor(ed, "description", setFieldValue)
                    }
                    defaultEditorState={EditorState.createWithContent(
                      ContentState.createFromBlockArray(
                        convertFromHTML(values?.description ?? "")
                      )
                    )}
                  />

                  {!values.description && (
                    <Typography.Text type="danger">
                      {"Please select description"}
                    </Typography.Text>
                  )}
                </div>
              </div>
              <InputGroup
                name="product_variant"
                label="Variant"
                value={values.product_variant}
                onChange={handleChange}
                error={touched.product_variant ? errors.product_variant : ""}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">Brand: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    value={values?.brand}
                    placeholder="Select Brand"
                    onSelect={(val, option) => {
                      setFieldValue("brand", val);
                      if (!option?.details?.id) {
                        setFieldValue("brandObj", getBrandObj({ name: val }));
                      } else {
                        setFieldValue("brandObj", option?.details);
                      }
                    }}
                    options={getFormatedOptions(brands)}
                    onDeselect={() => {
                      setFieldValue("brandObj", undefined);
                      setFieldValue("brand", undefined);
                    }}
                    optionFilterProp="label"
                  />
                  {!values.brand && (
                    <Typography.Text type="danger">
                      {"Please select brand"}
                    </Typography.Text>
                  )}
                </div>
                {/* {brandOption}
                </Select> */}
              </div>

              <InputGroup
                name="website"
                label="Website"
                value={values.website}
                onChange={handleChange}
                error={touched.website ? errors.website : ""}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">Market: </label>
                <div style={{ flex: 1 }}>
                  <Cascader
                    autoComplete="true"
                    showSearch={true}
                    className="gx-w-100"
                    value={Array.isArray(values?.market) ? values?.market : []}
                    placeholder="Select Market"
                    options={getFormatedOptions(countries)}
                    onChange={(val) => setFieldValue("market", val)}
                    name="market"
                  />
                  {!values.market && (
                    <Typography.Text type="danger">
                      {"Please select market"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              <div className="gx-form-group">
                <label htmlFor="date" className="gx-form-label">
                  Date published:
                </label>
                <div style={{ flex: 1 }}>
                  <DatePicker
                    onChange={(date, dateString) => {
                      setFieldValue("date_published", dateString);
                    }}
                    defaultValue={
                      products.date_published
                        ? new moment(products.date_published)
                        : null
                    }
                    name="date"
                    className="gx-mb-3 gx-w-100"
                  />
                  {!values.date_published && (
                    <Typography.Text type="danger">
                      {"Please select date published"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              <div className="gx-form-group">
                <label className="gx-form-label">Parent Category: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    maxTagCount={1}
                    className="gx-w-100"
                    value={values?.parent_category}
                    placeholder="Select Parent Category"
                    options={getFormatedOptions(
                      categoryModule.filter(
                        (c) => c?.module === moduleCode && c?.parent === null
                      )
                    )}
                    onSelect={(val, option) => {
                      if (!option?.details?.id) {
                        createNewCategory(
                          {
                            name: val,
                            code: val.toUpperCase(),
                            parent: null,
                            module: moduleCode,
                            status: true,
                          },
                          dispatch,
                          (saved) => {
                            setFieldValue("parent_category", saved?.id);
                          }
                        );
                      } else {
                        setFieldValue("parent_category", val);
                      }
                      setFieldValue("category", undefined);
                      setFieldValue("categoryObj", undefined);
                    }}
                    name="category"
                    onDeselect={() => {
                      setFieldValue("parent_category", undefined);
                      setFieldValue("parent_categoryObj", undefined);
                      setFieldValue("category", undefined);
                      setFieldValue("categoryObj", undefined);
                    }}
                    optionFilterProp="label"
                  />
                  {!values.parent_category && (
                    <Typography.Text type="danger">
                      {"Please select parent category"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              {values?.parent_category && (
                <div className="gx-form-group">
                  <label className="gx-form-label">Category: </label>
                  <div style={{ flex: 1 }}>
                    <Select
                      mode="tags"
                      maxTagCount={1}
                      className="gx-w-100"
                      value={values?.category}
                      placeholder="Select  Category"
                      options={getFormatedOptions(
                        categoryModule
                          .filter(
                            (c) =>
                              c?.module === moduleCode &&
                              c?.parent === values?.parent_category
                          )
                          .map((d) => ({ ...d, id: String(d?.id) }))
                      )}
                      onSelect={(val, option) => {
                        setFieldValue("category", val);
                        if (option?.details?.id) {
                          setFieldValue("categoryObj", option?.details);
                        } else {
                          setFieldValue(
                            "categoryObj",
                            getCategoryObj({
                              name: val,
                              parent: values?.parent_category,
                              module: moduleCode,
                            })
                          );
                        }
                      }}
                      name="category"
                      onDeselect={() => {
                        setFieldValue("category", undefined);
                        setFieldValue("categoryObj", undefined);
                      }}
                      optionFilterProp="label"
                    />
                    {!values.category && (
                      <Typography.Text type="danger">
                        {"Please select category"}
                      </Typography.Text>
                    )}
                  </div>
                </div>
              )}

              <InputGroup
                name="price_us_dollars"
                label="Price us dollars"
                type="number"
                value={values.price_us_dollars}
                onChange={handleChange}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">Positioning claims: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    className="gx-w-100"
                    mode="multiple"
                    // defaultValue={newsId && product.positioning}
                    value={values?.positioning_claims}
                    placeholder="Select Positioning claims"
                    onChange={(val, option) => {
                      setFieldValue("positioning_claims", val);
                    }}
                    options={getFormatedOptions(positioning_claims)}
                    optionFilterProp="label"
                  />
                </div>
                {/* {positioningClaimsOption}
                </Select> */}
              </div>

              <InputGroup
                name="storage"
                label="Storage"
                value={values.storage}
                onChange={handleChange}
                error={touched.storage ? errors.storage : ""}
              />

              <InputGroup
                name="price_euros"
                label="Price euros "
                type="number"
                value={values.price_euros}
                onChange={handleChange}
              />

              <InputGroup
                name="unit_pack_size"
                label="Unit pack size"
                type="number"
                value={values.unit_pack_size}
                onChange={handleChange}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">Packaging units: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    // defaultValue={newsId && product.packaging_units}
                    value={values?.packaging_units}
                    placeholder="Select Packaging units"
                    onSelect={(val, option) => {
                      setFieldValue("packaging_units", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "packagingUnitsObj",
                          packagingUnitsObj({ name: val })
                        );
                      } else {
                        setFieldValue("packagingUnitsObj", option?.details);
                      }
                    }}
                    options={getFormatedOptions(units)}
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("packaging_units", undefined);
                      setFieldValue("packagingUnitsObj", undefined);
                    }}
                  />

                  {!values.packaging_units && (
                    <Typography.Text type="danger">
                      {"Please select package material"}
                    </Typography.Text>
                  )}
                </div>
                {/* {parentTagOption}
                </Select> */}
              </div>

              <div className="gx-form-group">
                <label className="gx-form-label">Package type: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    // defaultValue={newsId && product.package_type}
                    value={values?.package_type}
                    placeholder="Select Package type"
                    onSelect={(val, option) => {
                      setFieldValue("package_type", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "packageTypeObj",
                          getCommonObj({ name: val })
                        );
                      } else {
                        setFieldValue("packageTypeObj", {
                          ...option?.details,
                          code: option?.details?.code
                            ? option?.details?.code
                            : option?.details?.name?.toUpperCase(),
                          description: option?.details?.description
                            ? option?.details?.description
                            : option?.details?.name?.toUpperCase(),
                        });
                      }
                    }}
                    options={getFormatedOptions(package_type)}
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("package_type", undefined);
                      setFieldValue("packageTypeObj", undefined);
                    }}
                  />
                  {!values.package_type && (
                    <Typography.Text type="danger">
                      {"Please select package type"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              <div className="gx-form-group">
                <label className="gx-form-label">Package material: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    // defaultValue={newsId && product.package_material}
                    value={values?.package_material}
                    placeholder="Select Package material"
                    onSelect={(val, option) => {
                      setFieldValue("package_material", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "packageMaterialObj",
                          getCommonObj({ name: val })
                        );
                      } else {
                        setFieldValue("packageMaterialObj", {
                          ...option?.details,
                          code: option?.details?.code
                            ? option?.details?.code
                            : option?.details?.name?.toUpperCase(),
                          description: option?.details?.description
                            ? option?.details?.description
                            : option?.details?.name?.toUpperCase(),
                        });
                      }
                    }}
                    options={getFormatedOptions(package_material)}
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("package_material", undefined);
                      setFieldValue("packageMaterialObj", undefined);
                    }}
                  />
                  {!values.package_material && (
                    <Typography.Text type="danger">
                      {"Please select package material"}
                    </Typography.Text>
                  )}
                </div>
                {/* {packageMaterialOption}
                </Select> */}
              </div>

              <div className="gx-form-group">
                <label htmlFor="nutrition" className="gx-form-label">
                  Nutrition:
                </label>
                <div style={{ flex: 1 }}>
                  <Editor
                    editorStyle={{
                      width: "100%",
                      minHeight: 100,
                      borderWidth: 1,
                      borderStyle: "solid",
                      borderColor: "lightgray",
                    }}
                    wrapperClassName="demo-wrapper"
                    onEditorStateChange={(ed) =>
                      handleEditor(ed, "nutrition", setFieldValue)
                    }
                    defaultEditorState={EditorState.createWithContent(
                      ContentState.createFromBlockArray(
                        convertFromHTML(values?.nutrition ?? "")
                      )
                    )}
                  />
                  {!values.nutrition && (
                    <Typography.Text type="danger">
                      {"Please select nutrition"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              <InputGroup
                name="number_of_variants"
                label="Number of variants"
                type="number"
                value={values.number_of_variants}
                onChange={handleChange}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">Launch type: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    value={values?.launch_type}
                    placeholder="Select Launch type"
                    onSelect={(val, option) => {
                      setFieldValue("launch_type", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "launchTypeObj",
                          getCommonObj({ name: val })
                        );
                      } else {
                        setFieldValue("launchTypeObj", {
                          ...option?.details,
                          code: option?.details?.code
                            ? option?.details?.code
                            : option?.details?.name?.toUpperCase(),
                          description: option?.details?.description
                            ? option?.details?.description
                            : option?.details?.name?.toUpperCase(),
                        });
                      }
                    }}
                    options={getFormatedOptions(launch_type)}
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("launch_type", undefined);
                      setFieldValue("launchTypeObj", undefined);
                    }}
                  />
                  {!values.launch_type && (
                    <Typography.Text type="danger">
                      {"Please select launch type"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              <div className="gx-form-group">
                <label className="gx-form-label">Private label: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    // defaultValue={newsId && product.private_label}
                    value={values?.private_label}
                    placeholder="Select Private label"
                    onSelect={(val, option) => {
                      setFieldValue("private_label", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "privateLabelObj",
                          getCommonObj({ name: val })
                        );
                      } else {
                        setFieldValue("privateLabelObj", {
                          ...option?.details,
                          code: option?.details?.code
                            ? option?.details?.code
                            : option?.details?.name?.toUpperCase(),
                          description: option?.details?.description
                            ? option?.details?.description
                            : option?.details?.name?.toUpperCase(),
                        });
                      }
                    }}
                    options={getFormatedOptions(private_label)}
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("private_label", undefined);
                      setFieldValue("privateLabelObj", undefined);
                    }}
                  />
                  {!values.private_label && (
                    <Typography.Text type="danger">
                      {"Please select private label"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              <div className="gx-form-group">
                <label className="gx-form-label">Currency: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    className="gx-w-100"
                    // defaultValue={newsId && product.currency}
                    value={values?.currency}
                    placeholder="Select Currency"
                    onSelect={(val, option) => {
                      setFieldValue("currency", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "currencyObj",
                          getBrandObj({ name: val })
                        );
                      } else {
                        setFieldValue("currencyObj", option?.details);
                      }
                    }}
                    options={getFormatedOptions(currency)}
                  />
                </div>
              </div>

              <InputGroup
                name="price_local_currency"
                label="Price local currency"
                type="number"
                value={values.price_local_currency}
                onChange={handleChange}
              />

              <InputGroup
                name="bar_code"
                label="Bar code"
                type="number"
                value={values.bar_code}
                onChange={handleChange}
              />

              <InputGroup
                name="production_code"
                label="Production code"
                value={values.production_code}
                onChange={handleChange}
                error={touched.production_code ? errors.production_code : ""}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">Flavours: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    value={values?.flavours}
                    placeholder="Select Flavours"
                    onSelect={(val, option) => {
                      setFieldValue("flavours", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "flavoursObj",
                          getCommonObj({ name: val })
                        );
                      } else {
                        setFieldValue("flavoursObj", {
                          ...option?.details,
                          code: option?.details?.code
                            ? option?.details?.code
                            : option?.details?.name?.toUpperCase(),
                          description: option?.details?.description
                            ? option?.details?.description
                            : option?.details?.name?.toUpperCase(),
                        });
                      }
                    }}
                    options={getFormatedOptions(flavours)}
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("flavours", undefined);
                      setFieldValue("flavoursObj", undefined);
                    }}
                  />
                  {!values.flavours && (
                    <Typography.Text type="danger">
                      {"Please select category"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              <div className="gx-form-group">
                <label className="gx-form-label">Fragrances: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    mode="tags"
                    className="gx-w-100"
                    // defaultValue={newsId && product.fragrances}
                    value={values?.fragrances}
                    placeholder="Select Fragrances"
                    onSelect={(val, option) => {
                      setFieldValue("fragrances", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "fragrancesObj",
                          getCommonObj({ name: val })
                        );
                      } else {
                        setFieldValue("fragrancesObj", {
                          ...option?.details,
                          code: option?.details?.code
                            ? option?.details?.code
                            : option?.details?.name?.toUpperCase(),
                          description: option?.details?.description
                            ? option?.details?.description
                            : option?.details?.name?.toUpperCase(),
                        });
                      }
                    }}
                    options={getFormatedOptions(fragrances)}
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("fragrances", undefined);
                      setFieldValue("fragrancesObj", undefined);
                    }}
                  />
                  {!values.fragrances && (
                    <Typography.Text type="danger">
                      {"Please select fragrances"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              <InputGroup
                name="primary_image"
                label="Primary image link"
                value={values.primary_image}
                onChange={handleChange}
                error={touched.primary_image ? errors.primary_image : ""}
              />

              <div className="gx-form-group">
                <label htmlFor="primary_image_upload" className="gx-form-label">
                  Primary image upload:
                </label>
                <div style={{ flex: 1 }}>
                  <Input
                    name="primary_image_upload"
                    encType="multipart/form-data"
                    type="file"
                    accept="image/*"
                    placeholder="Add Image"
                  />
                </div>
              </div>

              <InputGroup
                name="hyperlink"
                label="Hyperlink"
                value={values.hyperlink}
                onChange={handleChange}
                error={touched.hyperlink ? errors.hyperlink : ""}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">Ingredients: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    className="gx-w-100"
                    mode="multiple"
                    value={values?.ingredients}
                    placeholder="Select Ingredients"
                    onChange={(val) => {
                      setFieldValue("ingredients", val);
                    }}
                    options={getFormatedOptions(ingredients)}
                    optionFilterProp="label"
                  />
                </div>
              </div>

              <InputGroup
                name="control_method"
                label="Control method"
                value={values.control_method}
                onChange={handleChange}
                error={touched.control_method ? errors.control_method : ""}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">News: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    className="gx-w-100"
                    mode="multiple"
                    // defaultValue={newsId && product.news}
                    value={values?.news}
                    placeholder="Select News"
                    onChange={(val, option) => {
                      setFieldValue("news", val);
                    }}
                    options={getFormatedOptions(news)}
                  />
                </div>
                {/* {newsOption}
                </Select> */}
              </div>

              <div className="gx-form-group">
                <label className="gx-form-label">Awards: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    className="gx-w-100"
                    mode="multiple"
                    // defaultValue={newsId && product.tags}
                    value={values?.tags}
                    placeholder="Select Awards"
                    onChange={(val, option) => {
                      setFieldValue("awards", val);
                    }}
                    options={getFormatedOptions(awards.results, "title")}
                  />
                </div>
                {/* {awardsOption}
                </Select> */}
              </div>

              <InputGroup
                name="ownership_pattern"
                label="Ownership pattern"
                value={values.ownership_pattern}
                onChange={handleChange}
                error={
                  touched.ownership_pattern ? errors.ownership_pattern : ""
                }
              />
              <InputGroup
                name="revenue"
                label="Revenue"
                value={values.revenue}
                onChange={handleChange}
              />

              <InputGroup
                name="product_funding"
                label="Product funding"
                value={values.product_funding}
                onChange={handleChange}
              />

              <InputGroup
                name="sustainablity"
                label="Sustainablity: "
                type="text"
                value={values.sustainablity}
                onChange={handleChange}
              />

              <div className="gx-form-group">
                <label className="gx-form-label">Technology type: </label>
                <div style={{ flex: 1 }}>
                  <Select
                    className="gx-w-100"
                    // defaultValue={newsId && product.technology_type}
                    value={values?.technology_type}
                    placeholder="Select Technology type"
                    options={getFormatedOptions(tag_view)}
                    onSelect={(val, option) => {
                      setFieldValue("technology_type", val);
                      if (!option?.details?.id) {
                        setFieldValue(
                          "technologyTypeObj",
                          getTechnologyObj({ name: val })
                        );
                      } else {
                        setFieldValue("technologyTypeObj", option?.details);
                      }
                    }}
                    optionFilterProp="label"
                    onDeselect={() => {
                      setFieldValue("technology_type", undefined);
                      setFieldValue("technologyTypeObj", undefined);
                    }}
                  />
                  {!values.technology_type && (
                    <Typography.Text type="danger">
                      {"Please select technology type"}
                    </Typography.Text>
                  )}
                </div>
              </div>

              {/* <div className="gx-form-group">
                <label htmlFor="date" className="gx-form-label">
                  Approval date:
                </label>
                <div style={{ flex: 1 }}>
                  <DatePicker
                    name="approval_date"
                    // value={products.approval_date}
                    defaultValue={moment(products.approval_date, "DD-MM-YYYY")}
                    onChange={(date, dateString) => {
                      setFieldValue("approval_date", dateString);
                    }}
                    format={["DD-MM-YYYY", "DD-MM-YY"]}
                    // placeholder={product.approval_date}
                    className="gx-mb-3 gx-w-100"
                  />

                  {!values.approval_date && (
                    <Typography.Text type="danger">
                      {"Please select approval date"}
                    </Typography.Text>
                  )}
                </div>
              </div> */}

              <Button
                onClick={handleSubmit}
                style={{ float: "right" }}
                className="gx-w-20"
                type="primary"
                htmlType="submit"
              >
                Submit Details
              </Button>
              <Button
                onClick={handleClose}
                style={{ float: "right", marginRight: "15px" }}
                className="gx-w-20"
                type="primary"
                htmlType="submit"
              >
                Close
              </Button>
            </Form>
          )}
        </Formik>
      </Card>
    </div>
  );
};

export default ProductNews;
