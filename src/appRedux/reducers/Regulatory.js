import { CREATE_REGULATORY_SUCCESS, EDIT_REGULATORY_DETAILS, GET_ALL_REGULATORIES_SUCCESS, GET_REGULATORY_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_REGULATORY_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    regulatories: [],
    regulatory: {},
    showRemove: false,
    showEdit: false
}

export default function regulatoryReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_REGULATORY_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_REGULATORIES_SUCCESS: {
            return {
                ...state,
                loader: false,
                regulatories: action.payload
            }
        }
        case GET_REGULATORY_SUCCESS: {
            return {
                ...state,
                loader: false,
                regulatory: action.payload[0]
            }
        }
        case CREATE_REGULATORY_SUCCESS: {
            return {
                ...state,
                loader: false,
                regulatory: action.payload
            }
        }
        case UPDATE_REGULATORY_SUCCESS: {
            return {
                ...state,
                loader: false,
                regulatory: action.payload
            }
        }    
        default:
            return state;
    }
}
