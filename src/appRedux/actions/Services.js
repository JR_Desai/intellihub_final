import axios from 'axios';
import { CREATE_SERVICE_SUCCESS, GET_ALL_SERVICES_SUCCESS, GET_SERVICE_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_SERVICE_SUCCESS } from '../../constants/ActionTypes';

const module_link = "service"

export const createService = async (data , dispatch) => {
        try {
                const service = await axios.post(`/${module_link}/create/`, data);
                dispatch({
                        type: CREATE_SERVICE_SUCCESS,
                        payload: service.data
                })
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllServices = async (dispatch) => {
        try {
                const services = await axios.get(`/${module_link}/view/`);
                dispatch({
                        type: GET_ALL_SERVICES_SUCCESS,
                        payload: services.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getService = async (serviceID, dispatch) => {
        try {
                const service = await axios.get(`/${module_link}/view/?id=${serviceID}`);
                dispatch({
                        type: GET_SERVICE_SUCCESS,
                        payload: service.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getServiceTags = async (dispatch) => {
        try {
                const service = await axios.get(`/tag/view/`);
                // dispatch({
                //         type: UPDATE_RAW_MATERIAL_SUCCESS,
                //         payload: service.data.results
                // })
        } catch (err) {
                console.log(err);
        }
};

export const updateService = async (serviceID, data, dispatch) => {
        try {
                const service = await axios.put(`/${module_link}/update/${serviceID}`, data);
                dispatch({
                        type: UPDATE_SERVICE_SUCCESS,
                        payload: service.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusService = async (serviceID, data, dispatch) => {
        try {
                const service = await axios.patch(`/${module_link}/update/${serviceID}`, {
                        row_status: data.row_status
                }
                );
                dispatch({
                        type: UPDATE_SERVICE_SUCCESS,
                        payload: service.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const deleteService = async (serviceID, dispatch) => {
        try {
                await axios.delete(`/${module_link}/delete/${serviceID}`);
                dispatch ({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
        } catch (err) {
                console.log(err);
        }
};
