import React from "react";
import {
  Drawer,
  IconButton,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import { ChevronLeft, Menu } from "@material-ui/icons";
import { AutoComplete } from "antd";
import { FiSearch } from "react-icons/fi";
import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import {
  getAllCategoryModule,
  getAllCountries,
  getAllFlavour,
  getAllFragrance,
  getAllIngredient,
  getAllPositioningClaims,
} from "../../../../appRedux/actions";
import { useDispatch, useSelector } from "react-redux";
import { getAllCompanies } from "../../../../appRedux/actions/Companies";

const filterMainItems = [
  {
    name: "Category",
    module: "category",
    id: "categoryModule",
  },
  {
    name: "Sub Category",
    module: "sub_category",
    id: "subCategoryModule",
  },
  {
    name: "Region",
    module: "market",
    id: "countries",
  },
  {
    name: "Date",
    module: "date",
    id: "date",
  },
  {
    name: "Positioning Claim",
    module: "positioning_claims",
    id: "positioning_claims",
  },
  {
    name: "Ingredients",
    module: "ingredients",
    id: "ingredients",
  },
  {
    name: "Company",
    module: "company",
    id: "company",
  },
  {
    name: "Flavour",
    module: "flavours",
    id: "flavours",
  },
  {
    name: "Fragrance",
    module: "fragrances",
    id: "fragrances",
  },
];

const ProductDrawer = ({
  hqDrawer,
  classes,
  handleDrawerClose,
  //   filters
  toggle,
  clicked,
  tagState,
  handleTagChange,
}) => {
  const dispatch = useDispatch();
  React.useEffect(() => {
    getAllCountries(dispatch);
    getAllCategoryModule(dispatch);
    getAllPositioningClaims(dispatch);
    getAllIngredient(dispatch);
    getAllFragrance(dispatch);
    getAllFlavour(dispatch);
    getAllCompanies(dispatch);
  }, []);

  const configs = useSelector(({ config }) => config);
  const { companies } = useSelector(({ companies }) => companies);

  const [keyword, setKeyword] = React.useState("");

  const renderTags = filterMainItems.map((tag) => {
    let filterChildTag = Array.isArray(configs?.[tag?.id])
      ? configs[tag?.id]
      : [];

    if (tag?.id === "company") {
      filterChildTag = companies;
    }

    if (keyword !== "") {
      filterChildTag = filterChildTag.filter((c) =>
        c.name.toLowerCase().includes(keyword.toLowerCase())
      );
    }
    return (
      <>
        {filterChildTag.length > 0 && (
          <div className="wrap" onClick={() => toggle(tag.id)}>
            <h6>{tag.name}</h6>
            <span>
              {clicked === tag.id || keyword !== "" ? (
                <AiOutlineMinus className="acordian_icon" />
              ) : (
                <AiOutlinePlus className="acordian_icon" />
              )}
            </span>
          </div>
        )}
        {clicked === tag.id || keyword !== ""
          ? filterChildTag.map((childTag) => (
              <div className="checking">
                <div className="acordian_drop">
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={tagState.find(
                          (t) =>
                            t?.id === childTag.id &&
                            t?.name === childTag.name &&
                            t?.module === tag.module
                        )}
                        onChange={(e) =>
                          handleTagChange({
                            id: childTag.id,
                            name: childTag.name,
                            module: tag.module,
                            checked: e.target.checked,
                          })
                        }
                        name={childTag.name}
                      />
                    }
                    label={childTag.name}
                  />
                </div>
              </div>
            ))
          : null}
      </>
    );
  });
  return (
    <Drawer
      variant="persistent"
      anchor="left"
      open={hqDrawer}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className="acordian">
        <div>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeft />
            <Menu />
          </IconButton>
        </div>
        <div className="acordian_header">
          <div className="acordian_search">
            <FiSearch className="acordian_search_icon" />
            <br />
            <AutoComplete
              options={[]}
              style={{
                width: 200,
              }}
              onSelect={(val) => {}}
              onSearch={(val) => setKeyword(val)}
              placeholder="Search here"
            >
              <input
                type="text"
                aria-label="Text input with dropdown button"
                // placeholder="search here"
                className="acordian_ip"
              />
            </AutoComplete>
          </div>
        </div>

        {/***************acordian***************/}
        <div className="acordian_content">{renderTags}</div>
      </div>
    </Drawer>
  );
};

export default ProductDrawer;
