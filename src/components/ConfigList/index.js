import { Button, Switch } from 'antd';
import React, { useState } from 'react';
import ComposeConfig from '../ComposeConfig';


const ConfigList = ({}) => {

    const [openCompose, setOpenCompose] = useState(false);
    let count = 0;

    const list = [
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: false
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: true
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: false
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: false
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: true
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: true
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: true
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: false
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: false
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: true
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: false
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: true
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: false
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: true
        },
        {
            id: count++,
            title: "Demo Name",
            class: "Demoname",
            row_status: false
        },
    ]

    return (
        <div className="gx-grid-container">
            <div className="gx-grid-item-container gx-bg-white">
                <div className="gx-grid-item-header">
                    <span className="gx-grid-item-header-title">Categories</span>
                    <Button onClick={() => setOpenCompose(true)} type="text" className="gx-text-primary">Add Category</Button>
                </div>
                <div className="gx-grid-item-body">
                    {list.map((item, index) => {
                        return <div key={item.id} className="gx-grid-item gx-pt-3 gx-pb-3">
                            <span className="gx-grid-item-title">{item.title}</span>
                            <span className="gx-text-grey">{item.class}</span>
                            <Switch checkedChildren="enabled" unCheckedChildren="disabled" defaultChecked={item.row_status} onChange={(value) => item.row_status = value } />
                        </div>
                    })}
                </div>
            </div>
            <ComposeConfig open={openCompose} onClose={setOpenCompose} />
        </div>
    )
}

export default ConfigList;
