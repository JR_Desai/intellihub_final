import { CREATE_NEWS_SUCCESS, EDIT_NEWS_DETAILS, GET_ALL_NEWS_SUCCESS, GET_NEWS_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_NEWS_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    news: [],
    newnews: {},
    showRemove: false,
    showEdit: false
}

export default function newsReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_NEWS_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_NEWS_SUCCESS: {
            return {
                ...state,
                loader: false,
                news: action.payload
            }
        }
        case GET_NEWS_SUCCESS: {
            return {
                ...state,
                loader: false,
                newnews: action.payload[0]
            }
        }
        case CREATE_NEWS_SUCCESS: {
            return {
                ...state,
                loader: false,
                newnews: action.payload
            }
        }
        case UPDATE_NEWS_SUCCESS: {
            return {
                ...state,
                loader: false,
                newnews: action.payload
            }
        }    
        default:
            return state;
    }
}
