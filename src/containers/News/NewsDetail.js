import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import DOMPurify from 'dompurify';
import {Avatar, Badge, Col, Row, Image} from "antd";
import ConfirmPopup from '../../components/ConfirmPopup';
import { showConfirmRemove } from '../../appRedux/actions/Companies';
import Widget from '../../components/Widget';
import { deleteNews, getNews } from '../../appRedux/actions';

const NewsDetail = props => {

    const dispatch = useDispatch();

    const history = useHistory();

    const {newnews, showRemove} = useSelector(({news}) => news);

    const newsId = props.match.params.id;

    useEffect(() => {
        getNews(newsId, dispatch);
    }, [])

    const editTech = () => {
        history.push(`update/${newsId}`)
    }

    const onRemove = () => {
        deleteNews(newsId, dispatch);
        history.replace("/news");
    }

    const removeTech = () => {
        showConfirmRemove(true, dispatch);
        
    }
    const createMarkup = (html) => {
        return  {
            __html: DOMPurify.sanitize(html)
        }
    }
console.log(newnews)
    return (
        <div className="gx-main-content-wrapper">
            <div className="gx-module-detail gx-module-list">
                <div className="gx-module-detail-item gx-module-detail-header">
                    <Row>
                        <Col xs={24} sm={12} md={17} lg={12} xl={17}>
                            <div className="gx-flex-row">
                            <div className="gx-user-name gx-mr-md-4 gx-mr-2 gx-my-1">
                                <div className="gx-flex-row gx-align-items-center gx-pointer">
                                    <Avatar className="gx-mr-3" src="" alt="" />
                                    <h4 className="gx-mb-0">Demo Name</h4>
                                </div>
                            </div>
                            </div>
                        </Col>

                        <Col xs={24} sm={12} md={7} lg={12} xl={7}>
                            <div className="gx-flex-row gx-justify-content-end">
                                <i className="gx-icon-btn icon icon-edit" onClick={() =>
                                    editTech()}
                                />
                                <i className="gx-icon-btn icon icon-trash" onClick={() =>
                                    removeTech()}
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
                <Widget className="gx-module-detail-item">
                    <div className="gx-form-group gx-flex-row gx-align-items-center gx-flex-nowrap">
                        <Col className="gx-flex-row gx-flex-1 gx-flex-nowrap">
                            <div className="gx-w-25">
                                <Image
                                className='' 
                                src={newnews.upload_image || newnews.image_link}
                                alt="No image Found"
                                preview={false}
                                 />
                            </div>
                            <div className="gx-w-75 gx-ml-5">
                                <div className="h1">
                                    {newnews.headline}
                                </div>
                                    <span className="gx-ml-auto gx-mb-3">{newnews.date}</span>
                                <div className="gx-mt-3 gx-mb-5">
                                    <span className="h3">Short Description: </span>
                                    <p className="gx-text-grey" dangerouslySetInnerHTML={createMarkup(newnews.short_description)}>
                                    </p>
                                </div>
                                <div className="gx-mt-3 gx-mb-5">
                                    <span className="h3">Long Description: </span>
                                    <p className="gx-text-grey" dangerouslySetInnerHTML={createMarkup(newnews.long_description)}>
                                    </p>
                                </div>
                                <div className="gx-mt-4 gx-mb-auto">
                                    
                                    Tags:
                                    {newnews["tag"]?.map((label, index) => {
                                        return <Badge key={index} count={label.name} style={{backgroundColor: "#FD0090", margin: "0 5px"}} />
                                    })}
                                </div>
                            </div>
                        </Col>
                    </div>
                </Widget>
                <ConfirmPopup showStatus={showRemove} onConfirm={onRemove} />
            </div>
        </div>
    )
}

export default NewsDetail;
