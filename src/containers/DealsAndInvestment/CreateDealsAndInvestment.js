import React, { useEffect, useState } from 'react';
import { Button, Form, Card, Select, DatePicker, Input } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import InputGroup from '../../components/InputGroup';
import { useHistory } from 'react-router';
import { createDealsAndInvestment, getDealsAndInvestment, updateDealsAndInvestments, getAcquisitionStatus, getAcquisitionType } from "../../appRedux/actions";
import { getAllCompanyNames } from '../../appRedux/actions/Companies';
import TextArea from 'antd/lib/input/TextArea';

import { convertToRaw, EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";

const CreateDealsAndInvestmemnt = props => {

    const Option = Select.Option;

    const history = useHistory();

    const { deal, acquisitionStatus, acquisitionType } = useSelector(({ deals }) => deals);

    const { companyNames } = useSelector(({ companies }) => companies);

    const companiesOption = companyNames ?.map(
        (company, _) => <Option key={company.id}>{company.name}</Option>
    );

    const acquisitionStatusOption = acquisitionStatus ?.map(
        (acquisitionStatus, _) => <Option key={acquisitionStatus.id}>{acquisitionStatus.name}</Option>
    );

    const acquisitionTypeOption = acquisitionType ?.map(
        (acquisitionType, _) => <Option key={acquisitionType.id}>{acquisitionType.name}</Option>
    );

    useEffect(() => {
        getAllCompanyNames(dispatch);
        getAcquisitionStatus(dispatch);
        getAcquisitionType(dispatch);
        dealsAndInvestmentId && getDealsAndInvestment(dealsAndInvestmentId, dispatch);
    }, []);

    const dealsAndInvestmentId = props.match.params.id;

    const initState = {
        company: null,
        acquisition_status: null,
        acquired_by: "",
        acquired_url: "",
        announced_date: null,
        acquisition_type: null,
        acquisition_terms: "",
        acquisition_count: null,
        row_status: 3
    }


    if (dealsAndInvestmentId) {
        if (deal.acquisition_status && deal.acquisition_status.id) {
            deal.acquisition_status = deal.acquisition_status.id;
        }
        if (deal.acquisition_type && deal.acquisition_type.id) {
            deal.acquisition_type = deal.acquisition_type.id;
        }
        if (deal.company && deal.company.id) {
            deal.company = deal.company.id;
        }
        if (deal.row_status && deal.row_status.id) {
            deal.row_status = deal.row_status.id;
        }
    }

    const value = dealsAndInvestmentId ? deal : initState;

    const header = dealsAndInvestmentId ? "Update" : "Create";

    const [dealsAndInvestment, setDealsAndInvestment] = useState(value);
    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        dealsAndInvestmentId ? updateDealsAndInvestments(dealsAndInvestmentId, dealsAndInvestment, dispatch) : createDealsAndInvestment(dealsAndInvestment, dispatch);
        history.replace("/dai");
    }
    const handleClose = (e) => {
        history.replace("/dai");
    }

    const handleEditorTerms = (editorState) => {
        var htmlData = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        setDealsAndInvestment({ ...dealsAndInvestment, acquisition_terms: htmlData })
    }

    return (
        <div className="gx-main-content-wrapper">
            <Card className="gx-card" title={`${header} Deals And Investment`}>
                <Form
                    name="register"
                    scrollToFirstError
                >
                    <div className="gx-form-group">
                        <label className="gx-form-label">Companies: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={dealsAndInvestmentId && dealsAndInvestment.company}
                            placeholder="Select Companies"
                            onChange={(value) => setDealsAndInvestment({ ...dealsAndInvestment, company: value })}
                        >
                            {companiesOption}
                        </Select>
                    </div>

                    <div className="gx-form-group">
                        <label className="gx-form-label">Acquisition status: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={dealsAndInvestmentId && dealsAndInvestment.acquisition_status}
                            placeholder="Select Status"
                            onChange={(value) => setDealsAndInvestment({ ...dealsAndInvestment, acquisition_status: value })}
                        >
                            {acquisitionStatusOption}
                        </Select>
                    </div>


                    <InputGroup name="acquired_by" label="Acquired by: " type="text" value={dealsAndInvestment.acquired_by}
                        onChange={(e) => setDealsAndInvestment({ ...dealsAndInvestment, acquired_by: e.target.value })}
                    />

                    <InputGroup name="acquired_url" label="Acquired url: " type="text" value={dealsAndInvestment.acquired_url}
                        onChange={(e) => setDealsAndInvestment({ ...dealsAndInvestment, acquired_url: e.target.value })}
                    />

                    <div className="gx-form-group">
                        <label htmlFor="announced_date" className="gx-form-label">Announced date:</label>
                        <DatePicker name="announced_date" placeholder={dealsAndInvestment.announced_date}
                            onChange={(e) => setDealsAndInvestment({ ...dealsAndInvestment, announced_date: e.format("YYYY-MM-DD") })} className="gx-mb-3 gx-w-100"
                        />
                    </div>

                    <div className="gx-form-group">
                        <label className="gx-form-label">Acquisition type: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={dealsAndInvestmentId && dealsAndInvestment.acquisition_type}
                            placeholder="Select Type"
                            onChange={(value) => setDealsAndInvestment({ ...dealsAndInvestment, acquisition_type: value })}
                        >
                            {acquisitionTypeOption}
                        </Select>
                    </div>

                    <div className="gx-form-group">
                        <label name="acquisition_terms" className="gx-form-label">Acquisition terms: </label>
                        {/* <TextArea
                            name="acquisition_terms"
                            value={dealsAndInvestment.acquisition_terms}
                            rows={4}
                            onChange={(e) => setDealsAndInvestment({ ...dealsAndInvestment, acquisition_terms: e.target.value })}
                        /> */}
                        <Editor editorStyle={{
                            width: '100%',
                            minHeight: 100,
                            borderWidth: 1,
                            borderStyle: 'solid',
                            borderColor: 'lightgray'
                        }}
                            wrapperClassName="demo-wrapper"
                            onEditorStateChange={handleEditorTerms}
                        />
                    </div>


                    <InputGroup name="acquisition_count" label="Acquisition count: " type="number" value={dealsAndInvestment.acquisition_count}
                        onChange={(e) => setDealsAndInvestment({ ...dealsAndInvestment, acquisition_count: e.target.value })}
                    />
                    <Button onClick={handleSubmit} style={{ float: 'right' }} className="gx-w-20" type="primary" htmlType="submit">
                        Submit Details
                    </Button>
                    <Button onClick={handleClose} style={{ float: 'right', marginRight: '15px' }} className="gx-w-20" type="primary" htmlType="submit">
                        Close
                    </Button>
                </Form>
            </Card>
        </div>
    )

}

export default CreateDealsAndInvestmemnt;