import React from "react";
import { FiPlus } from "react-icons/fi";
import { Link } from "react-router-dom";

const CompareProductPanel = ({
  selectedProductsForComparision,
  setselectedProductsForComparision,
}) => {
  return (
    <div className="productAddToCompareWrap">
      <div className="container">
        <div className="productAddToCompareContent">
          <div className="productAddToCompareItemList">
            <ul>
              {selectedProductsForComparision.map(
                (selectedProdcutForComparision) => {
                  return (
                    <li>
                      <div className="productAddToCompareItem">
                        <div className="productAddToCompareItemImage">
                          <img
                            src={selectedProdcutForComparision?.primary_image}
                            alt=""
                          />
                        </div>
                        <button
                          onClick={() => {
                            setselectedProductsForComparision(
                              [...selectedProductsForComparision].filter(
                                (p) => p.id !== selectedProdcutForComparision.id
                              )
                            );
                          }}
                          class="productCloseIcon"
                        >
                          <FiPlus />
                        </button>
                      </div>
                    </li>
                  );
                }
              )}
            </ul>
          </div>
          <div className="productAddToCompareAction">
            <div className="compareAllBtn">
              <Link
                to={`/user/products-compare?ids=${selectedProductsForComparision
                  .map((p) => p?.id)
                  .join(",")}`}
                className="btn btn-primary"
              >
                Compare All
              </Link>
            </div>
            <div className="compareRemoveBtn">
              <button
                onClick={() => setselectedProductsForComparision([])}
                className="btn btn-white"
              >
                Remove All
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CompareProductPanel;
