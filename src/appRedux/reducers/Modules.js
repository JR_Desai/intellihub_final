import { GET_ALL_MODULES } from "../../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    modules: []
}

export default function moduleReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case GET_ALL_MODULES: {
            return {
                ...state,
                loader: false,
                modules: action.payload,
            }
        }
    
        default:
            return state;
    }
}