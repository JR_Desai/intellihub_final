import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import Regulatory from '../../../containers/Regulatory';
import Research from '../../../containers/Research';
import CreateResearchs from '../../../containers/Research/CreateResearchs';
import ResearchDetail from '../../../containers/Research/ResearchDetail';

const ResearchRoutes = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={Research} />
            <Route exact={true} path={`${match.url}/create`} component={CreateResearchs} />
            <Route exact={true} path={`${match.url}/:id`} component={ResearchDetail} />
            <Route exact={true} path={`${match.url}/update/:id`} component={CreateResearchs} />
        </Switch>
    )
}

export default ResearchRoutes;
