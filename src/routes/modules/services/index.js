import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import Services from '../../../containers/Services';

const ServiceRoutes = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={Services} />
        </Switch>
    )
}

export default ServiceRoutes;
