import { Spin } from "antd";
import React, { useEffect } from "react";
import { FiPlusCircle } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { getAllProducts } from "../../../appRedux/actions";
const queryString = require("query-string");

const ProductsCompare = (props) => {
  const dispatch = useDispatch();

  const history = useHistory();

  const { products } = useSelector(({ products }) => products);
  const { loading } = useSelector(({ common }) => common);

  useEffect(() => {
    var parsed = queryString.parse(props.location.search);
    getAllProducts(dispatch, {
      type: "operator",
      data: {
        attribute: "id",
        operator: "in",
        value: parsed?.ids?.split(","),
      },
    });
  }, [props.location.search]);

  const givenProducts = Array.isArray(products) ? products : [];
  return (
    <Spin spinning={loading}>
      <div className="productCompareWrapper">
        <div className="container">
          <div className="productCompareContent">
            <div className="productCompareHead">
              <h2 className="title-font">
                Compare{" "}
                {givenProducts
                  .map((givenProduct) => givenProduct?.name)
                  .join(", ")}
              </h2>
              <div className="productHeadActionBlock">
                <div className="productDownloadDetails productDownloadPink">
                  <a
                    className="download-dropdown-toggle dropdown-toggle"
                    href="#"
                    id="donwloadActions"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Download
                  </a>
                  <div
                    className="dropdown-menu downloadDropdown"
                    aria-labelledby="donwloadActions"
                  >
                    <ul>
                      <li>
                        <a href="#">All Format</a>
                      </li>
                      <li>
                        <a href="#">Excel</a>
                      </li>
                      <li>
                        <a href="#">PDF</a>
                      </li>
                      <li>
                        <a href="#">Powerpoint</a>
                      </li>
                      <li>
                        <a href="#">Images</a>
                      </li>
                      <li>
                        <a href="#">HTML</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div className="productCompareTableWrap">
              <div className="productCompareTableContent">
                <table className="productCompareTable productCompareTableShadow">
                  <tbody>
                    <tr>
                      <td>
                        <div className="productCompareAddProduct">
                          <a href="#">
                            <i className="addProductIcon">
                              <FiPlusCircle />
                            </i>{" "}
                            Add Product
                          </a>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareProduct">
                            <div className="productCompareImage">
                              <img src={givenProduct?.primary_image} alt="" />
                            </div>
                            <h4 className="title-font">
                              <Link to={`/user/products/${givenProduct?.id}`}>
                                {givenProduct?.name}
                              </Link>
                            </h4>
                          </div>
                        </td>
                      ))}
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div className="productCompareTableWrap">
              <div className="productCompareTableContent">
                <table className="productCompareTable">
                  <tbody>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Record ID</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.id}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Product Variant</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.product_variant}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Brand</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.brand?.name}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Company</h3>
                        </div>
                      </td>

                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.brand?.company?.name}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Parent Company</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.brand?.company?.name}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Market</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.brand?.company?.market}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Date Published</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.date_published}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Category</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.category?.name}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Sub-Category</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.category?.name}</p>
                          </div>
                        </td>
                      ))}
                    </tr>

                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Product Description</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: givenProduct?.description ?? "",
                                }}
                              ></div>
                            </p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Ingredient</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>
                              {Array.isArray(givenProduct?.ingredients)
                                ? givenProduct?.ingredients
                                    .map((i) => i?.name)
                                    .join(", ")
                                : ""}
                            </p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Positioning Claims</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>
                              {Array.isArray(givenProduct?.positioning_claims)
                                ? givenProduct?.positioning_claims
                                    .map((i) => i?.name)
                                    .join(", ")
                                : ""}
                            </p>
                          </div>
                        </td>
                      ))}
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div className="productCompareTableWrap">
              <div className="productCompareTableContent">
                <table className="productCompareTable">
                  <tbody>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Price in US Dollars</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.price_us_dollars}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Price in Euros</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.price_euros}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Storage</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.storage ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Unit Pack Size (ml/g)</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.unit_pack_size ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Packaging Units</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.packaging_units?.code ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>

                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Package Type</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.package_type?.name ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Package Material</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>
                              {givenProduct?.package_material?.name ?? "NA"}
                            </p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Nutrition</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.nutrition ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Number of Variants</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.number_of_variants ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Launch Type</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.launch_type?.name ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div className="productCompareTableWrap">
              <div className="productCompareTableContent">
                <table className="productCompareTable">
                  <tbody>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Company county/State</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>
                              {givenProduct?.brand?.company?.country?.name ?? "NA"}
                            </p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Private Label</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.private_label?.name ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Currency</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.currency?.code ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Price in local currency</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.price_local_currency ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Bar Code</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.bar_code ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Production Code</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.production_code ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Flavours</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.flavours?.name ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Fragrances</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.fragrances?.name ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                    <tr>
                      <td>
                        <div className="productCompareTitle">
                          <h3>Record hyperlink</h3>
                        </div>
                      </td>
                      {givenProducts.map((givenProduct) => (
                        <td>
                          <div className="productCompareInfo">
                            <p>{givenProduct?.hyperlink ?? "NA"}</p>
                          </div>
                        </td>
                      ))}
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Spin>
  );
};

export default ProductsCompare;
