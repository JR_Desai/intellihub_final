import React, { useEffect } from "react";
import {Col, Row} from "antd";
import Overview from "../../components/profile/Overview/index";
import ProfileCard from "../../components/profile/ProfileCard";
import Contact from "../../components/profile/Contact/index";

import Auxiliary from "utils/Auxiliary";
import ProfileHeader from "../../components/profile/ProfileHeader/index";
import ConfirmPopup from "../../components/ConfirmPopup";
import { deleteCompany, getCompany } from "../../appRedux/actions/Companies";
import { useDispatch, useSelector } from "react-redux";
import EventItem from "../../components/profile/ProfileCard/EventItem";
import Widget from "components/Widget/index";
import { useHistory } from "react-router";
import PersonCard from "../../components/profile/PersonCard";

const CompanyProfile = props => {

  const dispatch = useDispatch();

  const history = useHistory();

  const {company, showRemove} = useSelector(({companies}) => companies);

  const companyId = props.match.params.id;

  useEffect(() => {
    getCompany(companyId, dispatch);
  }, [])

  const onRemove = () => {
    deleteCompany(companyId, dispatch);
    history.replace("/companies");
  }

  return (
    <div className="gx-main-content-wrapper">
    <Auxiliary>
      <ProfileHeader companyId={companyId} />
      <div className="gx-profile-content">
        <Row>
          <Col xl={16} lg={14} md={14} sm={24} xs={24}>
            <Overview />
            <ProfileCard title={"Recent News & Development"} companyId={companyId} data={company.news} />
          </Col>

          <Col xl={8} lg={10} md={10} sm={24} xs={24}>
            <Contact />
            {/* <Widget styleName="gx-card-profile">
              <div className="ant-card-head gx-card-tabs">
                <span className="ant-card-head-title gx-mb-1">Headquarter</span>
              </div>
              <div className="gx-pt-md-3">
                    <EventItem desc={company.hq} isAwards={true} isLink={false} />
              </div>
            </Widget>
            <ProfileCard title={"Business Locations"} isAwards={true} isLink={false} />
            <PersonCard />
            <ProfileCard title={"Similar Companies"} isAwards={true} /> */}
          </Col>
        </Row>
      </div>
      <ConfirmPopup showStatus={showRemove} onConfirm={onRemove} />
    </Auxiliary>
    </div>
  );
};

export default CompanyProfile;


