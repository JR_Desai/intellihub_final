import React, { useState } from "react";
import {Button, Checkbox, Form, Input, Select} from "antd";

import UserOutlined from "@ant-design/icons/lib/icons/UserOutlined";
import LockOutlined from "@ant-design/icons/lib/icons/LockOutlined";
import {useDispatch} from "react-redux";
import { admin } from "../../models/admin";
import { Option } from "antd/lib/mentions";
import PhoneInput from "react-phone-input-2";
import { createUser } from "../../appRedux/actions";

const FormItem = Form.Item;

const CreateAdmin = () => {

    const dispatch = useDispatch();

    const initState = {
        firstName: "First Name",
        lastName: "Last Name",
        email: "e@email.com",
        password: "Password",
        country_code: "+91",
        mobile: "987654421",
        user_type: 1,

    }

    const [user, setUser] = useState(initState);

    const adminOptions = admin.map((user, index) => <Option value={user.value}>{user.label}</Option>);

    const onSubmit = () => {
        createUser(user, dispatch);
    }

    return (
        <div className="gx-login-content">
            <div className="gx-login-header gx-text-center">
            <h1 className="gx-login-title">Create Admin</h1>
            </div>
            <Form className="gx-signin-form gx-form-row0">
                <div className="gx-mt-3 gx-mb-3">
                    <Select
                        showSearch
                        style={{width: "100%"}}
                        value={user.user_type}
                        placeholder="Select a person"
                        optionFilterProp="children"
                        onChange={(value) => {setUser({...user, user_type:value})}}
                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                    {adminOptions}
                    </Select>
                </div>
                <div className="gx-mt-4 gx-mb-4">
                    <Input value={user.firstName} prefix={<UserOutlined style={{color: 'rgba(0,0,0,.25)'}}/>}
                        placeholder="First Name"
                        onChange={(e) => setUser({...user, firstName: e.target.value})}
                        />
                </div>
                <div className="gx-mt-4 gx-mb-4">
                    <Input value={user.lastName} prefix={<UserOutlined style={{color: 'rgba(0,0,0,.25)'}}/>}
                        placeholder="Last Name"
                        onChange={(e) => setUser({...user, lastName: e.target.value})}
                        />
                </div>
                <div className="gx-mt-4 gx-mb-4">
                    <Input value={user.email} prefix={<UserOutlined style={{color: 'rgba(0,0,0,.25)'}}/>}
                            placeholder="Email"
                            onChange={(e) => setUser({...user, email: e.target.value})}
                    />
                </div>
                <div className="gx-mt-4 gx-mb-4">
                    <PhoneInput
                        country={'in'}
                        enableSearch={true}
                        inputClass="gx-w-100"
                        inputStyle={{backgroundColor: "#F5F5F5"}}
                        value={user.country_code + user.mobile}
                        onChange={(value, data) => setUser({...user, mobile: value.slice(data.dialCode.length), country_code: "+" + data.dialCode})} 
                    />
                </div>
                <div className="gx-mt-4 gx-mb-4">
                <Input value={user.password} prefix={<LockOutlined style={{color: 'rgba(0,0,0,.25)'}}/>}
                        type="password"
                        onChange={(e) => setUser({...user, password: e.target.value})}
                        placeholder="Password"/>
                </div>
                <div className="gx-mt-4 gx-mb-4">
                    <Input prefix={<LockOutlined style={{color: 'rgba(0,0,0,.25)'}}/>} type="password"
                        placeholder="Confirm Password"
                        />
                </div>
                <Button className="gx-w-100" type="primary" htmlType="submit" onClick={onSubmit}>
                    Create
                </Button>
            </Form>
        </div>
    );
}

export default CreateAdmin;
