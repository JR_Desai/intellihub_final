import { Input } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import React from 'react';

const CreatePerson = ({open, close}) => {
    return (
        <Modal onCancel={() => close(false)} visible={open}>
            <div className="gx-form-group">
                <Input
                    placeholder="Name"
                    // onChange={(event) => setState({to: event.target.value})}
                    margin="normal"
                />
            </div>
            <div className="gx-form-group">
                <Input
                    placeholder="Image_link"
                    // onChange={(event) => setState({to: event.target.value})}
                    margin="normal"
                />
            </div>
            <div className="gx-form-group">
                <Input
                    placeholder="LinkedIn"
                    // onChange={(event) => setState({subject: event.target.value})}
                    margin="normal"
                />
            </div>
        </Modal>
    )
}

export default CreatePerson;
