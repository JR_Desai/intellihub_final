import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import News from '../../../containers/News';
import CreateNews from '../../../containers/News/CreateNews';
import NewsDetail from '../../../containers/News/NewsDetail.js';

const NewsRoutes = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={News} />
            <Route exact={true} path={`${match.url}/create`} component={CreateNews} />
            <Route exact={true} path={`${match.url}/:id`} component={NewsDetail} />
            <Route exact={true} path={`${match.url}/update/:id`} component={CreateNews} />
        </Switch>
    )
}

export default NewsRoutes;
