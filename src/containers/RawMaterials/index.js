import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllRawMaterials,
  updateStatusRawMaterial,
} from "../../appRedux/actions/RawMaterials";
import ListView from "../../components/ListView";

const RawMaterials = () => {
  const { rawMaterials } = useSelector(({ rawMaterials }) => rawMaterials);
  const dispatch = useDispatch();
  useEffect(() => {
    getAllRawMaterials(dispatch);
  }, []);

  return (
    <ListView
      listToBeShown={rawMaterials}
      searchTitleText="raw materials"
      link="raw_materials"
      title="Raw Materials"
      updateModuleItem={updateStatusRawMaterial}
    />
  );
};

export default RawMaterials;
