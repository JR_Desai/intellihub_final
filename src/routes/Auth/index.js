import React from "react";
import {Route, Switch, useRouteMatch} from "react-router";

import ForgotPassword from "../../containers/Auth/forgot_password";
import Login from "../../containers/Auth/login";
import OTPScreen from "../../containers/Auth/otp_screen";
import ResetPassword from "../../containers/Auth/reset_password";

const Auth = () => {

    const match = useRouteMatch()

    return (
        <div className="gx-login-container">
            <div className="gx-login-content">
                <Switch>
                    <Route path={`${match.url}/login`} component={Login} />
                    <Route path={`${match.url}/forgot-password`} component={ForgotPassword} />
                    <Route path={`${match.url}/reset-password`} component={ResetPassword} />
                    <Route path={`${match.url}/otp`} component={OTPScreen} />
                </Switch>
            </div>
        </div>
    )
}

export default Auth;
