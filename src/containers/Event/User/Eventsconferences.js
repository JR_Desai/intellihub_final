import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

import {
  Drawer,
  FormControlLabel,
  Checkbox,
  IconButton,
  ButtonGroup,
  Button,
 } from '@material-ui/core';

import {
  ChevronLeft as ChevronLeftIcon,
  ChevronRight as ChevronRightIcon, 
  Menu as MenuIcon,
  Share as ShareIcon,
  BookmarkBorder as BookmarkBorderIcon,
  Bookmark
} from '@material-ui/icons'; 

import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import * as ReactBootStrap from "react-bootstrap";

import { FiSearch, FiFacebook, FiLinkedin, FiTwitter } from "react-icons/fi";
import moment from 'moment';

import { Icon, Popup, Form, Button as SUIButton, Divider, TextArea, Label } from "semantic-ui-react";
import { AutoComplete, Spin } from 'antd';
import Fuse from 'fuse.js'

const drawerWidth = '240';

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth.concat,
    position: 'relative',
    marginTop: '109px',
    zIndex:'1029'
  },
  drawerHeader: {
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginLeft: 0
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const Eventsconferences = () => {
  const url = "http://3.108.46.195:9000";

  const classes = useStyles();
  const [eventDrawer, setEventDrawer] = useState(false);

  const handleDrawerOpen = () => setEventDrawer(true)
  const handleDrawerClose = () => setEventDrawer(false)

  const [clicked, setClicked] = useState(false);

  const toggle = (index) => {
    if (clicked === index) return setClicked(null)
    setClicked(index);
  };

  const [events, setEvents] = useState([])
  const [sort, setSort] = useState('dateDesc');

  useEffect(() => fetchEvents(), [])
  
  const fetchEvents = () => {
    fetch(`${url}/event_conference/view/?filters={"type":"operator","data":{"attribute":"row_status","operator":"=","value":"1"}}`)
    .then((res) => res.json())
    .then((result) => setEvents(result.results))
  }

  //Bookmarks
  const [bookmarks, setBookmark] = useState();
  const handleBookMark = (event) => { 
    console.log(event.target.checked)
    if(event.target.checked){
      console.log(event.target.value)
    }
  }

  var sortedEventData = [...events]

  const [loading, setLoading] = useState(false)

  const handleDescChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateDesc'), 2000)
    
    console.log(event.target)
  }
  
  const handleAscChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateAsc'), 2000)    
    console.log(event.target)
  }

  
  if(sort === 'dateDesc') sortedEventData = events.sort((a, b) => new Date(b.start_date) - new Date(a.start_date))
  else if(sort === 'dateAsc') sortedEventData = events.sort((a, b) => new Date(a.start_date) - new Date(b.start_date))

  const renderEvents = sortedEventData.map(event => {
    return <>
    <div className="sub_magazines2 eventConferenceItem">
      <span className="newThumb-right">
      <ReactBootStrap.Image
        className="magazines_img"
        src={event.image_link}
      />
      </span>
      <div className="magazines_info">
        <div className="info1">
          <p className="date">{event.industry}</p>
          <h6 className="card_title">{event.event_name}</h6>
          <div class="box">
            <p>
              {event.description || 'Description'} 
            </p>
            {/* <Popup
              trigger={<span style={{color:'rgb(201, 2, 201)',marginBottom:'10px',cursor: 'pointer'}}>Read More</span>}
              position="right center"
              on="click"
              flowing
              style={{height:'auto'}}
            >
            <p style={{width:"500px",wordBreak:"break-all"}}>
              {event.description || 'Description'} 
            </p>
            </Popup> */}
          </div>
        </div>
        <div className="info2">
          <p className="more mt-2 eventConferenceInfoList">
            <strong>Date :</strong>&nbsp;{moment(event.start_date).format("MMMM Do")} - {moment(event.end_date).format("MMMM Do YYYY")} <br/>
            <strong>Location :</strong>&nbsp;{event.location} <br/>
            <strong>Organizer :</strong>&nbsp;{event.organizer} <br/>
            <strong>Event Type :</strong>&nbsp;{event.event_type.event_type}
          </p>
          <div className="">
            <a href={event.registration_url} className="download" target="_blank">
              Register Here
            </a>
            <span className="socialActionBlock" style={{float:'right', marginTop:'-12px'}}>
            <Checkbox icon= {<BookmarkBorderIcon />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={event.id}/> 
            <Popup
              trigger={
                <IconButton><ShareIcon /></IconButton>
              }
              position="left center"
              on="click"
              flowing
              style={{height:'auto'}}
            >
              <div style={{width:"450px",wordBreak:"break-all"}}>
              <label>Share On</label><br/>
              <div className="row pl-3">
                <Link className="socialIcon socialLinkedin mr-1"><FiLinkedin /></Link>
                <Link className="socialIcon socialTwitter mr-1"><FiTwitter /></Link>
                <Link className="socialIcon socialFacebook mr-1"><FiFacebook /></Link>
              </div>
              <Divider />
              <label>Share With</label><br/>
              <Label as='a'>
                <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
                Elliot@gmail.com
                <Icon name='delete' />
              </Label>
              <Label as='a'>
                <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
                Stevie@gmail.com
                <Icon name='delete' />
              </Label>
              
              <Form className="mt-2">
                <Form.Field>
                  <input placeholder='Enter email id' />
                </Form.Field>
                <Form.Field>
                <TextArea placeholder='Type your message here' />
                </Form.Field>
                <Form.Field>
                  <SUIButton type='submit' color="pink">Submit</SUIButton>
                  <SUIButton type='submit' basic>Cancel</SUIButton>
                </Form.Field>
              </Form>
              </div>
            </Popup> 
            </span>
          </div>
        </div>
      </div>
    </div>
    </>
  });

  //Render tags
  const [parentTag, setParentTag] = useState([]);
  const [childTag, setChildTag] = useState([]);
  const [allTags, setAllTag] = useState([]);

  useEffect(() => { fetchParentTag() }, []);
  useEffect(() => { fetchChildTag() }, []);
  useEffect(() => { fetchAllTag() }, []);
  
  const fetchParentTag = () => {
    fetch(`${url}/tag_parent/view/`)
    .then((res) => res.json())
    .then((result) => setParentTag(result))
  }
  const fetchChildTag = () => {
    fetch(`${url}/tag_child/view/`)
    .then((res) => res.json())
    .then((result) => setChildTag(result))
  }
  const fetchAllTag = () => {
    fetch(`${url}/tag/view/`)
    .then((res) => res.json())
    .then((result) => setAllTag(result))
  }

  var [tagState, setTagState] = useState([undefined]);
  var [tagCheck, setTagCheck] = useState([]);

  const handleTagChange = (event) => {
    console.log(event.target.checked, event.target.name);
    setTagCheck({...tagCheck, [event.target.name]: event.target.checked})

    if(event.target.checked) {
      if(tagState.indexOf(parseInt(event.target.name)) === -1) {
        tagState.push(parseInt(event.target.name));
      }
    }
    if(!event.target.checked) {
      var index = tagState.indexOf(parseInt(event.target.name))
        if (index !== -1) {
          tagState.splice(index, 1);
          // setTagState([...tagState]);
        }
      // tagState = tagState.filter(tag => parseInt(tag) !== parseInt(event.target.name))
    }
    const fetchTagData = () => {  
      if(tagState.length === 0) {
        fetch(`${url}/magazines/view/`)
        .then((res) => res.json())
        .then((result) => setEvents(result.results))
      }
      else {
        fetch(`${url}/event_conference/view/?filters={"type":"operator","data":{"attribute":"tag","operator":"in","value":[${tagState}]}}`)
        .then((res) => res.json())
        .then((result) => setEvents(result.results))
      }
    }
    fetchTagData();
  }
  
var options = [];
const [selectOption, setSelectOption] = useState([]);
const [selectedSearchBar, setSearchBar] = useState(false);
const onSearch = (searchText) => {
  console.log(searchText);
  const fuse = new Fuse(options, {keys : ['value', 'id']});

  var results = fuse.search(searchText);
  results.length > 0 ? setSearchBar(true) : setSearchBar(false)
  console.log('searchresult', results);

  const characterResults = results.map(searchedOption => searchedOption.item)

  console.log('selectoptionvalue', characterResults)
  setSelectOption(characterResults);
};

const [renderSelectedTag, setRenderSelectedTag] = useState([]);

const onSelect = (data) => {
  console.log('onSelect', data);
  var idOfTag = options.filter( option => option.value === data ).map( option => option.id)

  var [ tagId ] = idOfTag;
  var filterSelectedChildTag = allTags.filter( tag => tag.id === tagId )
  var [ parentID ]  = filterSelectedChildTag.map( tag => tag.parent_id );

  var filterSelectedParentTag = allTags.filter( tag => tag.id === parentID )

  console.log(filterSelectedParentTag, 'parent', parentID)

  var renderSelectedTag1 = filterSelectedParentTag.map(parentTag => {
    return <>
    <div className="wrap" onClick={() => toggle(parentTag.id)}>
    <h6>{parentTag.name}</h6>
    <span>
    </span>
    </div>
    {
    filterSelectedChildTag.map((childTag) => {
      return <>
      <div className="checking">
        <div className="acordian_drop">
        <FormControlLabel
          control={<Checkbox checked={tagCheck.childId}
          onChange={handleTagChange} name={childTag.id} />}
          label={childTag.name}
        />
        </div>
      </div>
      </>
    })}
    </>
  })

  setRenderSelectedTag(renderSelectedTag1);
};

  
  var filterTags = parentTag.filter(tag => tag.module === 20)
    
    const renderTags = filterTags.map(tag => {
      var currentParentTag = tag.id;
      var currentModule = tag.module;
      var filterChildTag = childTag.filter(tag => tag.module === currentModule && tag.parent_id === currentParentTag)

      return <>      
      <div className="wrap" onClick={() => toggle(tag.id)}>
      <h6>{tag.name}</h6>
      <span>
          {clicked === tag.id ? (<AiOutlineMinus className="acordian_icon" />) : 
          (<AiOutlinePlus className="acordian_icon" />)}
      </span>
      </div>
      {clicked === tag.id ? (
      filterChildTag.map((tag) => (
        <div className="checking">
          <div className="acordian_drop">
          <FormControlLabel
            control={<Checkbox checked={tagState[tag.name]} onChange={handleTagChange} name={tag.name} />}
            label={tag.name}
          />
          </div>
        </div>
        ))
      ) : null}
      </>
    })

  //Main Page
  
  return (
    <div className="page newMainBlock">
      <Drawer
        variant="persistent"
        anchor="left"
        open={eventDrawer}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className="acordian">
        <div>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
            <MenuIcon />
          </IconButton>
        </div>
        <div className="acordian_header">
          <div className="acordian_search">
            <FiSearch className="acordian_search_icon" />
              <br />
            <AutoComplete
            options={selectOption}
            style={{
              width: 200,
            }}
            onSelect={onSelect}
            onSearch={onSearch}
            placeholder="Search here"
            >
            <input
              type="text"
              aria-label="Text input with dropdown button"
              // placeholder="search here"
              className="acordian_ip"
            />
            </AutoComplete>
            </div>
            </div>

            {/***************acordian***************/}
            <div className="acordian_content">
            {selectedSearchBar ? renderSelectedTag : renderTags}
            </div>
        </div>
      </Drawer>
      <div className="container">
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: eventDrawer
        })}
      >
      <div className={classes.drawerHeader} />

      <div className="news">
        <div className="news_header">
          <div className="news_left_header">
            {eventDrawer || (<IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
            >
              <MenuIcon />
              {/* <ChevronRightIcon /> */}
            </IconButton>)}
            <h4 className="main-header">Events &amp; Conference</h4>
          </div>
          <div className="news_right_header">
          <ButtonGroup size="large" variant="outlined" color="secondary" aria-label="large outlined button group" className="" disabled={loading}>
              <Button onClick={handleDescChange} startIcon={<Icon name='sort content descending' size='large' />} ></Button>
              <Button onClick={handleAscChange} startIcon={<Icon name='sort content ascending' size='large' />} ></Button>
          </ButtonGroup>
          </div>
        </div>
        <div className="news_line"></div>
        {/* Events list */}
        <Spin spinning={loading}>
        <div className="magazines mt-4">
          {renderEvents}
        </div>
        </Spin>
      </div>
      </main>
      </div>
    </div>
  );
};

export default Eventsconferences;
