import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import Magazine from '../../../containers/Magazine';
import CreateMagazine from '../../../containers/Magazine/CreateMagazine';
import MagazineDetails from '../../../containers/Magazine/MagazineDetails';

const MagazineRoutes = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={Magazine} />
            <Route exact={true} path={`${match.url}/create`} component={CreateMagazine} />
            <Route exact={true} path={`${match.url}/:id`} component={MagazineDetails} />
            <Route exact={true} path={`${match.url}/update/:id`} component={CreateMagazine} />
        </Switch>
    )
}

export default MagazineRoutes;
