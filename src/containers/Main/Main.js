import React from "react";
import {Layout} from "antd";

import Sidebar from "../Sidebar/index";

import Topbar from "../Topbar/index";
import App from "routes/index";

const {Content} = Layout;

const Main = () => {

    return (
        <Layout className="gx-app-layout">
        <Sidebar />
        <Layout>
            <Topbar />
            <Content className={`gx-layout-content gx-container-wrap`}>
                <App />
            </Content>
        </Layout>
        </Layout>
    )
}

export default Main
