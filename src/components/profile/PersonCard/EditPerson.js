import { Input } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import React from 'react';

const EditPerson = ({open, close, data}) => {
    return (
        <Modal onCancel={() => close(false)} visible={open}>
            <div className="gx-form-group">
                <Input
                    value={data.title}
                    placeholder="Name"
                    // onChange={(event) => setState({to: event.target.value})}
                    margin="normal"
                />
            </div>
            <div className="gx-form-group">
                <Input
                    value={data.address}
                    placeholder="Image_link"
                    // onChange={(event) => setState({to: event.target.value})}
                    margin="normal"
                />
            </div>
            <div className="gx-form-group">
                <Input
                    value={data.title}
                    placeholder="LinkedIn"
                    // onChange={(event) => setState({subject: event.target.value})}
                    margin="normal"
                />
            </div>
        </Modal>
    )
}

export default EditPerson;
