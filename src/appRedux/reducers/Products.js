import {
  CREATE_PRODUCT_SUCCESS,
  EDIT_PRODUCT_DETAILS,
  GET_ALL_PRODUCTS_SUCCESS,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_TAGS,
  SHOW_CONFIRM_REMOVE,
  UPDATE_PRODUCT_SUCCESS,
} from "../../constants/ActionTypes";

const INIT_STATE = {
  loader: false,
  products: [],
  product: {},
  showRemove: false,
  showEdit: false,
  tags: {},
};

export default function productReducer(state = INIT_STATE, action) {
  switch (action.type) {
    case SHOW_CONFIRM_REMOVE: {
      return {
        ...state,
        loader: false,
        showRemove: action.payload,
      };
    }
    case EDIT_PRODUCT_DETAILS: {
      return {
        ...state,
        loader: false,
        product: action.payload,
      };
    }
    case GET_ALL_PRODUCTS_SUCCESS: {
      return {
        ...state,
        loader: false,
        products: action.payload,
      };
    }
    case GET_PRODUCT_SUCCESS: {
      return {
        ...state,
        loader: false,
        product: action.payload[0],
      };
    }
    case CREATE_PRODUCT_SUCCESS: {
      return {
        ...state,
        loader: false,
        product: action.payload,
      };
    }
    case UPDATE_PRODUCT_SUCCESS: {
      return {
        ...state,
        loader: false,
        product: action.payload,
      };
    }
    case GET_PRODUCT_TAGS: {
      return {
        ...state,
        loader: false,
        tags: {
          ...state.tags,
          [action.payload?.module]: action.payload?.data,
        },
      };
    }
    default:
      return state;
  }
}
