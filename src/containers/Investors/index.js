import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getAllInvestors, updateStatusInvestors } from '../../appRedux/actions/Investors';
import ListView from '../../components/ListView';

const Investors = () => {

    const { investors } = useSelector(({ investors }) => investors);

    const dispatch = useDispatch();

    useEffect(() => {
        getAllInvestors(dispatch);
    }, [])

    return (
        <ListView listToBeShown={investors} link="investors" searchTitleText="Investors" title="Investors" getAllFunction={getAllInvestors} updateModuleItem={updateStatusInvestors} />
    )
}

export default Investors;
