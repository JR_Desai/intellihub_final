import React from "react";

import { Input, Typography } from "antd";
const { Text } = Typography;

const InputGroup = ({ name, label, type, value, onChange, error = "" }) => {
  return (
    <div className="gx-form-group">
      <label htmlFor={name} className="gx-form-label">
        {label}
      </label>
      <div style={{ flex: 1 }}>
        <Input name={name} type={type} value={value} onChange={onChange} />
        {error && error !== "" && <Text type="danger">{error}</Text>}
      </div>
    </div>
  );
};

export default InputGroup;
