import React, { useEffect, useState } from 'react'
import {
    Form,
    Button,
    Card,
    Select,
} from 'antd';

import { useDispatch, useSelector } from 'react-redux';
import InputGroup from '../../components/InputGroup';
import { useHistory } from 'react-router';
import { createTechnology, updateTechnology, getTechnologyTags } from '../../appRedux/actions/Technologies';
import { getAllCompanyNames } from '../../appRedux/actions/Companies';
import TextArea from 'antd/lib/input/TextArea';

const CreateTechnology = props => {

    const Option = Select.Option;

    const history = useHistory();

    const { technology, tags } = useSelector(({ technologies }) => technologies);
    const { companyNames } = useSelector(({ companies }) => companies);
    
    const technologyId = props.match.params.id;

    const companiesOption = companyNames ?.map(
        (company, _) => <Option key={company.id}>{company.name}</Option>
    );
    const parentTagOption = tags ?.map(
        (tag_parent, _) => <Option key={tag_parent.id} value={tag_parent.id}>{tag_parent.name}</Option>
    );
    const initState = {
        name: "Name",
        image_link: "",
        description: "description",
        company: [],
        categories: [],
        row_status: 3
    }

    useEffect(() => {
        getAllCompanyNames(dispatch);
        getTechnologyTags(dispatch);
    }, [])

    console.log(technology);

    if (technologyId) {
        if (technology.company && technology.company.length > 0) {
            let Company = [];
            technology.company.map((res) => {
                res.id ? Company.push(res.id) : Company.push(res);
            });
            technology.company = Company;
        }
        if (technology.tags && technology.tags.length > 0) {
            let Tags = [];
            technology.tags.map((res) => {
                res.id ? Tags.push(res.id) : Tags.push(res);
            });
            technology.tags = Tags;
        }
        if (technology.row_status && technology.row_status.id) {
            technology.row_status = technology.row_status.id;
        }
    }


    const values = technologyId ? technology : initState

    const [tech, setTech] = useState(values);

    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        technologyId ? updateTechnology(tech.id, tech, dispatch) : createTechnology(tech, dispatch);
        history.replace("/technology");
    }

    const header = technologyId ? "Update" : "Create"

    return (
        <div className="gx-main-content-wrapper">
            <Card className="gx-card" title={`${header} Technology`}>
                <Form
                    name="register"
                    scrollToFirstError
                >
                    <InputGroup name="name" label="Name: " type="text" value={tech.name}
                        onChange={(e) => setTech({ ...tech, name: e.target.value })}
                    />
                    <div className="gx-form-group">
                        <label name="description" className="gx-form-label">Description: </label>
                        <TextArea
                            name="description"
                            value={tech.description}
                            rows={4}
                            onChange={(e) => setTech({ ...tech, description: e.target.value })}
                        />
                    </div>
                    <InputGroup name="image_link" label="Image_link: " type="text" value={tech.image_link}
                        onChange={(e) => setTech({ ...tech, image_link: e.target.value })}
                    />
                    <div className="gx-form-group">
                        <label className="gx-form-label">Companies: </label>
                        <Select
                            className="gx-w-100"
                            mode="multiple"
                            defaultValue={technologyId && tech.company}
                            placeholder="Select Companies"
                            onChange={(value) => setTech({ ...tech, company: value })}
                        >
                            {companiesOption}
                        </Select>
                    </div>
                    <div className="gx-form-group">
                        <label className="gx-form-label">Tags: </label>
                        <Select
                            className="gx-w-100"
                            mode="multiple"
                            defaultValue={technologyId && tech.tags}
                            placeholder="Select Tags"
                            onChange={(value) => setTech({ ...tech, tags: value })}
                        >
                            {parentTagOption}
                        </Select>
                    </div>
                    <Button onClick={handleSubmit} className="gx-w-100" type="primary" htmlType="submit">
                        Submit
            </Button>
                </Form>
            </Card>
        </div>
    )
}

export default CreateTechnology
