import { CREATE_RESEARCH_SUCCESS, EDIT_RESEARCH_DETAILS, GET_ALL_RESEARCHES_SUCCESS, GET_RESEARCH_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_RESEARCH_SUCCESS, GET_APIKEY_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    researches: [],
    research: {},
    apikeys: [],
    showRemove: false,
    showEdit: false
}

export default function researchReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_RESEARCH_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_RESEARCHES_SUCCESS: {
            return {
                ...state,
                loader: false,
                researches: action.payload
            }
        }
        case GET_APIKEY_SUCCESS: {
            return {
                ...state,
                loader: false,
                apikeys: action.payload
            }
        }
        case GET_RESEARCH_SUCCESS: {
            return {
                ...state,
                loader: false,
                research: action.payload[0]
            }
        }
        case CREATE_RESEARCH_SUCCESS: {
            return {
                ...state,
                loader: false,
                research: action.payload
            }
        }
        case UPDATE_RESEARCH_SUCCESS: {
            return {
                ...state,
                loader: false,
                research: action.payload
            }
        }
        default:
            return state;
    }
}
