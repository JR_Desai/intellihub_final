import { Button } from 'antd';
import React, { useState } from 'react'
import {eventList} from "../../../containers/CompanyProfile/data"
import Widget from '../../Widget';
import CreatePerson from './CreatePerson';
import PersonItem from './PersonItem';

const PersonCard = () => {

    const [openClick, setOpen] = useState(true);

    const onClick = () => {
        setOpen(true);
    }

    return (
        <Widget styleName="gx-card-profile">
            <div className="ant-card-head gx-card-tabs gx-flex-row gx-align-items-center">
                <span className="ant-card-head-title gx-mb-4">
                    {/* {title} */}
                    People Portfolios
                </span>
                <span className="gx-align-self-center">
                    <Button className="gx-text-primary" type="text" onClick={onClick}>
                        Add Person
                    </Button>
                </span>
            </div>
            <div className="gx-pt-md-3">
                {eventList.map((data, index) =>
                    <PersonItem key={index} data={data} />
                )}
            </div>
            <CreatePerson open={openClick} close={setOpen} />
        </Widget>
    )
}

export default PersonCard;
