import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom"
import { useParams } from 'react-router-dom'
import * as ReactBootStrap from "react-bootstrap"
import moment from "moment";
import { Row, Col, Radio, Divider, Tabs, Space, Descriptions } from 'antd';
import {
    Share,
    BookmarkBorder,
    Bookmark,
} from '@material-ui/icons'; 
import { Checkbox, IconButton } from '@material-ui/core';
import { FiFacebook, FiLinkedin, FiTwitter } from "react-icons/fi";
import { Popup, Button as SUIButton, Label, Icon, Form, TextArea } from 'semantic-ui-react'


import axios from 'axios';

const { TabPane } = Tabs;
const CompanyDetails = () => { 


  const { dealsId } = useParams()
  const [ companyDetail, setData ] = useState([])
    
  const getData = async () => {
    try {
        const data = await axios.get(`/acquisition/view/?id=${dealsId}`);
        setData(data.data.results);
    } catch (err) {
        console.log(err);
    }
  }

  useEffect(() => {  
    getData()
  }, [])
  
  const [bookmarks, setBookmark] = useState();
  
  const handleBookMark = (event) => { 
    console.log(event.target.checked)
    if(event.target.checked){
      console.log(event.target.value)
    }
  }
  const [state,setState] = useState({
    tabPosition: 'Funding',
  });

  const changeTabPosition = e => {
    setState({ tabPosition: e.target.value });
  };


  const { tabPosition } = state;
  const renderCompanyDetails = companyDetail.map(company => { 
      return <>
        <Row style={{width: "99%"}}>
            <Col span={6}>
                <h6>
                <Link to="/user/dealsandinvestments">Deals </Link>{">"} {company.company.name}
                </h6>
                <div className="maga">
                    <ReactBootStrap.Image
                    style={{width:"165px",
                    height: "165px",marginLeft:"50px"}}
                    src={company.company.image_link}
                    />
                </div>
            </Col>
            <Col span={12} className="mt-5">
                <div className="maga-info">
                    <div className="company-header">
                        <h4>{company.company.name}</h4>
                    </div>
                </div>
                <div className="ml-3 mt-3">
                    <div className="info2 row">
                        <p className="more">
                        Established
                        </p>
                        <p className="more ml-3">
                        Location
                        </p>
                    </div>
                    <div className="info2 row">
                        <h6 className="more mr-3">
                        {moment(company.company.year_of_incorporation).format('YYYY')}
                        </h6>
                        <h6 className="more ml-5">
                        {company.company.hq}
                        </h6>
                    </div>
                    <div className="info2 row mt-3">
                        <p className="company-meta">
                            {company.company.description}
                        </p>
                    </div>
                </div>
            </Col>
            <Col className="mt-5">
                <div className="row">
                    <p className="more mr-3">
                    <Checkbox icon= {<BookmarkBorder />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={company.id}/>
                    </p>
                    <p className="more">

                        <Popup
                        trigger={
                        <IconButton><Share /></IconButton>
                        }
                        position="left center"
                        on="click"
                        flowing
                        style={{height:'auto'}}
                        >
                        <div style={{width:"450px",wordBreak:"break-all"}}>
                        <label>Share On</label><br/>
                        <div className="row pl-3">
                            <Link className="socialIcon socialLinkedin mr-1"><FiLinkedin /></Link>
                            <Link className="socialIcon socialTwitter mr-1"><FiTwitter /></Link>
                            <Link className="socialIcon socialFacebook mr-1"><FiFacebook /></Link>
                        </div>
                        <Divider />
                        <label>Share With</label><br/>
                        <Label as='a'>
                            <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
                            Elliot@gmail.com
                            <Icon name='delete' />
                            </Label>
                            <Label as='a'>
                            <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
                            Stevie@gmail.com
                            <Icon name='delete' />
                        </Label>
                        
                        <Form className="mt-2">
                        <Form.Field>
                            <input placeholder='Enter email id' />
                        </Form.Field>
                        <Form.Field>
                            <TextArea placeholder='Type your message here' />
                        </Form.Field>
                        <Form.Field>
                            <SUIButton type='submit' color="pink">Submit</SUIButton>
                            <SUIButton type='submit' basic>Cancel</SUIButton>
                        </Form.Field>
                        </Form>
                        </div>
                    </Popup>
                    </p>
                </div>
            </Col>
        </Row>
        <Divider />
        <Col offset={1}>
        <Space style={{ marginBottom: 10 }}>
        <Radio.Group value={tabPosition} onChange={changeTabPosition}>
            <Radio.Button value="Funding">Funding</Radio.Button>
            <Radio.Button value="M&A" style={{ marginLeft: 20 }}>M&A</Radio.Button>
            <Radio.Button value="Investments" style={{ marginLeft: 20 }}>Investments</Radio.Button>
        </Radio.Group>
        </Space>
            <Tabs activeKey={tabPosition}>
                <TabPane key="Funding">
                <div>
                    <Descriptions bordered title="Funding Info">
                        <Descriptions.Item label="Estimated Revenue Range" span={2}>{company.name || 'None'}</Descriptions.Item>
                        <Descriptions.Item label="Number of Acquisitions" span={2}>{company.acquisition_count}</Descriptions.Item>
                        <Descriptions.Item label="Number of Founders" span={2}>{company.alumni_founders}</Descriptions.Item>
                        <Descriptions.Item label="Transaction Name" span={2}>{company.transaction || 'None'}</Descriptions.Item>
                        <Descriptions.Item label="Founders" span={2}>None</Descriptions.Item>
                        <Descriptions.Item label="Price" span={2}>None</Descriptions.Item>
                        <Descriptions.Item label="Total Funding Amount" span={2}>None</Descriptions.Item>
                        <Descriptions.Item label="Price Currency" span={2}>None</Descriptions.Item>
                        <Descriptions.Item label="Total Funding Amount Currency" span={2}>None</Descriptions.Item>
                        <Descriptions.Item label="Price Currency (in USD)" span={2}>None</Descriptions.Item>
                        <Descriptions.Item label="Total Funding Amount (in USD)" span={2}>None</Descriptions.Item>
                        <Descriptions.Item label="Top 5 Investors" span={2}>None</Descriptions.Item>
                        <Descriptions.Item label="Number of Acquisitions" span={2}>None</Descriptions.Item>

                        


                    </Descriptions>
                </div>
                </TabPane>
                <TabPane key="M&A" >
                <div>
                    <Descriptions title="M&A Info" bordered>
                    <Descriptions.Item label="Estimated Revenue: INR 1Mn" span={2}></Descriptions.Item>
                    <Descriptions.Item label="Number Of Founders: 2" span={2}></Descriptions.Item>
                    <Descriptions.Item label="Founders : " span={2}></Descriptions.Item>
                    <Descriptions.Item label="Total Funding Amount: 300000000" span={2}></Descriptions.Item>
                    <Descriptions.Item label="Total Funding Amount(USD): 319999" span={3}></Descriptions.Item>
                    <Descriptions.Item label="No. Of acquisition : 4" span={2}></Descriptions.Item>
                    <Descriptions.Item label="Price: None" span={2}></Descriptions.Item>
                    </Descriptions>
                </div>
                </TabPane>
                <TabPane key="Investments" >
                <div>
                    <Descriptions title="Investment Info" bordered>
                    <Descriptions.Item label="Estimated Revenue: INR 1Mn" span={3}></Descriptions.Item>
                    <Descriptions.Item label="Number Of Founders: 2" span={3}></Descriptions.Item>
                    <Descriptions.Item label="Founders : " span={3}></Descriptions.Item>
                    <Descriptions.Item label="Total Funding Amount: 300000000" span={3}></Descriptions.Item>
                    <Descriptions.Item label="Total Funding Amount(USD): 319999" span={3}></Descriptions.Item>
                    <Descriptions.Item label="No. Of acquisition : 4" span={3}></Descriptions.Item>
                    <Descriptions.Item label="Price: None" span={3}></Descriptions.Item>
                    </Descriptions>
                </div>
                </TabPane>
            </Tabs>
        </Col>
        </>
    })
  
    return (
    <div className="magazine-1">
      {renderCompanyDetails}
      <br/>
      <div className="row col-md-12">
      </div>
    </div>
    )
}

export default CompanyDetails
