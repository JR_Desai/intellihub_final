import React from 'react';
import {Col, Row} from 'antd';

import ProductItem from './ProductItem';
import { useSelector } from 'react-redux';

const ProductList = () => {

  const {companies} = useSelector(({companies}) => companies);
  
  return (
    <div id="product">
      <Row>
        {companies.map(company => (
          <Col xl={8} lg={12} md={12} sm={12} xs={24}>
            <ProductItem item={company} key={company.id}/>
          </Col>
        ))}
      </Row>
    </div>
  );
};
export default ProductList;
