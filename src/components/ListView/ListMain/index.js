import React, { useState, setState } from 'react';
import { useDispatch } from 'react-redux';
import CustomScrollbars from 'utils/CustomScrollbars'
import ListItem from './ListItem';
import { showConfirmRemove } from '../../../appRedux/actions/Companies';
import { deleteDeal, getAllDealsAndInvestment } from '../../../appRedux/actions/DealsAndInvestment';
import { deleteInvestors } from '../../../appRedux/actions/Investors';
import ConfirmPopup from '../../ConfirmPopup';


const ListMain = ({ list, link, updateModuleItem, refreshList }) => {

    const dispatch = useDispatch();
    const [showAlert, setAlert] = useState(false);
    const [id, setId] = useState(null);

    const onRemoveItem = (id) => {
        setId(id);
        setAlert(true);
        showConfirmRemove(true, dispatch);
    }
    const onRemove = () => {
        if (link == 'dai') {
            deleteDeal(id, dispatch);
            setTimeout(() => {
                refreshList();
            }, 500);
        }
        if (link == 'investors') {
            deleteInvestors(id, dispatch);
            setTimeout(() => {
                refreshList();
            }, 500);
        }
        setAlert(false);
        setId(null);
    }
    const onCancel = () => {
        setAlert(false);
        setId(null);
    }
    return (
        <div className="gx-module-list">
            <CustomScrollbars className="gx-module-content-scroll">
                {list.sort((a, b) => new Date(b.created_at) - new Date(a.created_at)).map((item, index) =>
                    <ListItem key={index} item={item} link={link} onRemoveItem={onRemoveItem} updateModuleItem={updateModuleItem} />
                )}
            </CustomScrollbars>
            <ConfirmPopup showStatus={showAlert} onConfirm={onRemove} onCancel={onCancel} />
        </div>
    )
}

export default ListMain
