import React from "react";
import {useDispatch} from "react-redux";
import {Avatar, Popover} from "antd";
import {userSignOut} from "appRedux/actions/Auth";

const UserInfo = () => {

  const dispatch = useDispatch();

  const userMenuOptions = (
    <>
    <li className="gx-pb-3 gx-text-black gx-font-weight-semi-bold">System Admin</li>
    <ul className="gx-user-popover">
      <li>My Account</li>
      <li onClick={() => dispatch(userSignOut())}>Logout
      </li>
    </ul>
    </>
  );

  return (
    <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={userMenuOptions}
            trigger="click">
      <Avatar src={"https://via.placeholder.com/150"}
              className="gx-avatar gx-pointer" alt=""/>
      <span className=" gx-avatar-name gx-ml-3">Demo Name<i
      className="icon icon-chevron-down gx-fs-xs gx-ml-2"/></span>
    </Popover>
  )

}

export default UserInfo;
