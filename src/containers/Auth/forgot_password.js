import React from "react";
import {Button, Form, Input} from "antd";

const FormItem = Form.Item;


const ForgotPassword = () => {
    return (
        <>
            <div className="gx-login-header gx-text-center">
                <h1 className="gx-login-title">Forgot Password?</h1>
            </div>
            <Form
            initialValues={{ remember: true }}
            name="basic"
            className="gx-signin-form gx-form-row0">
                <FormItem name="email" rules={[{ required: true, message: 'Please input your E-mail!' }]}>
                    <Input className='gx-input-lineheight' type="email" placeholder="E-Mail Address"/>
                </FormItem>
                <FormItem className="gx-text-center">
                    <Button type="primary" htmlType="submit">
                    Send
                    </Button>
                </FormItem>
            </Form>
        </>
    );
}

export default ForgotPassword
