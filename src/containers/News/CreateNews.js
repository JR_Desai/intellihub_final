import React, { useEffect, useRef, useState } from 'react'
import {
    Form,
    Button,
    Card,
    Select,
    DatePicker,
    Input,
    Spin,
    message,
    Tooltip
} from 'antd';
import { UploadOutlined } from '@ant-design/icons';

import { useDispatch, useSelector } from 'react-redux';
import InputGroup from '../../components/InputGroup';
import { useHistory } from 'react-router';
import { createNews, updateNews } from '../../appRedux/actions/News';
import { updateCompany } from '../../appRedux/actions/Companies';
import {convertToRaw, EditorState, ContentState, convertFromRaw} from "draft-js";
// import {Editor} from "react-draft-wysiwyg";
// import draftToHtml from "draftjs-to-html";
import { CKEditor as Editor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import custom_editor_config from '../EditorConfig'
import { createTechnology, updateTechnology, getTechnologyTags } from '../../appRedux/actions/Technologies';
import axios from 'axios'
import { string } from 'prop-types';
import moment from 'moment';
const CreateNews = props => {
    const selectedChildValue = useRef()
    var [allParentTag, setAllParents] = useState([]);
    var parentTagOption = [];

    useEffect(() => {
        getTechnologyTags(dispatch);
        parentTagFetch()
    }, [])
    var parentTags = [];

    const parentTagFetch = () => {
        axios.get(`/tag_parent/view/`).then(results => setAllParents(results.data))
    }
    const Option = Select.Option;

    parentTagOption = allParentTag.map((tag_parent) => {
        return <Option key={tag_parent.id} value={tag_parent.id}>{tag_parent.name}</Option>
    })
    const history = useHistory();
    const { technology, tags } = useSelector(({ technologies }) => technologies);

    const {newnews} = useSelector(({news}) => news);
    const {company} = useSelector(({companies}) => companies);

    const newsId = props.match.params.id;

    const companyUpdate = history.location.pathname.match(`/companies/${newsId}/createNews`);

    const initState = {
        image_link: "",
        date: "2019-02-11",
        headline: "Headline",
        short_description: "Enter Short Description",
        long_description: "Enter Long Description",
        upload_image: null,
        tag: [],
        link: "",
        row_status: 3,
    }

    const values = newsId ? newnews : initState

    const header = newsId ? companyUpdate ? "Create Company" : "Update" : "Create"

    const [news, setNews] = useState(values);
    const [loading, setLoading] = useState(false)
    const [uploadImage, setUploadImage] = useState([{upload_image : undefined}])
    const [selectedTag, setSelectedTag] = useState({tag: null})
    const dispatch = useDispatch();
    


    if (newsId) {
        if (technology.company && technology.company.length > 0) {
            let Company = [];
            technology.company.map((res) => {
                res.id ? Company.push(res.id) : Company.push(res);
            });
            technology.company = Company;
        }
        if (technology.tags && technology.tags.length > 0) {
            let Tags = [];
            technology.tags.map((res) => {
                res.id ? Tags.push(res.id) : Tags.push(res);
            });
            technology.tags = Tags;
        }
        if (technology.row_status && technology.row_status.id) {
            technology.row_status = technology.row_status.id;
        }
    }

    const rules={
        image: [{
          required: true,
          message: 'Please input your username!',
        }],
        name: [{
            required: true,
            message: 'Please input headline',
        }],
    }

    // const [selectedParentTag, setSelectedParentTag] = useState([]);
    var [selectedParentTagID, setSelectedParentTagId] = useState(news.tag ?.map(tag => tag.parent_id)[0]);
    const [parentId, setParentId] = useState();
    var [selectedChildTag,setSelectedChildTag] = useState(news.tag ?.map(tag => tag.name))
    let selectedParentTag = news.tag ?.map(tag => tag.parent_id)[0]

    var childTagOption = []
    let options = []
    let childOptions = []
    options = tags.filter((tag) => tag.parent_id === selectedParentTagID)
    if(options.length > 0) {
        childOptions = options.map((tag_child) => {
            return <Option key={tag_child.id} value={tag_child.id}>{tag_child.name}</Option>
        })
    }
    childTagOption = childOptions;

    const handleSubmit = (e) => {
        e.preventDefault()

        let formData = new FormData();
        if(uploadImage.upload_image){ 
            formData.append("upload_image", uploadImage.upload_image, uploadImage.upload_image.name);
        }
        let tag_data = []
        if(selectedTag.tag !== null && selectedTag.tag.length > 0) {
            tag_data = selectedTag.tag.map(selectedTag => {
                let tagValues_withoutCode = tags.filter(tag => tag.id === selectedTag && tag.code === "")[0]
                if(tagValues_withoutCode !== undefined) {
                    return {...tagValues_withoutCode , code : tagValues_withoutCode.name }
                }

                let tagValues_withName = tags.filter(tag => tag.id === selectedTag)[0]

                if (tagValues_withName === undefined) {
                    return {name: selectedTag, code: selectedTag, parent_id: parentId, module: 2, status: true}
                } 
                return (tagValues_withName !== undefined ) && tagValues_withName
            })

            news.tag = tag_data;
        } else {selectedTag.tag === null ? news.tag = [] : news.tag = selectedTag.tag}
        setLoading(true)
        setTimeout(() => {
            //Create & Update Method
            // companyUpdate ? createNews(companyUpdate, company, news, formData, dispatch) : newsId ? updateNews(newsId, news, formData, dispatch) : createNews(companyUpdate, company, news, formData, dispatch);
        }, 2000)
        
        setTimeout(() => {
            //Url change Method
            setLoading(false)
            // !companyUpdate ? history.replace("/news") : history.replace(`/companies/${newsId}`);
        }, 2000)
    }
    const handleClose = (e) => {
        history.replace("/news")
    }

    const createParentTag = (value) => {
        if(typeof value === 'string')
        axios.post('/tag/create/', {
            'name':value,
            'code':value,
            'parent_id':null,
            'module':2,
            'status':false
        }).then(res => setParentId(res.data.id))
        .catch(err => console.log(err))
    }

    const [formValidate, setValidation] = useState([]);
    const [shortDescValid, setShortDescValid] = useState()
    return (
        <Spin spinning={loading}>
        <div className="gx-main-content-wrapper">
        <Card className="gx-card" title={`${header} News`}>
        <Form
            name="register"
            scrollToFirstError
            onSubmitCapture={(value) => console.log('valuein form',value)}
            >
                <div className="gx-form-group">
                    <label htmlFor="image_upload" className="gx-form-label">Upload Image:</label>
                    <Input name="upload_image"
                    onChange={(e) => {
                        console.log(e.target.value?'hello':'nothello')
                        setUploadImage({upload_image: e.target.files[0]})
                    }}
                        encType='multipart/form-data' type="file" accept="image/*" placeholder="Add Image"
                    />
                    <span></span>
                </div>
                {
                    news.upload_image ? 
                        <>
                        <div className="gx-form-group">
                        <label htmlFor="cover_image" className="gx-form-label">Current Image in Upload:</label>
                        <div className="gx-w-25">
                            <img
                            className='' src={news.upload_image}
                            alt="..." />
                        </div>
                        </div>
                        </> :
                        null
                }
                <div className="gx-form-group">
                <label htmlFor="image_link" className="gx-form-label">Image Link:</label>
                <Tooltip visible={news.image_link?false:true} placement="bottomLeft" title="Image link is Required" color={'red'}>
                <Input name="image_link" type="text" value={news.image_link} 
                    onChange={(e) => setNews({...news, image_link: e.target.value})} 
                />
                </Tooltip>
                </div>
                {
                    news.image_link?
                    <>
                        <div className="gx-form-group">
                        <label htmlFor="cover_image" className="gx-form-label">Current Image in Image Link:</label>
                        <div className="gx-w-25">
                            <img
                            className='' src={news.image_link}
                            alt="..." />
                        </div>
                        </div>
                    </> :
                        null
                }
                <div className="gx-form-group">
                    <label htmlFor="date" className="gx-form-label">Date:</label>
                    <DatePicker name="date" placeholder={news.date}
                        onChange={(e) => setNews({...news, date: e.format("YYYY-MM-DD")})} className="gx-mb-3 gx-w-100"
                    />
                </div>
                <div className="gx-form-group">
                <label htmlFor="headline" className="gx-form-label">Headline:</label>
                <Tooltip visible={news.headline?false:true} placement="bottomLeft" title="Headline is Required" color={'red'}>
                <Input name="headline" type="text" value={news.headline} 
                    onChange={(e) => 
                        setNews({...news, headline: e.target.value})
                    }
                    className="ant-form-item-has-error"
                />
                </Tooltip >
                </div>
                <div className="gx-form-group">
                    <label htmlFor="editorShort" className="gx-form-label">Short Description:</label>
                    <div style={{width:"100%"}}>

                    <Editor
                    editor={ ClassicEditor }
                    data={news.short_description}
                    onChange={( event, editor ) => {
                        const data = editor.getData();
                        console.log(data)
                        setNews({...news, short_description:data})
                    }}
                    config={custom_editor_config}
                    name="short_description"
                    />
                    <Tooltip visible={news.short_description?false:true} placement="bottomLeft" title="Short Description is Required" color={'red'}>
                    </Tooltip>

                    </div>
                </div>
                <div className="gx-form-group">
                    <label htmlFor="editorLong" className="gx-form-label">Long Description: </label>
                    <div style={{width:"100%"}}>
                    <Editor
                    editor={ ClassicEditor }
                    data={news.long_description}
                    onChange={( event, editor ) => {
                        const data = editor.getData();
                        setNews({...news, long_description:data})
                    }}
                    config={custom_editor_config}
                    name="long_description"
                    />
                    <Tooltip visible={news.long_description?false:true} placement="bottomLeft" title="Long Description is Required" color={'red'}>
                    </Tooltip>
                    </div>
                </div>
                <div className="gx-form-group">
                    <label className="gx-form-label">Parent Tag: </label>
                    <Select
                        className="gx-w-100"
                        mode="tags"
                        // value={newsId && selectedTag.tag[0].parent_id}
                        // defaultValue={newsId && news.tag ?.map(tag => tag.name)}
                        placeholder="Select Tags"
                        defaultValue={newsId && selectedParentTag}
                        onChange={
                            (value) => {
                                if(value.length > 1) {
                                    message.warning(
                                    {
                                        content:'Select only One Tag',
                                        style: {
                                            marginTop: '10vh',
                                        },
                                    })
                                    value.pop()
                                }else {
                                    setSelectedParentTagId(value[0])
                                }
                            }
                        }
                        onSelect= {(value) => {
                            if(typeof value === 'string') {
                                createParentTag(value)
                            } else {
                                setParentId(value)
                            }
                        }}
                        onDeselect ={() => {
                            setSelectedChildTag(null)
                            setSelectedTag({tag:null})
                            parentTagFetch()
                        }}
                        name="parent_tag"
                    >
                        {parentTagOption}
                    </Select>
                </div>
                <div className="gx-form-group">
                    <label className="gx-form-label">Tags: </label>
                    <Select
                        className="gx-w-100"
                        mode="tags"
                        value = {newsId && selectedChildTag?.map(tag => tag)}
                        placeholder="Select Tags"
                        onChange={(value) => {
                            setSelectedChildTag([...value])
                            setSelectedTag({ ...selectedTag ,tag: value })
                            }
                        }
                        name="child_tag"
                    >
                        {childTagOption}
                    </Select>
                </div>
                <div className="gx-form-group">
                <label className="gx-form-label">Link: </label>
                <Tooltip visible={news.link?false:true} placement="bottomLeft" title="Link is Required" color={'red'}>                
                <Input name="link" type="text" value={news.link} 
                    onChange={(e) => setNews({...news, link: e.target.value})} 
                />
                </Tooltip>
                </div>
                <Form.Item>
                <Tooltip placement="top" title={news.headline && news.short_description && news.long_description && news.link && news.image_link ? "" : "Please fill required fields" } >
                <Button name="submit" onClick={handleSubmit} style={{float:'right'}} className="gx-w-20" type="primary" htmlType="submit" disabled={news.headline && news.short_description && news.long_description && news.link && news.image_link ? false : true}>
                    Submit Details
                </Button>
                </Tooltip>
                <Button onClick={handleClose} style={{float:'right', marginRight:'15px'}} className="gx-w-20" type="primary" htmlType="button" disabled={loading} name="close">
                    Close
                </Button>
                </Form.Item>
        </Form>
    </Card>
    </div>
    </Spin>
    )
}

export default CreateNews;
