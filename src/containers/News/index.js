import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getAllNews, updateStatusNews } from '../../appRedux/actions';
import { getAllTechnologies, getTechnologyTags, updateStatusTechnology } from '../../appRedux/actions/Technologies';
import ListView from '../../components/ListView';
import axios from 'axios'

const News = () => {

    const {news} = useSelector(({news}) => news);
    const {technologies, tags} = useSelector(({technologies}) => technologies);

    const dispatch = useDispatch();

    useEffect(() => {
        getAllNews(dispatch);
        getTechnologyTags(dispatch);
    }, [])

    return (
        <ListView listToBeShown={news} searchTitleText="news" link="news" title="News" getAllFunction={getAllNews} updateModuleItem={updateStatusNews} tags={tags}/>
    )
}

export default News;
