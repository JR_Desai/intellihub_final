import { CREATE_SERVICE_SUCCESS, EDIT_SERVICE_DETAILS, GET_ALL_SERVICES_SUCCESS, GET_SERVICE_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_SERVICE_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    services: [],
    service: {},
    showRemove: false,
    showEdit: false
}

export default function serviceReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_SERVICE_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_SERVICES_SUCCESS: {
            return {
                ...state,
                loader: false,
                services: action.payload
            }
        }
        case GET_SERVICE_SUCCESS: {
            return {
                ...state,
                loader: false,
                service: action.payload[0]
            }
        }
        case CREATE_SERVICE_SUCCESS: {
            return {
                ...state,
                loader: false,
                service: action.payload
            }
        }
        case UPDATE_SERVICE_SUCCESS: {
            return {
                ...state,
                loader: false,
                service: action.payload
            }
        }    
        default:
            return state;
    }
}
