import React, { useState } from "react";
import {Button} from "antd";
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from "react-redux";
import { editCompanyDetails, showConfirmRemove } from "../../../appRedux/actions/Companies";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";

const ProfileHeader = () => {

  const dispatch = useDispatch();

  const history = useHistory();

  const {company} = useSelector(({companies}) => companies);

  const [showRemove] = useState(true);

  const handleEditClick = () => {
    editCompanyDetails(true, dispatch);
  }

  const handleRemoveClick = () => {
    showConfirmRemove(showRemove, dispatch);
  }

  return (
    <div className="gx-profile-banner">
      <div className="gx-profile-container">
        <div className="gx-profile-banner-top">
          <div className="gx-profile-banner-top-left">
            <div className="gx-profile-banner-avatar">
              <img className="gx-size-90 gx-rounded-sm" alt={company.name} src={company.image_link} />
            </div>
            <div className="gx-profile-banner-avatar-info gx-w-75">
              <h2 className="gx-mb-2 gx-mb-sm-3 gx-fs-xxl gx-font-weight-light">{company.name}</h2>
              <p className="gx-mb-0 gx-fs-lg">{company.description}</p>
            </div>
              <div className="gx-profile-banner-top-right">
                <Link to={`${history.location.pathname}/update`}>
                  <div>
                  <Button type="primary" onClick={handleEditClick} className="gx-profile-btn gx-mb-3 gx-px-lg-5" icon={<EditOutlined />}> {"Edit"} </Button>
                  </div>
                </Link>
                <Button type="primary" onClick={handleRemoveClick} className="gx-profile-btn gx-px-5" icon={<DeleteOutlined />}>
                  Delete
                </Button>
              </div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default ProfileHeader;
