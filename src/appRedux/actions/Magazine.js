import axios from 'axios';
import { CREATE_MAGAZINES_SUCCESS, GET_ALL_MAGAZINES_SUCCESS, GET_MAGAZINES_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_MAGAZINES_SUCCESS } from '../../constants/ActionTypes';

export const createMagazine = async (data, uploads, dispatch) => {
        try {
                const magazine = await axios.post(`/magazines/create/`, data);
                const magazineId = magazine.data.id;
                const fileUpdate = await axios.patch(`magazines/update/${magazineId}`, uploads)
                dispatch({
                        type: CREATE_MAGAZINES_SUCCESS,
                        payload: fileUpdate.data.results
                });
                getAllMagazines(dispatch);
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllMagazines = async (dispatch) => {
        try {
                const magazines = await axios.get(`/magazines/view/`);
                magazines.data.results.forEach((result) => result["image_link"] = result["cover_image"]);
                dispatch({
                        type: GET_ALL_MAGAZINES_SUCCESS,
                        payload: magazines.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getMagazine = async (magazineId, dispatch) => {
        try {
                const magazine = await axios.get(`/magazines/view/?id=${magazineId}`);
                dispatch({
                        type: GET_MAGAZINES_SUCCESS,
                        payload: magazine.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateMagazine = async (magazineId, data, uploads, dispatch) => {
        try {
                const magazine = await axios.all(
                [
                        axios.put(`/magazines/update/${magazineId}`, {
                                cover_image: data.cover_image,
                                name: data.name,
                                published_date: data.published_date,
                                link: data.link,
                                short_description : data.short_description,
                                row_status: 3,
                                row_status_code : "READY FOR REVIEW",
                                status: true
                        }),
                        axios.patch(`/magazines/update/${magazineId}`, uploads)
                ]);
                dispatch({
                        type: UPDATE_MAGAZINES_SUCCESS,
                        payload: magazine.data
                })
                getAllMagazines(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusMagazine = async (magazineId, data, dispatch) => {
        try {
                const magazine = await axios.put(`/magazines/update/${magazineId}`, {
                        name: data.name,
                        description: data.description,
                        row_status: data.row_status
                }
                );
                dispatch({
                        type: UPDATE_MAGAZINES_SUCCESS,
                        payload: magazine.data
                })
                getAllMagazines(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const deleteMagazine = async (magazineId, dispatch) => {
        try {
                await axios.delete(`/magazines/delete/${magazineId}`);
                dispatch ({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
                getAllMagazines(dispatch);
                } catch (err) {
                console.log(err);
        }
};
