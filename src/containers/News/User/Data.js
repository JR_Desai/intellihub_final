export const news = {
    "count": 11,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": "35f13233-cd67-4370-8571-2eebfca432b3",
            "headline": "Western Pest Services Helps Local Churches Open for Services",
            "date": "2021-04-07",
            "short_description": "Western helped keep bats out of a churchâ€™s belfry and donated Western PurClean service to help two churches open up for services.",
            "long_description": "PARSIPPANY, N.J. â€” Churches are getting ready to open their doors to more and more parishioners as the capacity restrictions start to loosen. Pests and pathogens could put a damper on that and to all the people that have missed their church community. In order to help prepare these churches to have in-person services, Western Pest Services stepped in to help.The First Presbyterian Church of Rutherford, N.J., has been a place of worship for over 150 years. Their community outreach programs â€“ before and during the pandemic â€“ reach far beyond their local community. After the destruction brought on by Hurricane Katrina in New Orleans, the First Presbyterian Church of Rutherford adopted a Presbyterian Church there to assist in the rebuilding and recovery of their building. They have been providing online services but based on the loosening restrictions, they are ready to open their doors again.",
            "image_link": "https://giecdn.azureedge.net/storage/fileuploads/image/2021/04/07/westernchurckclean.jpg?w=736&h=414&mode=crop",
            "upload_image": null,
            "link": "https://www.pctonline.com/article/western-helps-local-churches-opens/",
            "video_link": null,
            "tag": [
                5
            ],
            "row_status": {
                "id": 2,
                "status_code": "DISAPPROVED",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-04T16:07:25.999159Z",
            "updated_at": "2021-05-04T16:07:25.999159Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "4d8aa895-85e2-492f-8099-817c9ceee3c1",
            "headline": "3-Watt High Power UV Flashlight",
            "date": "2021-04-09",
            "short_description": "J.F.Oakes announces its new Pro-Pest rechargeable, more powerful UV LED Flashlight.",
            "long_description": "The flashlight is a 3-watt high power UV product with 3-volt battery and on/off switch. LEDâ€™s are energy saving and contain no filament to burn our or break.The Pro-Pest UV LED Flashlight can be used to florescent rodent urine and to locate rodent habitats.The Pro-Pest UV LED Flashlight comes individually boxed with AC adaptor and handy holster.For more information, contact J. F. Oakes at 662-746-7276",
            "image_link": "https://giecdn.azureedge.net/storage/fileuploads/image/products/201829/oakes_flashlight.jpg?w=400&&scale=width",
            "upload_image": null,
            "link": "https://www.pctonline.com/product/3-watt-high-power-uv-flashlight/",
            "video_link": null,
            "tag": [
                1
            ],
            "row_status": {
                "id": 2,
                "status_code": "DISAPPROVED",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-04T16:07:26.461166Z",
            "updated_at": "2021-05-04T16:07:26.461166Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "4da4790c-2883-443b-8cd9-e1cb61e50032",
            "headline": "NPMA Legislative Day Attendees Make Their Voices Heard Virtually",
            "date": "2020-03-18",
            "short_description": "COVID-19 moved Legislative Day from Washington to computer screens, but industry professionals still connected with their representatives.",
            "long_description": "Despite a change in format from in-person to virtual, the pest control industry sill got its messages to Congress during Legislative Day.Using the Soapbox platform, attendees met with their Senators and Congressmen to share why the pest control industry is an essential service and to advocate for the industry on the following issues:â€¢ Educating members about the pest control industryâ€™s connection to public health.â€¢ Priorities in future COVID packagesâ€¢ Educating members on the problematic provisions of the PACTPA (Neguse/Udall) Bill from last Congress, which is expected to be re-introduced.EDUCATION. An important goal for this yearâ€™s Legislative Day attendees was to raise awareness of the importance of the pest control industry. In a Zoom presentation, Rollinsâ€™ Bonnie Rabe addressed Legislative Day attendees on this topic, noting, â€œWe were designated as essential during the pandemic, which proved correct given the increases we saw in calls from our customers. Insects and rodents do not heed the call to socially distance.'Among the messages NPMA asked attendees so share with their congressional reps was that the pest control is essential every day. â€œWe protect children who may be allergic from stinging insects on the playgrounds where they play,' Rabe said. â€œWe control cockroaches in homes and schools, which bring allergies or asthma attacks. In yards and in recreational areas, where we all like to spend our free time these days, we donâ€™t have to worry about things like is Rocky Mountain spotted fever.'Rabe added that while there are several important issues for attendees to discuss with their reps, she encouraged them to share a bit about their professionalism and their businesses because â€œit will make a difference and bring it home to them and can open many doors for discussions down the road, because pests affect everyone and we are the protectors of public health, property and families.'COVID-RELATED ISSUES. Pest control operators, like other business owners, have felt the stress of COVID-19 on numerous levels. Legislative Day attendees asked their congressional representatives to support a pair of issues in future bills aimed at providing relief to businesses impacted by COVID-19:  The Healthy Workplaces Tax Credit Act (S. 537); and the AG CHAIN Act (Assistance and Gratitude for Coronavirus Heroes in Agribusiness who are Invaluable to the Nation). NPMA Director of Public Policy Jake Plevelich reviewed these two issues with attendees.The Healthy Workplaces Tax Credit Act (S. 537). This bipartisan bill creates a refundable tax credit for 50% of the costs incurred by a business for disinfecting, extra cleaning, and other measures to keep Americans safe and healthy. Plevelich said this is a great tax credit because it cuts two ways. â€œIt's a tax credit that we can use for PPE and various equipment if we're doing pest control or to reopen our own offices. Or for our customers to hire us since we are performing disinfectant services.'AG CHAIN Act. Introduced in May 2020 by Rep. Glenn Thompson (R-Pa.) and Rep. Dwight Evans (D-Pa.), the AG CHAIN Act is an extension of the previously introduced GROCER Act. It would provide a federal tax holiday and a payroll tax exemption for all essential employees in the food and agriculture industry defined by recent Department of Homeland Security guidance. These provisions would take place during the first quarter of the year for individuals making less than $75,000 annually. For those eligible â€œIt's a big thank you and recognition for their service and their dedication to operating and making sure that we can have an operative food supply chain during the COVID-19 pandemic,' Plevelich said.PESTICIDE ISSUES. NPMA Vice President of Public Policy Ashley Amidon reviewed with attendees an issue related to pesticide regulation, H.R.7940/ S.4406), 'The Protect Americaâ€™s Children from Toxic Pesticides Act (PACTPA). Introduced by Rep. Joe Neguse (D-CO) and Senator Tom Udall (D-NM) in 2020, this bill has several problematic parts, including the repeal of pesticide preemption from the 44 states where it currently exists, and allow local governments to regulate pesticides instead.  This would allow every local community to enact legislation and other policies without being vetoed or preempted by state law.'So, for example, you could have a county board of supervisors, the city council, or if you live in a borough or a parish, whatever governs those entitiesâ€¦these are groups that don't have scientific expertise,' said Amidon. She added that since many local governments are facing budgeting restraints they donâ€™t have the resources to bring in â€œan economist or an epidemiologists, anyone to really look at the science or the economic impact, which means that those decisions become political.'The call to action for Legislative Day attendees was to explain to their congressional reps their opposition to the reintroduction of H.R.7940/S.4406 or any similar legislation that reduces the role that science and state lead agencies play. Other highlights from 2021 Legislative Day included:Former U.S. Hud Secretary JuliÃ¡n Castro and former U.S. Senator Jeff Flake spoke on the current political climate in a General Session sponsored by FMC. Flake reflected on his time in Congress, noting that the divisiveness among Republicans and Democrats in Congress is not as bad as it looks, but 'unfortunately there are just too few political incentives these days to want to legislate and reach across the aisle.' Relating his talk to Legislative Day attendees, Castro said that small business support has been one of the strong bipartisan areas of agreement.Prior to Castro and Flakeâ€™s session, FMC National Sales Manager for North America Professional Solutions presented the sixth PestVets â€˜Veteran of the Yearâ€™ award to Stan Cope (AP&G). Dan Carrothers commercial director for North American Professional Solutions, then presented the FMC Legislative Day Award to Rollinsâ€™ Bonnie Rabe.Legislative Day speaker and political pundit Nathan Gonzales said heâ€™s not seeing the Republican party â€œgo off into the political wilderness and start to search for their soul' as is usually the case after losing a Presidential election and control of the Senate. His session was sponsored by Corteva AgriScience.PCT and BASF presented the 2020 Technician of the Year Awards via a video presentation (click here to watch). Following the presentation, PCTâ€™s Dan Moreland reviewed BASF-sponsored research done in partnership with PCT and NPMA that examined the generational shift in the pest management industry. Moreland will be giving a complimentary in-depth presentation on March 25, at 11 a.m, (EST) on this topic. Click here to register.Prior to Capitol Hill visits, MGK hosted the â€œCapitol Hill Virtual Appointments Kick Off.' Attendees heard from U.S. Sen. Joni Ernst (R-Iowa) who complimented attendees for staying involved in the political process despite the pandemic. Legislative Day participants then proceeded to their virtual Hill meetings. NPMA said a total of 166 meetings, 102 House appointments and 64 Senate appointments took place.",
            "image_link": "https://giecdn.azureedge.net/storage/fileuploads/image/2021/03/22/legislativedaycollagev2.jpg?w=736&h=414&mode=crop",
            "upload_image": null,
            "link": "https://www.pctonline.com/article/legislative-day-2021-recep/",
            "video_link": null,
            "tag": [],
            "row_status": {
                "id": 2,
                "status_code": "DISAPPROVED",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-04T16:07:25.686167Z",
            "updated_at": "2021-05-04T16:07:25.686167Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "5d5e4afa-e3e1-4aff-8dfd-7ce7ebfd397c",
            "headline": "Spotta Bed Bug Detection System",
            "date": "2020-03-11",
            "short_description": "PMPs can enhance their pest management program with Spottaâ€™s bed bug detection system.",
            "long_description": "PMPs can enhance their pest management program with Spottaâ€™s bed bug detection system. Spotta says its Bed Pod monitors can pest management professionals identify the presence of bed bugs early and treat the problem with pinpoint accuracy and speed. Designed for lodging and accommodation businesses â€” including hotels, managed retirement villages and care homes â€” the smart technology is easy-to-use and low maintenance. A pheromone lure draws the pests into the Bed Pod which is fitted beneath the mattress. On entering, a tiny camera is triggered and Spottaâ€™s AI detection system analyzes and classifies the pest, sending users an email alert of its presence. Spotta says it has protected hundreds of thousands of room-nights across Europe and the UK. Now available in the U.S., Spotta complements bed bug treatment services by offering an early detection program.",
            "image_link": "https://giecdn.azureedge.net/storage/fileuploads/image/2020/05/11/bedpodv2.jpg?w=400&h=300&mode=crop&scale=both",
            "upload_image": null,
            "link": "https://www.pctonline.com/products/category/bed-bug-control/",
            "video_link": null,
            "tag": [
                1
            ],
            "row_status": {
                "id": 2,
                "status_code": "DISAPPROVED",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-04T16:07:25.207582Z",
            "updated_at": "2021-05-04T16:07:25.207582Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "67b76c42-f5ec-496d-87d4-c6930152b204",
            "headline": "UPFDA Members to Meet Virtually at Spring Conference",
            "date": "2021-04-05",
            "short_description": "Members from the United Producers, Formulators & Distributors Association (UPFDA) will meet virtually for an afternoon of learning and networking on April 21.",
            "long_description": "FREDERICKSBURG, Va. â€“ Everyone would agree it has been an unprecedented year for businesses serving the structural pest control industry, including members of the United Producers, Formulators & Distributors Association (UPFDA), who will meet virtually at their annual Spring Conference April 21. This yearâ€™s event is designed to help UPFDA members â€œnavigate the new world of business' brought on by the COVID-19 pandemic, according to Executive Director Andrea Coron. What Sustainable Protocols and Strategies will you put into place to ensure continual growth and development for your business? How will you build Meaningful B2B Relationships in this new hybrid in-person/virtual business world? And when the pressure builds, which it will, do you have the Pressure Cooker Confidence that will enable you to press through when the going gets tough? These are the educational sessions planned for the UPFDA Spring Conference featuring an all-star lineup of speakers including:â€¢ Great companies, regardless of their size, demand extraordinary leadership, strategic thinking, well-crafted planning, teamwork, implementation, and execution, according to Greg Clendenin, CEO of the Clendenin Consulting Group, a company with deep roots in the structural pest control industry. In this informative educational session devoted to Sustainable Protocols and Strategies, learn about the keys to long-term business growth from an industry leader with a proven track record of success at such well-known companies as Middleton Lawn & Pest Control, Sears Authorized Termite and Pest Control, and Heron Home & Outdoor.",
            "image_link": "https://giecdn.azureedge.net/storage/fileuploads/image/2021/04/05/upfdaspeakers.jpg?w=736&h=414&mode=crop",
            "upload_image": null,
            "link": "https://www.pctonline.com/article/upfda-spring-conference-virtual/",
            "video_link": null,
            "tag": [],
            "row_status": {
                "id": 2,
                "status_code": "DISAPPROVED",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-04T16:07:26.151153Z",
            "updated_at": "2021-05-04T16:07:26.151153Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "9c964994-6b79-4167-ac2f-82e06f21f039",
            "headline": "Arrow Exterminators Acquires Tomasello",
            "date": "2021-03-31",
            "short_description": "Atlanta-based Arrow Exterminators announced the merger and acquisition of Tomasello, Inc., with offices in West Palm Beach, Fla.",
            "long_description": "Atlanta-based Arrow Exterminators announced the merger and acquisition of Tomasello, Inc., with offices in West Palm Beach, Fla. Arrow says this merger strengthens its presence in West Palm Beach and enables the company to service more residential and commercial customers along the coast from Jupiter to Boynton Beach.Founded in 1928 by Rudolph (Rudy) and Blanche Tomasello and later owned by their daughter Regina Tomasello Doll, current President Charles (Chuck) Doll took over as President of Tomasello in 1997. Tomasello has been a full-service company since the early days, offering general pest, lawn and ornamental, termite and fumigation services. 'Tomaselloâ€™s history mirrors that of Arrow Exterminators, with both being family companies,' said Charles Doll.  â€œWhen it came time to find a company to acquire our family business, it was an easy decision to select Arrow. The sale of a business is always difficult for company owners.  You want the acquiring company to take care of your employees like family.' 'We are so pleased to welcome the customers and team members of Tomasello to the Arrow family,' said Emily Thomas Kendrick, chief executive officer of Arrow Exterminators. â€œThe experience of this team and their desire to take great care of customers makes them a perfect fit for Arrow.'Tim Pollard, president and chief operating officer, Arrow Exterminators, said, â€œTomasello is an extremely well-respected name in the state of Florida, and we are proud to have this solid team of seasoned professionals join the Arrow team. We feel fortunate that Chuck selected Arrow to take care of their team members and customers. Florida is a key state for us as we continuously look to merge with high-quality companies who share our goals, principles and culture.'",
            "image_link": "https://giecdn.azureedge.net/storage/fileuploads/image/2021/03/30/arrowtomasello.jpg?w=736&h=414&mode=crop",
            "upload_image": null,
            "link": "https://www.pctonline.com/article/arrow-acquires-tomasello/",
            "video_link": null,
            "tag": [],
            "row_status": {
                "id": 2,
                "status_code": "DISAPPROVED",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-04T16:07:25.523153Z",
            "updated_at": "2021-05-04T16:07:25.523153Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "abe4b410-ff47-437c-b85d-8c9e83acd65d",
            "headline": "Frank Meek Recognized with IPM Award",
            "date": "2021-04-12",
            "short_description": "Meek, technical services manager at Rollins, recently received the International IPM Award of Excellence (IPM Practitioner â€“ Non-Academic), for his outstanding work in developing and implementing novel IPM-based strategies of controlling pests in the urban environment.",
            "long_description": "ATLANTA â€” Frank Meek, technical services manager, Rollins recently received the International IPM Award of Excellence (IPM Practitioner â€“ Non-Academic), for his outstanding work in developing and implementing novel IPM-based strategies of controlling pests in the urban environment.The 2021 International IPM Achievement Awards recognize individuals who have made exceptional achievements in IPM adoption, implementation and sustainability. Meek has spent his entire career in the urban pest management industry. He has worked with manufacturers to help research new ways of controlling pests, specifically German cockroaches in the urban environment. Meek has written protocols that emphasize the importance of inspection, monitoring, and physical controls and cultural controls within a program where cockroach gel bait was necessary. His work on the introduction of cockroach gel bait into an Integrated Pest Management (IPM) plan to Orkin was a game changer in the 1990â€™s. Meek is known for his excellent communication skills, which led him to over 60 different countries, providing initial training and orientation to local staff. 'It is truly an honor to receive this recognition. However, this is not my recognition alone, but also to the many coworkers and teammates across the company and industry who have been a part of advancing our work and industry,' said Meek.",
            "image_link": "https://giecdn.azureedge.net/storage/fileuploads/image/2021/04/12/frankmeekrollins.jpg?w=736&h=414&mode=crop",
            "upload_image": null,
            "link": "https://www.pctonline.com/article/meek-rollins-ipm-award/",
            "video_link": null,
            "tag": [],
            "row_status": {
                "id": 2,
                "status_code": "DISAPPROVED",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-04T16:07:25.840165Z",
            "updated_at": "2021-05-04T16:07:25.840165Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "b74da30f-d476-44f4-bf0f-5c26bae98652",
            "headline": "Name",
            "date": "2019-02-11",
            "short_description": "organizer",
            "long_description": "description",
            "image_link": null,
            "upload_image": null,
            "link": "hq",
            "video_link": null,
            "tag": [],
            "row_status": {
                "id": 3,
                "status_code": "READY FOR REVIEW",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-05T15:51:54.801345Z",
            "updated_at": "2021-05-05T15:51:54.801345Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "ccddc1f0-dda6-48ff-948a-73d6d72b21b5",
            "headline": "Blight May Increase Public Health Risk from Mosquito-borne Diseases",
            "date": "2021-03-16",
            "short_description": "LSU researchers recently published findings that blight leads to an increased abundance of disease-carrying mosquitoes.",
            "long_description": "LSU researchers recently published findings that blight leads to an increased abundance of disease-carrying mosquitoes. The researchers investigated the presence of several mosquito species in two adjacent but socio-economically contrasting neighborhoods in Baton Rouge: the historic Garden District, a high income neighborhood, and the Old South neighborhood, a low-income area. They found significantly higher adult and larvae abundance of the Asian tiger mosquito (a carrier of Zika and dengue) and higher mosquito habitat availabilityâ€”particularly discarded tiresâ€”in the Old South neighborhood. This indicates that environmental conditions in the low-income neighborhood were most ideal for this mosquito to breed and proliferate.'These two neighborhoods are very similar in terms of vegetation cover, human population and density of households. One of the main differences is blight. One neighborhood has a lot of blight in the form of abandoned residences, empty lots and mismanaged waste, and the other neighborhood does not. It was the perfect set of conditions for addressing this question,' said Rebeca de JesÃºs Crespo, lead author and an assistant professor in LSUâ€™s College of the Coast & Environment. In recent years, the Old South neighborhood has been the focus of revitalization plans by multiple stakeholder groups. The researchers recommended that these blight reduction efforts continue for the benefit of public health.'This is an area at high risk of these mosquito-borne diseases,' said Madison Harrison, co-author of the publication. â€œAll that it takes for these diseases to spread is for the right vector to be infected with the pathogen and to bite humans at the right point of incubation of said pathogen.'  So far, Zika and dengue are not currently present in the state. However, Louisianaâ€™s climate is ideal for these diseases to spread once introduced.  According to de JesÃºs Crespo, Harrison was an invaluable addition to the team. Currently, she is a public health masterâ€™s student at LSU Health New Orleans. 'In the College of the Coast & Environment, for almost every project that we do, we are integrated and interdisciplinary. We take a broad approach to doing research that is important for solving problems in real time. For this project, it was important to me to include Madison from LSU Health Sciences as well as community stakeholders who could provide their expertise and unique perspectives,' de JesÃºs Crespo said.  The researchers examined the prevalence of two container-breeding species of mosquitoes that are known to spread disease, the Asian tiger mosquito and the southern house mosquito (a carrier of West Nile virus). They inspected potential larvae habitats (such as discarded tires, discarded Styrofoam cups and snack bags, plant pots and water baths) in publicly accessible locations and calculated the percentage of those that contained larvae. Additionally, they placed adult mosquito traps around the perimeter of some private homes with the permission from the homeowner; in an abandoned house and in an empty lot with trash accumulation in the higher income neighborhood. They found that the adult population of the southern house mosquito was fairly diffuse, but the lower income neighborhood had significantly higher numbers of Asian tiger mosquito (adults and larvae) and higher numbers of total mosquito larvae. This shows that the presence of discarded container habitats due to neglect provides more breeding grounds for disease-carrying mosquitoes, disproportionately affecting low-income groups.  'I think everybody can agree that urban blight is a problem we need to solve here in Baton Rouge. Mosquito risk is one of those factors that could impact human health and that adds another level of importance with that,' de JesÃºs Crespo said.  Additional collaborators on this project include Rachel Rogers, a graduate student in the LSU Department of Environmental Sciences, and Randy Vaeth from East Baton Rouge Parish Mosquito Abatement and Rodent Control. Source: Louisiana State University",
            "image_link": "https://giecdn.azureedge.net/storage/fileuploads/image/2021/03/16/asiantigermosquito3.jpg?w=736&h=414&mode=crop",
            "upload_image": null,
            "link": "https://www.pctonline.com/article/blight-increase-public-health-risks-mosquito-borne-diseases/",
            "video_link": null,
            "tag": [
                5
            ],
            "row_status": {
                "id": 2,
                "status_code": "DISAPPROVED",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-04T16:07:25.370152Z",
            "updated_at": "2021-05-04T16:07:25.370152Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "d7ce98a7-e36d-4e8c-b827-c9d846325c0d",
            "headline": "Name",
            "date": "2019-02-11",
            "short_description": "organizer",
            "long_description": "description",
            "image_link": null,
            "upload_image": null,
            "link": "hq",
            "video_link": null,
            "tag": [],
            "row_status": {
                "id": 3,
                "status_code": "READY FOR REVIEW",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-05T15:51:48.288110Z",
            "updated_at": "2021-05-05T15:51:48.288110Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "dee4188c-56de-4c53-9cbf-2e97f6c406ee",
            "headline": "New Invasive Mosquito Species Found in South Florida",
            "date": "2021-04-09",
            "short_description": "Aedes scapularis is capable of transmitting disease and poses a potential public health risk, University of Floridaâ€™s Institute of Food and Agricultural Sciences (UF/IFAS) scientists reported.",
            "long_description": "VERO BEACH, Fla. â€“ A new mosquito species capable of transmitting disease, Aedes scapularis, has arrived in Florida and shows signs it could survive across multiple urban and rural habitats, posing a potential public health risk. In a new study from the University of Floridaâ€™s Institute of Food and Agricultural Sciences (UF/IFAS), scientists predict where in Florida environmental conditions may be suitable for this new species to spread, now that it has invaded the Florida peninsula. This new, nonnative mosquito the team discovered and announced last November can transmit yellow fever virus, Venezuelan equine encephalitis virus, dog heartworm and other pathogens to humans and animals. It has a wide range, from Texas to parts of South America and throughout much of the Caribbean. The species is widespread in Miami-Dade and Broward counties.â€¯In the latest study â€“â€“ a follow-up published in the Multidisciplinary Digital Publishing Institute journal Insects â€“â€“ scientists at the UF/IFAS Florida Medical Entomology Laboratory indicated through model predictions that suitable environments for Aedes scapularis could be present along coastal counties in much of Florida. More specifically, the areas along Floridaâ€™s Atlantic and Gulf coastlines predicted to be highly suitable for this species are from Monroe and Miami-Dade counties, north to Martin County on the Atlantic Coast, and in Citrus County on the Gulf Coast.",
            "image_link": "https://giecdn.azureedge.net/storage/fileuploads/image/2021/04/07/aedesscapularis.jpg?w=736&h=414&mode=crop",
            "upload_image": null,
            "link": "https://www.pctonline.com/article/new-invasive-mosquito-species-found-in-south-florida/",
            "video_link": null,
            "tag": [
                5
            ],
            "row_status": {
                "id": 2,
                "status_code": "DISAPPROVED",
                "status": true
            },
            "status": true,
            "created_at": "2021-05-04T16:07:26.300153Z",
            "updated_at": "2021-05-04T16:07:26.301153Z",
            "created_by": null,
            "updated_by": null
        }
    ]
}

export const company = {
    "count": 6,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": "12a259a7-38fc-4974-a98a-4d2698130e97",
            "name": "Wipro",
            "parent": {
                "id": "3a061b59-e083-4cdc-acda-f4af1ce82c77",
                "name": "Reliance Industries",
                "image_link": "http://1c78ba753881.ngrok.io/media/media/company/logos/reliance.png",
                "row_status_code": "APPROVED",
                "status": true
            },
            "image_link": null,
            "website": "https://www.wipro.com",
            "row_status": {
                "id": 1,
                "status_code": "APPROVED",
                "status": true
            },
            "description": "Wipro Limited is an Indian multinational corporation that provides information technology, consulting and business process services. It is headquartered in Bangalore, Karnataka, India. In 2013, Wipro separated its non-IT businesses and formed the privately owned Wipro Enterprises.",
            "year_of_incorporation": "1995-06-07",
            "hq": "India",
            "location": {
                "locations": [
                    {
                        "mail": "queries@wipro.com",
                        "name": "Main Office",
                        "phone": "+918046827999",
                        "address": "Maker Chambers - IV, Nariman Point Mumbai - 400 021, India"
                    }
                ]
            },
            "market": "India",
            "funding": null,
            "valuation": null,
            "no_of_employees_min": 0,
            "no_of_employees_max": 5000,
            "business_model": "Agile",
            "country_phone_code": "+91",
            "phone": 2235550005,
            "email": "queries@wipro.com",
            "similar_companies": null,
            "business_revenue_mix": {
                "2019": 20000,
                "2020": 40000
            },
            "operating_status": true,
            "alumni_founders": null,
            "news": [],
            "awards": [],
            "company_size": "0f0d2ca0-1ea5-44a8-9b2c-ae1302ca3942",
            "ownership_pattern": false,
            "category": {
                "id": 1,
                "name": "Pest Control",
                "code": "PT",
                "parent": null,
                "module": 3,
                "status": true
            },
            "linkedin": "",
            "country": {
                "id": 96,
                "name": "Guyana",
                "code": "GUY",
                "status": true
            },
            "state": {
                "id": 11,
                "name": "Helmand",
                "code": "HEL",
                "country": {
                    "id": 1,
                    "name": "Afghanistan",
                    "code": "AFG",
                    "status": true
                },
                "status": true
            },
            "city": {
                "id": 19,
                "name": "Bāmyān",
                "state": {
                    "id": 5,
                    "name": "Bamyan",
                    "code": "BAM",
                    "country": {
                        "id": 1,
                        "name": "Afghanistan",
                        "code": "AFG",
                        "status": true
                    },
                    "status": true
                },
                "status": true
            },
            "status": true,
            "created_at": "2021-05-05T10:23:14.290005Z",
            "updated_at": "2021-05-06T05:21:21.254000Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "3a061b59-e083-4cdc-acda-f4af1ce82c77",
            "name": "Reliance Industries",
            "parent": null,
            "image_link": "http://1c78ba753881.ngrok.io/media/media/company/logos/reliance.png",
            "website": "https://www.ril.com/",
            "row_status": {
                "id": 1,
                "status_code": "APPROVED",
                "status": true
            },
            "description": "Reliance Industries Limited is an Indian multinational conglomerate company headquartered in Mumbai, India. Reliance owns businesses across India engaged in energy, petrochemicals, textiles, natural resources, retail, and telecommunications.",
            "year_of_incorporation": "2002-07-31",
            "hq": "India",
            "location": {
                "locations": [
                    {
                        "mail": "info@ril.com",
                        "name": "Reliance Industries Limited Mumbai",
                        "phone": "+91-22-3555-0000",
                        "address": "Maker Chambers - IV, Nariman Point Mumbai - 400 021, India"
                    }
                ]
            },
            "market": "India",
            "funding": null,
            "valuation": null,
            "no_of_employees_min": 0,
            "no_of_employees_max": 5000,
            "business_model": "Agile",
            "country_phone_code": "+91",
            "phone": 2235550009,
            "email": "info@ril.com",
            "similar_companies": null,
            "business_revenue_mix": {
                "2019": 20000,
                "2020": 40000
            },
            "operating_status": true,
            "alumni_founders": 1,
            "news": [],
            "awards": [],
            "company_size": null,
            "ownership_pattern": false,
            "category": null,
            "linkedin": "",
            "country": {
                "id": 1,
                "name": "Afghanistan",
                "code": "AFG",
                "status": true
            },
            "state": {
                "id": 2,
                "name": "Badghis",
                "code": "BDG",
                "country": {
                    "id": 1,
                    "name": "Afghanistan",
                    "code": "AFG",
                    "status": true
                },
                "status": true
            },
            "city": {
                "id": 3,
                "name": "Jurm",
                "state": {
                    "id": 1,
                    "name": "Badakhshan",
                    "code": "BDS",
                    "country": {
                        "id": 1,
                        "name": "Afghanistan",
                        "code": "AFG",
                        "status": true
                    },
                    "status": true
                },
                "status": true
            },
            "status": true,
            "created_at": "2021-05-05T10:23:06.254289Z",
            "updated_at": "2021-05-05T10:23:06.254289Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "59b0d4ca-9b97-4fe4-b1ab-3c1ed7305171",
            "name": "Larsen & Toubro",
            "parent": null,
            "image_link": "http://1c78ba753881.ngrok.io/media/media/company/logos/LT.png",
            "website": "https://www.larsentoubro.com/",
            "row_status": {
                "id": 1,
                "status_code": "APPROVED",
                "status": true
            },
            "description": "Larsen & Toubro Limited, commonly known as L&T, is an Indian multinational technology, engineering, construction, manufacturing and financial services conglomerate headquartered in Mumbai, Maharashtra, India. It was founded by two Danish engineers taking refuge in India.",
            "year_of_incorporation": "1968-02-07",
            "hq": "India",
            "location": {
                "locations": [
                    {
                        "mail": "infodesk@larsentoubro.com",
                        "name": "Main Office",
                        "phone": "1800-209-4545",
                        "address": "Maker Chambers - IV, Nariman Point Mumbai - 400 021, India"
                    }
                ]
            },
            "market": "India",
            "funding": null,
            "valuation": null,
            "no_of_employees_min": 0,
            "no_of_employees_max": 5000,
            "business_model": "Agile",
            "country_phone_code": "+91",
            "phone": 2235550002,
            "email": "infodesk@larsentoubro.com",
            "similar_companies": null,
            "business_revenue_mix": {
                "2019": 20000,
                "2020": 40000
            },
            "operating_status": true,
            "alumni_founders": 1,
            "news": [],
            "awards": [],
            "company_size": null,
            "ownership_pattern": false,
            "category": null,
            "linkedin": "",
            "country": {
                "id": 55,
                "name": "Cote D'Ivoire (Ivory Coast)",
                "code": "CIV",
                "status": true
            },
            "state": {
                "id": 900,
                "name": "San José Province",
                "code": "SJ",
                "country": {
                    "id": 54,
                    "name": "Costa Rica",
                    "code": "CRI",
                    "status": true
                },
                "status": true
            },
            "city": {
                "id": 20820,
                "name": "Acosta",
                "state": {
                    "id": 900,
                    "name": "San José Province",
                    "code": "SJ",
                    "country": {
                        "id": 54,
                        "name": "Costa Rica",
                        "code": "CRI",
                        "status": true
                    },
                    "status": true
                },
                "status": true
            },
            "status": true,
            "created_at": "2021-05-05T10:23:09.793441Z",
            "updated_at": "2021-05-05T10:23:09.793441Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "684577e3-40ba-4d77-bf30-6d241ffd45e3",
            "name": "Infosys",
            "parent": null,
            "image_link": "http://1c78ba753881.ngrok.io/media/media/company/logos/infosys.jpg",
            "website": "https://www.infosys.com",
            "row_status": {
                "id": 1,
                "status_code": "APPROVED",
                "status": true
            },
            "description": "Infosys Limited is an Indian multinational information technology company that provides business consulting, information technology and outsourcing services. The company is headquartered in Bangalore.",
            "year_of_incorporation": "1995-06-07",
            "hq": "India",
            "location": {
                "locations": [
                    {
                        "mail": "queries@infosys.com",
                        "name": "Main Office",
                        "phone": " +2348021500111",
                        "address": "Maker Chambers - IV, Nariman Point Mumbai - 400 021, India"
                    }
                ]
            },
            "market": "India",
            "funding": null,
            "valuation": null,
            "no_of_employees_min": 0,
            "no_of_employees_max": 5000,
            "business_model": "Agile",
            "country_phone_code": "+91",
            "phone": 2235550004,
            "email": "queries@infosys.com",
            "similar_companies": null,
            "business_revenue_mix": {
                "2019": 20000,
                "2020": 40000
            },
            "operating_status": true,
            "alumni_founders": null,
            "news": [],
            "awards": [],
            "company_size": null,
            "ownership_pattern": false,
            "category": null,
            "linkedin": "",
            "country": {
                "id": 136,
                "name": "Maldives",
                "code": "MDV",
                "status": true
            },
            "state": {
                "id": 2500,
                "name": "Kuala Lumpur",
                "code": "14",
                "country": {
                    "id": 135,
                    "name": "Malaysia",
                    "code": "MYS",
                    "status": true
                },
                "status": true
            },
            "city": {
                "id": 64686,
                "name": "Kuala Lumpur",
                "state": {
                    "id": 2500,
                    "name": "Kuala Lumpur",
                    "code": "14",
                    "country": {
                        "id": 135,
                        "name": "Malaysia",
                        "code": "MYS",
                        "status": true
                    },
                    "status": true
                },
                "status": true
            },
            "status": true,
            "created_at": "2021-05-05T10:23:12.268782Z",
            "updated_at": "2021-05-05T10:23:12.268782Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "aaa8b89a-84e9-4756-b120-5fac0a5cdb21",
            "name": "Bharti Airtel",
            "parent": null,
            "image_link": "http://1c78ba753881.ngrok.io/media/media/company/logos/airtel.png",
            "website": "https://www.airtel.in/",
            "row_status": {
                "id": 1,
                "status_code": "APPROVED",
                "status": true
            },
            "description": "Bharti Airtel Limited, also known as Airtel, is an Indian multinational telecommunications services company based in New Delhi, Delhi NCT, India. It operates in 18 countries across South Asia and Africa, and also in the Channel Islands.",
            "year_of_incorporation": "1995-06-07",
            "hq": "India",
            "location": {
                "locations": [
                    {
                        "mail": "customercare@.airtel.com",
                        "name": "Main Office",
                        "phone": " +2348021500111",
                        "address": "Maker Chambers - IV, Nariman Point Mumbai - 400 021, India"
                    }
                ]
            },
            "market": "India",
            "funding": null,
            "valuation": null,
            "no_of_employees_min": 0,
            "no_of_employees_max": 5000,
            "business_model": "Agile",
            "country_phone_code": "+91",
            "phone": 2235550003,
            "email": "customercare@airtel.com",
            "similar_companies": null,
            "business_revenue_mix": {
                "2019": 20000,
                "2020": 40000
            },
            "operating_status": true,
            "alumni_founders": 1,
            "news": [],
            "awards": [],
            "company_size": null,
            "ownership_pattern": false,
            "category": null,
            "linkedin": "",
            "country": {
                "id": 215,
                "name": "Swaziland",
                "code": "SWZ",
                "status": true
            },
            "state": {
                "id": 3900,
                "name": "Kronoberg County",
                "code": "G",
                "country": {
                    "id": 216,
                    "name": "Sweden",
                    "code": "SWE",
                    "status": true
                },
                "status": true
            },
            "city": {
                "id": 110195,
                "name": "Cubillas de los Oteros",
                "state": {
                    "id": 3816,
                    "name": "Léon",
                    "code": "LE",
                    "country": {
                        "id": 210,
                        "name": "Spain",
                        "code": "ESP",
                        "status": true
                    },
                    "status": true
                },
                "status": true
            },
            "status": true,
            "created_at": "2021-05-05T10:23:11.248188Z",
            "updated_at": "2021-05-05T10:23:11.248188Z",
            "created_by": null,
            "updated_by": null
        },
        {
            "id": "e8ad07a1-5162-4e1e-a90a-35ebeb59a2cf",
            "name": "Tata Consultancy Services",
            "parent": null,
            "image_link": "http://1c78ba753881.ngrok.io/media/media/company/logos/tcs.jpg",
            "website": "https://www.tcs.com",
            "row_status": {
                "id": 1,
                "status_code": "APPROVED",
                "status": true
            },
            "description": "Tata Consultancy Services is an Indian multinational information technology services and consulting company, headquartered in Mumbai, Maharashtra, India. As of February 2021 TCS is largest company in the IT sector in the world by Market capitalisation of $169.2 billion.",
            "year_of_incorporation": "1968-04-01",
            "hq": "India",
            "location": {
                "locations": [
                    {
                        "mail": "head.talentacquisition@tcs.com",
                        "name": "Main Office",
                        "phone": "022-67784070",
                        "address": "Maker Chambers - IV, Nariman Point Mumbai - 400 021, India"
                    }
                ]
            },
            "market": "India",
            "funding": null,
            "valuation": null,
            "no_of_employees_min": 0,
            "no_of_employees_max": 5000,
            "business_model": "Agile",
            "country_phone_code": "+91",
            "phone": 2235550001,
            "email": "head.talentacquisition@tcs.com",
            "similar_companies": null,
            "business_revenue_mix": {
                "2019": 20000,
                "2020": 40000
            },
            "operating_status": true,
            "alumni_founders": 1,
            "news": [],
            "awards": [],
            "company_size": null,
            "ownership_pattern": false,
            "category": null,
            "linkedin": "",
            "country": {
                "id": 5,
                "name": "American Samoa",
                "code": "ASM",
                "status": true
            },
            "state": {
                "id": 125,
                "name": "Sétif",
                "code": "19",
                "country": {
                    "id": 4,
                    "name": "Algeria",
                    "code": "DZA",
                    "status": true
                },
                "status": true
            },
            "city": {
                "id": 406,
                "name": "Aïn Arnat",
                "state": {
                    "id": 125,
                    "name": "Sétif",
                    "code": "19",
                    "country": {
                        "id": 4,
                        "name": "Algeria",
                        "code": "DZA",
                        "status": true
                    },
                    "status": true
                },
                "status": true
            },
            "status": true,
            "created_at": "2021-05-05T10:23:08.248477Z",
            "updated_at": "2021-05-05T10:23:08.248477Z",
            "created_by": null,
            "updated_by": null
        }
    ]
}