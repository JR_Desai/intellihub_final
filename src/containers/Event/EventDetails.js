import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

import {Avatar, Badge, Col, Row} from "antd";
import ConfirmPopup from '../../components/ConfirmPopup';
import { showConfirmRemove } from '../../appRedux/actions/Companies';
import Widget from '../../components/Widget';
import { deleteEvents, getEvents} from '../../appRedux/actions';

const EventDetails = props => {

    const dispatch = useDispatch();

    const history = useHistory();

    const {event, showRemove} = useSelector(({events}) => events);

    const eventId = props.match.params.id;

    useEffect(() => {
        getEvents(eventId, dispatch);
    }, [])

    const editTech = () => {
        history.push(`update/${eventId}`)
    }

    const onRemove = () => {
        deleteEvents(eventId, dispatch);
        history.replace("/event");
    }

    const removeTech = () => {
        showConfirmRemove(true, dispatch);
    }

    return (
        <div className="gx-main-content-wrapper">
            <div className="gx-module-detail gx-module-list">
                <div className="gx-module-detail-item gx-module-detail-header">
                    <Row>
                        <Col xs={24} sm={12} md={17} lg={12} xl={17}>
                            <div className="gx-flex-row">
                            <div className="gx-user-name gx-mr-md-4 gx-mr-2 gx-my-1">
                                <div className="gx-flex-row gx-align-items-center gx-pointer">
                                    <Avatar className="gx-mr-3" src="" alt="" />
                                    <h4 className="gx-mb-0">Demo Name</h4>
                                </div>
                            </div>
                            </div>
                        </Col>

                        <Col xs={24} sm={12} md={7} lg={12} xl={7}>
                            <div className="gx-flex-row gx-justify-content-end">
                                <i className="gx-icon-btn icon icon-edit" onClick={() =>
                                    editTech()}
                                />
                                <i className="gx-icon-btn icon icon-trash" onClick={() =>
                                    removeTech()}
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
                <Widget className="gx-module-detail-item">
                    <div className="gx-form-group gx-flex-row gx-align-items-center gx-flex-nowrap">
                        <Col className="gx-flex-row gx-flex-1 gx-flex-nowrap">
                            <div className="gx-w-25">
                                <img
                                className='' src={event.image_link}
                                alt="..." />
                            </div>
                            <div className="gx-w-75 gx-ml-5">
                                <div className="h1">
                                    {event.event_name}
                                </div>
                                    <span className="gx-ml-auto gx-mt-1 gx-mb-3">{event.start_date}</span>
                                <div className="gx-mt-5 gx-mb-5">
                                    <span className="h3">Link: </span>
                                        <a href={event.link} target="_blank" className="gx-text-primary">{event.link}</a>
                                </div>
                                <div className="gx-mt-4 gx-mb-auto">
                                    Tags:
                                    {event["tags"]?.map((label, index) => {
                                        return <Badge key={index} count={label.name} style={{backgroundColor: "#FD0090", margin: "0 5px"}} />
                                    })}
                                </div>
                            </div>
                        </Col>
                    </div>
                </Widget>
                <ConfirmPopup showStatus={showRemove} onConfirm={onRemove} />
            </div>
        </div>
    )
}

export default EventDetails;
