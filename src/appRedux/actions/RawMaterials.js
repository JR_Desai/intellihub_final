import axios from "axios";
import moment from "moment";
import {
  CREATE_RAW_MATERIAL_SUCCESS,
  FETCH_ERROR,
  FETCH_START,
  FETCH_SUCCESS,
  GET_ALL_RAW_MATERIALS_SUCCESS,
  GET_RAW_MATERIAL_SUCCESS,
  SHOW_CONFIRM_REMOVE,
  UPDATE_RAW_MATERIAL_SUCCESS,
} from "../../constants/ActionTypes";

const module_link = "raw_material";

export const patchImage = async (id, image) => {
  const data = new FormData();
  data.append("image_upload", image);
  await axios.patch(`/${module_link}/update/${id}`, data, {
    headers: {
      "content-type": "multipart/form-data",
    },
  });
};

export const createRawMaterials = async (data, dispatch, callback) => {
  try {
    const rawMaterials = await axios.post(
      `/${module_link}/create/`,
      { ...data, image_upload: null },
      {
        headers: {
          // "content-type": "multipart/form-data",
        },
      }
    );

    if (rawMaterials.data?.id && data?.image_upload) {
      await patchImage(rawMaterials.data?.id, data?.image_upload);
    }

    dispatch({
      type: CREATE_RAW_MATERIAL_SUCCESS,
      payload: rawMaterials.data,
    });
    getAllRawMaterials(dispatch);
    callback && callback();
  } catch (err) {
    console.log(err.message);
  }
};

export const getAllRawMaterials = async (dispatch) => {
  try {
    dispatch({
      type: FETCH_START,
    });
    const rawMaterials = await axios.get(`/${module_link}/view/`);
    const mappedData = Array.isArray(rawMaterials.data.results)
      ? rawMaterials.data.results.map((d) => {
          return {
            ...d,
            image_link: d?.image_upload || d?.image_link,
          };
        })
      : [];
    dispatch({
      type: GET_ALL_RAW_MATERIALS_SUCCESS,
      payload: mappedData,
    });
    dispatch({
      type: FETCH_SUCCESS,
    });
  } catch (err) {
    dispatch({
      type: FETCH_ERROR,
    });
    console.log(err);
  }
};

export const getRawMaterial = async (rawMaterialID, dispatch) => {
  try {
    const rawMaterial = await axios.get(
      `/${module_link}/view/?id=${rawMaterialID}`
    );
    dispatch({
      type: GET_RAW_MATERIAL_SUCCESS,
      payload: rawMaterial.data.results,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getRawMaterialTags = async (dispatch) => {
  try {
    const rawMaterials = await axios.get(`/tag/view/`);
    // dispatch({
    //         type: UPDATE_RAW_MATERIAL_SUCCESS,
    //         payload: technology.data.results
    // })
  } catch (err) {
    console.log(err);
  }
};

export const updateRawMaterial = async (
  rawMaterialID,
  data,
  dispatch,
  callback
) => {
  try {
    const rawMaterials = await axios.put(
      `/${module_link}/update/${rawMaterialID}`,
      { ...data, image_upload: null }
    );
    if (rawMaterialID && data?.image_upload) {
      await patchImage(rawMaterials.data?.id, data?.image_upload);
    }
    dispatch({
      type: UPDATE_RAW_MATERIAL_SUCCESS,
      payload: rawMaterials.data,
    });
    callback && callback();
  } catch (err) {
    console.log(err);
  }
};

export const updateStatusRawMaterial = async (
  rawMaterialID,
  data,
  dispatch
) => {
  try {
    const rawMaterial = await axios.patch(
      `/${module_link}/update/${rawMaterialID}`,
      {
        row_status: data.row_status,
        approval_date: moment().format("YYYY-MM-DD"),
      }
    );
    dispatch({
      type: UPDATE_RAW_MATERIAL_SUCCESS,
      payload: rawMaterial.data,
    });
    getAllRawMaterials(dispatch);
  } catch (err) {
    console.log(err);
  }
};

export const deleteRawMaterial = async (rawMaterialID, dispatch, callback) => {
  try {
    await axios.delete(`/${module_link}/delete/${rawMaterialID}`);
    dispatch({
      type: SHOW_CONFIRM_REMOVE,
      payload: false,
    });
    callback && callback();
  } catch (err) {
    console.log(err);
  }
};
