export const admin = [
    {
        value: 1,
        label: "System Admin"
    },
    {
        value: 2,
        label: "QC Admin"
    },
    {
        value: 3,
        label: "Analyst Admin"
    },
    {
        value: 4,
        label: "Client Admin"
    },
    {
        value: 5,
        label: "User Admin"
    }
]

