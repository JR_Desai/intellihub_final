import React, {useEffect, useState} from 'react'
import {FiBell, FiSearch} from "react-icons/fi";
import * as ReactBootStrap from "react-bootstrap";
import {Link} from "react-router-dom";
import {Menu} from 'antd';
import axios from 'axios';
import './NavbarStyle.css'
import logo from 'images/intellihub-logo.png';
import userAvatar from 'images/men-avatar.png';
import dummyImage from 'images/dummy-image.png';

const { SubMenu } = Menu;

function NavbarNew() {

    const dontClose = (e) => {
        e.stopPropagation();
    };
    const [state, setState] = useState({
        current: 'mail',
    });
    
    const handleClick = e => {
        console.log('click ', e);
        setState({ current: e.key });
    };

    const [module, setModule] = useState([]);

    const getModule = async () => {
        try {
            const module = await axios.get(`/module/view/`);
            setModule(module.data);
        } catch (err) {
            console.log(err);
        }
    }
    useEffect(() => {getModule()}, [])

    var [subModule, setSubModule] = useState([]);

    const getSubModule = async (id) => {
      try {
        const subModuleData = await axios.get(`/sub_module/view/?filters={"type":"operator","data":{"attribute":"parent_module_id","operator":"=","value":"${id}"}}`);
        setSubModule(subModuleData.data)
      } catch (err) {
          console.log(err);
      }
    };

    let showOverlay = false;

    const activateSubMenu = function() {
      showOverlay = !showOverlay;
      console.log('showOverlay ==>', showOverlay);
    };

    const prepareMenuItems = () => {
      let parentMenuItems = [];
      for (let i = 0; i < module.length; i++) {
        if (module[i].parent_module_id === null) {
          parentMenuItems.push(module[i]);
        } else {
          parentMenuItems.map( (item, itemIndex) => {
            if (parentMenuItems[itemIndex].id === module[i].parent_module_id) {
              if (!parentMenuItems[itemIndex].childMenu) {
                parentMenuItems[itemIndex].childMenu = [];
                parentMenuItems[itemIndex].childModuleCodes = [];
              }
              if (parentMenuItems[itemIndex].childModuleCodes.indexOf(module[i].module_code) < 0) {
                parentMenuItems[itemIndex].childMenu.push(module[i]);
                parentMenuItems[itemIndex].childModuleCodes.push(module[i].module_code)
              }
            }
          });
        }
      }
      return parentMenuItems;
    };
    const searchCategories = [
        {id: 0, name: 'Latest Around The World'},
        {id: 1, name: 'Research & Literature'},
        {id: 2, name: 'Products'},
        {id: 3, name: 'Regulatory & Safety'},
        {id: 4, name: 'Companies'},
        {id: 5, name: 'Market Reports'},
        {id: 6, name: 'Technology'},
        {id: 7, name: 'Archive'},
        {id: 8, name: 'Deals & Investments'},
        {id: 9, name: 'Newsletters'},
        {id: 10, name: 'Magazines'},
        {id: 11, name: 'Events and Conferences'},
        ];

    const notificationList = [
      {
        id: 0,
        img: 'http://via.placeholder.com/300x180',
        title: 'New Documentary Details WSDA’s Hunt for the Asian Giant Hornet',
        publishDate: '28 JAN, 2021',
        category: 'technology',
        read: false
      },
      {
        id: 1,
        img: 'http://via.placeholder.com/300x180',
        title: 'Dettol partners with The Football Association',
        publishDate: '28 JAN, 2021',
        category: 'Companies',
        read: false
      },
      {
        id: 2,
        img: 'http://via.placeholder.com/300x180',
        title: 'RB partners with British Red Cross and joins Disaster Relief Alliance',
        publishDate: '28 JAN, 2021',
        category: 'Regulatory Updates',
        read: true
      },
      {
        id: 3,
        img: 'http://via.placeholder.com/300x180',
        title: 'One year of transformation: Our journey',
        publishDate: '28 JAN, 2021',
        category: 'Market trends',
        read: true
      },
      {
        id: 4,
        img: 'http://via.placeholder.com/300x180',
        title: 'Harpic Mission Paani hosts India’s largest Waterthon on Republic Day',
        publishDate: '28 JAN, 2021',
        category: 'Companies',
        read: true
      },
    ];

    const userDetails = {
      firstName: 'Karan',
      lastName: 'Panchal',
      profilePicture: userAvatar
    };



    return (
        <>
          {/*<div className={'full-bg-overlay' + (showOverlay ? '' : ' d-none')} />*/}
          <section className="header-top-section">
            <div className="container">
            <div className="row">
              <div className="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12">

                <nav className="navbar navbar-expand-lg p-0 top-nav-section">
                  <a className="navbar-brand site-logo p-0" href="/user"><ReactBootStrap.Image src={logo} alt="Intellihub" /></a>
                  <div className="collapse navbar-collapse">
                    <form className="form-inline my-2 my-lg-0 mr-auto ml-auto">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <button className="btn dropdown-toggle mr-0 header-category-dropdown" type="button"
                                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category
                          </button>
                          <div className="dropdown-menu search-category-dropdown-menu">
                            <div className="row">
                            {searchCategories.map(category=> <div className="category_check col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <input
                                  type="checkbox"
                                  className="checkbox"
                                  value={category.id}
                                  onClick={dontClose}
                                  id={"categoryCheckbox-" + category.id}
                              />
                              <label htmlFor={"categoryCheckbox-" + category.id} className="checkbox_title">{category.name}</label>
                            </div>)}
                            </div>
                          </div>
                        </div>
                        <div className="input-group-append">
                          <input className="form-control header-search-input" name="q" type="search" placeholder="Search here" aria-label="Search" />
                          <button className="btn btn-header-search" type="submit"><FiSearch className="search-icon" /></button>
                        </div>
                      </div>
                    </form>
                    <ul className="navbar-nav ml-auto">
                      <li className="nav-item dropdown">
                        <a className="nav-link notification-dropdown-toggle active-notification" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <FiBell className="notification-icon" />
                        </a>
                        <div className="dropdown-menu dropdown-menu-right notification-dropdown" aria-labelledby="navbarDropdown">
                          <div className="notification-dropdown-header title-font">Notifications</div>
                          <ul className="notification-dropdown-list">
                            {notificationList.map(notifiication =>
                            <li className={(notifiication.read ? 'notification-read' : 'notification-unread') + ' notification-dropdown-list-item'}>
                              <a href="#" title={notifiication.title}>
                                <div className="card-horizontal">
                                  <div className="img-square-wrapper">
                                    <img className="" src={notifiication.img} alt={notifiication.title} />
                                  </div>
                                  <div className="notification-content">
                                    <p className="card-title char-cut-line-2">{notifiication.title}</p>
                                    <p className="card-text char-cut-line-1">{notifiication.category} - {notifiication.publishDate}</p>
                                  </div>
                                </div>
                              </a>
                            </li>
                            )}
                          </ul>
                        </div>
                      </li>
                      <li className="nav-item dropdown ml-3">
                        <a className="nav-link user-profile-dropdown-toggle dropdown-toggle" href="#" id="userProfileActions" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <div className="user-image">
                            <img src={userDetails.profilePicture} title={userDetails.firstName + ' ' + userDetails.lastName} />
                          </div>
                          <div className="user-title">{userDetails.firstName}</div>
                        </a>
                        <div className="dropdown-menu dropdown-menu-right user-profile-dropdown" aria-labelledby="userProfileActions">
                          <ul className="user-profile-dropdown-list">
                            <li><a href="#" className="active">Your Profile</a></li>
                            <li><a href="#">Analyst Support</a></li>
                            <li><a href="#">Bookmarks</a></li>
                            <li><a href="#">Archive</a></li>
                            <li><a href="#">Notification Settings</a></li>
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </div>
                </nav>
              </div>
            </div>
          </div>
          </section>
          <section className="header-nav-section">
            <div className="container">
              <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <nav className="navbar navbar-expand-lg p-0">
                  <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav main-menu-bar">
                      {prepareMenuItems.call().map( item =>
                        <li className={'nav-item' + (item.childMenu && item.childMenu.length > 0 ? ' dropdown' : '')} >
                          <a className={'nav-link' + (item.childMenu && item.childMenu.length > 0 ? ' dropdown-toggle' : '')} href={item.childMenu && item.childMenu.length > 0 ? '#' : '/user/' + item.module_name.replaceAll(/\s/g,'').toLowerCase()} id={item.module_code} {...(item.childMenu && item.childMenu.length > 0 ? {'data-toggle':'dropdown', 'aria-haspopup':'true', 'aria-expanded':'false'} : {})}>{item.module_name}</a>
                          {(item.childMenu && item.childMenu.length > 0) && <div className="dropdown-menu" aria-labelledby={item.module_code}>
                            {item.childMenu.map( childItem => <a className="dropdown-item" href={'/user/' + childItem.module_name.replaceAll(/\s/g,'').toLowerCase()}><div className="sub-menu-icon"><img src={dummyImage}/></div><div className="sub-menu-title">{childItem.module_name}</div></a> )}
                          </div>}
                        </li>
                      )}
                    </ul>
                  </div>
                </nav>

              </div>
              </div>
            </div>
          </section>
        </>
    )
}

export default NavbarNew;
