import { CREATE_RAW_MATERIAL_SUCCESS, EDIT_RAW_MATERIAL_DETAILS, GET_ALL_RAW_MATERIALS_SUCCESS, GET_RAW_MATERIAL_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_RAW_MATERIAL_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    rawMaterials: [],
    rawMaterial: {},
    showRemove: false,
    showEdit: false
}

export default function rawMaterialReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_RAW_MATERIAL_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_RAW_MATERIALS_SUCCESS: {
            return {
                ...state,
                loader: false,
                rawMaterials: action.payload
            }
        }
        case GET_RAW_MATERIAL_SUCCESS: {
            return {
                ...state,
                loader: false,
                rawMaterial: action.payload[0]
            }
        }
        case CREATE_RAW_MATERIAL_SUCCESS: {
            return {
                ...state,
                loader: false,
                rawMaterial: action.payload
            }
        }
        case UPDATE_RAW_MATERIAL_SUCCESS: {
            return {
                ...state,
                loader: false,
                rawMaterial: action.payload
            }
        }    
        default:
            return state;
    }
}
