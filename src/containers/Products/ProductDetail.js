import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";

import { Avatar, Badge, Col, Divider, List, Row, Typography } from "antd";
import ConfirmPopup from "../../components/ConfirmPopup";
import { showConfirmRemove } from "../../appRedux/actions/Companies";
import Widget from "../../components/Widget";
import { deleteProduct, getProducts } from "../../appRedux/actions";
const DefaultImage = "/assets/images/layouts/default_img.jpg";

const NewsDetail = (props) => {
  const dispatch = useDispatch();

  const history = useHistory();

  const { product, showRemove } = useSelector(({ products }) => products);

  const productId = props.match.params.id;

  useEffect(() => {
    getProducts(productId, dispatch);
  }, [props.match.params.id]);

  const edit = () => {
    history.push(`update/${productId}`);
  };

  const onRemove = () => {
    deleteProduct(productId, dispatch, () => {
      history.replace("/products");
    });
  };

  const remove = () => {
    showConfirmRemove(true, dispatch);
  };
  console.log({ product });
  return (
    <div className="gx-main-content-wrapper">
      <div className="gx-module-detail gx-module-list">
        <div className="gx-module-detail-item gx-module-detail-header">
          <Row>
            <Col xs={24} sm={12} md={17} lg={12} xl={17}>
              <div className="gx-flex-row">
                <div className="gx-user-name gx-mr-md-4 gx-mr-2 gx-my-1">
                  <div className="gx-flex-row gx-align-items-center gx-pointer">
                    <img
                      style={{
                        objectFit: "contain",
                        height: 60,
                        width: 60,
                      }}
                      className="gx-mr-3"
                      src={
                        product?.primary_image ||
                        product?.primary_image_upload ||
                        DefaultImage
                      }
                      alt=""
                    />
                    <h4 className="gx-mb-0">
                      {product?.name} ({product?.row_status?.status_code})
                    </h4>
                  </div>
                </div>
              </div>
            </Col>

            <Col xs={24} sm={12} md={7} lg={12} xl={7}>
              <div className="gx-flex-row gx-justify-content-end">
                <i
                  className="gx-icon-btn icon icon-edit"
                  onClick={() => edit()}
                />
                <i
                  className="gx-icon-btn icon icon-trash"
                  onClick={() => remove()}
                />
              </div>
            </Col>
          </Row>
        </div>
        <Widget className="gx-module-detail-item">
          <div className="gx-form-group gx-flex-row gx-align-items-center gx-flex-nowrap">
            <Col className="gx-flex-row gx-flex-1 gx-flex-nowrap">
              <div className="gx-w-25">
                <img
                  className=""
                  src={
                    product?.primary_image ||
                    product?.primary_image_upload ||
                    DefaultImage
                  }
                  alt="..."
                />
              </div>
              <div className="gx-w-75 gx-ml-5">
                <div className="h1">{product?.name}</div>
                <span className="gx-ml-auto gx-mb-3">
                  {product?.category?.name || ""}
                </span>
                <div className="gx-mt-3 gx-mb-5">
                  <span className="h3">Description: </span>
                  <p className="gx-text-grey">{product?.description}</p>
                </div>
                <Divider orientation="left">General Details</Divider>
                <List
                  size="small"
                  bordered
                  dataSource={[
                    {
                      title: "Bar code",
                      value: product?.bar_code,
                    },
                    {
                      title: "Production code",
                      value: product?.production_code,
                    },
                    {
                      title: "Website",
                      value: product?.website,
                    },
                    {
                      title: "Date published",
                      value: product?.date_published,
                    },
                    {
                      title: "Approval date",
                      value: product?.approval_date,
                    },
                    {
                      title: "Variant",
                      value: product?.product_variant,
                    },
                    {
                      title: "No of Variant",
                      value: product?.number_of_variants,
                    },
                    {
                      title: "Brand",
                      value: product?.brand?.name,
                    },
                    {
                      title: "Company",
                      value: `${product?.brand?.company?.name} (
                        ${product?.brand?.company?.market}))`,
                    },
                    {
                      title: "Launch type",
                      value: product?.launch_type?.name,
                    },
                    {
                      title: "Private label",
                      value: product?.private_label?.name,
                    },
                  ]}
                  renderItem={(item) => (
                    <List.Item>
                      <Typography.Text strong>{item.title}</Typography.Text> :{" "}
                      {item.value}
                    </List.Item>
                  )}
                />

                <Divider orientation="left">Price Details</Divider>
                <List
                  size="small"
                  bordered
                  dataSource={[
                    {
                      title: "Price in US dollars",
                      value: product?.price_us_dollars,
                    },
                    {
                      title: "Price in euros",
                      value: product?.price_euros,
                    },
                    {
                      title: "Local currency price",
                      value: product?.price_local_currency,
                    },
                    {
                      title: "Currency",
                      value: `${product?.currency?.name || "-"} (${
                        product?.currency?.code
                      })`,
                    },
                  ]}
                  renderItem={(item) => (
                    <List.Item>
                      <Typography.Text strong>{item.title}</Typography.Text> :{" "}
                      {item.value}
                    </List.Item>
                  )}
                />

                <Divider orientation="left">Package Details</Divider>
                <List
                  size="small"
                  bordered
                  dataSource={[
                    {
                      title: "Unit Pack Size",
                      value: product?.unit_pack_size,
                    },
                    {
                      title: "Packaging units",
                      value: product?.packaging_units?.name,
                    },
                    {
                      title: "Packaging units code",
                      value: product?.packaging_units?.code,
                    },
                    {
                      title: "Package type",
                      value: product?.package_type?.name,
                    },
                    {
                      title: "Package material",
                      value: product?.package_material?.name,
                    },
                  ]}
                  renderItem={(item) => (
                    <List.Item>
                      <Typography.Text strong>{item.title}</Typography.Text>:{" "}
                      {item.value}
                    </List.Item>
                  )}
                />

                <Divider orientation="left">Ingredients</Divider>
                <List
                  size="small"
                  bordered
                  dataSource={
                    Array.isArray(product["ingredients"])
                      ? product["ingredients"]
                      : []
                  }
                  renderItem={(item) => <List.Item>{item.name}</List.Item>}
                />

                <Divider orientation="left">Positioning Claims</Divider>
                <List
                  size="small"
                  bordered
                  dataSource={
                    Array.isArray(product["positioning_claims"])
                      ? product["positioning_claims"]
                      : []
                  }
                  renderItem={(item) => <List.Item>{item.name}</List.Item>}
                />
                <Divider orientation="left">Other information</Divider>
                <List
                  size="small"
                  bordered
                  dataSource={[
                    {
                      title: "Storage",
                      value: product?.storage,
                    },
                    {
                      title: "Nutrition",
                      value: product?.nutrition,
                    },
                    {
                      title: "Flavours",
                      value: product?.flavours?.name,
                    },
                    {
                      title: "Fragrances",
                      value: `${product?.fragrances?.name}`,
                    },
                    {
                      title: "Hyperlink",
                      value: `${product?.hyperlink}`,
                    },
                    {
                      title: "Ownership pattern",
                      value: `${product?.ownership_pattern}`,
                    },
                    {
                      title: "Revenue",
                      value: `${product?.revenue}`,
                    },
                    {
                      title: "Product funding",
                      value: `${product?.product_funding}`,
                    },
                    {
                      title: "Sustainablity",
                      value: `${product?.sustainablity}`,
                    },
                    {
                      title: "Technology type",
                      value: `${product?.technology_type?.name}`,
                    },
                  ]}
                  renderItem={(item) => (
                    <List.Item>
                      <Typography.Text strong>{item.title}</Typography.Text> :{" "}
                      {item.value}
                    </List.Item>
                  )}
                />
              </div>
            </Col>
          </div>
        </Widget>
        <ConfirmPopup showStatus={showRemove} onConfirm={onRemove} />
      </div>
    </div>
  );
};

export default NewsDetail;
