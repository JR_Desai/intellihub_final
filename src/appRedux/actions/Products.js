import axios from "axios";
import moment from "moment";
import {
  CREATE_PRODUCT_SUCCESS,
  FETCH_ERROR,
  FETCH_START,
  FETCH_SUCCESS,
  GET_ALL_PRODUCTS_SUCCESS,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_TAGS,
  SHOW_CONFIRM_REMOVE,
  UPDATE_PRODUCT_SUCCESS,
} from "../../constants/ActionTypes";

const module_link = "finished_product";

export const createProducts = async (data, dispatch, callback) => {
  try {
    const products = await axios.post(`/${module_link}/create/`, data);
    dispatch({
      type: CREATE_PRODUCT_SUCCESS,
      payload: products.data,
    });
    callback && callback();
  } catch (err) {
    console.log(err.message);
  }
};

export const getAllProducts = async (dispatch, filter) => {
  try {
    dispatch({
      type: FETCH_START,
    });
    let url = `/${module_link}/view/`;
    if (filter) {
      url += `?filters=${JSON.stringify(filter)}`;
    }
    const products = await axios.get(url);

    products.data.results.forEach(
      (result) => (result["image_link"] = result["primary_image"])
    );
    dispatch({
      type: GET_ALL_PRODUCTS_SUCCESS,
      payload: products.data.results,
    });
    dispatch({
      type: FETCH_SUCCESS,
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: FETCH_ERROR,
    });
  }
};

export const getProducts = async (productID, dispatch) => {
  try {
    dispatch({
      type: FETCH_START,
    });
    const product = await axios.get(`/${module_link}/view/?id=${productID}`);
    dispatch({
      type: GET_PRODUCT_SUCCESS,
      payload: product.data.results,
    });
    dispatch({
      type: FETCH_SUCCESS,
    });
  } catch (err) {
    dispatch({
      type: FETCH_ERROR,
    });
    console.log(err);
  }
};

export const updateProducts = async (productID, data, dispatch, callback) => {
  try {
    const product = await axios.put(
      `/${module_link}/update/${productID}`,
      data
    );
    dispatch({
      type: UPDATE_PRODUCT_SUCCESS,
      payload: product.data,
    });
    callback && callback();
  } catch (err) {
    console.log(err);
  }
};

export const updateStatusProduct = async (productID, data, dispatch) => {
  try {
    const product = await axios.patch(`/${module_link}/update/${productID}`, {
      row_status: data.row_status,
      approval_date: moment().format("YYYY-MM-DD"),
    });
    dispatch({
      type: UPDATE_PRODUCT_SUCCESS,
      payload: product.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const deleteProduct = async (productID, dispatch, callback) => {
  try {
    await axios.delete(`/${module_link}/delete/${productID}`);
    dispatch({
      type: SHOW_CONFIRM_REMOVE,
      payload: false,
    });
    callback && callback();
  } catch (err) {
    console.log(err);
  }
};

export const getProductTags = async (
  dispatch,
  module = "tag_parent",
  filter
) => {
  try {
    let url = `/${module}/view/`;
    if (filter) {
      url += `?filters=${JSON.stringify(filter)}`;
    }
    const tags = await axios.get(url);
    dispatch({
      type: GET_PRODUCT_TAGS,
      payload: {
        data: tags.data,
        module,
      },
    });
  } catch (err) {
    console.log(err);
  }
};
