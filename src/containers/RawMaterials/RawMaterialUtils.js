import moment from "moment";
import * as yup from "yup";

export const rawMaterialValidationSchema = yup.object().shape({
  name: yup.string().required("Please enter name"),
  manufacturer: yup.string().required("Please select manufacturer"),
  parent_category: yup.number().required("Please select parent category"),
  package_unit: yup.string().required("Please select package unit"),
});

export const getManufacturerObj = (d) => {
  return {
    name: d?.name,
    description: d?.description || null,
    country_phone_code: d?.country_phone_code || null,
    phone: d?.phone || null,
    address: d?.address || null,
    web_link: d?.web_link || null,
    image_link: d?.image_link || null,
    status: true,
    created_by: null,
    updated_by: null,
    id: d?.id,
  };
};

export const getCategoryObj = (d) => {
  return {
    name: d?.name,
    code: d?.name.toUpperCase(),
    parent: d?.parent,
    module: d?.module,
    status: true,
  };
};

export const getPkUnitObj = (d) => {
  return {
    name: d?.name,
    code: null,
    status: true,
  };
};

export const getContainsObj = (d) => {
  return {
    code: d?.code || null,
    description: d?.description || null,
    name: d?.name,
    status: true,
  };
};

export const initialValues = {
  name: "",
  variant: "",
  manufacturer: undefined,
  description: "",
  country: undefined,
  parent_category: undefined,
  category: undefined,
  web_link: "",
  image_link: "",
  image_upload: null,
  row_status: null,
  contains: [],
  properties: "",
  distributor_channel: "",
  regions_covered: "",
  price: 0,
  package_size: 0,
  package_unit: undefined,
  approval_date: null,
  status: true,
};

export const getFormatedOptions = (
  data,
  labelKey = "name",
  valueKey = "id"
) => {
  if (!Array.isArray(data)) return [];
  return data.map((d) => ({
    key: d?.[labelKey],
    label: d?.[labelKey],
    value: d?.[valueKey],
    // value: d?.[labelKey],

    details: d,
  }));
};

export const formateRequestPayload = (values) => {
  const formData = new FormData();
  const obj = {
    name: values?.name,
    variant: values?.variant,
    manufacturer: values?.manufacturerObj,
    description: values?.description,
    country: Array.isArray(values?.country)
      ? values?.country[0]
      : values?.country,
    category: values?.categoryObj,
    web_link: values?.web_link,
    image_link: values?.image_link,
    image_upload: values?.image_upload,
    row_status: values?.row_status,
    contains: values?.containsObj,
    properties: values?.properties,
    distributor_channel: values?.distributor_channel,
    regions_covered: values?.regions_covered,
    price: values?.price,
    package_size: values?.package_size,
    package_unit: values?.package_unitObj,
    status: true,
  };

  return obj;
  Object.keys(obj).map((k) => {
    if (k !== "contains" && k !== "image_upload") {
      if (typeof obj[k] === "object") {
        formData.append(k, JSON.stringify(obj[k]));
      } else {
        formData.append(k, obj[k]);
      }
    }
    if (k === "contains" && Array.isArray(obj?.[k])) {
      obj?.[k].forEach((d) => {
        formData.append(`${k}[]`, JSON.stringify(d));
      });
    }

    if (k === "image_upload" && obj?.[k]) {
      formData.append(k, obj[k]);
    }
  });

  return formData;
};
