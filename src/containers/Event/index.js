import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getAllEvents, updateStatusEvent } from '../../appRedux/actions';
import ListView from '../../components/ListView';

const Event = () => {

    const {events} = useSelector(({events}) => events);

    const dispatch = useDispatch();

    useEffect(() => {
        getAllEvents(dispatch);
    }, [])

    return (
        <ListView listToBeShown={events} searchTitleText="events and conference" link="event" title="Events & Conference" 
            updateModuleItem={updateStatusEvent}
        />
    )
}

export default Event;
