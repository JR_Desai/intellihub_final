import React, { useEffect } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import Companies from '../../../containers/Companies';
import CompanyProfile from '../../../containers/CompanyProfile';
import CreateCompany from '../../../containers/Companies/CreateCompany';
import { useDispatch } from 'react-redux';
import { getAllCompanies } from '../../../appRedux/actions/Companies';
import CreateNews from '../../../containers/News/CreateNews';

const CompanyRoutes = () => {

    const match = useRouteMatch()

    const dispatch = useDispatch();

    useEffect(() => {
        getAllCompanies(dispatch);
    }, [])

    return (
            <Switch>
                <Route exact path={`${match.url}`} component={Companies} />
                <Route exact path={`${match.url}/create`} component={CreateCompany} />
                <Route exact path={`${match.url}/:id/update`} component={CreateCompany} />
                <Route exact path={`${match.url}/:id`} component={CompanyProfile} />
                <Route exact path={`${match.url}/:id/createnews`} component={CreateNews} />
            </Switch>
    );
}

export default CompanyRoutes;
