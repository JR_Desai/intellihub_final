import React, { useState } from "react";
import {Button, Form,  Input} from "antd";
import {Link} from "react-router-dom";
import UserOutlined from "@ant-design/icons/lib/icons/UserOutlined";
import LockOutlined from "@ant-design/icons/lib/icons/LockOutlined";
import { useDispatch } from "react-redux";

const Login = () => {

    const dispatch = useDispatch();

    const initState = {
        firstName: "First Name",
        lastName: "Last Name",
        email: "e@email.com",
        password: "Password",
        country_code: "+91",
        mobile: "987654421",
        user_type: 1,

    }

    const [user, setUser] = useState(initState);

    const onSubmit = () => {
        
    }

    return (
        <>
            <div className="gx-login-header gx-text-center">
            <h1 className="gx-login-title">Login</h1>
            </div>
            <Form className="gx-signin-form gx-form-row0">
                <div className="gx-mt-4 gx-mb-4">
                    <Input value={user.email} prefix={<UserOutlined style={{color: 'rgba(0,0,0,.25)'}}/>}
                            placeholder="Email"
                            onChange={(e) => setUser({...user, email: e.target.value})}
                    />
                </div>
                <div className="gx-mt-4 gx-mb-4">
                <Input value={user.password} prefix={<LockOutlined style={{color: 'rgba(0,0,0,.25)'}}/>}
                        type="password"
                        onChange={(e) => setUser({...user, password: e.target.value})}
                        placeholder="Password"/>
                </div>
                <Link className="gx-mt-4 gx-mb-4" to="/auth/forgot-password">
                    Forgot password
                </Link>
                <Button className="gx-w-100" type="primary" htmlType="submit" onClick={onSubmit}>
                    Create
                </Button>
            </Form>
        </>
    );
};

export default Login;
