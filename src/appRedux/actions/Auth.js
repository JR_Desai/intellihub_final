import axios from "axios";
import {
  SIGNIN_USER,
  SIGNIN_USER_SUCCESS,
  SIGNOUT_USER,
  SIGNOUT_USER_SUCCESS,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS
} from "constants/ActionTypes";

export const userSignUp = (user) => {
  return {
    type: SIGNUP_USER,
    payload: user
  };
};

export const userSignIn = (user) => {
  return {
    type: SIGNIN_USER,
    payload: user
  };
};

export const userSignOut = () => {
  return {
    type: SIGNOUT_USER
  };
};

export const userSignUpSuccess = (authUser) => {
  return {
    type: SIGNUP_USER_SUCCESS,
    payload: authUser
  };
};

export const userSignInSuccess = (authUser) => {
  return {
    type: SIGNIN_USER_SUCCESS,
    payload: authUser
  }
};

export const userSignOutSuccess = () => {
  return {
    type: SIGNOUT_USER_SUCCESS,
  }
};

export const createUser = async (data , dispatch) => {
        try {
                const user = await axios.post(`/account/create/`, {
                    username: data.email,
                    email: data.email,
                    password: data.password,
                    country_code: data.country_code,
                    mobile: data.phone,
                    first_name: data.firstName,
                    last_name: data.lastName,
                    user_type: data.user_type,
                    is_verified: true,
                });
                console.log(user);
                dispatch({
                  type: SIGNUP_USER,
                  payload: user
                });
        } catch (err) {
                console.log(err.message);
        }
};

// export const getAllTechnologies = async (dispatch) => {
//         try {
//                 const technologies = await axios.get(`/technology/view/`);
//                 dispatch({
//                         type: GET_ALL_TECHNOLOGIES_SUCCESS,
//                         payload: technologies.data.results
//                 })
//         } catch (err) {
//                 console.log(err);
//         }
// };

// export const getTechnology = async (technologyID, dispatch) => {
//         try {
//                 const technology = await axios.get(`/technology/view/?id=${technologyID}`);
//                 dispatch({
//                         type: GET_TECHNOLOGY_SUCCESS,
//                         payload: technology.data.results
//                 })
//         } catch (err) {
//                 console.log(err);
//         }
// };

// export const getTechnologyTags = async (dispatch) => {
//         try {
//                 const technology = await axios.get(`/tag/view/`);
//                 dispatch({
//                         type: GET_TECHNOLOGY_TAGS_SUCCESS,
//                         payload: technology.data.results
//                 })
//         } catch (err) {
//                 console.log(err);
//         }
// };

// export const updateTechnology = async (technologyID, data, dispatch) => {
//         try {
//                 const technology = await axios.put(`/technology/update/${technologyID}`, data);
//                 dispatch({
//                         type: UPDATE_TECHNOLOGY_SUCCESS,
//                         payload: technology.data
//                 })
//                 getAllTechnologies(dispatch);
//         } catch (err) {
//                 console.log(err);
//         }
// };

// export const updateStatusTechnology = async (technologyID, data, dispatch) => {
//         try {
//                 const technology = await axios.put(`/technology/update/${technologyID}`, {
//                         name: data.name,
//                         description: data.description,
//                         row_status: data.row_status
//                 }
//                 );
//                 dispatch({
//                         type: UPDATE_TECHNOLOGY_SUCCESS,
//                         payload: technology.data
//                 })
//                 getAllTechnologies(dispatch);
//         } catch (err) {
//                 console.log(err);
//         }
// };

// export const deleteTechnology = async (technologyID, dispatch) => {
//         try {
//                 await axios.delete(`/technology/delete/${technologyID}`);
//                 dispatch ({
//                         type: SHOW_CONFIRM_REMOVE,
//                         payload: false
//                 })
//                 getAllTechnologies(dispatch);
//                 } catch (err) {
//                 console.log(err);
//         }
// };
