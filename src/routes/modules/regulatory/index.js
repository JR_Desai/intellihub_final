import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import Regulatory from '../../../containers/Regulatory';
import CreateRegulatory from '../../../containers/Regulatory/CreateRegulatory';

const RegulatoryRoutes = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={Regulatory} />
            <Route exact={true} path={`${match.url}/create`} component={CreateRegulatory} />
        </Switch>
    )
}

export default RegulatoryRoutes;
