import React from "react";

const AppModuleHeader = (props) => {

    const {placeholder, onChange, value} = props;

    return (
      <div className="gx-w-100 gx-module-box-header-inner">
            <i className="icon icon-search gx-mt-1"></i>
            <input className="gx-w-100 gx-input" type="search" placeholder={placeholder}
                  onChange={onChange}
                  value={value}/>
      </div>
    )
};

export default AppModuleHeader;

AppModuleHeader.defaultProps = {
  styleName: '',
  value: '',
  notification: true,
  apps: true
};
