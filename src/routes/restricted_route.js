export const RestrictedRoute = ({component: Component, location, authUser, ...rest}) =>
    <Route
        {...rest}
        render={props =>
        authUser
            ? <Component {...props} />
            : <Redirect
            to={{
                pathname: '/auth/login',
                state: {from: location}
            }}
            />}
    />;