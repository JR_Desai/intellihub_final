import React, { useState } from 'react'
import {
    Form,
    Button,
    Card,
    Select,
    DatePicker,
} from 'antd';

import { useDispatch, useSelector } from 'react-redux';
import InputGroup from '../../components/InputGroup';
import { useHistory } from 'react-router';
import TextArea from 'antd/lib/input/TextArea';
import { createMarketReports, updateMarketReports } from '../../appRedux/actions';

const CreateMarketReports = props => {


    const history = useHistory();

    const {marketReport} = useSelector(({marketReports}) => marketReports);

    const marketReportsId = props.match.params.id;


    const initState = {
        report_name: "Name",
        publication_date: "",
        cover_image: "",
        short_description: "description",
        long_description: "description",
        author: "",
        view_link: "",
        download_link: ""
    }

    const values = marketReportsId ? marketReport : initState

    const [report, setReport] = useState(values);

    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        marketReportsId ? updateMarketReports(report.id, report, dispatch) : createMarketReports(report,dispatch);
        history.replace("/mr");
    }

    const header = marketReportsId ? "Update" : "Create"

    return (
    <div className="gx-main-content-wrapper">
    <Card className="gx-card" title={`${header} Technology`}>
        <Form
            name="register"
            scrollToFirstError
            >
                <InputGroup name="name" label="Report Name: " type="text" value={report.report_name} 
                    onChange={(e) => setReport({...report, report_name: e.target.value})} 
                />
                <div className="gx-form-group">
                    <label htmlFor="publication_date" className="gx-form-label">Publication Date:</label>
                    <DatePicker name="publication_date" placeholder={report.publication_date}
                        onChange={(e) => setReport({...report, publication_date: e.format("YYYY-MM-DD")})} className="gx-mb-3 gx-w-100"
                    />
                </div>
                <div className="gx-form-group">
                    <label name="short_description" className="gx-form-label">Short Description: </label>
                    <TextArea 
                        name="short_description" 
                        value={report.short_description} 
                        rows={4}
                        onChange={(e) => setReport({...report, short_description: e.target.value})}  
                    />
                </div>
                <div className="gx-form-group">
                    <label name="long_description" className="gx-form-label">Long Description: </label>
                    <TextArea 
                        name="long_description" 
                        value={report.long_description} 
                        rows={4}
                        onChange={(e) => setReport({...report, long_description: e.target.value})}  
                    />
                </div>
                <InputGroup name="author" label="Author: " type="text" value={report.author} 
                    onChange={(e) => setReport({...report, author: e.target.value})} 
                />
                <InputGroup name="cover_image" label="Cover Image: " type="text" value={report.cover_image} 
                    onChange={(e) => setReport({...report, cover_image: e.target.value})} 
                />
                <InputGroup name="view_link" label="View Link: " type="text" value={report.view_link} 
                    onChange={(e) => setReport({...report, view_link: e.target.value})} 
                />
                <InputGroup name="download_link" label="Download Link: " type="text" value={report.download_link} 
                    onChange={(e) => setReport({...report, download_link: e.target.value})} 
                />
            <Button onClick={handleSubmit} className="gx-w-100" type="primary" htmlType="submit">
            Submit
            </Button>
        </Form>
    </Card>
    </div>
    )
}

export default CreateMarketReports;
