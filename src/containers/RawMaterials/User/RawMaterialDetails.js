import moment from "moment";
import React, { useEffect } from "react";
import { FiBookmark, FiShare2 } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { getRawMaterial } from "../../../appRedux/actions/RawMaterials";

const RawMaterialDetails = (props) => {
  const dispatch = useDispatch();
  useEffect(() => {
    getRawMaterial(props.match.params.rawMaterialId, dispatch);
  }, [props.match.params.rawMaterialId]);
  const { rawMaterial } = useSelector(({ rawMaterials }) => rawMaterials);

  console.log({ rawMaterial });
  return (
    <div className="rawMaterialDetailsWrapper">
      <div className="container">
        <div className="rawMaterialDetailsContent">
          <div className="rawMaterialDetailsHead">
            <div className="rawMaterialDetailsTitleBlock">
              <div className="rawMaterialDetailsIdDate">
                <ul>
                  <li>
                    Record ID -{" "}
                    <span className="recordID">{rawMaterial?.id}</span>
                  </li>
                  <li>
                    Date -{" "}
                    <span>{moment(rawMaterial?.id).format("DD/MM/YYYY")}</span>
                  </li>
                </ul>
              </div>
              <h2 className="rawMaterialTitle">{rawMaterial?.name}</h2>
              <h3 className="rawMaterialSubTitle">
                {rawMaterial?.category?.name}
              </h3>
            </div>
            <div className="rawMaterialHeadActionBlock">
              <div className="rawMaterialDownloadDetails">
                <a
                  className="download-dropdown-toggle dropdown-toggle"
                  href="#"
                  id="donwloadActions"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Download All Details
                </a>
                <div
                  className="dropdown-menu downloadDropdown"
                  aria-labelledby="donwloadActions"
                >
                  <ul>
                    <li>
                      <a href="#">All Format</a>
                    </li>
                    <li>
                      <a href="#">Excel</a>
                    </li>
                    <li>
                      <a href="#">PDF</a>
                    </li>
                    <li>
                      <a href="#">Powerpoint</a>
                    </li>
                    <li>
                      <a href="#">Images</a>
                    </li>
                    <li>
                      <a href="#">HTML</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="rawMaterialAction-list">
                <div className="rawMaterialItem">
                  <ul>
                    <li>
                      <a href="#" className="rawMaterialIcon iconBookmark">
                        <FiBookmark />
                      </a>
                    </li>
                    <li>
                      <a href="#" className="rawMaterialIcon iconShare">
                        <FiShare2 />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="rawMaterialDetailsInfo">
            <p>
              <div
                dangerouslySetInnerHTML={{
                  __html: rawMaterial?.description ?? "",
                }}
              ></div>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RawMaterialDetails;
