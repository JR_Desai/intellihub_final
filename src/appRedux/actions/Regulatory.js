import axios from 'axios';
import { CREATE_REGULATORY_SUCCESS, GET_ALL_REGULATORIES_SUCCESS, GET_REGULATORY_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_REGULATORY_SUCCESS } from '../../constants/ActionTypes';

const module_link = "regulatory_safety"

export const createRegulatory = async (data , dispatch) => {
        try {
                const regulatory = await axios.post(`/${module_link}/create/`, data);
                dispatch({
                        type: CREATE_REGULATORY_SUCCESS,
                        payload: regulatory.data
                })
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllRegulatories = async (dispatch) => {
        try {
                const regulatories = await axios.get(`/${module_link}/view/`);
                dispatch({
                        type: GET_ALL_REGULATORIES_SUCCESS,
                        payload: regulatories.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getRegulatory = async (regulatoryID, dispatch) => {
        try {
                const regulatory = await axios.get(`/${module_link}/view/?id=${regulatoryID}`);
                dispatch({
                        type: GET_REGULATORY_SUCCESS,
                        payload: regulatory.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getRegulatoryTags = async (dispatch) => {
        try {
                const regulatory = await axios.get(`/tag/view/`);
                // dispatch({
                //         type: UPDATE_RAW_MATERIAL_SUCCESS,
                //         payload: service.data.results
                // })
        } catch (err) {
                console.log(err);
        }
};

export const updateRegulatory = async (regulatoryID, data, dispatch) => {
        try {
                const regulatory = await axios.put(`/${module_link}/update/${regulatoryID}`, data);
                dispatch({
                        type: UPDATE_REGULATORY_SUCCESS,
                        payload: regulatory.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusRegulatory = async (regulatoryID, data, dispatch) => {
        try {
                const regulatory = await axios.patch(`/${module_link}/update/${regulatoryID}`, {
                        row_status: data.row_status
                }
                );
                dispatch({
                        type: UPDATE_REGULATORY_SUCCESS,
                        payload: regulatory.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const deleteRegulatory = async (regulatoryID, dispatch) => {
        try {
                await axios.delete(`/${module_link}/delete/${regulatoryID}`);
                dispatch ({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
        } catch (err) {
                console.log(err);
        }
};
