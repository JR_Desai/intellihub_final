import moment from "moment";
import * as yup from "yup";

export const productUtils = yup.object().shape({
  name: yup.string().required("Please enter name"),
  description: yup.string().required("Please enter description"),
  product_variant: yup.string().required("Please enter product variant"),
  website: yup.string().required("Please enter product website"),
  date_published: yup.string().required("Please enter date published"),
  storage: yup.string().required("Please enter product storage"),
  nutrition: yup.string().required("Please enter product nutrition"),
  production_code: yup.string().required("Please enter production code"),
  primary_image: yup.string().required("Please enter primary image"),
  control_method: yup.string().required("Please enter control method"),
  ownership_pattern: yup.string().required("Please enter ownership pattern"),
  hyperlink: yup.string().required("Please enter hyperlink"),
  brand: yup.string().required("Please select brand"),
  approval_date: yup.string().required("Please select approval date"),
  flavours: yup.string().required("Please select flavours"),
  fragrances: yup.string().required("Please select fragrances"),
  launch_type: yup.string().required("Please select launch type"),
  package_material: yup.string().required("Please select package material"),
  package_type: yup.string().required("Please select package type"),
  private_label: yup.string().required("Please select private label"),
  technology_type: yup.string().required("Please select technology type"),
  category: yup.string().required("Please select category"),
  // parent_category: yup.number().required("Please select parent category"),
});

export const getCategoryObj = (d) => {
  return {
    name: d?.name,
    code: d?.code ? d?.code : d?.name.toUpperCase(),
    parent: d?.parent,
    module: d?.module,
    status: true,
  };
};

export const getCommonObj = (d) => {
  console.log(d);
  return {
    name: d?.name,
    code: d?.code ? d?.code : d?.name.toUpperCase(),
    description: d?.description ? d?.description : d?.name.toUpperCase(),
    status: d?.status || true,
    id: d?.id,
  };
};

export const getBrandObj = (d) => {
  return {
    name: d?.name,
    description: d?.description ? d?.description : d?.name.toUpperCase(),
    status: true,
  };
};

export const packagingUnitsObj = (d) => {
  return {
    name: d?.name,
    code: d?.code ? d?.code : d?.name.toUpperCase(),
    status: true,
  };
};

export const getTechnologyObj = (d) => {
  return {
    name: d?.name,
    code: d?.code ? d?.code : d?.name.toUpperCase(),
    parent_id: d?.parent || null,
    module: d?.module || null,
    status: false,
  };
};

export const initialValues = {
  name: "",
  description: null,
  product_variant: "",
  website: "",
  date_published: null,
  category: null,
  price_us_dollars: null,
  price_euros: null,
  // positioning_claims: [],
  storage: "",
  unit_pack_size: null,
  nutrition: "",
  number_of_variants: null,
  currency: null,
  price_local_currency: null,
  bar_code: null,
  production_code: "",
  primary_image: "",
  primary_image_upload: null,
  hyperlink: "",
  ingredients: [],
  control_method: "",
  news: [],
  awards: [],
  ownership_pattern: "",
  revenue: null,
  product_funding: null,
  sustainablity: null,
  approval_date: moment().format("YYYY-MM-DD"),
  status: true,
};

export const getFormatedOptions = (
  data,
  labelKey = "name",
  valueKey = "id"
) => {
  if (!Array.isArray(data)) return [];
  return data.map((d) => ({
    label: d?.[labelKey],
    value: d?.[valueKey],
    details: d,
  }));
};

export const formateRequestPayload = (values) => {
  console.log({ values }, moment(values.date_published).format("YYYY-MM-DD"));
  const formData = new FormData();
  const obj = {
    name: values?.name,
    product_variant: values?.product_variant,
    brand: values?.brandObj,
    website: values.website,
    market: Array.isArray(values?.market) ? values?.market[0] : values?.market,
    date_published: moment(values.date_published).format("YYYY-MM-DD"),
    description: values?.description,
    category: values?.categoryObj,
    price_us_dollars: values.price_us_dollars,
    price_euros: values.price_euros,
    positioning_claims: values.positioningClaimsObj,
    storage: values.storage,
    unit_pack_size: values.unit_pack_size,
    packaging_units: values.packagingUnitsObj,
    package_type: values.packageTypeObj,
    package_material: values.packageMaterialObj,
    nutrition: values.nutrition,
    number_of_variants: values.number_of_variants,
    launch_type: values.launchTypeObj,
    private_label: values.privateLabelObj,
    currency: values.currency,
    price_local_currency: values.price_local_currency,
    bar_code: values.bar_code,
    production_code: values.production_code,
    flavours: values.flavoursObj,
    fragrances: values.fragrancesObj,
    primary_image: values.primary_image,
    primary_image_upload: null,
    hyperlink: values.hyperlink,
    ingredients: values.ingredients,
    control_method: values.control_method,
    news: values.news,
    awards: values.awards,
    ownership_pattern: values.ownership_pattern,
    revenue: values.revenue,
    product_funding: values.product_funding,
    sustainablity: values.sustainablity,
    technology_type: values.technologyTypeObj,
    // row_status: values.row_status,
    approval_date: moment(values.approval_date).format("YYYY-MM-DD"),
    status: true,
  };

  return obj;
  Object.keys(obj).map((k) => {
    if (k !== "contains" && k !== "image_upload") {
      if (typeof obj[k] === "object") {
        formData.append(k, JSON.stringify(obj[k]));
      } else {
        formData.append(k, obj[k]);
      }
    }
    if (k === "contains" && Array.isArray(obj?.[k])) {
      obj?.[k].forEach((d) => {
        formData.append(`${k}[]`, JSON.stringify(d));
      });
    }

    if (k === "image_upload" && obj?.[k]) {
      formData.append(k, obj[k]);
    }
  });

  return formData;
};
