import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Avatar, Badge, Col, Divider, List, Row, Typography } from "antd";

import { showConfirmRemove } from "../../appRedux/actions/Companies";
import {
  deleteRawMaterial,
  getRawMaterial,
} from "../../appRedux/actions/RawMaterials";
import Widget from "../../components/Widget";
import ConfirmPopup from "../../components/ConfirmPopup";

const DefaultImage = "/assets/images/layouts/default_img.jpg";

const RawMaterialDetails = (props) => {
  const rawMaterailId = props.match.params.id;

  const dispatch = useDispatch();
  const history = useHistory();
  const { rawMaterial, showRemove } = useSelector(
    ({ rawMaterials }) => rawMaterials
  );

  useEffect(() => {
    getRawMaterial(rawMaterailId, dispatch);
  }, [props.match.params.id]);

  console.log({ rawMaterial });
  const edit = () => {
    history.push(`update/${rawMaterailId}`);
  };

  const onRemove = () => {
    deleteRawMaterial(rawMaterailId, dispatch, () => {
      history.replace("/raw_materials");
    });
  };

  const remove = () => {
    showConfirmRemove(true, dispatch);
  };
  console.log({ rawMaterial });
  return (
    <div className="gx-main-content-wrapper">
      <div className="gx-module-detail gx-module-list">
        <div className="gx-module-detail-item gx-module-detail-header">
          <Row>
            <Col xs={24} sm={12} md={17} lg={12} xl={17}>
              <div className="gx-flex-row">
                <div className="gx-user-name gx-mr-md-4 gx-mr-2 gx-my-1">
                  <div className="gx-flex-row gx-align-items-center gx-pointer">
                    <Avatar
                      className="gx-mr-3"
                      src={rawMaterial.image_link || DefaultImage}
                      alt=""
                    />
                    <h4 className="gx-mb-0">
                      {rawMaterial.name} ({rawMaterial?.row_status?.status_code}
                      )
                    </h4>
                  </div>
                </div>
              </div>
            </Col>

            <Col xs={24} sm={12} md={7} lg={12} xl={7}>
              <div className="gx-flex-row gx-justify-content-end">
                <i
                  className="gx-icon-btn icon icon-edit"
                  onClick={() => edit()}
                />
                <i
                  className="gx-icon-btn icon icon-trash"
                  onClick={() => remove()}
                />
              </div>
            </Col>
          </Row>
        </div>
        <Widget className="gx-module-detail-item">
          <div className="gx-form-group gx-flex-row gx-align-items-center gx-flex-nowrap">
            <Col className="gx-flex-row gx-flex-1 gx-flex-nowrap">
              <div className="gx-w-25">
                <img
                  className=""
                  src={rawMaterial.image_link || DefaultImage}
                  alt="..."
                />
              </div>
              <div className="gx-w-75 gx-ml-5">
                <div className="h1">{rawMaterial.name}</div>
                {rawMaterial.image_upload && rawMaterial.image_upload !== "" && (
                  <div className="gx-mt-3 gx-mb-2">
                    <span className="h3">Uploaded Image: </span>
                    <div className="gx-w-25">
                      <img
                        style={{ height: 100, width: 100, marginTop: 12 }}
                        className=""
                        src={rawMaterial.image_upload}
                        alt="..."
                      />
                    </div>
                  </div>
                )}

                <div className="gx-mt-3 gx-mb-2">
                  <a target="_blank" href={rawMaterial.web_link || "#"}>
                    {rawMaterial.web_link}
                  </a>
                </div>

                <div className="gx-mt-3 gx-mb-2">
                  <span className="h3">Description: </span>
                  <p className="gx-text-grey">
                    <div
                      dangerouslySetInnerHTML={{
                        __html: rawMaterial.description || "-",
                      }}
                    ></div>
                  </p>
                </div>

                <Divider orientation="left">General Details</Divider>
                <List
                  size="small"
                  bordered
                  dataSource={[
                    {
                      title: "Variant",
                      value: rawMaterial.variant,
                    },
                    {
                      title: "Category",
                      value: rawMaterial.category?.name,
                    },
                    {
                      title: "Manufacturer",
                      value: rawMaterial.manufacturer?.name,
                    },
                    { title: "Price", value: rawMaterial.price },
                    {
                      title: "Country",
                      value: `${rawMaterial.country?.name || "-"} (
                        ${rawMaterial.country?.code || "-"})`,
                    },
                  ]}
                  renderItem={(item) => (
                    <List.Item>
                      <Typography.Text strong>{item.title}</Typography.Text>:{" "}
                      {item.value}
                    </List.Item>
                  )}
                />

                <Divider orientation="left">Package Details</Divider>
                <List
                  size="small"
                  bordered
                  dataSource={[
                    { title: "Package size", value: rawMaterial.package_size },
                    {
                      title: "Package unit name",
                      value: rawMaterial.package_unit?.name,
                    },
                    {
                      title: "Package unit code",
                      value: rawMaterial.package_unit?.code,
                    },
                  ]}
                  renderItem={(item) => (
                    <List.Item>
                      <Typography.Text strong>{item.title}</Typography.Text>:{" "}
                      {item.value}
                    </List.Item>
                  )}
                />

                <Divider orientation="left">Contains</Divider>
                <List
                  size="small"
                  bordered
                  dataSource={
                    Array.isArray(rawMaterial["contains"])
                      ? rawMaterial["contains"]
                      : []
                  }
                  renderItem={(item) => <List.Item>{item.name}</List.Item>}
                />
              </div>
            </Col>
          </div>
        </Widget>
        <ConfirmPopup showStatus={showRemove} onConfirm={onRemove} />
      </div>
    </div>
  );
};

export default RawMaterialDetails;
