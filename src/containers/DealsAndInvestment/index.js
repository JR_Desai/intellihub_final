import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getAllDealsAndInvestment, updateStatusDealsAndInvestments } from '../../appRedux/actions';
import ListView from '../../components/ListView';

const DealsAndInvestment = () => {

    const { deals } = useSelector(({ deals }) => deals);

    const dispatch = useDispatch();

    useEffect(() => {
        getAllDealsAndInvestment(dispatch);
    }, []);

    const refreshList = () => {
        getAllDealsAndInvestment(dispatch);
    }

    return (
        <ListView listToBeShown={deals} link="dai" searchTitleText="deals and investments" title="Deals & Invesments" refreshList={refreshList} updateModuleItem={updateStatusDealsAndInvestments} />
    )
}

export default DealsAndInvestment;
