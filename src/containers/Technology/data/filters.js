export default [
  {
    'id': 1,
    'handle': 'all',
    'title': 'All',
    'icon': 'all-contacts'
  },
  {
    'id': 2,
    'handle': 'approved',
    'title': 'Approved',
    'icon': 'check-circle-o'
  },
  {
    'id': 3,
    'handle': 'removed',
    'title': 'Disapproved',
    'icon': 'close-circle'
  }
];
