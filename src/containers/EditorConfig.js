const custom_editor_config = {
    toolbar: {
      items: [
          'heading','paragraph','|','bold','italic','link','list','bulletedList','numberedList','alignment','|','blockQuote','insertTable','|','undo','redo','essentials'
      ]
    },
    table: {
      contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
    }
}
export default custom_editor_config;