import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";

import { Drawer,
         IconButton,
         FormControlLabel,
         Checkbox,
         Button,
         ButtonGroup,
 } from '@material-ui/core';
         
import { AiOutlinePlus , AiOutlineMinus } from "react-icons/ai";
import * as ReactBootStrap from "react-bootstrap";
import { FiSearch } from "react-icons/fi";

import { Menu,
         ChevronLeft,
         ChevronRight,
} from "@material-ui/icons";

// import { company } from '../LatestAroundtheWorld/Data';
import { Grid, Card, Icon } from 'semantic-ui-react'
import { AutoComplete, Spin } from 'antd';
import Fuse from 'fuse.js'

import './Company.css'

const drawerWidth = '240';

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth.concat,
    position: 'relative',
    marginTop: '109px',
    zIndex:'1029'
  },
  drawerHeader: {
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginLeft: 0
  }
}));

const Headquarters = () => {
  const url = "http://3.108.46.195:9000";

  const classes = useStyles();
  const [hqDrawer, setHqDrawer] = useState(false);
  const [clicked, setClicked] = useState(false);
  const [show, setShow] = useState(false);
  const handleDrawerOpen = () => setHqDrawer(true)
  const handleDrawerClose = () => setHqDrawer(false)

  const toggle = (index) => {
    if (clicked === index) return setClicked(null);
    setClicked(index);
  };
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  const [companies, setCompanies] = useState([])
  const [sort, setSort] = useState('dateDesc');

  useEffect(() => fetchCompanies(), [])

  const fetchCompanies = () => {
    fetch(`${url}/company/view/?filters={"type":"operator","data":{"attribute":"row_status","operator":"=","value":"1"}}`)
    .then((res) => res.json())
    .then((result) => setCompanies(result.results))
  }

  var sortedCompaniesData = [...companies]
  
  const [loading, setLoading] = useState(false)

  const handleDescChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateDesc'), 2000)
    
    console.log(event.target)
  }
  
  const handleAscChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateAsc'), 2000)    
    console.log(event.target)
  }

  
  if(sort === 'dateDesc') sortedCompaniesData = companies.sort((a, b) => new Date(b.year_of_incorporation) - new Date(a.year_of_incorporation))
  else if(sort === 'dateAsc') sortedCompaniesData = companies.sort((a, b) => new Date(a.year_of_incorporation) - new Date(b.year_of_incorporation))

  const renderBelowCards = sortedCompaniesData.map(company => {
    return <>
    <Grid.Column>
      <div className="company-card">
      <Card>
      <div className="row">
      <Link to={`/user/companies/${company.id}`}>
      <span className="company-image-box">
      <ReactBootStrap.Image
        src={company.image_link}
        className="company-image mr-4"
      />
      </span>
      </Link>
      <Card.Content>
        <Card.Header>
        <Link to={`/user/companies/${company.id}`}>
        
          <div className="company-header">
          {company.name}
          </div>
          </Link>
        </Card.Header>
        <Card.Meta>
        <div className="company-meta">
          {company.hq}
        </div>
        </Card.Meta>
        <Card.Description>
        <div className="company-data">
          {company.business_model}
        </div>
        </Card.Description>
      </Card.Content>
      </div>
      </Card>
      </div>
    </Grid.Column>
    </>
  })
  
   //Render tags
  const [parentTag, setParentTag] = useState([]);
  const [childTag, setChildTag] = useState([]);
  const [allTags, setAllTag] = useState([]);

  useEffect(() => { fetchParentTag() }, [])
  useEffect(() => { fetchChildTag() }, [])
  useEffect(() => { fetchAllTag() }, []);

   
  const fetchParentTag = () => {
    fetch(`${url}/tag_parent/view/`)
    .then((res) => res.json())
    .then((result) => setParentTag(result))
  }
  const fetchChildTag = () => {
    fetch(`${url}/tag_child/view/`)
    .then((res) => res.json())
    .then((result) => setChildTag(result))
  }
  
  const fetchAllTag = () => {
    fetch(`${url}/tag/view/`)
    .then((res) => res.json())
    .then((result) => setAllTag(result))
  }

  var [tagState, setTagState] = useState([undefined]);
  var [tagCheck, setTagCheck] = useState([]);

   const handleTagChange = (event) => {
    console.log(event.target.checked, event.target.name);
    setTagCheck({...tagCheck, [event.target.name]: event.target.checked})
      
    if(event.target.checked) {
      if(tagState.indexOf(parseInt(event.target.name)) === -1) {
        tagState.push(parseInt(event.target.name));
      }
    }
    if(!event.target.checked) {
      var index = tagState.indexOf(parseInt(event.target.name))
        if (index !== -1) {
          tagState.splice(index, 1);
          // setTagState([...tagState]);
        }
      // tagState = tagState.filter(tag => parseInt(tag) !== parseInt(event.target.name))
    }
    const fetchTagData = () => {  
      if(tagState.length === 0) {
        fetch(`${url}/company/view/`)
        .then((res) => res.json())
        .then((result) => setCompanies(result.results))
      }
      else {
        fetch(`${url}/company/view/?filters={"type":"operator","data":{"attribute":"tag","operator":"in","value":[${tagState}]}}`)
        .then((res) => res.json())
        .then((result) => setCompanies(result.results))
      }
    }
    fetchTagData();
   }
   
   
var options = [];
const [selectOption, setSelectOption] = useState([]);
const [selectedSearchBar, setSearchBar] = useState(false);
const onSearch = (searchText) => {
  console.log(searchText);
  const fuse = new Fuse(options, {keys : ['value', 'id']});

  var results = fuse.search(searchText);
  results.length > 0 ? setSearchBar(true) : setSearchBar(false)
  console.log('searchresult', results);

  const characterResults = results.map(searchedOption => searchedOption.item)

  console.log('selectoptionvalue', characterResults)
  setSelectOption(characterResults);
};

const [renderSelectedTag, setRenderSelectedTag] = useState([]);

const onSelect = (data) => {
  console.log('onSelect', data);
  var idOfTag = options.filter( option => option.value === data ).map( option => option.id)

  var [ tagId ] = idOfTag;
  var filterSelectedChildTag = allTags.filter( tag => tag.id === tagId )
  var [ parentID ]  = filterSelectedChildTag.map( tag => tag.parent_id );

  var filterSelectedParentTag = allTags.filter( tag => tag.id === parentID )

  console.log(filterSelectedParentTag, 'parent', parentID)

  var renderSelectedTag1 = filterSelectedParentTag.map(parentTag => {
    return <>
    <div className="wrap" onClick={() => toggle(parentTag.id)}>
    <h6>{parentTag.name}</h6>
    <span>
    </span>
    </div>
    {
    filterSelectedChildTag.map((childTag) => {
      return <>
      <div className="checking">
        <div className="acordian_drop">
        <FormControlLabel
          control={<Checkbox checked={tagState[childTag.name]} onChange={handleTagChange} name={childTag.id} />}
          label={childTag.name}
        />
        </div>
      </div>
      </>
    })}
    </>
  })

  setRenderSelectedTag(renderSelectedTag1);
};
   var filterTags = parentTag.filter(tag => tag.module === 3)
     
     const renderTags = filterTags.map(tag => {
       var currentParentTag = tag.id;
       var currentModule = tag.module;
       var filterChildTag = childTag.filter(tag => tag.module === currentModule && tag.parent_id === currentParentTag)
 
       return <>      
       <div className="wrap" onClick={() => toggle(tag.id)}>
       <h6>{tag.name}</h6>
       <span>
           {clicked === tag.id ? (<AiOutlineMinus className="acordian_icon" />) : 
           (<AiOutlinePlus className="acordian_icon" />)}
       </span>
       </div>
       {clicked === tag.id ? (
       filterChildTag.map((tag) => (
         <div className="checking">
           <div className="acordian_drop">
           <FormControlLabel
             control={<Checkbox checked={tagCheck.childId}
             onChange={handleTagChange} name={tag.name} />}
             label={tag.name}
           />
           </div>
         </div>
         ))
       ) : null}
       </>
     }) 

     const [listView, setListView] = useState(false);
     const handleChangeList = () => {setListView(true)}
     const handleChangeGrid = () => {setListView(false)}
     console.log(listView);
  //Main Page
    return (
      // Side navbar
  
      <div className="page newMainBlock">
      <Drawer
        variant="persistent"
        anchor="left"
        open={hqDrawer}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className="acordian">
          <div>
            <IconButton onClick={handleDrawerClose}>
              <ChevronLeft />
              <Menu />
            </IconButton>
          </div>
          <div className="acordian_header">
            <div className="acordian_search">
              <FiSearch className="acordian_search_icon" />
              <br />
              <AutoComplete
              options={selectOption}
              style={{
                width: 200,
              }}
              onSelect={onSelect}
              onSearch={onSearch}
              placeholder="Search here"
              >
              <input
                type="text"
                aria-label="Text input with dropdown button"
                // placeholder="search here"
                className="acordian_ip"
              />
              </AutoComplete>
              </div>
              </div>

              {/***************acordian***************/}
              <div className="acordian_content">
              {selectedSearchBar ? renderSelectedTag : renderTags}
              </div>
        </div>
      </Drawer>
      {/* Mid Magazines */}
      <div className="container">
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: hqDrawer
        })}
      >
      <div className={classes.drawerHeader} />

      <div className="news">
        <div className="news_header">
          <div className="news_left_header">
            {hqDrawer || (<IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
            >
              <Menu />
              {/* <ChevronRight /> */}
            </IconButton>)}
            <h4 className="main-header">Companies</h4>
          </div>
          <div className="news_right_header">
          <ButtonGroup size="large" variant="outlined" color="secondary" aria-label="large outlined button group" className="mr-5">
            <Button startIcon={<Icon name='list' size='large' />} onClick={handleChangeList}></Button>
            <Button startIcon={<Icon name='grid layout' size='large' />} onClick={handleChangeGrid}></Button>
          </ButtonGroup>
          <ButtonGroup size="large" variant="outlined" color="secondary" aria-label="large outlined button group" className="" disabled={loading}>
              <Button onClick={handleDescChange} startIcon={<Icon name='sort content descending' size='large' />} ></Button>
              <Button onClick={handleAscChange} startIcon={<Icon name='sort content ascending' size='large' />} ></Button>
          </ButtonGroup>
          </div>
        </div>
        <div className="news_line"></div>
        {/* Magazines list */}
        <Spin spinning={loading}>
        {listView ?
          <Grid columns={1}>
            {renderBelowCards}
          </Grid> :
          <Grid columns={ hqDrawer ? 2 : 3}>
            {renderBelowCards}
          </Grid>
        }
        </Spin>
      </div>
      </main>
      </div>
    </div>
    )
}

export default Headquarters;