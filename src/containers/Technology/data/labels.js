export default [
  {
    'id': 1,
    'handle': 'frontend',
    'title': 'HTML',
  },
  {
    'id': 2,
    'handle': 'backend',
    'title': 'CSS',
  },
  {
    'id': 3,
    'handle': 'api',
    'title': 'Laravel',
  },
  {
    'id': 4,
    'handle': 'issue',
    'title': 'Node JS',
  }
];
