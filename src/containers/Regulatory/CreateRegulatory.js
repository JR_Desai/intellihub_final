import React, { useState } from 'react'
import {
    Form,
    Button,
    Card,
    Select,
    DatePicker,
} from 'antd';

import { useDispatch, useSelector } from 'react-redux';
import InputGroup from '../../components/InputGroup';
import { useHistory } from 'react-router';
import TextArea from 'antd/lib/input/TextArea';
import { createMarketReports, createRegulatory, updateMarketReports, updateRegulatory } from '../../appRedux/actions';

const CreateRegulatory = props => {


    const history = useHistory();

    const {regulatory} = useSelector(({regulatories}) => regulatories);
    const {countries} = useSelector(({config}) => config);

    const regulatoryId = props.match.params.id;

    const countryOptions = [];
    
    countries.map((country, index) => {
        countryOptions.push({value: country.id, label: country.name});
    });

    const initState = {
        report_name: "Name",
        region: "",
        date: "",
        cover_image: "",
        short_description: "description",
        long_description: "description",
        author: "",
        download_link: ""
    }

    const values = regulatoryId ? regulatory : initState

    const [regulatorySafety, setRegulatorySafety] = useState(values);

    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        regulatoryId ? updateRegulatory(regulatorySafety.id, regulatorySafety, dispatch) : createRegulatory(regulatorySafety, dispatch);
        history.replace("/mr");
    }

    const header = regulatoryId ? "Update" : "Create"

    return (
    <div className="gx-main-content-wrapper">
    <Card className="gx-card" title={`${header} Technology`}>
        <Form
            name="register"
            scrollToFirstError
            >
                <InputGroup name="name" label="Report Name: " type="text" value={regulatorySafety.report_name} 
                    onChange={(e) => setRegulatorySafety({...regulatorySafety, report_name: e.target.value})} 
                />
                <div className="gx-form-group">
                    <label htmlFor="date" className="gx-form-label">Date:</label>
                    <DatePicker name="date" placeholder={regulatorySafety.date}
                        onChange={(e) => setRegulatorySafety({...regulatorySafety, date: e.format("YYYY-MM-DD")})} className="gx-mb-3 gx-w-100"
                    />
                </div>
                <div className="gx-form-group">
                    <label name="short_description" className="gx-form-label">Short Description: </label>
                    <TextArea 
                        name="short_description" 
                        value={regulatorySafety.short_description} 
                        rows={4}
                        onChange={(e) => setRegulatorySafety({...regulatorySafety, short_description: e.target.value})}  
                    />
                </div>
                <div className="gx-form-group">
                    <label name="long_description" className="gx-form-label">Long Description: </label>
                    <TextArea 
                        name="long_description" 
                        value={regulatorySafety.long_description} 
                        rows={4}
                        onChange={(e) => setRegulatorySafety({...regulatorySafety, long_description: e.target.value})}  
                    />
                </div>
                <InputGroup name="author" label="Author: " type="text" value={regulatorySafety.author} 
                    onChange={(e) => setRegulatorySafety({...regulatorySafety, author: e.target.value})} 
                />
                <InputGroup name="cover_image" label="Cover Image: " type="text" value={regulatorySafety.cover_image} 
                    onChange={(e) => setRegulatorySafety({...regulatorySafety, cover_image: e.target.value})} 
                />
                <InputGroup name="view_link" label="View Link: " type="text" value={regulatorySafety.view_link} 
                    onChange={(e) => setRegulatorySafety({...regulatorySafety, view_link: e.target.value})} 
                />
                <InputGroup name="download_link" label="Download Link: " type="text" value={regulatorySafety.download_link} 
                    onChange={(e) => setRegulatorySafety({...regulatorySafety, download_link: e.target.value})} 
                />
            <Button onClick={handleSubmit} className="gx-w-100" type="primary" htmlType="submit">
            Submit
            </Button>
        </Form>
    </Card>
    </div>
    )
}

export default CreateRegulatory;
