import {
  GET_ALL_COUNTRIES,
  GET_ALL_STATES,
  GET_ALL_DESIGNATIONS,
  GET_ALL_EVENT_TYPE,
  GET_ALL_MANUFACTURERS,
  GET_ALL_UNITS,
  GET_ALL_CONTAINS,
  GET_ALL_MODULE_STATUS,
  GET_ALL_PACKAGE_TYPE,
  GET_ALL_POSITIONING_CLAIMS,
  GET_ALL_BRANDS,
  GET_ALL_PACKAGE_MATERIAL,
  GET_ALL_LAUNCH_TYPE,
  GET_ALL_PRIVATE_LABEL,
  GET_ALL_CURRENCY,
  GET_ALL_AWARDS,
  GET_ALL_INGREDIENT,
  GET_ALL_FRAGRANCES,
  GET_ALL_FLAVOUR,
  GET_ALL_CATEGORY_MODULE,
  GET_ALL_CATEGORY_PARENT,
  GET_ALL_TAG_VIEW,
} from "../../constants/ActionTypes";

const INIT_STATE = {
  loader: false,
  countries: [],
  states: [],
  designations: [],
  event_types: [],
  manufacturer: [],
  units: [],
  contains: [],
  module_status: [],
  brands: [],
  positioning_claims: [],
  package_type: [],
  package_material: [],
  launch_type: [],
  private_label: [],
  currency: [],
  awards: [],
  ingredients: [],
  fragrances: [],
  flavours: [],
  categoryModule: [],
  categoryParent: [],
};

export default function configReducer(state = INIT_STATE, action) {
  switch (action.type) {
    case GET_ALL_COUNTRIES: {
      return {
        ...state,
        loader: false,
        countries: action.payload,
      };
    }
    case GET_ALL_STATES: {
      return {
        ...state,
        loader: false,
        states: action.payload,
      };
    }
    case GET_ALL_DESIGNATIONS: {
      return {
        ...state,
        loader: false,
        designations: action.payload,
      };
    }
    case GET_ALL_EVENT_TYPE: {
      return {
        ...state,
        loader: false,
        event_types: action.payload,
      };
    }
    case GET_ALL_BRANDS: {
      return {
        ...state,
        loader: false,
        brands: action.payload,
      };
    }
    case GET_ALL_POSITIONING_CLAIMS: {
      return {
        ...state,
        loader: false,
        positioning_claims: action.payload,
      };
    }
    case GET_ALL_PACKAGE_TYPE: {
      return {
        ...state,
        loader: false,
        package_type: action.payload,
      };
    }
    case GET_ALL_PACKAGE_MATERIAL: {
      return {
        ...state,
        loader: false,
        package_material: action.payload,
      };
    }
    case GET_ALL_LAUNCH_TYPE: {
      return {
        ...state,
        loader: false,
        launch_type: action.payload,
      };
    }
    case GET_ALL_PRIVATE_LABEL: {
      return {
        ...state,
        loader: false,
        private_label: action.payload,
      };
    }
    case GET_ALL_CURRENCY: {
      return {
        ...state,
        loader: false,
        currency: action.payload,
      };
    }
    case GET_ALL_AWARDS: {
      return {
        ...state,
        loader: false,
        awards: action.payload,
      };
    }
    case GET_ALL_INGREDIENT: {
      return {
        ...state,
        loader: false,
        ingredients: action.payload,
      };
    }
    case GET_ALL_FRAGRANCES: {
      return {
        ...state,
        loader: false,
        fragrances: action.payload,
      };
    }
    case GET_ALL_FLAVOUR: {
      return {
        ...state,
        loader: false,
        flavours: action.payload,
      };
    }
    case GET_ALL_CATEGORY_MODULE: {
      return {
        ...state,
        loader: false,
        categoryModule: action.payload,
      };
    }
    case GET_ALL_CATEGORY_PARENT: {
      return {
        ...state,
        loader: false,
        categoryParent: action.payload,
      };
    }

    case GET_ALL_MANUFACTURERS: {
      return {
        ...state,
        loader: false,
        manufacturer: action.payload,
      };
    }
    case GET_ALL_UNITS: {
      return {
        ...state,
        loader: false,
        units: action.payload,
      };
    }
    case GET_ALL_CONTAINS: {
      return {
        ...state,
        loader: false,
        contains: action.payload,
      };
    }
    case GET_ALL_MODULE_STATUS: {
      return {
        ...state,
        loader: false,
        module_status: action.payload,
      };
    }
    case GET_ALL_TAG_VIEW: {
      return {
        ...state,
        loader: false,
        tag_view: action.payload,
      };
    }
    default:
      return state;
  }
}
