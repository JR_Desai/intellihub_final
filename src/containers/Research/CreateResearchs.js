import React, { useEffect, useState } from 'react'
import { Form, Button, Card, Select, Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import InputGroup from '../../components/InputGroup';
import { useHistory } from 'react-router';
import { createResearch, updateResearch, getResearchApiKey } from '../../appRedux/actions/Research';

const CreateResearchs = props => {

    const Option = Select.Option;

    const history = useHistory();

    const { research, apikeys } = useSelector(({ researches }) => researches);
    const researchId = props.match.params.id;

    const initState = {
        api_key: null,
        q: "",
        search_type: null,
        time_period: null,
        status: false
    }

    const apikeyOption = apikeys?.map(
        (apikey, _) => <Option key={apikey.id}>{apikey.api_key}</Option>
    );

    const values = researchId ? research : initState

    const header = researchId ? "Update" : "Create";

    const [researchs, setResearchs] = useState(values);
    const dispatch = useDispatch();

    useEffect(() => {
        getResearchApiKey(dispatch);
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault()
        researchId ? updateResearch(researchs.id, researchs, dispatch) : createResearch(researchs, dispatch);
        history.replace("/ral");
    }
    return (
        <div className="gx-main-content-wrapper">
            <Card className="gx-card" title={`${header} Research & Literature`}>
                <Form
                    name="register"
                    scrollToFirstError
                >

                    <div className="gx-form-group">
                        <label className="gx-form-label">Api key: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={researchId && researchs.api_key}
                            placeholder="Select"
                            onChange={(value) => setResearchs({ ...researchs, api_key: value })}
                        >
                            {apikeyOption}
                        </Select>
                    </div>


                    <InputGroup name="q" label="Q: " type="text" value={researchs.q}
                        onChange={(e) => setResearchs({ ...researchs, q: e.target.value })}
                    />

                    <div className="gx-form-group">
                        <label className="gx-form-label">Search type: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={researchId && researchs.search_type}
                            placeholder="Select"
                            onChange={(value) => setResearchs({ ...researchs, search_type: value })}
                        >
                            <Option key="1">news</Option>
                            <Option key="2">images</Option>
                            <Option key="3">videos</Option>
                            <Option key="4">autocomplate</Option>
                            <Option key="5">places</Option>
                            <Option key="6">place_reviews</Option>
                            <Option key="7">place_products</Option>
                            <Option key="8">place_posts</Option>
                            <Option key="9">shopping</Option>
                            <Option key="10">scholor</Option>
                        </Select>
                    </div>
                    <div className="gx-form-group">
                        <label className="gx-form-label">Time period: </label>
                        <Select
                            className="gx-w-100"
                            mode="single"
                            defaultValue={researchId && researchs.time_period}
                            placeholder="Select"
                            onChange={(value) => setResearchs({ ...researchs, time_period: value })}
                        >
                            <Option key="1">last_hours</Option>
                            <Option key="2">last_day</Option>
                            <Option key="3">last_week</Option>
                            <Option key="4">last_month</Option>
                            <Option key="5">last_year</Option>
                        </Select>
                    </div>


                    <Button onClick={handleSubmit} className="gx-w-100" type="primary" htmlType="submit">
                        Submit Details
                </Button>
                </Form>
            </Card>
        </div>
    )
}
export default CreateResearchs;
