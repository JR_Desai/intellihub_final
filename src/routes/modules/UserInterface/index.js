import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import ViewAllNews from '../../../containers/News/User/ViewAllNews'; 
import ViewNews from '../../../containers/News/User/ViewDetail'; 
import ViewAllMagazines from '../../../containers/Magazine/User/ViewAllMagazines';
import ViewMagazineDetail from '../../../containers/Magazine/User/ViewMagazineDetail';
import Event from '../../../containers/Event/User/Eventsconferences';
import Companies from '../../../containers/Companies/User/ViewCompanies';
import CompanyDetail from '../../../containers/Companies/User/ViewDetails';
import ViewTechnology from '../../../containers/Technology/User/ViewTechnology';
import ViewTechnologyDetail from '../../../containers/Technology/User/ViewDetails';
import Deals from '../../../containers/DealsAndInvestment/User/ViewDeals';
import DealsDetail from '../../../containers/DealsAndInvestment/User/DetailDeals';
import Investors from '../../../containers/DealsAndInvestment/User/ViewInvestors';
import InvestorDetail from '../../../containers/DealsAndInvestment/User/InvestorDetails';
import Demo from '../../../containers/Demo/Demo'
import RawMaterialList from 'containers/RawMaterials/User/RawMaterialList';
import RawMaterialDetails from 'containers/RawMaterials/User/RawMaterialDetails'

import ProductList from 'containers/Products/User/ProductList';
import ProductDetails from 'containers/Products/User/ProductDetails';
import ProductsCompare from 'containers/Products/User/ProductsCompare';
import ProductAnaytics from 'containers/Products/User/ProductAnaytics';
import ScheduleDemo from '../../../containers/ScheduleDemo/User/ScheduleDemo';

const UserInterface = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={ViewAllNews} />
            <Route exact={true} path={`${match.url}/news`} component={ViewAllNews} />
            <Route exact={true} path={`${match.url}/news/:newsId`} component={ViewNews} />
            
            <Route exact={true} path={`${match.url}/magazines`} component={ViewAllMagazines} />
            <Route exact={true} path={`${match.url}/magazines/:magId`} component={ViewMagazineDetail} />
            
            <Route exact={true} path={`${match.url}/eventsandconferences`} component={Event} />
            
            <Route exact={true} path={`${match.url}/companies`} component={Companies} />
            <Route exact={true} path={`${match.url}/companies/:companyId`} component={CompanyDetail} />
            
            <Route exact={true} path={`${match.url}/technology`} component={ViewTechnology} />
            <Route exact={true} path={`${match.url}/technology/:techId`} component={ViewTechnologyDetail} />
            
            <Route exact={true} path={`${match.url}/dealsandinvestments`} component={Deals} />
            <Route exact={true} path={`${match.url}/dealsandinvestments/:dealsId`} component={DealsDetail} /> 

            <Route exact={true} path={`${match.url}/investor`} component={Investors} />
            <Route exact={true} path={`${match.url}/investor/:investorId`} component={InvestorDetail} />

            <Route exact={true} path={`${match.url}/rawmaterials`} component={RawMaterialList} />
            <Route exact={true} path={`${match.url}/rawmaterials/:rawMaterialId`} component={RawMaterialDetails} />

            <Route exact={true} path={`${match.url}/products`} component={ProductList} />
            <Route exact={true} path={`${match.url}/products/:productId`} component={ProductDetails} />
            <Route exact={true} path={`${match.url}/products-compare`} component={ProductsCompare} />
            <Route exact={true} path={`${match.url}/product-analytics/:productId`} component={ProductAnaytics} />

            <Route exact={true} path={`${match.url}/demo`} component={Demo} />

            <Route exact={true} path={`${match.url}/scheduledemo`} component={ScheduleDemo} />
        </Switch>
    )
}

export default UserInterface;
