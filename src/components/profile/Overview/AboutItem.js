import React from "react";
import Auxiliary from "utils/Auxiliary";

const AboutItem = ({icon, isValue = false, title, desc, handleSubmit, handleChange}) => {


  return (
    <Auxiliary>
      <div className="gx-media gx-flex-nowrap gx-mt-3 gx-mt-lg-4 gx-mb-2"> 
        <div className="gx-mr-3">
          <i className={`icon icon-${icon} gx-fs-xlxl gx-text-primary`}/>
        </div>
        <div className="gx-media-body">
          <h6 className="gx-mb-1 gx-text-grey">{title}</h6>
            <p className="gx-mb-0">{isValue && "$"} {desc}</p>
        </div>
      </div>
    </Auxiliary>
  );
};

export default AboutItem;
