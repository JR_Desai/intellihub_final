import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import ManageAdmin from '../../../containers/ManageAdmin';

const ManageAdminRoutes = () => {

    const match = useRouteMatch();

    return (
        <div className="gx-main-content-wrapper">
            <Switch>
                <Route exact={false} path={`${match.url}`} component={ManageAdmin} />
            </Switch>
        </div>
    )
}

export default ManageAdminRoutes
