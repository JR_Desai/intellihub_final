import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import ConfigurationRoute from './configuration';
import ManageAdminRoutes from './manage_admin';

const Routes = () => {

    const match = useRouteMatch()

    return (
            <Switch>
                <Route exact={false} path={`${match.url}/configuration`} component={ConfigurationRoute} />
                <Route exact={false} path={`${match.url}/manage_admins`} component={ManageAdminRoutes} />
            </Switch>
    );
}

export default Routes;
