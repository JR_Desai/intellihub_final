import React from "react";
import {Menu} from "antd";
import {Link} from "react-router-dom";
import CustomScrollbars from "utils/CustomScrollbars";
import {useSelector} from "react-redux";
import SidebarLogo from "./SidebarLogo";

const MenuItemGroup = Menu.ItemGroup;

const SidebarContent = ({sidebarCollapsed, setSidebarCollapsed}) => {

  let {pathname} = useSelector(({common}) => common);

  const selectedKeys = pathname.substr(1);
  const defaultOpenKeys = selectedKeys.split('/')[1];

  return (
    <>
      <SidebarLogo sidebarCollapsed={sidebarCollapsed} setSidebarCollapsed={setSidebarCollapsed} />
      <div className="gx-sidebar-content">
          <Menu
            defaultOpenKeys={[defaultOpenKeys]}
            selectedKeys={[selectedKeys]}
            mode="inline">

            <MenuItemGroup key="main" className="gx-menu-group" title="Modules">
              
              {moduleLink("news", "/news", "News", "map-km-layer")}
              {moduleLink("magazine", "/magazine", "Magazines", "folder-o")}
              {moduleLink("event", "/event", "Events & Conference", "auth-screen")}
              {moduleLink("raw_materials", "/raw_materials", "Raw Materials", "shopping-cart")}
              {moduleLink("products", "/products", "Products", "icon")}
              {moduleLink("services", "/services", "Services", "hotel-booking")}
              {moduleLink("companies", "/companies", "Companies", "company")}
              {moduleLink("technology", "/technology", "Technology", "refer")}
              {moduleLink("dai", "/dai", "Deals & Investments", "team")}
              {moduleLink("investors", "/investors", "Investors", "profile")}
              {moduleLink("ral", "/ral", "Research & Literature", "map-drawing")}
              {moduleLink("ras", "/ras", "Regulatory & Safety", "tasks")}
              {moduleLink("mr", "/mr", "Market Reports", "select")}

            </MenuItemGroup>

            <MenuItemGroup key="other" className="gx-menu-group" title="Others">

              {moduleLink("others/configuration", "/others/configuration", "Configuration", "extra-components")}
              {moduleLink("others/manage_admins", "/others/manage_admins", "Manage Admins", "map-street-view")}

            </MenuItemGroup>
          </Menu>
      </div>
    </>
  );
};

export default SidebarContent;

const moduleLink = (key, link, title, icon) => {
  return <Menu.Item key={key}>
    <Link to={link}>
      <span><i className={`icon icon-${icon}`} style={{height: "2em"}} />
                    {title}</span></Link>
  </Menu.Item>;
}

