import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';
import moment from 'moment';
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { scheduleDemo } from '../../../appRedux/actions/ScheduleDemo';
import {
    Form,
    Button,
    Card,
    Select,
    DatePicker,
    TimePicker,
    Input,
    Cascader
} from 'antd';
import { getAllCountries } from '../../../appRedux/actions';
import { Player } from 'video-react';
import "video-react/dist/video-react.css"; // import css
import image from '../../../assets/video-back.jpeg';
import './styles.css';

const ScheduleDemo = () => {
	useEffect(() => {
        getAllCountries(dispatch);
    }, [])

	const history = useHistory();

	const [name, setName] = useState('');
	const [email, setEmail] =  useState('');
	const [mobile, setMobile] =  useState('');
	const [companyName, setCompany] =  useState('');
	const [country, setCountry] = useState('');
	const [jobRole, setJobRole] = useState('');
	const [requirement,setRequirement] = useState('');
	const [date, setDate] = useState('');
	const [time, setTime] = useState('');
	const { TextArea } = Input;
	const dateFormat = 'YYYY/MM/DD';

	const initState = {
        name: "",
        email: "",
        mobile: "",
        company: "",
        country: [""],
        job_role: "",
        requirements: "",
        date: "",
        time: ""
    };

	const values = initState;
	const {countries, states} = useSelector(({config}) => config);
	const countryOptions = [];
    countries.map((country, index) => {
        countryOptions.push({value: country.id, label: country.name});
    });
    const [event, setEvent] = useState(values);
    const dispatch = useDispatch();

    const formData = new FormData();

	const handleSubmit = (e) => {
		e.preventDefault();
		scheduleDemo(event, dispatch);
		history.replace("/user/scheduledemo");
	};

	const onCountryChange = (value) => {
        setEvent({...event, country: value[0]});
        console.log("dispatch",dispatch)
    };

    const onDateChange = (date,dateString) => {
    	setEvent({...event, date: dateString});
    };

    const onTimeChange = (time,timeString) => {
    	setEvent({...event, time: timeString});
    };

    return (
	    <div className="container">
	        <div className="row mt-5">
	        	<div className="col-7">
	        		<Player
	        			fluid={false}
	        			controls={false}
	        			width={620} 
	        			height={750}	
				    	playsInline
				    	poster={image}
				      	src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
				    />
	        	</div>
	        	<div className="col-5">
	        		<h2 className="header">Schedule A Demo</h2>
	        		<p className="mt-20">
	        			Need help growing your business? Some stories are better shown than told.
	        			Request a live demo to see how SAP software can help your business run
	        			better.
	        		</p>
	        		<Form>
			            <div className="row mt-30">
			            	<div className="col-6">
			            		<Input placeholder="Full Name" className="input" value={event.name} 
                    				onChange={(e) => setEvent({...event, name: e.target.value})} />
			            	</div>
			            	<div className="col-6">
			            		<Input placeholder="Email ID" type="email" className="input" value={event.email} 
                    				onChange={(e) => setEvent({...event, email: e.target.value})}/>
			            	</div>
			            </div>
			            <div className="row mt-20">
			            	<div className="col-6">
			            		<Input placeholder="Mobile Number" type="number" className="input" value={event.mobile} 
                    				onChange={(e) => setEvent({...event, mobile: e.target.value})}/>
			            	</div>
			            	<div className="col-6">
			            		<Input placeholder="Company Name" className="input" value={event.company} 
                    				onChange={(e) => setEvent({...event, company: e.target.value})}/>
			            	</div>
			            </div>
			            <div className="row mt-20">
			            	<div className="col-6">
			            		<Cascader size="large" options={countryOptions} autoComplete="true" showSearch={true} placeholder="Country" onChange={onCountryChange}  />
			            	</div>
			            	<div className="col-6">
			            		<Input placeholder="Job Role" className="input" value={event.job_role} 
                    				onChange={(e) => setEvent({...event, job_role: e.target.value})}/>
			            	</div>
			            </div>
			            <div className="row mt-20">
			            	<div className="col-12">
			            		<TextArea rows={4} className="textarea" placeholder="Your Requirements" value={event.requirements} 
                    				onChange={(e) => setEvent({...event, requirements: e.target.value})}/>
			            	</div>
			            </div>
			            <div className="row mt-20">
			            	<div className="col-6">
			            		 <DatePicker className="input1" size="large" onChange={onDateChange} />
			            	</div>
			            	<div className="col-6">
			            		<TimePicker size="large" className="input1"	 onChange={onTimeChange} defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} />,
			            	</div>
			            </div>
			            <div className="row mt-20">
			            	<div className="col-12">
			            		<Button type="primary" className="button" onClick={handleSubmit}>Submit</Button>
			            	</div>
			            </div>
			        </Form>
			        <div className="row">
			        	<div className="col-12">
			        		<p className="mt-1 msg">
			        			Our representatives will get back to you in the next 24 hours to schedule a time
			        			for a live demonstration
			        		</p>
			        	</div>
			        </div>
	        	</div>
	        </div>
	    </div>
    );
};

export default ScheduleDemo;
