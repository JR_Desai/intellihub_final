import {
  GET_ALL_MANUFACTURERS,
  GET_ALL_UNITS,
  GET_ALL_CONTAINS,
  GET_ALL_MODULE_STATUS,
  GET_ALL_BRANDS,
  GET_ALL_POSITIONING_CLAIMS,
  GET_ALL_COUNTRIES,
  GET_ALL_EVENT_TYPE,
  GET_ALL_STATES,
  GET_ALL_PACKAGE_TYPE,
  GET_ALL_PACKAGE_MATERIAL,
  GET_ALL_LAUNCH_TYPE,
  GET_ALL_PRIVATE_LABEL,
  GET_ALL_CURRENCY,
  GET_ALL_AWARDS,
  GET_ALL_INGREDIENT,
  GET_ALL_FRAGRANCES,
  GET_ALL_FLAVOUR,
  GET_ALL_CATEGORY_MODULE,
  GET_ALL_CATEGORY_PARENT,
  GET_ALL_TAG_VIEW,
} from "../../constants/ActionTypes";
import axios from "axios";

export const getAllCountries = async (dispatch) => {
  try {
    const categoryParent = await axios.get(`/country/view/`);
    dispatch({
      type: GET_ALL_COUNTRIES,
      payload: categoryParent.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllStates = async (countryId, dispatch) => {
  if (countryId !== null) {
    console.log(countryId);
    try {
      const states = await axios.get(`/state/view/?country=${countryId}`);
      dispatch({
        type: GET_ALL_STATES,
        payload: states.data,
      });
    } catch (err) {
      console.log(err);
    }
  }
};
export const getAllDesignations = async (dispatch) => {
  try {
    const designations = await axios.get(`/designation/view/`);
    dispatch({
      type: GET_ALL_STATES,
      payload: designations.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllEventTypes = async (dispatch) => {
  try {
    const eventTypes = await axios.get(`/event/view/`);
    dispatch({
      type: GET_ALL_EVENT_TYPE,
      payload: eventTypes.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllManufacturers = async (dispatch) => {
  try {
    const manufacturer = await axios.get(`/manufacturer/view/`);
    dispatch({
      type: GET_ALL_MANUFACTURERS,
      payload: manufacturer.data?.results,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllUnits = async (dispatch) => {
  try {
    const units = await axios.get(`/unit/view/`);
    dispatch({
      type: GET_ALL_UNITS,
      payload: units.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllContains = async (dispatch) => {
  try {
    const units = await axios.get(`/contain/view/`);
    dispatch({
      type: GET_ALL_CONTAINS,
      payload: units.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllModuleStatus = async (dispatch) => {
  try {
    const module_status = await axios.get(`/module_status/view/`);
    dispatch({
      type: GET_ALL_MODULE_STATUS,
      payload: module_status.data?.results,
    });
  } catch (err) {
    console.log(err);
  }
};

// newwwww

export const getAllBrands = async (dispatch) => {
  try {
    const brands = await axios.get(`/brand/view/`);
    dispatch({
      type: GET_ALL_BRANDS,
      payload: brands.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllPositioningClaims = async (dispatch) => {
  try {
    const positioningClaims = await axios.get(
      `/products/positioning_claims/view/`
    );
    dispatch({
      type: GET_ALL_POSITIONING_CLAIMS,
      payload: positioningClaims.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllPackageType = async (dispatch) => {
  try {
    const packageType = await axios.get(`/package_type/view/`);
    dispatch({
      type: GET_ALL_PACKAGE_TYPE,
      payload: packageType.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllPackageMaterial = async (dispatch) => {
  try {
    const packageMaterial = await axios.get(`/package_material/view/`);
    dispatch({
      type: GET_ALL_PACKAGE_MATERIAL,
      payload: packageMaterial.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllLaunchType = async (dispatch) => {
  try {
    const launchType = await axios.get(`/launch_type/view/`);
    dispatch({
      type: GET_ALL_LAUNCH_TYPE,
      payload: launchType.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllPrivateLabel = async (dispatch) => {
  try {
    const privateLabel = await axios.get(`/private_label/view/`);
    dispatch({
      type: GET_ALL_PRIVATE_LABEL,
      payload: privateLabel.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllCurrency = async (dispatch) => {
  try {
    const currency = await axios.get(`/currency/view/`);
    dispatch({
      type: GET_ALL_CURRENCY,
      payload: currency.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllAwards = async (dispatch) => {
  try {
    const awards = await axios.get(`/awards/view/`);
    dispatch({
      type: GET_ALL_AWARDS,
      payload: awards.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllIngredient = async (dispatch) => {
  try {
    const ingredient = await axios.get(`/ingredient/view/`);
    dispatch({
      type: GET_ALL_INGREDIENT,
      payload: ingredient.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllFragrance = async (dispatch) => {
  try {
    const fragrance = await axios.get(`/fragrance/view/`);
    dispatch({
      type: GET_ALL_FRAGRANCES,
      payload: fragrance.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllFlavour = async (dispatch) => {
  try {
    const flavours = await axios.get(`/flavour/view/`);
    dispatch({
      type: GET_ALL_FLAVOUR,
      payload: flavours.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllCategoryModule = async (dispatch) => {
  try {
    const categoryModule = await axios.get(`/category_module/view/`);
    dispatch({
      type: GET_ALL_CATEGORY_MODULE,
      payload: categoryModule.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getAllCategoryParent = async (dispatch) => {
  try {
    const categoryParent = await axios.get(`/category_module/view/`);
    dispatch({
      type: GET_ALL_CATEGORY_PARENT,
      payload: categoryParent.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const createNewCategory = async (data, dispatch, callback) => {
  try {
    const categorySaved = await axios.post(`/category/create/`, data);
    callback && callback(categorySaved?.data);
    getAllCategoryModule(dispatch);
  } catch (err) {
    console.log(err);
  }
};

export const getAllTagView = async (dispatch) => {
  try {
    const categoryParent = await axios.get(
      `/tag/view/?filters=%7B%22type%22:%22operator%22,%22data%22:%7B%22attribute%22:%22module%22,%22operator%22:%22=%22,%22value%22:%2210%22%7D%7D`
    );
    dispatch({
      type: GET_ALL_TAG_VIEW,
      payload: categoryParent.data,
    });
  } catch (err) {
    console.log(err);
  }
};
