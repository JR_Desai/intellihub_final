import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import Auth from "../../routes/Auth"
import Main from './Main';
import UserMain from './User/Main';

const Index = () => {

    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={false} path={`${match.url}auth`}>
                <Auth />
            </Route>
            <Route exact={false} path={`${match.url}user`}>
                <UserMain />
            </Route>
            <Route exact={false} path={`${match.url}`}>
                <Main />
            </Route>
        </Switch>
    )
}

export default Index;
