import React from "react";
// import Sidebar from "../Sidebar/index";
// import Navbar from "../../Topbar/User/Navbar";
import Navbar from "../../Topbar/User/NavbarNew";
import UserFooter from "containers/UserFooter/UserFooter.js";


// import Topbar from "../Topbar/index";
import App from "routes/modules/UserInterface/index";
import "semantic-ui-css/semantic.min.css";
import '../../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css'


const UserMain = () => {

    return (
    <>
        <Navbar />
        <App />
        <UserFooter />
    </>
    )
}

export default UserMain
