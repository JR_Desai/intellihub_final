import React, { useEffect, useState } from "react";
import { Card, Grid, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";
import * as ReactBootStrap from "react-bootstrap";
import Fuse from "fuse.js";
import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import { FiSearch } from "react-icons/fi";
import {
  Button,
  ButtonGroup,
  Checkbox,
  Drawer,
  FormControlLabel,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import { ChevronLeft, ChevronRight, Menu } from "@material-ui/icons";
import { AutoComplete, Spin } from "antd";
import { FiBookmark, FiShare2 } from "react-icons/fi";
import clsx from "clsx";
import { getAllRawMaterials } from "../../../appRedux/actions";
import { useDispatch, useSelector } from "react-redux";
import { RawMaterialItem } from "./component/RawMaterialItem";
const drawerWidth = "240";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth.concat,
    position: "relative",
    marginTop: "109px",
    zIndex: "1029",
  },
  drawerHeader: {
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));
const RawMaterialList = () => {
  const url = "http://3.108.46.195:9000";

  const classes = useStyles();
  const [hqDrawer, setHqDrawer] = useState(false);
  const [clicked, setClicked] = useState(false);
  const [companies, setCompanies] = useState([]);
  const [sort, setSort] = useState("dateDesc");
  const [parentTag, setParentTag] = useState([]);
  const [childTag, setChildTag] = useState([]);
  const [allTags, setAllTag] = useState([]);
  const [tagState, setTagState] = useState([undefined]);
  const [selectOption, setSelectOption] = useState([]);
  const [selectedSearchBar, setSearchBar] = useState(false);
  const [renderSelectedTag, setRenderSelectedTag] = useState([]);
  const [listIconActiveClass, setListIconActiveClass] =
    useState("active-filter");
  const [gridIconActiveClass, setGridIconActiveClass] = useState("");
  const [productListClass, setProductListClass] = useState(
    "productList-row productList-1col"
  );

  const handleDrawerOpen = () => setHqDrawer(true);
  const handleDrawerClose = () => setHqDrawer(false);

  const toggle = (index) => {
    if (clicked === index) return setClicked(null);
    setClicked(index);
  };

  const { rawMaterials } = useSelector(({ rawMaterials }) => rawMaterials);
  const { loading } = useSelector(({ common }) => common);

  const dispatch = useDispatch();
  useEffect(
    () =>
      getAllRawMaterials(dispatch, {
        type: "operator",
        data: {
          attribute: "row_status",
          operator: "=",
          value: "1",
        },
      }),
    []
  );
  useEffect(() => {
    fetchParentTag();
  }, []);
  useEffect(() => {
    fetchChildTag();
  }, []);
  useEffect(() => {
    fetchAllTag();
  }, []);

  const handleDescChange = (event) => {
    event.preventDefault();
    setTimeout(() => setSort("dateDesc"), 2000);
    console.log(event.target);
  };

  const handleAscChange = (event) => {
    event.preventDefault();
    setTimeout(() => setSort("dateAsc"), 2000);
    console.log(event.target);
  };

  //Render tags

  const fetchParentTag = () => {
    fetch(`${url}/tag_parent/view/`)
      .then((res) => res.json())
      .then((result) => setParentTag(result));
  };
  const fetchChildTag = () => {
    fetch(`${url}/tag_child/view/`)
      .then((res) => res.json())
      .then((result) => setChildTag(result));
  };

  const fetchAllTag = () => {
    fetch(`${url}/tag/view/`)
      .then((res) => res.json())
      .then((result) => setAllTag(result));
  };

  const handleTagChange = (event) => {
    console.log(event.target.checked, event.target.name);

    if (event.target.checked) {
      if (tagState.indexOf(parseInt(event.target.name)) === -1) {
        tagState.push(parseInt(event.target.name));
      }
    }
    if (!event.target.checked) {
      var index = tagState.indexOf(parseInt(event.target.name));
      if (index !== -1) {
        tagState.splice(index, 1);
        // setTagState([...tagState]);
      }
      // tagState = tagState.filter(tag => parseInt(tag) !== parseInt(event.target.name))
    }
    const fetchTagData = () => {
      if (tagState.length === 0) {
        fetch(`${url}/raw_material/view/`)
          .then((res) => res.json())
          .then((result) => setCompanies(result.results));
      } else {
        fetch(
          `${url}/raw_material/view/?filters={"type":"operator","data":{"attribute":"tag","operator":"in","value":[${tagState}]}}`
        )
          .then((res) => res.json())
          .then((result) => setCompanies(result.results));
      }
    };
    fetchTagData();
  };

  var options = [];

  const onSearch = (searchText) => {
    console.log(searchText);
    const fuse = new Fuse(options, { keys: ["value", "id"] });

    var results = fuse.search(searchText);
    results.length > 0 ? setSearchBar(true) : setSearchBar(false);
    console.log("searchresult", results);

    const characterResults = results.map(
      (searchedOption) => searchedOption.item
    );

    console.log("selectoptionvalue", characterResults);
    setSelectOption(characterResults);
  };

  const onSelect = (data) => {
    console.log("onSelect", data);
    var idOfTag = options
      .filter((option) => option.value === data)
      .map((option) => option.id);

    var [tagId] = idOfTag;
    var filterSelectedChildTag = allTags.filter((tag) => tag.id === tagId);
    var [parentID] = filterSelectedChildTag.map((tag) => tag.parent_id);

    var filterSelectedParentTag = allTags.filter((tag) => tag.id === parentID);

    console.log(filterSelectedParentTag, "parent", parentID);

    var renderSelectedTag1 = filterSelectedParentTag.map((parentTag) => {
      return (
        <>
          <div className="wrap" onClick={() => toggle(parentTag.id)}>
            <h6>{parentTag.name}</h6>
            <span></span>
          </div>
          {filterSelectedChildTag.map((childTag) => {
            return (
              <>
                <div className="checking">
                  <div className="acordian_drop">
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={tagState[childTag.name]}
                          onChange={handleTagChange}
                          name={childTag.id}
                        />
                      }
                      label={childTag.name}
                    />
                  </div>
                </div>
              </>
            );
          })}
        </>
      );
    });

    setRenderSelectedTag(renderSelectedTag1);
  };
  var filterTags = parentTag.filter((tag) => tag.module === 3);

  const renderTags = filterTags.map((tag) => {
    var currentParentTag = tag.id;
    var currentModule = tag.module;
    var filterChildTag = childTag.filter(
      (tag) =>
        tag.module === currentModule && tag.parent_id === currentParentTag
    );

    return (
      <>
        <div className="wrap" onClick={() => toggle(tag.id)}>
          <h6>{tag.name}</h6>
          <span>
            {clicked === tag.id ? (
              <AiOutlineMinus className="acordian_icon" />
            ) : (
              <AiOutlinePlus className="acordian_icon" />
            )}
          </span>
        </div>
        {clicked === tag.id
          ? filterChildTag.map((tag) => (
              <div className="checking">
                <div className="acordian_drop">
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={tagState[tag.name]}
                        onChange={handleTagChange}
                        name={tag.name}
                      />
                    }
                    label={tag.name}
                  />
                </div>
              </div>
            ))
          : null}
      </>
    );
  });

  const handleChangeList = (event) => {
    event.preventDefault();
    setProductListClass("rawMaterial-row rawMaterial-1col");
    setGridIconActiveClass("");
    setListIconActiveClass("active-filter");
  };
  const handleChangeGrid = (event) => {
    event.preventDefault();
    setProductListClass("rawMaterial-row");
    setGridIconActiveClass("active-filter");
    setListIconActiveClass("");
  };

  //Main Page
  return (
    // Side navbar

    <div className="page newMainBlock">
      <Drawer
        variant="persistent"
        anchor="left"
        open={hqDrawer}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className="acordian">
          <div>
            <IconButton onClick={handleDrawerClose}>
              <ChevronLeft />
              <Menu />
            </IconButton>
          </div>
          <div className="acordian_header">
            <div className="acordian_search">
              <FiSearch className="acordian_search_icon" />
              <br />
              <AutoComplete
                options={selectOption}
                style={{
                  width: 200,
                }}
                onSelect={onSelect}
                onSearch={onSearch}
                placeholder="Search here"
              >
                <input
                  type="text"
                  aria-label="Text input with dropdown button"
                  // placeholder="search here"
                  className="acordian_ip"
                />
              </AutoComplete>
            </div>
          </div>

          {/***************acordian***************/}
          <div className="acordian_content">
            {selectedSearchBar ? renderSelectedTag : renderTags}
          </div>
        </div>
      </Drawer>
      {/* Mid Magazines */}
      <div className="container">
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: hqDrawer,
          })}
        >
          <div className={classes.drawerHeader} />

          <div className="news">
            <div className="news_header">
              <div className="news_left_header">
                {hqDrawer || (
                  <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                  >
                    <Menu />
                    {/* <ChevronRight /> */}
                  </IconButton>
                )}
                <h4 className="main-header">Raw Materials</h4>
              </div>
              <div className="news_right_header">
                <ButtonGroup
                  size="large"
                  variant="outlined"
                  color="secondary"
                  aria-label="large outlined button group"
                  className="mr-3"
                >
                  <Button
                    className={listIconActiveClass}
                    startIcon={<Icon name="list" size="large" />}
                    onClick={handleChangeList}
                  ></Button>
                  <Button
                    className={gridIconActiveClass}
                    startIcon={<Icon name="grid layout" size="large" />}
                    onClick={handleChangeGrid}
                  ></Button>
                </ButtonGroup>
                <ButtonGroup
                  size="large"
                  variant="outlined"
                  color="secondary"
                  aria-label="large outlined button group"
                  className=""
                  disabled={loading}
                >
                  <Button
                    onClick={handleDescChange}
                    startIcon={
                      <Icon name="sort content descending" size="large" />
                    }
                  ></Button>
                  <Button
                    onClick={handleAscChange}
                    startIcon={
                      <Icon name="sort content ascending" size="large" />
                    }
                  ></Button>
                </ButtonGroup>
              </div>
            </div>
            <div className="news_line"></div>
            {/* Magazines list */}
            <Spin spinning={loading}>
              <div className="rawMaterial-block">
                <div className={productListClass}>
                  {Array.isArray(rawMaterials) &&
                    rawMaterials.map((rawMaterial) => (
                      <RawMaterialItem rawMaterial={rawMaterial} />
                    ))}
                </div>
                <div className="paginationBlock">
                  <ul>
                    <li>
                      <div className="paginatArrow PaginatPrev">
                        <a href="#">
                          <ChevronLeft />
                        </a>
                      </div>
                    </li>
                    <li className="active">
                      <a href="#">1</a>
                    </li>
                    <li>
                      <a href="#">2</a>
                    </li>
                    <li>
                      <a href="#">3</a>
                    </li>
                    <li>
                      <a href="#">4</a>
                    </li>
                    <li>
                      <a href="#">5</a>
                    </li>
                    <li>
                      <a href="#">6</a>
                    </li>
                    <li>
                      <a href="#">7</a>
                    </li>
                    <li>
                      <a href="#">8</a>
                    </li>
                    <li>
                      <a href="#">9</a>
                    </li>
                    <li>
                      <div className="paginatArrow PaginatNext">
                        <a href="#">
                          <ChevronRight />
                        </a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </Spin>
          </div>
        </main>
      </div>
    </div>
  );
};

export default RawMaterialList;
