import React, {useState, useSelector} from 'react'
import { Row, Col, Image, DatePicker, TimePicker, Spin } from 'antd';
import { Form } from 'semantic-ui-react'
import moment from 'moment'
import axios from 'axios'

const layout = {
    labelCol: {
      span: 0,
    },
    wrapperCol: {
      span: 8,
    },
  };
  /* eslint-disable no-template-curly-in-string */
  
  const validateMessages = {
    required: '${label} is required!',
    types: {
      email: '${label} is not a valid email!',
      number: '${label} is not a valid number!',
    },
    number: {
      range: '${label} must be between ${min} and ${max}',
    },
  };
  /* eslint-enable no-template-curly-in-string */


const Demo = () => {
    const [formState, setFormState] = useState([])
    const [loading, setLoading] = useState(false)

	const {countries, states} = useSelector(({config}) => config);
	const countryOptions = [];
    countries.map((country, index) => {
        countryOptions.push({value: country.id, label: country.name});
    });


    const handleChange = (e, { name, value }) => setFormState({ ...formState ,[name]:value })
    const onSubmitForm = () => {
        setFormState({...formState,'status': false})
    
        setLoading(true)
        axios.post('/schedule_demo/create/', JSON.stringify(formState),
        {
            headers: {
                "Content-Type": "application/json"
            }
        }
        )
        .then(res => {
            console.log(res)
            setTimeout(() => setLoading(false), 2000)
        })
        .catch(err => {
            console.log(err)
            setTimeout(() => setLoading(false), 2000)
        })
        console.log('JSON',JSON.stringify(formState))
        setTimeout(() => setLoading(false), 2000)

    };

    return (
        <div className="container mt-5">
            <Row>
                <Col span={12}>
                <Image
                    width={'auto'}
                    height={'auto'}
                    src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                    preview={false}
                    />
                </Col>
                <Col span={12}>
                    <h4 className="main-header mb-5">Schedule A Demo</h4>
                    <div className="box">
                    <p className="char-cut-line-2 mb-5">
                        Need help growing your bussiness? Some stories are better shown than told.<br />
                        Request a live demo to see how SAP software can help your bussiness run better.
                    </p>
                    </div>
                    <Spin spinning={loading}>
                    <Form size={'big'}>
                        <Form.Group widths='equal'>
                            <Form.Input fluid placeholder='Your Full Name' name='name'
                            onChange={handleChange}/>
                            <Form.Input fluid placeholder='Email id' name="email"
                            onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Form.Input fluid placeholder='Mobile No.' name="mobile"
                            onChange={handleChange} />
                            <Form.Input fluid placeholder='Company Name' name="company" onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group widths='equal'>
                        <Form.Select
                            fluid
                            name="country"
                            
                            placeholder='Select Country'
                            onChange={handleChange}
                        />
                            <Form.Input fluid placeholder='Job Role' name="job_role" onChange={handleChange} />
                        </Form.Group>
                        <Form.TextArea placeholder='Your Requirements' name="requirements" onChange={handleChange} />
                    </Form>

                    <div className="box mt-4 mb-4">
                    
                    <TimePicker size={'large'} style={{width: '49%'}} name='date' onChange={val => setFormState({...formState, "time":moment(val).format('h:mm:ss a')})}/>
                    
                    <DatePicker size={'large'} style={{width: '49%', float:'right'}} name='time' onChange={val => setFormState({...formState, "date":moment(val).format('YYYY-MM-DD')})}/>
                    </div>

                    <Form.Button type='submit' fluid color={'pink'} style={{width: '49%'}} size={'big'} onClick={onSubmitForm} loading={loading}>Submit</Form.Button>
                    </Spin>
                    
                    <div className="box mt-4">
                    <p className="char-cut-line-2 mb-5" style={{fontSize: '12px'}}>
                        Our representatives will get back to you in the next 24 hours to schedule a time for demonstration.
                    </p>
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default Demo