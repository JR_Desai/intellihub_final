import React from 'react'
import {Button, Form} from "antd";

const FormItem = Form.Item;

const OTPScreen = () => {

    // const [_, setOTP] = useState();

    // const handleChange = (e) => {
    //     setOTP(e.target.value);
    // }

    return (
        <>
            <div className="gx-login-header gx-text-center">
                <h1 className="gx-login-title">OTP Screen</h1>
            </div>
            <Form
            initialValues={{ remember: true }}
            name="basic"
            className="gx-signin-form gx-form-row0">
            <FormItem rules={ [{required: true, message: 'Please enter the otp!'}]} name="otp" className="gx-text-center">
            </FormItem>

            <FormItem className="gx-text-center">
                <Button type="primary" htmlType="submit">
                    Verify OTP
                </Button>
            </FormItem>
            </Form>
        </>
    )
}

export default OTPScreen
