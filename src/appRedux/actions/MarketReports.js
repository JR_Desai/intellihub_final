import axios from 'axios';
import { CREATE_MARKET_REPORTS_SUCCESS, GET_ALL_MARKET_REPORTS_SUCCESS, GET_MARKET_REPORTS_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_MARKET_REPORTS_SUCCESS } from '../../constants/ActionTypes';

const module_link = "market_reports"

export const createMarketReports = async (data , dispatch) => {
        try {
                const marketReport = await axios.post(`/${module_link}/create/`, data);
                dispatch({
                        type: CREATE_MARKET_REPORTS_SUCCESS,
                        payload: marketReport.data
                })
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllMarketReports = async (dispatch) => {
        try {
                const marketReports = await axios.get(`/${module_link}/view/`);
                dispatch({
                        type: GET_ALL_MARKET_REPORTS_SUCCESS,
                        payload: marketReports.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getMarketReports = async (marketReportID, dispatch) => {
        try {
                const marketReport = await axios.get(`/${module_link}/view/?id=${marketReportID}`);
                dispatch({
                        type: GET_MARKET_REPORTS_SUCCESS,
                        payload: marketReport.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getMarketReportTags = async (dispatch) => {
        try {
                const marketReport = await axios.get(`/tag/view/`);
                // dispatch({
                //         type: UPDATE_RAW_MATERIAL_SUCCESS,
                //         payload: service.data.results
                // })
        } catch (err) {
                console.log(err);
        }
};

export const updateMarketReports = async (marketReportID, data, dispatch) => {
        try {
                const marketReport = await axios.put(`/${module_link}/update/${marketReportID}`, data);
                dispatch({
                        type: UPDATE_MARKET_REPORTS_SUCCESS,
                        payload: marketReport.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusMarketReports = async (marketReportID, data, dispatch) => {
        try {
                const marketReport = await axios.patch(`/${module_link}/update/${marketReportID}`, {
                        row_status: data.row_status
                }
                );
                dispatch({
                        type: UPDATE_MARKET_REPORTS_SUCCESS,
                        payload: marketReport.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const deleteMarketReports = async (marketReportID, dispatch) => {
        try {
                await axios.delete(`/${module_link}/delete/${marketReportID}`);
                dispatch ({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
        } catch (err) {
                console.log(err);
        }
};
