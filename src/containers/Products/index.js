import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProducts, updateStatusProduct } from "../../appRedux/actions";
import ListView from "../../components/ListView";

const Products = () => {
  const { products } = useSelector(({ products }) => products);

  const dispatch = useDispatch();

  useEffect(() => {
    getAllProducts(dispatch);
  }, []);

  return (
    <ListView
      listToBeShown={products}
      searchTitleText="Finished products"
      link="products"
      title="Finished Products"
      updateModuleItem={updateStatusProduct}
    />
  );
};

export default Products;
