import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
// import { news as newsDataJ} from './Data'
import clsx from "clsx";
import './NewsStyle.css'

import { makeStyles,
         Typography,
         Drawer,
         IconButton,
         FormControlLabel,
         Checkbox,
         ButtonGroup,
         Button,
} from "@material-ui/core";

import {
  ChevronLeft as ChevronLeftIcon,
  Menu as MenuIcon,
  Share as ShareIcon,
  BookmarkBorder as BookmarkBorderIcon,
  MoreHoriz as MoreHorizIcon,
  Bookmark,
  MailOutline,
} from '@material-ui/icons';

import { Label ,Dropdown, Menu, Grid, Header, Divider, Icon, Card, Popup, Form, TextArea, Button as SUIButton, Placeholder } from "semantic-ui-react";

import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";

import * as ReactBootStrap from "react-bootstrap";
import { FiSearch, FiLinkedin, FiFacebook, FiTwitter } from "react-icons/fi";
import moment from "moment";
import { AutoComplete, Spin } from 'antd';
import Fuse from 'fuse.js'
import DOMPurify from 'dompurify';

const drawerWidth = '240';

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth.concat,
    position: 'relative',
    marginTop: '109px',
    zIndex:'1029'
  },
  drawerHeader: {
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginLeft: 0
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const ViewAllNews = () => {

  const url = "http://3.108.46.195:9000";

  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [sort, setSort] = useState('dateDesc');
  const [news, setNews] = useState([]);
  const [clicked, setClicked] = useState(false);

  const handleDrawerOpen = () => setOpen(true)
  const handleDrawerClose = () => setOpen(false)
    
  const toggle = (index) => {
    if (clicked === index) return setClicked(null)
    setClicked(index)
  };
  
  // var news = [...newsDataJ.results]
  useEffect(() => { fetchNews() }, [])

  const fetchNews = () => {
    fetch(`${url}/news/view/?filters={"type":"operator","data":{"attribute":"row_status","operator":"=","value":"1"}}`)
    .then((res) => res.json())
    .then((result) => {
      console.log(result.results)
      setNews(result.results)})
  }
  //Bookmarks

  const [bookmarks, setBookmark] = useState();
  const handleBookMark = (event) => { 
    console.log(event.target.checked)
    if(event.target.checked){
      console.log(event.target.value)
    }
  }

  var sortedNewsData = [...news];
  const [loading, setLoading] = useState(false)

  const handleDescChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateDesc'), 2000)
    
    console.log(event.target)
  }
  
  const handleAscChange = (event) => {
    setLoading(true);
    event.preventDefault()
    setTimeout(() => setLoading(false), 2000)
    setTimeout(() => setSort('dateAsc'), 2000)    
    console.log(event.target)
  }

  
  if(sort === 'dateDesc') sortedNewsData = news.sort((a, b) => new Date(b.date) - new Date(a.date))
  else if(sort === 'dateAsc') sortedNewsData = news.sort((a, b) => new Date(a.date) - new Date(b.date))

  const createMarkup = (html) => {
    return  {
      __html: DOMPurify.sanitize(html)
    }
  }
  const renderCarousel = sortedNewsData.slice(0, 5).map(news => {
    return <ReactBootStrap.Carousel.Item className="overlay">
        <div className="newsSliderImage">
          <img
            className="d-block w-100 img-fluid"
            src={news.upload_image || news.image_link}
            alt="First slide"
          />
        </div>

        <ReactBootStrap.Carousel.Caption className="text-left caption">
          <Typography paragraph={true} variant="subtitle2" color="inherit">
          {news.tag.map(tag => {
            return <Label basic size={'mini'} key={tag.id}>{tag.name}</Label>

          })}&#xa0;
          &middot; {moment(news.date).format('DD MMM YYYY').toUpperCase()}</Typography>
        <Link to={`/user/news/${news.id}`}>
          <Header as='h1' inverted className="mb-3">{news.headline}</Header>
        </Link>
        <span style={{color: "#FFFFFF", fontSize: "16px"}} dangerouslySetInnerHTML={{__html: news.short_description}}></span>
        </ReactBootStrap.Carousel.Caption>
      </ReactBootStrap.Carousel.Item>
  })

  const shareContentSubmit = (values) => {
    console.log('Received values of form: ', values);
  }

  const renderBelowCards = sortedNewsData.map(news => {
    return <div className="sub-cards">
    <Link to={`/user/news/${news.id}`} className="news_mid_thumb_img">
    <ReactBootStrap.Image
      src={ news.upload_image || news.image_link || <Placeholder>
      <Placeholder.Image />
    </Placeholder>}
      className="card-img"
    />
    </Link>
    <div className="below-news-card">
    <Card >
      <Card.Content>
        <Card.Header>
        <Link to={`/user/news/${news.id}`}>
        <h6 className="news-header-right">
          {news.headline}
        </h6>
        </Link>
        </Card.Header>

        <Card.Description>
        <div className="box">
          <p className="char-cut-line-2" dangerouslySetInnerHTML={{__html: news.short_description}}/>
        </div>
        </Card.Description>
      </Card.Content>

      <Card.Content>
      <span class="metatext-news">
      {news.tag.map(tag => {
          return <Label basic size={'mini'} color={'pink'} key={tag.id}>{tag.name}</Label>
      })}
      <span>&#xa0;&#x0358;&#xa0;</span>
      {moment(news.date).format("MMMM Do, YYYY")}</span>
      <span className="news_mid_actions socialActionBlock">
      <Checkbox icon= {<BookmarkBorderIcon />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={news.id}/>
        {/* &#xa0;&#x0358;&#xa0; */}
      <Popup
          trigger={<IconButton aria-label="share"><ShareIcon /></IconButton>}
          position="top center"
          on="click"
          flowing
          style={{height:'auto'}}
        >
        <div style={{width:"450px",wordBreak:"break-all"}}>
        <label>Share On</label><br/>
        <div className="row pl-3">
          <Link className="socialIcon socialLinkedin mr-1"><FiLinkedin /></Link>
          <Link className="socialIcon socialTwitter mr-1"><FiTwitter /></Link>
          <Link className="socialIcon socialFacebook mr-1"><FiFacebook /></Link>
        </div>
        <Divider />
        <label>Share With</label><br/>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
          Elliot@gmail.com
          <Icon name='delete' />
        </Label>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
          Stevie@gmail.com
          <Icon name='delete' />
        </Label>
        
        <Form className="mt-2">
          <Form.Field>
            <input placeholder='Enter email id' />
          </Form.Field>
          <Form.Field>
          <TextArea placeholder='Type your message here' />
          </Form.Field>
          <Form.Field>
            <SUIButton type='submit' color="pink">Submit</SUIButton>
            <SUIButton type='submit' basic>Cancel</SUIButton>
          </Form.Field>
        </Form>
        </div>
      </Popup>
      </span>
      </Card.Content>
    </Card>
    </div>
  </div>
  })

  const renderRightCards = sortedNewsData.slice(0,3).map(news => {
    return <> 
    <div className="sub_mid_right">
      <Link to={`/user/news/${news.id}`} className="newThumb-right">
        <ReactBootStrap.Image
          className="det-right-image"
          src={news.upload_image || news.image_link}
        />
      </Link>
      <div>
      <div className="news-card">
      <Card>
      <Card.Content>
        <Card.Header>
        <Link to={`/user/news/${news.id}`}>
        <h6 className="news-header-right char-cut-line-2">
          {news.headline}
        </h6>
        </Link>
        </Card.Header>

        <Card.Description>
        <div className="box">
          <p className="char-cut-line-2" dangerouslySetInnerHTML={{__html: news.short_description }}/>
        </div>
        </Card.Description>
      </Card.Content>

      <Card.Content extra >
      <span className="metatext-news">
      {news.tag.map(tag => {
        return <Label basic size={'mini'} color={'pink'} key={tag.id}>{tag.name}</Label>

      })}  
      <span>&#xa0;&#x0358;&#xa0;</span>
      {moment(news.date).format("MMMM Do, YYYY")}</span>
      <Dropdown
          simple
          text={<MoreHorizIcon />}  
          style={{color: "#415A6C", float:"right"}}
          pointing='top right'
          icon={null}
        >
        <Menu>
          <p className="anno-right">
            <Checkbox icon= {<BookmarkBorderIcon />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={news.id}/>
            BookMark
          </p>
      <Popup
        trigger={
          <p className="anno-right"><IconButton><ShareIcon /></IconButton> Share</p>
        }
        position="left center"
        on="click"
        flowing
        style={{height:'auto'}}
      >
        <div style={{width:"450px",wordBreak:"break-all"}}>
        <label>Share On</label><br/>
        <SUIButton circular color='facebook' icon='facebook' />
        <SUIButton circular color='twitter' icon='twitter' />
        <SUIButton circular color='linkedin' icon='linkedin' />
        <Divider />
        <label>Share With</label><br/>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
          Elliot@gmail.com
          <Icon name='delete' />
        </Label>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
          Stevie@gmail.com
          <Icon name='delete' />
        </Label>
        
        <Form className="mt-2">
          <Form.Field>
            <input placeholder='Enter email id' />
          </Form.Field>
          <Form.Field>
          <TextArea placeholder='Type your message here' />
          </Form.Field>
          <Form.Field>
            <SUIButton type='submit' color="pink">Submit</SUIButton>
            <SUIButton type='submit' basic>Cancel</SUIButton>
          </Form.Field>
        </Form>
        </div>
      </Popup>
        </Menu>
      </Dropdown>
      </Card.Content>
      </Card>
      </div>
      </div>
    </div>
    </>
  })

  const renderRightCardsBelow = sortedNewsData.slice(0,4).map(news => {
    return <> 
    <Grid.Column width={8}>
    <div className="sub_mid_right">
      <Link to={`/user/news/${news.id}`} className="newThumb-right">
        <ReactBootStrap.Image
          className="det-right-image"
          src={ news.upload_image || news.image_link }
        />
      </Link>
      <div className="news-card">
      <Card>
      <Card.Content>
        <Card.Header>
        <Link to={`/user/news/${news.id}`}>
        <h6 className="news-header-right">
          {news.headline}
        </h6>
        </Link>
        </Card.Header>

        <Card.Description>
        <div className="box">
          <p className="char-cut-line-2">
          {news.short_description}
          </p>
        </div>
        </Card.Description>
      </Card.Content>

      <Card.Content extra >
      <span className="metatext-news">
      {news.tag.map(tag => {
        return <Label basic size={'mini'} color={'pink'} key={tag.id}>{tag.name}</Label>
      })}  
      <span>&#xa0;&#x0358;&#xa0;</span>
      {moment(news.date).format("MMMM Do, YYYY")}</span>
      <Dropdown
          simple
          text={<MoreHorizIcon />}  
          style={{ color: "#415A6C", float: "right" }}
          pointing='top right'
          icon={null}
        >
        <Menu>
          <p className="anno-right">
            <Checkbox icon= {<BookmarkBorderIcon />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={news.id}/>Bookmark
          </p>
      <Popup
        trigger={
          <p className="anno-right"><IconButton><ShareIcon /></IconButton> Share</p>
        }
        position="left center"
        on="click"
        flowing
        style={{height:'auto'}}
      >
        <div style={{width:"450px",wordBreak:"break-all"}}>
        <label>Share On</label><br/>
        <SUIButton circular color='facebook' icon='facebook' />
        <SUIButton circular color='twitter' icon='twitter' />
        <SUIButton circular color='linkedin' icon='linkedin' />
        <Divider />
        <label>Share With</label><br/>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
          Elliot@gmail.com
          <Icon name='delete' />
        </Label>
        <Label as='a'>
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
          Stevie@gmail.com
          <Icon name='delete' />
        </Label>
        
        <Form className="mt-2">
          <Form.Field>
            <input placeholder='Enter email id' />
          </Form.Field>
          <Form.Field>
          <TextArea placeholder='Type your message here' />
          </Form.Field>
          <Form.Field>
            <SUIButton type='submit' color="pink">Submit</SUIButton>
            <SUIButton type='submit' basic>Cancel</SUIButton>
          </Form.Field>
        </Form>
        </div>
      </Popup>
        </Menu>
      </Dropdown>
      </Card.Content>
      </Card>
      </div>
    </div>
    </Grid.Column>
    </>
  })

  //Render tags

  const [parentTag, setParentTag] = useState([]);
  const [childTag, setChildTag] = useState([]);
  const [allTags, setAllTag] = useState([]);

  useEffect(() => { fetchParentTag() }, []);
  useEffect(() => { fetchChildTag() }, []);
  useEffect(() => { fetchAllTag() }, []);

  const fetchParentTag = () => {
    fetch(`${url}/tag_parent/view/`)
    .then((res) => res.json())
    .then((result) => setParentTag(result))
  }
  const fetchChildTag = () => {
    fetch(`${url}/tag_child/view/`)
    .then((res) => res.json())
    .then((result) => setChildTag(result))
  }
  const fetchAllTag = () => {
    fetch(`${url}/tag/view/`)
    .then((res) => res.json())
    .then((result) => setAllTag(result))
  }

  var [tagState, setTagState] = useState([]);
  var [tagCheck, setTagCheck] = useState([]);

  const handleTagChange = (event) => {
      console.log(event.target.checked, event.target.name)
      setTagCheck({...tagCheck, [event.target.name]: event.target.checked})
      if(event.target.checked) {
        console.log(tagState, "inside true");
        if(tagState.indexOf(parseInt(event.target.name)) === -1) {
          tagState.push(parseInt(event.target.name));

        }
      }

      if(!event.target.checked) {
        console.log(tagState, "inside false");
        var index = tagState.indexOf(parseInt(event.target.name))
        if (index !== -1) {
          tagState.splice(index, 1);
          // setTagState([...tagState]);
        }
        // tagState.filter(tag => parseInt(tag) !== parseInt(event.target.name))
      }
      console.log(tagState,tagState.length);

    const fetchTagNews = () => {  
      if(tagState.length === 0) {
        fetch(`${url}/news/view/?filters={"type":"operator","data":{"attribute":"row_status","operator":"=","value":"1"}}`)
        .then((res) => res.json())
        .then((result) => setNews(result.results))
      }
      else {
        fetch(`${url}/news/view/?filters={"type":"and","data":[{"type":"operator","data":{"attribute":"tag","operator":"in","value":[${tagState}]}},{"type":"operator","data":{"attribute":"row_status","operator":"in","value":["1"]}}]}`)
        .then((res) => res.json())
        // .then((result) => console.log(result))
        .then((result) => {
          console.log(result)
          setNews(result.results)
        })
        

      }
    }
    fetchTagNews();
  }

  var options = [];
  const [selectOption, setSelectOption] = useState([]);
  const [selectedSearchBar, setSearchBar] = useState(false);
  const onSearch = (searchText) => {
    console.log(searchText);
    const fuse = new Fuse(options, {keys : ['value', 'id']});

    var results = fuse.search(searchText);
    results.length > 0 ? setSearchBar(true) : setSearchBar(false)
    console.log('searchresult', results);

    const characterResults = results.map(searchedOption => searchedOption.item)

    console.log('selectoptionvalue', characterResults)
    setSelectOption(characterResults);
  };

  const [renderSelectedTag, setRenderSelectedTag] = useState([]);

  const onSelect = (data) => {
    console.log('onSelect', data);
    var idOfTag = options.filter( option => option.value === data ).map( option => option.id)

    var [ tagId ] = idOfTag;
    var filterSelectedChildTag = allTags.filter( tag => tag.id === tagId )
    var [ parentID ]  = filterSelectedChildTag.map( tag => tag.parent_id );
 
    var filterSelectedParentTag = allTags.filter( tag => tag.id === parentID )

    console.log(filterSelectedParentTag, 'parent', parentID)

    var renderSelectedTag1 = filterSelectedParentTag.map(parentTag => {
      return <>
      <div className="wrap" onClick={() => toggle(parentTag.id)}>
      <h6>{parentTag.name}</h6>
      <span>
      </span>
      </div>
      {
      filterSelectedChildTag.map((childTag) => {
        let childId = childTag.id;
        return <>
        <div className="checking">
          <div className="acordian_drop">
          <FormControlLabel
            control={<Checkbox checked={tagCheck.childId} onChange={handleTagChange} name={childTag.id} />}
            label={childTag.name}
          />
          </div>
        </div>
        </>
      })}
      </>
    })

    setRenderSelectedTag(renderSelectedTag1);
  };
  
  var filterTags = parentTag.filter(tag => tag.module === 2)
  // console.log(parentTag);

  const renderTags = filterTags.map(tag => {

    var currentParentTag = tag.id;
    var currentModule = tag.module;
    var filterChildTag = childTag.filter(tag => tag.module === currentModule && tag.parent_id === currentParentTag)
    
    filterChildTag.map(tag => {
      var indexOfTag = options.findIndex(o => o.value === tag.name)
      if(indexOfTag === -1) {
        options.push({value :tag.name, id : tag.id});
      }
    })

    return <>
      <div className="wrap" onClick={() => toggle(tag.id)}>
      <h6>{tag.name}</h6>
      <span>
          {clicked === tag.id ? (<AiOutlineMinus className="acordian_icon" />) : 
          (<AiOutlinePlus className="acordian_icon" />)}
      </span>
      </div>
      {clicked === tag.id ? (
      filterChildTag.map((tag) => {
        return <>
        <div className="checking">
          <div className="acordian_drop">
          <FormControlLabel
            control={<Checkbox checked={tagCheck[tag.id]} onChange={handleTagChange} name={tag.id} />}
            label={tag.name}
          />
          </div>
        </div>
        </>
      })
      ) : null}
    </>
  })

  //Main Page
  return (
    // Side navbar
    <div className="page newMainBlock">
      
      <Drawer
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className="acordian">
          <div>
            <IconButton onClick={handleDrawerClose}>
              <ChevronLeftIcon />
              <MenuIcon />
            </IconButton>
          </div>
          <div className="acordian_header">
            <div className="acordian_search">
              <FiSearch className="acordian_search_icon" />
              
              <br />
              <AutoComplete
                options={selectOption}
                style={{
                  width: 200,
                }}
                onSelect={onSelect}
                onSearch={onSearch}
                placeholder="Search here"
              >
                <input
                  type="text"
                  aria-label="Text input with dropdown button"
                  // placeholder="search here"
                  className="acordian_ip"
                />
              </AutoComplete>
            </div>
          </div>

          {/***************acordian***************/}
          <div className="acordian_content">
            {selectedSearchBar ? renderSelectedTag : renderTags}
          </div>
        </div>
      </Drawer>
      {/* Mid news */}
      <div className="container">
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open
        })}
      >
      <div className={classes.drawerHeader} />
      <div className="news">

        <div className="news_header">
          <div className="news_left_header">
            {open || (<IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
            >
              <MenuIcon />
              {/* <ChevronRightIcon /> */}
            </IconButton>)}
            <h4 className="main-header">News</h4>
          </div>


          <div className="news_right_header" >
          <ButtonGroup size="large" variant="outlined" color="secondary" aria-label="large outlined button group" className="" disabled={loading}>
              <Button startIcon={<Icon name='sort content descending' size='large' />} onClick={handleDescChange} ></Button>
              <Button startIcon={<Icon name='sort content ascending' size='large' />} onClick={handleAscChange} ></Button>
          </ButtonGroup>
          </div>
        </div>

        <Divider />
        <Spin spinning={loading}>
        <div className="news_mid">
          <Grid>
            <Grid.Column width={open? 16 : 10}>
            <ReactBootStrap.Carousel interval={null} controls={false}>
              {renderCarousel}
            </ReactBootStrap.Carousel>
            </Grid.Column>
            { open ? renderRightCardsBelow :(<Grid.Column width={open? 8 : 6}>
              {renderRightCards}
            </Grid.Column>) }
          </Grid>
          <Grid className="news_mid_container">
            <Grid.Column width={open? 12 : 10}>
              {renderBelowCards}
            </Grid.Column>
          </Grid>
        </div>
        </Spin>
      </div>
      </main>
      </div>
    </div>
  );
};

export default ViewAllNews;
