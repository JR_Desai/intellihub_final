import React, { useState } from 'react'
import ConfirmPopup from '../../../ConfirmPopup';
import EditPerson from '../EditPerson';

const PersonItem = ({data}) => {

    const [openEdit, setOpenEdit] = useState(false);
    const [openRemove, setOpenRemove] = useState(false);

    const editTech = () => {
        setOpenEdit(!openEdit)
    }

    const removePerson = () => {
        setOpenEdit(false)
    }



    return (
        <div className="gx-media gx-featured-item" >
            <div className="gx-featured-thumb">
                <img className="gx-rounded-lg" src={data.image} alt="..."/>
            </div>
            <div className=" gx-media-body gx-featured-content">
                <div className="gx-featured-content-left"> 
                <h3 className="gx-mb-2">{data.title}</h3>
                <div className="ant-row-flex">
                    <div className={`gx-text-primary gx-mb-1`}>
                        <span className="gx-media-body">{data.address}</span>
                    </div>
                </div>
                </div>
            </div>
            <div className="">
                <i className="gx-icon-btn icon icon-edit" onClick={() =>
                    editTech()}
                />
                <i className="gx-icon-btn icon icon-trash" onClick={() =>
                    removePerson()}
                />
            </div>
            <EditPerson open={openEdit} close={setOpenEdit} data={data} />
            <ConfirmPopup onConfirm={() => {}} showStatus={openRemove} />
        </div>
    )
}

export default PersonItem;
