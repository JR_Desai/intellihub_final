import { Input, Modal } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React from 'react'

const ComposeConfig = ({onClose, open}) => {
    return (
        <Modal onCancel={() => onClose(false)} visible={open}>
            <div className="gx-form-group">
            <Input
                placeholder="To*"
                onChange={(event) => this.setState({to: event.target.value})}
                margin="normal"/>
            </div>
            <div className="gx-form-group">
            <Input
                placeholder="Subject"
                onChange={(event) => this.setState({subject: event.target.value})}
                margin="normal"
            />
            </div>
            <div className="gx-form-group">
            <TextArea
                placeholder="Message"
                onChange={(event) => this.setState({message: event.target.value})}
                autosize={{minRows: 2, maxRows: 6}}
                margin="normal"/>
            </div>
        </Modal>
    )
}

export default ComposeConfig;
