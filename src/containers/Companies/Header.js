import React from "react";
import {Button} from "antd";
import { TAB_SIZE } from "../../constants/ThemeSetting";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const AlgoliaHeader = () => (
  <div className="gx-algolia-header">
    <ConnectedSearchBox />
  </div>
);


function CustomSearchBox() {

  const {width} = useSelector(({common}) => common)

  return (
  <div className="gx-companies-header">
    <div className="gx-search-bar gx-lt-icon-search-bar">
      <i className="gx-icon-search icon-search icon"/>
      <div className="gx-form-group">
        <input
          type="search"
          placeholder="Search here..."
          onChange={() => {}}
          autoComplete="off"
          className="search-input form-control"
          id="q"
        />
      </div>
    </div>
    <Link to="/companies/create" className="">
      <Button type="primary" className="gx-btn-end">
        Create Company
      </Button>
    </Link>
  </div>
  );
};
const ConnectedSearchBox = CustomSearchBox;

export default AlgoliaHeader;
