import { CREATE_TECHNOLOGY_SUCCESS, GET_ALL_TECHNOLOGIES_SUCCESS, GET_TECHNOLOGY_SUCCESS, GET_TECHNOLOGY_TAGS_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_TECHNOLOGY_SUCCESS } from "../../constants/ActionTypes"

import axios from 'axios';

export const createTechnology = async (data , dispatch) => {
        try {
                const technology = await axios.post(`/technology/create/`, data);
                dispatch({
                        type: CREATE_TECHNOLOGY_SUCCESS,
                        payload: technology.data
                })
                getAllTechnologies(dispatch);
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllTechnologies = async (dispatch) => {
        try {
                const technologies = await axios.get(`/technology/view/`);
                dispatch({
                        type: GET_ALL_TECHNOLOGIES_SUCCESS,
                        payload: technologies.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getTechnology = async (technologyID, dispatch) => {
        try {
                const technology = await axios.get(`/technology/view/?id=${technologyID}`);
                dispatch({
                        type: GET_TECHNOLOGY_SUCCESS,
                        payload: technology.data.results
                })
        } catch (err) {
                console.log(err);
        }
};

export const getTechnologyTags = async (dispatch) => {
        try {
                const technology = await axios.get(`/tag_child/view/`);
                dispatch({
                        type: GET_TECHNOLOGY_TAGS_SUCCESS,
                        payload: technology.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const getParentTags = async (dispatch) => {
        try {
                const technology = await axios.get(`/tag_parent/view/`);
                dispatch({
                        type: GET_TECHNOLOGY_TAGS_SUCCESS,
                        payload: technology.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateTechnology = async (technologyID, data, dispatch) => {
        try {
                const technology = await axios.put(`/technology/update/${technologyID}`, data);
                dispatch({
                        type: UPDATE_TECHNOLOGY_SUCCESS,
                        payload: technology.data
                })
                getAllTechnologies(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusTechnology = async (technologyID, data, dispatch) => {
        try {
                const technology = await axios.put(`/technology/update/${technologyID}`, {
                        name: data.name,
                        description: data.description,
                        row_status: data.row_status
                }
                );
                dispatch({
                        type: UPDATE_TECHNOLOGY_SUCCESS,
                        payload: technology.data
                })
                getAllTechnologies(dispatch);
        } catch (err) {
                console.log(err);
        }
};

export const deleteTechnology = async (technologyID, dispatch) => {
        try {
                await axios.delete(`/technology/delete/${technologyID}`);
                dispatch ({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
                getAllTechnologies(dispatch);
                } catch (err) {
                console.log(err);
        }
};
