import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import {FiLinkedin, FiTwitter, FiMail, FiBookmark, FiDownload} from "react-icons/fi"
import { Icon } from 'semantic-ui-react'
import * as ReactBootStrap from "react-bootstrap";
import { useParams } from 'react-router-dom'
import moment from 'moment'
import readingTime from 'reading-time'
import { Label } from 'semantic-ui-react'
import DOMPurify from 'dompurify'
import Pdf from 'react-to-pdf'

const ref = React.createRef();

const DetailedNews = () => {
  const url = "http://3.108.46.195:9000";

  const { newsId } = useParams()
  const [newsDetail, setNewsDetail] = useState([])
  useEffect(() => {
    const fetchDetailNews = () => {
      fetch(`${url}/news/view/?id=${newsId}`)
      .then((res) => res.json())
      .then((result) => {
        setNewsDetail(result.results);
        }
      )
    }
    fetchDetailNews()
  }, [])
  
  const createMarkup = (html) => {
    return  {
      __html: DOMPurify.sanitize(html)
    }
  }
  const options = {
    orientation: 'portrait',
    unit: 'in',
  };

  const renderDetailNews = newsDetail.map(news => { 
    const stats = readingTime(news.long_description)
    const filename = `${news.headline}.pdf`
    return <>
    <div className="container">
      <div className="miniContainer">
        <div className="newsDetails-social-block">
          <ul className="newsDetails-socialList">
            <li><Link className="socialIcon socialLinkedin"><FiLinkedin /></Link></li>
            <li><Link className="socialIcon socialTwitter"><FiTwitter /></Link></li>
            <li><Link className="socialIcon socialMail"><FiMail /></Link></li>
            <li><Link className="socialIcon socialSave"><FiBookmark /></Link></li>
            <li>
              <Pdf targetRef={ref} filename={filename} options={options} x={.7} y={.5} scale={0.8}>
                {({ toPdf }) => <Link onClick={toPdf} className="socialIcon pdfDownload"><FiDownload /></Link>}
              </Pdf>
            </li>
          </ul>
        </div>
        <div className="magazine_left">
          <Link to="/user/news">News </Link>
          <Icon name='angle right' />
          {news.headline}

          <div className="newsDetails-block" ref={ref}>
            <h2 dangerouslySetInnerHTML={createMarkup(news.headline)}></h2>
            <div className="newsEntryDetails">
              <div className="newsNameDate">
                <span>{news.tag.map(tag => {
                  return <Label basic color={'pink'} key={tag.id}>{tag.name}</Label>
                })}</span>
                <span>{moment(news.date).format('DD MMM YYYY').toUpperCase()}</span>
              </div>
              <div className="newsReadtime">
                <span>{stats.text}</span>
              </div>
            </div>
            <div className="maga">
              <ReactBootStrap.Image
                className="magazine-page-img"
                src={ news.upload_image || news.image_link }
              />
            </div>
            <div className="newsDesctiptions">
              <p dangerouslySetInnerHTML={createMarkup(news.short_description)}></p>

              <p dangerouslySetInnerHTML={createMarkup(news.long_description)}></p>
            </div>
          </div>
        </div>
          
          {/* <div className="maga-right">
            <div className="maga-info">
              <h4>{news.headline}</h4>
              <p>
                {news.short_description}
              </p>
              <p>
                {news.long_description}
              </p>
            </div>
          </div> */}

      </div>
    </div>
    </>
  })
  return (
    <div className="magazine-1 inner-wrapper">
      {renderDetailNews}
    </div>
  );
};

export default DetailedNews;
