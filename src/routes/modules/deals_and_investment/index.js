import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import DealsAndInvestment from '../../../containers/DealsAndInvestment';

import CreateDealsAndInvestment from '../../../containers/DealsAndInvestment/CreateDealsAndInvestment';
import DealsAndInvestmentDetail from '../../../containers/DealsAndInvestment/DealsAndInvestmentDetail';

const DealsAndInvestmentRoutes = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={DealsAndInvestment} />
            <Route exact={true} path={`${match.url}/create`} component={CreateDealsAndInvestment} />
            <Route exact={true} path={`${match.url}/:id`} component={DealsAndInvestmentDetail} />
            <Route exact={true} path={`${match.url}/update/:id`} component={CreateDealsAndInvestment} />
        </Switch>
    )
}

export default DealsAndInvestmentRoutes;
