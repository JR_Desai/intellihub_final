import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import ModuleRoutes from './modules';
import OtherRoutes from './others';

const Routes = () => {

    const match = useRouteMatch()

    return (
            <Switch>
                <Route exact={false} path={`${match.url}others`} component={OtherRoutes} />
                <Route exact={false} path={`${match.url}`} component={ModuleRoutes} />
            </Switch>
    );
}

export default Routes;
