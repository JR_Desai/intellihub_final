import { CREATE_MARKET_REPORTS_SUCCESS, EDIT_MARKET_REPORTS_DETAILS, GET_ALL_MARKET_REPORTS_SUCCESS, GET_MARKET_REPORTS_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_MARKET_REPORTS_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    marketReports: [],
    marketReport: {},
    showRemove: false,
    showEdit: false
}

export default function marketReportsReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_MARKET_REPORTS_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_MARKET_REPORTS_SUCCESS: {
            return {
                ...state,
                loader: false,
                marketReports: action.payload
            }
        }
        case GET_MARKET_REPORTS_SUCCESS: {
            return {
                ...state,
                loader: false,
                marketReport: action.payload[0]
            }
        }
        case CREATE_MARKET_REPORTS_SUCCESS: {
            return {
                ...state,
                loader: false,
                marketReport: action.payload
            }
        }
        case UPDATE_MARKET_REPORTS_SUCCESS: {
            return {
                ...state,
                loader: false,
                marketReport: action.payload
            }
        }    
        default:
            return state;
    }
}
