import axios from 'axios';
import { CREATE_RESEARCH_SUCCESS, GET_ALL_RESEARCHES_SUCCESS, GET_RESEARCH_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_RESEARCH_SUCCESS, GET_APIKEY_SUCCESS } from '../../constants/ActionTypes';

const module_link = "research_config"

export const createResearch = async (data, dispatch) => {
        try {
                const research = await axios.post(`/${module_link}/create/`, data);
                dispatch({
                        type: CREATE_RESEARCH_SUCCESS,
                        payload: research.data
                })
        } catch (err) {
                console.log(err.message);
        }
};

export const getAllResearches = async (dispatch) => {
        try {
                const researches = await axios.get(`/${module_link}/view/`);
                researches.data.forEach((result) => result["name"] = result["q"]);
                dispatch({
                        type: GET_ALL_RESEARCHES_SUCCESS,
                        payload: researches.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const getResearch = async (researchID, dispatch) => {
        try {
                const research = await axios.get(`/${module_link}/view/?id=${researchID}`);
                research.data.map((res) => {
                        res['api_key'] = res['api_key'].id;
                        res['time_period'] = res['time_period'] ? res['time_period'].toString() : res['time_period'];
                        res['search_type'] = res['search_type'] ? res['search_type'].toString() : res['search_type'];
                });
                dispatch({
                        type: GET_RESEARCH_SUCCESS,
                        payload: research.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const getResearchApiKey = async (dispatch) => {
        try {
                const research = await axios.get(`/api_config/view/`);
                dispatch({
                        type: GET_APIKEY_SUCCESS,
                        payload: research.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateResearch = async (researchID, data, dispatch) => {
        try {
                const research = await axios.put(`/${module_link}/update/${researchID}`, data);
                dispatch({
                        type: UPDATE_RESEARCH_SUCCESS,
                        payload: research.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const updateStatusResearch = async (researchID, data, dispatch) => {
        try {
                const research = await axios.patch(`/${module_link}/update/${researchID}`, {
                        row_status: data.row_status
                }
                );
                dispatch({
                        type: UPDATE_RESEARCH_SUCCESS,
                        payload: research.data
                })
        } catch (err) {
                console.log(err);
        }
};

export const deleteResearch = async (researchID, dispatch) => {
        try {
                await axios.delete(`/${module_link}/delete/${researchID}`);
                dispatch({
                        type: SHOW_CONFIRM_REMOVE,
                        payload: false
                })
        } catch (err) {
                console.log(err);
        }
};
