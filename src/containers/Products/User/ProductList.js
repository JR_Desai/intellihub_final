import React, { useEffect, useState } from "react";
import {  makeStyles } from "@material-ui/core";
import { Empty, Spin } from "antd";
import clsx from "clsx";
import {
  getAllProducts,
} from "../../../appRedux/actions/Products";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";

import ProductItem from "./components/ProductItem";
import CompareProductPanel from "./components/CompareProductPanel";
import ProductContentHeader from "./components/ProductContentHeader";
import ProductDrawer from "./components/ProductDrawer";

const drawerWidth = "240";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth.concat,
    position: "relative",
    marginTop: "109px",
    zIndex: "1029",
  },
  drawerHeader: {
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

const moduleCode = 6;
const ProductList = () => {
  const dispatch = useDispatch();
  const { products, tags } = useSelector(({ products }) => products);
  const { loading } = useSelector(({ common }) => common);
  const classes = useStyles();

  const [hqDrawer, setHqDrawer] = useState(false);
  const [clicked, setClicked] = useState(false);
  const [listIconActiveClass, setListIconActiveClass] =
    useState("active-filter");
  const [gridIconActiveClass, setGridIconActiveClass] = useState("");
  const [productListClass, setProductListClass] = useState(
    "productList-row productList-1col"
  );

  const [selectedProductsForComparision, setselectedProductsForComparision] =
    useState([]);

  // filters regarding
  const [parentTag] = useState([]);
  const [childTag] = useState([]);
  const [filters, setFilters] = useState([]);

  // filters regarding
  useEffect(() => {
    const urlFilter = [
      {
        type: "operator",
        data: { attribute: "row_status", operator: "=", value: "1" },
      },
    ];
    if (filters.length) {
      filters.forEach((filter) => {
        urlFilter.push({
          type: "operator",
          data: {
            attribute: filter?.module,
            operator: "=",
            value: String(filter?.id),
          },
        });
      });
    }

    getAllProducts(dispatch, {
      type: urlFilter.length > 1 ? "and" : "operator",
      data: urlFilter.length > 1 ? urlFilter : urlFilter?.[0]?.data,
    });
  }, [filters]);

  const handleDrawerOpen = () => setHqDrawer(true);
  const handleDrawerClose = () => setHqDrawer(false);

  const toggle = (index) => {
    if (clicked === index) return setClicked(null);
    setClicked(index);
  };

  const handleTagChange = (data) => {
    console.log({ data });
    if (data.checked) {
      setFilters([...filters, data]);
    } else {
      setFilters([
        ...filters.filter((f) => f?.id !== data?.id && f?.name !== data?.name),
      ]);
    }
  };

  const filterTags = parentTag.filter((tag) => tag.module === 3);

  const handleChangeList = (event) => {
    event.preventDefault();
    setProductListClass("productList-row productList-1col");
    setGridIconActiveClass("");
    setListIconActiveClass("active-filter");
  };
  const handleChangeGrid = (event) => {
    event.preventDefault();
    setProductListClass("productList-row productListGridView");
    setGridIconActiveClass("active-filter");
    setListIconActiveClass("");
  };

  return (
    <div className="page newMainBlock">
      <ProductDrawer
        hqDrawer={hqDrawer}
        classes={classes}
        handleDrawerClose={handleDrawerClose}
        filterTags={filterTags}
        childTag={childTag}
        toggle={toggle}
        clicked={clicked}
        tagState={filters}
        handleTagChange={handleTagChange}
      />

      <div className="container">
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: hqDrawer,
          })}
        >
          <div className={classes.drawerHeader} />
          <div className="news">
            <ProductContentHeader
              hqDrawer={hqDrawer}
              handleDrawerOpen={handleDrawerOpen}
              handleDrawerOpen={handleDrawerOpen}
              listIconActiveClass={listIconActiveClass}
              handleChangeList={handleChangeList}
              gridIconActiveClass={gridIconActiveClass}
              handleChangeGrid={handleChangeGrid}
            />
            {/* Magazines list */}
            <Spin spinning={loading}>
              <div className="productWrap">
                <div className="productListingBlock">
                  <div className={productListClass}>
                    {/* products */}
                    {products.length === 0 && !loading && (
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          height: "70vh",
                          width: "100%",
                        }}
                      >
                        <Empty />
                      </div>
                    )}
                    {products.map((product) => (
                      <ProductItem
                        product={product}
                        selectedProductsForComparision={
                          selectedProductsForComparision
                        }
                        setselectedProductsForComparision={
                          setselectedProductsForComparision
                        }
                      />
                    ))}
                  </div>
                </div>
              </div>
            </Spin>
          </div>
        </main>
      </div>

      {selectedProductsForComparision.length > 1 && (
        <CompareProductPanel
          selectedProductsForComparision={selectedProductsForComparision}
          setselectedProductsForComparision={setselectedProductsForComparision}
        />
      )}
    </div>
  );
};

export default ProductList;
