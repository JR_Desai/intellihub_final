import React from 'react'
import {Card, Table} from "antd";
import { productsData } from "../../models/companies";

const Compare = () => {

    const columns = [
        {
            title: 'Parameters',
            dataIndex: 'parameters',
        }, {
            title: 'Company Name 1',
            dataIndex: 'company1',
        }, {
            title: 'Company Name 2',
            dataIndex: 'company2',
        }
    ];


    return (
        <div title="gx-table-responsive">
            <Table className="gx-table-no-bordered"
                columns={columns}
                dataSource={productsData}
                pagination={false}
            />

        </div>
    )
}

export default Compare;
