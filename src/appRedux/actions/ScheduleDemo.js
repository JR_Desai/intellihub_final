import axios from 'axios';
import { SCHEDULE_DEMO } from '../../constants/ActionTypes';

const module_link = "schedule_demo";

export const scheduleDemo = async (data, dispatch) => {
	try {
        const demo = await axios.post(`/${module_link}/create/`, data);
        dispatch ({
	        type: SCHEDULE_DEMO,
	        payload: demo.data
	    })
    } catch (err) {
        console.log(err.message);
    }
};