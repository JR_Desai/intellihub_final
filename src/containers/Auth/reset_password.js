import React from "react";
import {Button, Form, Input} from "antd";

const FormItem = Form.Item;


const ResetPassword = () => {

    return (
        <>
            <div className="gx-login-header gx-text-center">
                <h1 className="gx-login-title">Reset Password</h1>
            </div>
            <Form
            initialValues={{ remember: true }}
            name="basic"
            className="gx-signin-form gx-form-row0">

            <FormItem rules={ [{required: true, message: 'Please input your Password!'}]} name="password">
                <Input className='gx-input-lineheight' type="password" placeholder="Password"/>
            </FormItem>

            <FormItem rules={ [{required: true, message: 'Please confirm your password!'}]} name="confirm">
                <Input className='gx-input-lineheight' placeholder="Retype New Password" type="password"/>
            </FormItem>
            <FormItem className="gx-text-center">
                <Button type="primary" htmlType="submit">
                    Reset Password
                </Button>
            </FormItem>
            </Form>
        </>
    );
};

export default ResetPassword;
