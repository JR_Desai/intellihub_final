import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getAllMagazines, updateStatusMagazine } from '../../appRedux/actions';
import ListView from '../../components/ListView';

const Magazine = () => {

    const {magazines} = useSelector(({magazines}) => magazines);

    const dispatch = useDispatch();

    useEffect(() => {
        getAllMagazines(dispatch);
    }, [])

    return (
        <ListView listToBeShown={magazines} searchTitleText="magazines" link="magazine" title="Magazines" updateModuleItem={updateStatusMagazine} />
    )
}

export default Magazine;
