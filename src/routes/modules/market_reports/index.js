import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import MarketReports from '../../../containers/MarketReports';
import CreateMarketReports from '../../../containers/MarketReports/CreateMarketReports';

const MarketReportsRoutes = () => {

    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={MarketReports} />
            <Route exact={true} path={`${match.url}/create`} component={CreateMarketReports} />
        </Switch>
    )
}

export default MarketReportsRoutes;
