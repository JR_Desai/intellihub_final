import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getAllResearches, updateStatusResearch } from '../../appRedux/actions/Research';
import ListView from '../../components/ListView';

const Research = () => {

    const { researches } = useSelector(({ researches }) => researches);
    const dispatch = useDispatch();

    useEffect(() => {
        getAllResearches(dispatch);
    }, []);

    return (
        <ListView listToBeShown={researches} searchTitleText="research and literature" link="ral" title="Research & Literature" updateModuleItem={updateStatusResearch} />
    )
}

export default Research;
