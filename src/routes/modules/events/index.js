import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import Events from '../../../containers/Event';
import CreateEvent from '../../../containers/Event/CreateEvent';
import EventDetails from '../../../containers/Event/EventDetails';

const EventsRoutes = () => {
    const match = useRouteMatch();

    return (
        <Switch>
            <Route exact={true} path={`${match.url}`} component={Events} />
            <Route exact={true} path={`${match.url}/create`} component={CreateEvent} />
            <Route exact={true} path={`${match.url}/:id`} component={EventDetails} />
        </Switch>
    )
}

export default EventsRoutes;
