import { CREATE_TECHNOLOGY_SUCCESS, EDIT_TECHNOLOGY_DETAILS, GET_ALL_TECHNOLOGIES_SUCCESS, GET_TECHNOLOGY_SUCCESS, GET_TECHNOLOGY_TAGS_SUCCESS, SHOW_CONFIRM_REMOVE, UPDATE_TECHNOLOGY_SUCCESS } from "../../constants/ActionTypes"

const INIT_STATE = {
    loader: false,
    technologies: [],
    technology: {},
    showRemove: false,
    showEdit: false,
    tags: [],
}

export default function technologyReducer(state = INIT_STATE, action) {
    switch (action.type) {
        case SHOW_CONFIRM_REMOVE: {
            return {
                ...state,
                loader: false,
                showRemove: action.payload
            }
        }
        case EDIT_TECHNOLOGY_DETAILS: {
            return {
                ...state,
                loader: false,
                showEdit: action.payload,
            }
        }
        case GET_ALL_TECHNOLOGIES_SUCCESS: {
            return {
                ...state,
                loader: false,
                technologies: action.payload
            }
        }
        case GET_TECHNOLOGY_SUCCESS: {
            return {
                ...state,
                loader: false,
                technology: action.payload[0]
            }
        }
        case GET_TECHNOLOGY_TAGS_SUCCESS: {
            return {
                ...state,
                loader: false,
                tags: action.payload
            }
        }
        case CREATE_TECHNOLOGY_SUCCESS: {
            return {
                ...state,
                loader: false,
                technology: action.payload
            }
        }
        case UPDATE_TECHNOLOGY_SUCCESS: {
            return {
                ...state,
                loader: false,
                technology: action.payload
            }
        }    
        default:
            return state;
    }
}
