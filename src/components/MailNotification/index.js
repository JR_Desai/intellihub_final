import React from "react";
import CustomScrollbars from 'utils/CustomScrollbars'
import Auxiliary from "utils/Auxiliary";

const MailNotification = () => {
  return (
    <Auxiliary>
      <div className="gx-popover-header">
        <h3 className="gx-mb-0">Support</h3>
        <i className="gx-icon-btn icon icon-charvlet-down"/>
      </div>
      <CustomScrollbars className="gx-popover-scroll">
        
      </CustomScrollbars>
    </Auxiliary>
  )
};

export default MailNotification;

