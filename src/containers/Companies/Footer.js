import React from "react";

export default function Footer ({children}) {
  return (
    <div className="gx-algolia-footer gx-text-primary">
      {children}
    </div>
  );
}