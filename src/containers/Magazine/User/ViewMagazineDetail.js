import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useParams } from 'react-router-dom'
import * as ReactBootStrap from "react-bootstrap"
import { Image, Grid, Divider, Segment, Icon, Button as SUIButton, Popup, Label, Form, TextArea, Placeholder } from 'semantic-ui-react'
import { Button, Typography, Checkbox, IconButton } from '@material-ui/core'
import { Share, BookmarkBorder, Bookmark } from '@material-ui/icons'
import Pdf from "react-to-pdf";
import moment from 'moment'
import DOMPurify from 'dompurify';
const ref = React.createRef();

const Particularmagazine = () => {
  const url = "http://3.108.46.195:9000";
  const createMarkup = (html) => {
    return  {
      __html: DOMPurify.sanitize(html)
    }
  }

  const { magId } = useParams()
  const [magDetail, setMagDetail] = useState([])
  const [bookmarks, setBookmark] = useState();
  const handleBookMark = (event) => { 
    console.log(event.target.checked)
    if(event.target.checked){
      console.log(event.target.value)
    }
  }
  useEffect(() => {  
    const fetchMag = () => {
      fetch(`${url}/magazines/view/?id=${magId}`)
      .then((res) => res.json())
      .then((result) => {
        console.log(result)
        setMagDetail(result.results);
        }
      )
    }
    fetchMag()
  }, [])
  const options = {
    orientation: 'portrait',
    unit: 'in',
  };

  const renderParticularMags = magDetail.map(mag => { 
     const filename = `${mag.name}.pdf`

      return <>
      <Grid>
      <div className="container">
        <div className="magazine_left">
          <Link to='/user/magazines'>
          Magazines
          </Link>
          <Icon name='angle right' />
          {mag.name}
        </div>
        <Divider hidden />
      <div ref={ref}>
      <Grid.Row columns={2} className="magazineDetails">
        <Grid.Column className="magazineDetailsImg">
          {mag.cover_image_upload || mag.cover_image ? <Image src={mag.cover_image_upload || mag.cover_image} /> : 
          <Placeholder style={{ height: '417px' }}>
            <Placeholder.Image />
          </Placeholder>
          }
          <Pdf targetRef={ref} filename={filename} x={0.1} y={.5} scale={0.8} options={options}>
            { mag.attachment !== null ? <Button variant="contained" value={mag.attachment} color="secondary" fullWidth className="mt-4"> Download</Button> : ({ toPdf }) => <Button variant="contained" onClick={toPdf} color="secondary" fullWidth className="mt-4"> Download</Button>}
          </Pdf>
        </Grid.Column>

        <Grid.Column className="magazineDetailsInfo">
          <Segment vertical>
            <Grid.Row>
              <Grid.Column>
                <Typography variant="subtitle2">
                  DATE - {moment(mag.published_date).format('DD/MM/YYYY')}
                </Typography>
                <Typography variant="h4">
                  {mag.name}
                </Typography>

                <div className="magazineDescriptions">
                  <Typography variant="body1">
                    <p dangerouslySetInnerHTML={createMarkup(mag.short_description)}></p>
                  </Typography>
                  <div className="magazineDetailsActions">
                    <Checkbox icon= {<BookmarkBorder />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={mag.id}/>
                    <Share className="ml-3"/>
                  </div>
                </div>

              </Grid.Column>
              
              {/* <Grid.Column>
                <div className="m-4">
                <Checkbox icon= {<BookmarkBorder />} checkedIcon= {<Bookmark />}  onChange={handleBookMark} value={mag.id}/>
            <Popup
              trigger={
                <IconButton><Share/></IconButton>
              }
              position="left center"
              on="click"
              flowing
              style={{height:'auto'}}
            >
              <div style={{width:"450px",wordBreak:"break-all"}}>
              <label>Share On</label><br/>
              <SUIButton circular color='facebook' icon='facebook' />
              <SUIButton circular color='twitter' icon='twitter' />
              <SUIButton circular color='linkedin' icon='linkedin' />
              <Divider />
              <label>Share With</label><br/>
              <Label as='a'>
                <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1" />
                Elliot@gmail.com
                <Icon name='delete' />
              </Label>
              <Label as='a'>
                <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' className="mr-1"/>
                Stevie@gmail.com
                <Icon name='delete' />
              </Label>
              
              <Form className="mt-2">
                <Form.Field>
                  <input placeholder='Enter email id' />
                </Form.Field>
                <Form.Field>
                <TextArea placeholder='Type your message here' />
                </Form.Field>
                <Form.Field>
                  <SUIButton type='submit' color="pink">Submit</SUIButton>
                  <SUIButton type='submit' basic>Cancel</SUIButton>
                </Form.Field>
              </Form>
              </div>
            </Popup> 
                </div>
              </Grid.Column> */}
            </Grid.Row>
          </Segment>
          <Segment vertical>
            <h3 className="magazineSubTitle">Features:</h3>
            <ul className="magazineDetailList">
              <li><strong>PCT Event:</strong> Education Made Easy</li>
              <li><strong>Industry Events:</strong> Purdue Goes Virtual</li>
              <li><strong>Cover Story:</strong> How Low Will They Go?</li>
              <li><strong>Termite Control:</strong> Need to Know</li>
            </ul>
          </Segment>
          <Segment vertical>
            <h3 className="magazineSubTitle">Bed Bug Supplement:</h3>
            <ul className="magazineDetailList">
              <li><strong>Bed Bug Supplement:</strong> Bed Bug Myths</li>
              <li><strong>Bed Bug Supplement:</strong> Getting Schooled</li>
              <li><strong>Bed Bug Supplement:</strong> Bed Bug Ministry</li>
              <li><strong>Bed Bug Supplement:</strong> Lessons Learned in the Trenches</li>
              <li><strong>Bed Bug Supplement:</strong> Uphill Battle</li>
              <li><strong>Bed Bug Supplement:</strong> How Fast Can Infestations Grow?</li>
            </ul>
          </Segment>
        </Grid.Column>
      </Grid.Row>
      </div>
      </div>
      </Grid>
      </>
    })

  return (
    <div className="magazine-1 inner-wrapper">
      {renderParticularMags}
    </div>
  )
};

export default Particularmagazine;
