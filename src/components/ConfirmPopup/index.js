import React from 'react'
import SweetAlert from "react-bootstrap-sweetalert";
import { useDispatch } from 'react-redux';
import { showConfirmRemove } from '../../appRedux/actions/Companies';

const ConfirmPopup = ({ showStatus, onConfirm, onCancel }) => {

    const dispatch = useDispatch();

    const onCancelClick = () => {
        showConfirmRemove(false, dispatch);
        onCancel && onCancel();
    };


    return (
        <SweetAlert show={showStatus}
            showCancel
            customClass="gx-p-5"
            confirmBtnText="Remove"
            confirmBtnBsStyle="primary"
            cancelBtnBsStyle="default"
            title="Are you sure?"
            onConfirm={onConfirm}
            onCancel={onCancelClick}
        ></SweetAlert >
    )
}

export default ConfirmPopup;
